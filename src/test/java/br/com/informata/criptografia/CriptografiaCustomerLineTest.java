/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.criptografia;

import br.com.informata.customerline.infra.CriptografiaCustomerLine;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author danilomuniz
 */
public class CriptografiaCustomerLineTest {

    public CriptografiaCustomerLineTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

        
    @Test
    public void testeCripografiaCustomerLine() throws UnsupportedEncodingException {
        CriptografiaCustomerLine criptografiaCustomerLine = new CriptografiaCustomerLine();
        String senha = criptografiaCustomerLine.Crypt("jogp7984");//pj0824 --dmuniz
        System.out.println("senha: " + senha);
    }
    /*
    -----------------------
    
    cliente: Terrazoo
    login : John.Kaio
    senha : johnkaio
    
    cliente: informata  
    login : jean.nascimento
    senha : jn3445    
    
    -----------------------

    */
    @Test
    public void testeDescripografiaCustomerLine() throws UnsupportedEncodingException {
        CriptografiaCustomerLine criptografiaCustomerLine = new CriptografiaCustomerLine();
        String senha = criptografiaCustomerLine.DeCrypt("ko6554");//patricia:qk9175 --- jean :ko6554
        System.out.println("senha: " + senha);
    }
    
    @Test
    public void testesAleatorios() throws UnsupportedEncodingException {
         File dir1 = new File (".");
         //dir1.getCanonicalPath();
        try {
            System.out.println ("Current dir : " + dir1.getCanonicalPath());
        } catch (IOException ex) {
            Logger.getLogger(CriptografiaCustomerLineTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @Test
    public void testeDataHora() throws UnsupportedEncodingException {
        SimpleDateFormat sdf1= new SimpleDateFormat("dd/MM/yyyy"); //você pode usar outras máscaras 
        Date y = new Date();
        System.out.println(sdf1.format(y));        
    }
    
    
    @Test
    public void testeEncoding() {
        try {
            System.out.println(new String("q".getBytes("Cp1252")));
            String[] codes = {"ISO-8859-1", "UTF-8", "Cp1252"};
            String palavra = "k";

            for (String encoding : codes) {
                byte[] b = palavra.getBytes(encoding);
                System.out.printf("%10s\t%d\t", encoding, b.length);
                for (int k = 0; k < b.length; k++) {
                    String hex = Integer.toHexString(((b[k] + 256) % 256));
                    if (hex.length() == 1) {
                        hex = "0" + hex;
                    }
                    System.out.print(hex);
                }
                System.out.println();
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(CriptografiaCustomerLineTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
