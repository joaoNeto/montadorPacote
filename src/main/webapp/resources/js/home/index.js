var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");

$(document).ready(function(){

    listarTodosArquivos();

    /**
     * upload dos arquivos via file
     */ 
   $("#selecao-arquivo-files").on("change",function(e){
        var arquivo = [];
        arquivo = e.target.files;
        var formDataFile = new FormData();
        for (var i = 0; i < arquivo.length; i++){
            formDataFile.append("file", arquivo[i]);
        }       

        $.ajax({
            url: urlContext + '/uploadArquivosViaFile',
            type: 'POST',
            data: formDataFile,
            enctype: 'multipart/form-data',
            contentType: false,
            processData: false,            
            beforeSend: function (xhr) {
                xhr.setRequestHeader(header, token);
            },
            success: function (data) {
                $.get(urlContext + '/separarArquivosViaFile');
                var i = 0;
                $(document).ajaxStop(function(){
                    if(i == 0){
                        listarTodosArquivos();
                        i++;
                    }
                });
            },
            error: function (data) {
                alert("erro ao fazer upload de arquivos");
            }
        });

    });

    /**
     * upload dos arquivos via tortoise
     */     
    $(".upload-file-tortoise").on("click",function(){
        
        $(".modal-carregar-tortoise").show();
        var frase ="Baixando arquivos do tortoise <br>"; 
        $(".texto-modal-carregar-tortoise").html(frase);
        var carregando = setInterval(function(){
          frase += ".";
          $(".texto-modal-carregar-tortoise").html(frase);
        },1000);
        $(".esconderUrlRepo").click();
        
        var arrDados = {"url" : $(".url-repo.form-repo").val(), 
                        "usuario" : $(".usuario-repo.form-repo").val(),
                        "senha" : $(".senha-repo.form-repo").val()};
        
        $.ajax({
            url: urlContext + '/uploadArquivosViaTortoise',
            type: 'POST',
            data: arrDados,         
            beforeSend: function (xhr) {
                xhr.setRequestHeader(header, token);
            },
            success: function (data) {
                clearInterval(carregando);
                $(".texto-modal-carregar-tortoise").html('Fazendo separação dos arquivos...');
                // FAZER A SEPARAÇÃO DOS ARQUIVOS
                setTimeout(function(){ 
                 $.get(urlContext+"separarArquivosViaTortoise",function(){
                     $(".modal-carregar-tortoise").hide();
                     listarTodosArquivos();
                 });
                }, 5000);
            },
            error: function (data) {
                alert("erro ao fazer upload de arquivos");
            }
        });        
        
    });

    $("#subir-arquivo-tortoise").click(function(){
        $("#formulario").hide();
        $("#urlRepo").show();           
    });

    $(".esconderUrlRepo").click(function(){
        $("#formulario").show();
        $("#urlRepo").hide();                         
    });       
    
    /**
     * excluir todos os arquivos que estiver na pasta upload da sessao do usuario
     */ 
    $(".excluir-todos-arquivos").on("click",function(){
      var excluir = confirm("Deseja refazer o pacote?");
      if(excluir == 1){
          excluirTodosArquivos("upload"); 
      }        
    });
        
    /**
     * monta pacote dos arquivos que estão na tela
     */ 
    $(".gerar-pacote-files").on("click",function(){
        $(this).val("gerando pacote...");
        $(this).prop("disabled",true);
      
        $.get(urlContext+"montarPacoteFile?nome="+$(".rodape .nome-pacote").val()+"&versaobanco="+$(".rodape .nome-versao-banco").val()+"&versaoreversao="+$(".rodape .nome-versao-reversao").val(),function(data){
            data = jsonConverter(data);
            excluirTodosArquivos("upload"); 
            $(".rodape .gerar-pacote-files").val("Gerar pacote");
            $(".rodape .gerar-pacote-files").prop("disabled",false);
            $(".baixar-file-pack").html("<a class='baixar-file' href='"+urlContext+"resources/sessao/"+data+"'> <input type='button' value='baixar "+$(".rodape .nome-pacote").val()+"_"+$(".rodape .nome-versao-banco").val()+".zip' id='gerar-pacote' class='voltar-botao-pack-dinamic estilo-button-pacote' > </a>");
            $(".voltar-botao-pack-dinamic").click(function(){
              $(".baixar-file-pack").html("<input type='button' value='Gerar pacote' class='gerar-pacote-files gerar-pacote estilo-button-pacote col-md-3' disabled='' style='background: #999;opacity: 0.5;'>");
            });
            $(".rodape .nome-pacote").val("");
            $(".rodape .nome-versao-banco").val("");
            $(".rodape .nome-versao-reversao").val("");            
            
        });
    });
    
    $(".nome-pacote, .nome-versao-banco, .nome-versao-reversao").keyup(function(){
      var nome_pacote = $(".nome-pacote");
      var versao_apl = $(".nome-versao-banco");
      var versao_rev = $(".nome-versao-reversao");

      if((nome_pacote.val() !== "") && (versao_apl.val() !== "") && (versao_rev.val() !== ""))
        $(".gerar-pacote").removeAttr('disabled').css({'background-color': '#005d94',opacity:'1'});
      else
        $(".gerar-pacote").attr('disabled','disabled').css({'background-color': '#999',opacity:'0.5'});
    });    
 
    
});


/**
 * exibindo os dados do usuario
 */
function listarTodosArquivos(){
    $.get(urlContext+'listaArquivos',function(data){
        data = jsonConverter(data);
        data = JSON.parse(data);
        
        // LIMPANDO OS ARQUIVOS JA SALVOS
        $(".banco .dbamdata").html("");
        $(".banco .dbamdatagrant").html("");
        $(".banco .dbamdatascripts").html("");
        $(".banco .dbamdatatrigger").html("");
        $(".banco .infomdlog").html("");
        $(".reversao .dbamdata").html("");
        $(".reversao .dbamdatagrant").html("");
        $(".reversao .dbamdatascripts").html("");
        $(".reversao .dbamdatatrigger").html("");
        $(".reversao .infomdlog").html("");
        
        
        // -- LISTA DE ARQUIVOS DE BANCO
        for(var nomeFile in data.banco.dbamdata){
            $(".banco .dbamdata").append(styleFileName(data.banco.dbamdata[nomeFile], "banco", "dbamdata"));           
        }
        
        for(var nomeFile in data.banco.dbamdatagrant){
            $(".banco .dbamdatagrant").append(styleFileName(data.banco.dbamdatagrant[nomeFile], "banco", "dbamdatagrant"));            
        }

        for(var nomeFile in data.banco.dbamdatascripts){
            $(".banco .dbamdatascripts").append(styleFileName(data.banco.dbamdatascripts[nomeFile], "banco", "dbamdatascripts"));            
        }

        for(var nomeFile in data.banco.dbamdatatrigger){
            $(".banco .dbamdatatrigger").append(styleFileName(data.banco.dbamdatatrigger[nomeFile], "banco", "dbamdatatrigger"));            
        }

        for(var nomeFile in data.banco.infomdlog){
            $(".banco .infomdlog").append(styleFileName(data.banco.infomdlog[nomeFile], "banco", "infomdlog"));            
        }

        // -- LISTA DE ARQUIVOS DE REVERSÃO
        for(var nomeFile in data.reversao.dbamdata){
            $(".reversao .dbamdata").append(styleFileName(data.reversao.dbamdata[nomeFile], "reversao", "dbamdata"));            
        }
        
        for(var nomeFile in data.reversao.dbamdatagrant){
            $(".reversao .dbamdatagrant").append(styleFileName(data.reversao.dbamdatagrant[nomeFile], "reversao", "dbamdatagrant"));            
        }

        for(var nomeFile in data.reversao.dbamdatascripts){
            $(".reversao .dbamdatascripts").append(styleFileName(data.reversao.dbamdatascripts[nomeFile], "reversao", "dbamdatascripts"));            
        }

        for(var nomeFile in data.reversao.dbamdatatrigger){
            $(".reversao .dbamdatatrigger").append(styleFileName(data.reversao.dbamdatatrigger[nomeFile], "reversao", "dbamdatatrigger"));            
        }

        for(var nomeFile in data.reversao.infomdlog){
            $(".reversao .infomdlog").append(styleFileName(data.reversao.infomdlog[nomeFile], "reversao", "infomdlog"));            
        }

        /**
         * abre o modal para exibir o conteudo do arquivo
         */ 
        $(".ler-arquivo").on("click", function(){
            $(".modal-ler-arquivo .titulo-arquivo").html($(this).parent().attr('nomeArquivo'));
            $(".modal-ler-arquivo .conteudo-arquivo").html("Buscando conteudo...");
            $.get(urlContext+"lerArquivo?nomeArquivo="+$(this).parent().attr('nomeArquivo')+"&dirPrincipal="+$(this).parent().attr('dirPrincipal')+"&dirSecundario="+$(this).parent().attr('dirSecundario'),function(data){
                data = jsonConverter(data);
                $(".modal-ler-arquivo .conteudo-arquivo").html(data);
            });
        }); 
        
        /**
         * deleta o arquivo
         */ 
        $(".deletar-arquivo").on("click", function(){
            $.get(urlContext+"deletarArquivo?nomeArquivo="+$(this).parent().attr('nomeArquivo')+"&dirPrincipal="+$(this).parent().attr('dirPrincipal')+"&dirSecundario="+$(this).parent().attr('dirSecundario'),function(data){
                data = jsonConverter(data);
                listarTodosArquivos();
            });
        }); 

        /**
         * exibe modal para mover arquivo
         */ 
        $(".mover-arquivo").on("click", function(){
            var html = "Mover "+$(this).parent().attr('nomeArquivo');
            html += "<input type='hidden' class='mover nomeArquivo' value='"+$(this).parent().attr('nomeArquivo')+"'>";
            html += "<input type='hidden' class='mover dirPrincipal' value='"+$(this).parent().attr('dirPrincipal')+"'>";
            html += "<input type='hidden' class='mover dirSecundario' value='"+$(this).parent().attr('dirSecundario')+"'>";  
            $(".modal-mover-arquivo .titulo-arquivo").html(html);
            $(".modal-mover-arquivo .conteudo-arquivo").html("Buscando conteudo...");
            $.get(urlContext+"listaDiretoriosSubdiretorios", function(data){
                var html = "";
                data = jsonConverter(data);
                data = JSON.parse(data);
                for(var diretorio in data){
                   html += data[diretorio]+" <input type='radio' class='dirDestinoValue' name='dirDestino' value='"+data[diretorio]+"'><br>";
                }
                html += "<br><button class='save'>Mover arquivo</button>";
                $(".modal-mover-arquivo .conteudo-arquivo").html(html);
                
                $("#modalMoverArquivo .save").on("click",function(){                    
                    $.get(urlContext+"moverArquivo?arquivo="+$(".mover.nomeArquivo").val()+"&dirPrincipalOrigem="+$(".mover.dirPrincipal").val()+"&dirSecundarioOrigem="+$(".mover.dirSecundario").val()+"&dirDestino="+$(".dirDestinoValue:checked").val(), function(){
                        listarTodosArquivos();
                        $(".close-modal-mover").click();
                    });
                });
                
            });
        }); 
        
    });
}

function excluirTodosArquivos(pasta){
    $.get(urlContext+"excluirTodosArquivos?diretorio="+pasta,function(data){
        listarTodosArquivos();
        data = jsonConverter(data);
    });    
}


function styleFileName(nomeArquivo, dirPrincipal, dirSecundario){
    return "<div class='box-filename' nomeArquivo='"+nomeArquivo+"' dirPrincipal='"+dirPrincipal+"' dirSecundario="+dirSecundario+"> "+nomeArquivo+"\
             <i class='fa fa-trash-o deletar-arquivo' aria-hidden='true' title='Deletar o arquivo'></i>  \n\
             <a href='#modalLerArquivo' class='ler-arquivo' data-toggle='modal'>\n\
                 <i class='fa fa-book' title='ler o arquivo'></i>\n\
             </a>  \n\
         \n\<a href='#modalMoverArquivo' class='mover-arquivo' data-toggle='modal'>\n\
                <i class='fa fa-exchange' aria-hidden='true' title='Mover o arquivo'></i> \n\
         \n\</a> \n\
            </div>";
}

