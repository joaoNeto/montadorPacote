/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    var codStakeholder;
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    
    $('#example-getting-started').multiselect();
        
    $(".select-cliente").change(function(){
        listaProdutos();
        listaSolicitantes();
        listaRelease($(".select-produto").val(), $(".select-cliente").val());
    });
    
    $(".select-produto").change(function(){
        listaTipo();
        listaRelease($(".select-produto").val(), $(".select-cliente").val());
        localizacao($(".select-produto").val());
    });    

    // LISTANDO OS CLIENTES
    $.get(urlContext+"solicitacao/getdadoscliente",function(data){
        data = JSON.parse(data);
        var codCliente = data.codCliente;
        data = data.data;
        data = JSON.parse(data);
        if(data[0].disabled === true){
            $(".select-cliente").html("<option value='"+codCliente+"'></option>");
        }else{     
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].code+"'>"+data[i].customerName+"</option>";
            $(".select-cliente").html(htmlSelect);
            $(".select-cliente").val(codCliente);
        }
        
        // LISTANDO OS SOLICITANTES
        listaSolicitantes();

        // LISTANDO OS PRODUTOS
        listaProdutos();
        
    });

    // LISTANDO AS FONTES
    $.get(urlContext+"solicitacao/getdadosfonte",function(data){
        data = JSON.parse(data);
        codStakeholder = data.codStakeholder;
        data = data.data;
        if(data[0].disabled === true){
            $(".div-fonte").hide();
            $(".select-fonte").html("<option value='"+codStakeholder+"'></option>");
        }else{
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++){
                if(data[i].selecionado == "true"){
                    codStakeholder = data[i].StakeholderCode;
                    $(".input-responsavel").val(data[i].StakeholderName);
                }
                htmlSelect += "<option value='"+data[i].StakeholderCode+"'>"+data[i].StakeholderName+"</option>";
            }
            $(".select-fonte").html(htmlSelect);
            $(".select-fonte").val(codStakeholder);
        }
    }); 

    listaPrioridades();
    fabricaResponsavel();
    situacao();
    estado();    
    dataehora();
    complexidade();
    categoria();
    responsavelAtendimento();
                
    function listaSolicitantes(){
        $.get(urlContext+"solicitacao/getdadossolicitante?codCliente="+$(".select-cliente").val(),function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].StakeholderCode+"'>"+data[i].StakeholderName+"</option>";
            
            $(".select-solicitante").html(htmlSelect);
            $(".select-solicitante").val(codStakeholder);
        });     
    }    
    
    function listaProdutos(){
        $.get(urlContext+"solicitacao/getdadosproduto?codCliente="+$(".select-cliente").val(),function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].projCode+"'>"+data[i].projDescription+"</option>";
            
            $(".select-produto").html(htmlSelect);
            
            // LISTANDO TIPOS
            listaTipo();
            
            // LISTANDO RELEASES
            listaRelease($(".select-produto").val(), $(".select-cliente").val());
            
            // LISTANDO LOCALIZAÇÕES
            localizacao($(".select-produto").val());

        });             
    }
    
    function listaTipo(){
        $.get(urlContext+"solicitacao/getdadostipoporproject?codProj="+$(".select-produto").val(),function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].MudancaCode+"'>"+data[i].MudancaDesc+"</option>";
            
            $(".select-tipo-project").html(htmlSelect);
        });                     
    }
    
    
    function listaRelease(codProjeto, codCliente){
        $.get(urlContext+"solicitacao/getdadosrelease?codProj="+codProjeto+"&codCliente="+codCliente,function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
            
            $(".select-release").html(htmlSelect);
        });                     
    }
    
    function listaPrioridades(){
        $.get(urlContext+"solicitacao/getdadosprioridades",function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
            
            $(".select-prioridade").html(htmlSelect);
        });                             
    }
    
    function fabricaResponsavel(){
        $.get(urlContext+"solicitacao/getdadosfabricaresponsavel",function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
            
            $(".select-fabrica-responsavel").html(htmlSelect);
            // a fabrica de origem é a fabrica responsavel
            $(".input-fab-origem").val(data[0].name);
        });                                     
    }
    
    function situacao(){
        $.get(urlContext+"solicitacao/getdadosituacao",function(data){
            data = JSON.parse(data);
            $(".input-situacao").val(data.code);
            $(".input-situacao-cod").val(data.text);
        });         
    }
    
    function estado(){
        $.get(urlContext+"solicitacao/getdadosestado",function(data){
            data = JSON.parse(data);
            var codigoStatus = data.code;
            $(".input-estado").val(data.text);
            $(".input-estado-estado").val(codigoStatus);
        });           
    }

    function dataehora(){
        $.get(urlContext+"solicitacao/getdadosdataehora",function(data){
            $(".input-data-registro").val(data);
        });           
    }    
    
    function complexidade(){
        $.get(urlContext+"solicitacao/getdadosconfiglevels",function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
            
            $(".select-complexidade").html(htmlSelect);            
        });           
    }    
    
    function localizacao(codProj){
         $.get(urlContext+"solicitacao/getdadoslocalizacao?codProj="+codProj,function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
            
            $(".select-localizacao").html(htmlSelect);            
        });           
    }

    function categoria(){
         $.get(urlContext+"solicitacao/getdadoscategoria",function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
            
            $(".select-categoria").html(htmlSelect);            
        });           
    }
    
    function responsavelAtendimento(){
         $.get(urlContext+"solicitacao/getdadosresponsavelatendimento",function(data){
            data = JSON.parse(data);
            nvl_atendimento = data.nivelAtendimento;
            enabledSelect   = data.mostrarOpcaoSelect;
            attendenceCode  = data.attendanceLevelCode;
            data = data.responsavelAtendimento;
            var tamArr = data.length;
            var htmlSelectAtendimento = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelectAtendimento += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
            
            //$(".select-responsavel-atendimento").html(htmlSelect); 
            
            $(".nvl-atendimento").val(nvl_atendimento);
            $(".attendanceLevelCode").val(attendenceCode);
            if(enabledSelect){
               // disabled or not o nvl-atendimento 
            }
         });      
    }
    
});
