$(document).ready(function () {


    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    
    $('#example-getting-started').multiselect();
    
    listaPrioridades();
    fabricaResponsavel();
    situacao();
    estado();    
    dataehora();
    complexidade();
    categoria();
    responsavelAtendimento();
    
    var arquivo = [];
    
    $(".arquivo").change(function (e) {
        arquivo = e.target.files;
        var htmlList = "<ul>";
        for (var i = 0; i < arquivo.length; i++){
            htmlList += "<li>"+arquivo[i].name+"</li>";
        }
        htmlList += "</ul>";
        $(".lista-arquivos").html(htmlList);
    });

    $(".select-cliente").change(function(){
        listaProdutos();
        listaSolicitantes();
        listaRelease($(".select-produto").val(), $(".select-cliente").val());
    });
    
    $(".select-produto").change(function(){
        listaTipo();
        listaRelease($(".select-produto").val(), $(".select-cliente").val());
        localizacao($(".select-produto").val());
    });    

    // LISTANDO OS CLIENTES
    $.get(urlContext+"solicitacao/getdadoscliente",function(data){
        data = JSON.parse(data);
        var codCliente = data.codCliente;
        data = data.data;
        data = JSON.parse(data);
        if(data[0].disabled === true){
            $(".select-cliente").html("<option value='"+data[0].codInformata+"'>informata</option>");
        }else{     
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].code+"'>"+data[i].customerName+"</option>";
            $(".select-cliente").html(htmlSelect);
            $(".select-cliente").val($(".atb.codCliente").val());
        }

        // LISTANDO OS SOLICITANTES
        listaSolicitantes();

        // LISTANDO OS PRODUTOS
        listaProdutos();
        
    });
            
    // LISTANDO AS FONTES
    $.get(urlContext+"solicitacao/getdadosfonte",function(data){
        data = JSON.parse(data);
        var codStakeholder = data.codStakeholder;
        data = data.data;        
        if(data[0].disabled === true){
            $(".select-fonte").html("<option value='"+codStakeholder+"'></option>");
        }else{
            var tamArr = data.length;
            var htmlSelect = "";
            var selecionado = "";
            for(var i = 0 ; i < tamArr; i++){
                if(data[i].selecionado == "true"){
                    selecionado = data[i].StakeholderCode;
                    //$(".input-responsavel").val(data[i].StakeholderName);
                    $(".input-responsavel").val($(".atb.nomeResponsavel").val()); 
                }
                htmlSelect += "<option value='"+data[i].StakeholderCode+"'>"+data[i].StakeholderName+"</option>";
            }
            $(".select-fonte").html(htmlSelect);
            $(".select-fonte").val($(".atb.codFonte").val());
        }
    }); 
    
    function listaSolicitantes(){
        $.get(urlContext+"solicitacao/getdadossolicitante?codCliente="+$(".select-cliente").val(),function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].StakeholderCode+"'>"+data[i].StakeholderName+"</option>";
            
            $(".select-solicitante").html(htmlSelect);
            $(".select-solicitante").val($(".atb.codSolicitante").val());
        });     
    }    
    
    function listaProdutos(){
        $.get(urlContext+"solicitacao/getdadosproduto?codCliente="+$(".select-cliente").val(),function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].projCode+"'>"+data[i].projDescription+"</option>";
            
            $(".select-produto").html(htmlSelect);
            $(".select-produto").val($(".atb.codProduto").val());
            
            // LISTANDO TIPOS
            listaTipo();
            
            // LISTANDO RELEASES
            listaRelease($(".select-produto").val(), $(".select-cliente").val());
            
            // LISTANDO LOCALIZAÇÕES
            localizacao($(".select-produto").val());

        });             
    }
    
    function listaTipo(){
        $.get(urlContext+"solicitacao/getdadostipoporproject?codProj="+$(".select-produto").val(),function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].MudancaCode+"'>"+data[i].MudancaDesc+"</option>";
            
            $(".select-tipo-project").html(htmlSelect);
            $(".select-tipo-project").val($(".atb.codTipo").val());
        });                     
    }
    
    
    function listaRelease(codProjeto, codCliente){
        $.get(urlContext+"solicitacao/getdadosrelease?codProj="+codProjeto+"&codCliente="+codCliente,function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
            
            $(".select-release").html(htmlSelect);
            $(".select-release").val($(".atb.codVersao").val());
        });                     
    }
    
    function listaPrioridades(){
        $.get(urlContext+"solicitacao/getdadosprioridades",function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
            
            $(".select-prioridade").html(htmlSelect);
            $(".select-prioridade").val($(".atb.codPrioridade").val());
        });                             
    }
    
    function fabricaResponsavel(){
        $.get(urlContext+"solicitacao/getdadosfabricaresponsavel",function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
            
            $(".select-fabrica-responsavel").html(htmlSelect);

            // a fabrica de origem é a fabrica responsavel
            $(".select-fabrica-responsavel").val($(".atb.codFabResponsavel").val());
            $(".input-fab-origem").val("Matriz");
        });                                     
    }
    
    function situacao(){
        $.get(urlContext+"solicitacao/getdadosituacao",function(data){
            data = JSON.parse(data);
            //$(".input-situacao").val(data.code);
            //$(".input-situacao-cod").val(data.text);
            $("input.input-situacao").val($(".atb.nomeSituacao").val());
            $("input.input-situacao-cod").val($(".atb.situacao").val());  

        });         
    }
    
    function estado(){
        $.get(urlContext+"solicitacao/getdadosestado",function(data){
            data = JSON.parse(data);
            var codigoStatus = data.code;
            $(".input-estado").val($(".atb.nomeEstado").val());
            $(".input-estado-estado").val($(".atb.estado").val());
        });           
    }

    function dataehora(){
        $.get(urlContext+"solicitacao/getdadosdataehora",function(data){

            var arrDataHora = $(".atb.dataRegistro").val().split(" ");
            var arrData = arrDataHora[0].split("-");
            var strFinal = arrData[2]+"/"+arrData[1]+"/"+arrData[0];

            $(".input-data-registro").val(strFinal); // tem que manipular a data
        });           
    }    
    
    function complexidade(){
        $.get(urlContext+"solicitacao/getdadosconfiglevels",function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
            
            $(".select-complexidade").html(htmlSelect);   
            $(".select-complexidade").val($(".atb.codComplexidade").val());          
        });           
    }    
    
    function localizacao(codProj){
         $.get(urlContext+"solicitacao/getdadoslocalizacao?codProj="+codProj,function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            var codigoSelected;
            for(var i = 0 ; i < tamArr; i++){
                htmlSelect += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
                if(data[i].name == $(".atb.codLocalizacao").val())
                    codigoSelected = data[i].code;
            }
            
            $(".select-localizacao").html(htmlSelect); 
            $(".select-localizacao").val(codigoSelected);           
        });           
    }

    function categoria(){
         $.get(urlContext+"solicitacao/getdadoscategoria",function(data){
            data = JSON.parse(data);
            var tamArr = data.length;
            var htmlSelect = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelect += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
            
            $(".select-categoria").html(htmlSelect);   
            $(".select-categoria").val($(".atb.codCategoria").val());         
        });           
    }
    
    function responsavelAtendimento(){
         $.get(urlContext+"solicitacao/getdadosresponsavelatendimento",function(data){
            data = JSON.parse(data);
            nvl_atendimento = data.nivelAtendimento;
            enabledSelect   = data.mostrarOpcaoSelect;
            attendenceCode  = data.attendanceLevelCode;
            data = data.responsavelAtendimento;
            var tamArr = data.length;
            var htmlSelectAtendimento = "";
            for(var i = 0 ; i < tamArr; i++)
                htmlSelectAtendimento += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
            
            //$(".select-responsavel-atendimento").html(htmlSelect); 
            
            $(".nvl-atendimento").val(nvl_atendimento);
            $(".attendanceLevelCode").val(attendenceCode);
            $(".nvl-atendimento").val($(".atb.nivelAtendimento").val());
            $(".attendanceLevelCode").val($(".atb.codNivelAtendimento").val());

            if(enabledSelect){
               // disabled or not o nvl-atendimento 
            }
         });      
    }
    

    /*------------------------------------------------------------------------*/
    /*-- SCRIPT PROPRIO DO EDITAR --------------------------------------------*/
    /*------------------------------------------------------------------------*/

    // DEFININDO CONFIGURAÇÕES INICIAIS
    var crcValue = $(".atb.crc").val();
    $(".text-code").val(crcValue.substring(3,crcValue.length));
    $(".txt-descricao").val($(".atb.descricao").val());
    $(".txt-justificativa").val($(".atb.justificativa").val());
    $(".txt-observacao").val($(".atb.observacao").val());
    $(".txt-fase-descoberta").val($(".atb.faseDescoberta").val());
    $(".select-produto").val($(".atb.codProduto").val());
    $(".txt-solicitante-final").val($(".atb.solicitanteFinal").val());

    $(".select-produto").prop( "disabled", true );
    $(".select-solicitante").prop("disabled", true);
    $(".div-justificativa").css("display","none");
    $(".div-fonte").css("display","none");
    $(".div-fabrica-responsavel").css("display","none");
    $(".div-fab-origem").css("display","none");
    $(".div-fase-descoberta").css("display","none");
    $(".div-estado").css("display","none");
    $(".div-responsavel").css("display","none");
    $(".div-tipo").css("display","none");
    $(".div-complexidade").css("display","none");
    $(".div-observacao").css("display","none");
    $(".nvl-atendimento-select-insert").css("display","none");
    $(".div-situacao").css("display","none");
    $(".div-solicitante.esconder").css("display","none");

    // REDIMENSSIONANDO INPUTS
    $(".div-ticket").removeClass("col-md-2");
    $(".div-cliente").removeClass("col-md-2");
    $(".div-solicitante").removeClass("col-md-3");
    $(".div-produto").removeClass("col-md-2");
    $(".div-localizacao").removeClass("col-md-6");
    $(".div-versao").removeClass("col-md-3");
    $(".div-prioridade").removeClass("col-md-3");    
    $(".div-categoria").removeClass("col-md-2");
    
    $(".div-ticket").addClass("col-md-4");
    $(".div-cliente").addClass("col-md-4");
    $(".div-solicitante").addClass("col-md-4");
    $(".div-produto").addClass("col-md-4");
    $(".div-localizacao").addClass("col-md-4");
    $(".div-versao").addClass("col-md-4");
    $(".div-prioridade").addClass("col-md-4");
    $(".div-categoria").addClass("col-md-4");
    
    $(".div-registros").removeClass("col-md-2");
    $(".div-responsavel2").removeClass("col-md-2");
    
    $(".div-registros").addClass("col-md-6");
    $(".div-responsavel2").addClass("col-md-4");  
    
    // INPUT NVL ATENDIMENTO STATUS
    $(".div-nvl-atendimento-status").css("display","block");
    $(".nvl-atendimento-status").val($(".atb.nivelAtendimento").val()+" / "+$(".atb.nomeSituacao").val());
    
    // O campo “CLIENTE” só deve ser exibida se o usuário logado tiver acesso a mais de um cliente;
    if($(".atb.profileRole").val() != "1"){
        $(".select-cliente").prop("disabled",true);
        $(".div-ticket").addClass("col-md-4");
        $(".div-cliente").addClass("col-md-4");
        $(".div-solicitante").addClass("col-md-4");
        $(".div-produto").addClass("col-md-4");
        $(".div-localizacao").addClass("col-md-4");
        $(".div-tipo").addClass("col-md-4");
        $(".div-versao").addClass("col-md-4");
        $(".div-prioridade").addClass("col-md-4");
    }else{
        $(".div-cliente").css("display","none");
        $(".div-ticket").addClass("col-md-3");
        $(".div-cliente").addClass("col-md-3");
        $(".div-solicitante").addClass("col-md-3");
        $(".div-produto").addClass("col-md-3");
        $(".div-localizacao").addClass("col-md-3");
        $(".div-tipo").addClass("col-md-3");
        $(".div-versao").addClass("col-md-3");
        $(".div-prioridade").addClass("col-md-3");
        $(".div-categoria").removeClass("col-md-4");
        $(".div-categoria").addClass("col-md-3");
    }
    
    $(".btn.salvar").click(function(){
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");        
        
        $(this).html("Atualizando dados...");
        $(this).prop( "disabled", true );        
        
        var formData = new FormData();

        formData.append("opForm", false);
        formData.append("state", $(".input-estado").val());
        formData.append("crcNum", $(".atb.crc").val());
        formData.append("stateCode", $(".input-estado-estado").val());
        formData.append("stakeholderSelected", $(".select-fonte").val());
        formData.append("codProduto", $(".select-produto").val());
        formData.append("nomeProduto", $(".select-produto :selected").text());
        formData.append("data", $(".input-data-registro").val());
        formData.append("status", $("input.input-situacao").text());
        formData.append("statusCode", $(".input-situacao-cod").val());
        formData.append("selectRelease", $(".select-release").val());
        formData.append("nomeRelease", $(".select-release :selected").text());
        formData.append("selectTipoProject", $(".select-tipo-project").val());
        formData.append("txtFaseDescoberta", $(".txt-fase-descoberta").val());
        formData.append("txtDescricao", $(".txt-descricao").val());
        formData.append("selectSolicitante", $(".select-solicitante").val());
        formData.append("selectPrioridade", $(".select-prioridade").val());
        formData.append("txtJustificativa", $(".txt-descricao").val());
        formData.append("selectCliente", $(".select-cliente").val());
        formData.append("nomeSelectCliente", $(".select-cliente :selected").text());
        formData.append("selectFabricaResponsavel", $(".select-fabrica-responsavel").val());
        formData.append("nomeFabricaResponsavel", $(".select-fabrica-responsavel :selected").text());
        formData.append("attendanceLevelCode", $(".attendanceLevelCode").val());
        formData.append("nomeCliente", $(".select-cliente :selected").text());
        formData.append("txtAtendimento", $(".nvl-atendimento").val());
        formData.append("txtObservacao", $(".txt-observacao").val());
        formData.append("textSolicitante", $(".select-solicitante :selected").text());
        formData.append("txtLocalization", $(".select-localizacao :selected").text());
        formData.append("selectComplexidade", $(".select-complexidade").val());
        formData.append("selectCategoria", $(".select-categoria").val());
        formData.append("textCode", "CRC"+$(".text-code").val());
        formData.append("responsavel", $(".input-responsavel").val());
        formData.append("selectResponsavelAtendimento", $(".select-responsavel-atendimento").val());
        formData.append("solicitanteFinal", $(".txt-solicitante-final").val());
        
        $.ajax({
            url: urlContext + 'solicitacao/inserirsmc',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader(header, token);
            },
            success: function (data) {

                if (arquivo.length > 0) {
                    // REQUISIÇÃO AJAX PARA ANEXAR ARQUIVO A CRC
                    var formDataFile = new FormData();
                    for (var i = 0; i < arquivo.length; i++){
                        formDataFile.append("file", arquivo[i]);
                    }
                    formDataFile.append("crc", data);
                    formDataFile.append("codProduto", $(".select-produto").val());

                    $.ajax({
                        dataType: 'json',
                        url: urlContext + 'solicitacao/subirarquivo',
                        type: 'POST',
                        data: formDataFile,
                        enctype: 'multipart/form-data',
                        contentType: false,
                        processData: false,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader(header, token);
                        },
                        success: function (data2) {
                                alert("Salvo com sucesso!");
                                window.location.href = urlContext+"solicitacao/editar?crc="+$(".atb.crc").val()+"&codproj="+$(".atb.codProduto").val();
                                $(".btn.salvar").html("Salvar");
                                $(".btn.salvar").prop( "disabled", false );
                        },
                        error: function (data2) {
                            alert("Infelismente houve um erro ao anexar arquivos a crc");
                            $(".btn.salvar").html("Salvar");
                            $(".btn.salvar").prop("disabled", false);
                        }
                    });//fim ajax post                            

                } else {
                        alert("Salvo com sucesso!");
                        window.location.href = urlContext+"solicitacao/editar?crc="+$(".atb.crc").val()+"&codproj="+$(".atb.codProduto").val();
                        $(".btn.salvar").html("Salvar");
                        $(".btn.salvar").prop( "disabled", false );
                }

            },
            error: function (data) {
                alert("Erro ao salvar");
            }
        });//fim ajax post        
        
    });

    /*------------------------------------------------------------------------*/
    /*-- TABELAS DATATABLE ---------------------------------------------------*/
    /*------------------------------------------------------------------------*/
    
    // TABELA HISTORICO DE ALOCAÇÃO
    window.informata.dataTable('tabela-historico-alocacao', {
        url: urlContext + "/solicitacao/gethistoricoalocacao?crc="+$(".atb.crc").val()+"&codProjeto="+$(".atb.codProduto").val(),
        search: true,
        qtdItens: 10,
        order: [[1, 'desc']],
        scrollX: true,
        columns: [
            {data: "A", targets: 0, className: 'text-center minimo', search: false, orderable: false, name: ''},
            {data: "B", targets: 1, className: 'text-left pequeno', orderable: true, name: "DATA DE REGISTRO"},
            {data: "C", targets: 2, className: 'text-left minimo', name: "FÁBRICA"},
            {data: "D", targets: 3, className: 'text-left medio', name: "DE"},
            {data: "E", targets: 4, className: 'text-left medio', name: "PARA"},
            {data: "F", targets: 5, className: 'text-left  false largo', name: "NOTA"}
        ],
        drilldown: {
            enabled: true
        }
    }).init();
        
    // TABELA DE HISTORICO DA CRC
    window.informata.dataTable('tabela-historico-crc', {
        url: urlContext + "/solicitacao/gethistoricocrc?crc="+$(".atb.crc").val()+"&codProjeto="+$(".atb.codProduto").val(),
        search: true,
        qtdItens: 10,
        order: [[1, 'desc']],
        scrollX: true,
        columns: [
            {data: "A", targets: 0, className: 'text-center minimo', search: false, orderable: false, name: ''},
            {data: "B", targets: 1, className: 'text-left pequeno', orderable: true, name: "DATA DE REGISTRO"},
            {data: "C", targets: 2, className: 'text-left medio',   name: "RESPONSÁVEL"},
            {data: "D", targets: 3, className: 'text-left pequeno', name: "SITUAÇÃO"},
            {data: "E", targets: 4, className: 'text-left pequeno',   name: "ESTADO"},
            {data: "F", targets: 5, className: 'text-left  false no-print largo',   name: "MOTIVO"}
        ],
        drilldown: {
            enabled: true
        }
    }).init();

    $(".show-details.false").click(function(){
        $(this).removeClass("false");
        $(this).addClass("true");
        $(this).css("whiteSpace","inherit");    
    });

    $(".show-details.true").click(function(){
        $(this).removeClass("true");
        $(this).addClass("false");
        $(this).css("whiteSpace","nowrap");
    });    
    
});

function dadosResponsavelAtendimento(){
    $.get(urlContext+"solicitacao/getdadosselectresponsavelatendimento?codNvlAtendimento="+$(".select-all-nvl-atendimento").val(),function(data){
       data = JSON.parse(data);
       var tamArr = data.length;
       var htmlSelectAtendimento = "<option value=''></option>";
       for(var i = 0 ; i < tamArr; i++)
           htmlSelectAtendimento += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
       $(".select-responsavel-atendimento").html(htmlSelectAtendimento);
    });
}

function dadosNivelAtendimento(){
    
    $.get(urlContext+"solicitacao/getdadosnvlatendimento",function(data){
       $(".nvl-atendimento-select-insert").html("<label>Nível de Atendimento</label> <select class='form-control select select-all-nvl-atendimento' id='' name=''></select>"); 
       data = JSON.parse(data);
       var tamArr = data.length;
       var htmlSelectAtendimento;
       for(var i = 0 ; i < tamArr; i++)
           htmlSelectAtendimento += "<option value='"+data[i].code+"'>"+data[i].text+"</option>";
       $(".select-all-nvl-atendimento").html(htmlSelectAtendimento);
       $(".select-all-nvl-atendimento").val($(".atb.codNivelAtendimento").val());
       
       dadosResponsavelAtendimento();
       
       $(".select-all-nvl-atendimento").change(function(){
           dadosResponsavelAtendimento();
       });
    
    });
    
}

function selectSituacao(){
    
    $.get(urlContext+"solicitacao/getdadosselectsituacao?status="+$(".atb.situacao").val()+"&state="+$(".atb.estado").val(),function(data){
               
       $(".div-situacao").html("<label>Situação</label> <select class='form-control select select-situacao' id='' name=''></select>"); 
       data = JSON.parse(data);
       var tamArr = data.length;
       var htmlSelectSituacao;
       for(var i = 0 ; i < tamArr; i++)
           htmlSelectSituacao += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
     
       $(".select-situacao").html(htmlSelectSituacao);
       $(".select-situacao").val($(".atb.situacao").val());
       
       selectEstado(false);
       
       $(".select-situacao").change(function(){
           selectEstado(true);
       });
    
    });

}

function selectEstado(boolFiltro){
    
    if($(".select-situacao").val() == $(".atb.situacao").val())
        boolFiltro = false;
    
    $.get(urlContext+"solicitacao/getdadosselectestado?status="+$(".select-situacao").val()+"&state="+$(".atb.estado").val()+"&bool="+boolFiltro,function(data){
               
       $(".div-estado").html("<label>Estado</label> <select class='form-control select select-estado' id='' name=''></select>"); 
       data = JSON.parse(data);
       var tamArr = data.length;
       var htmlSelectEstado;
       for(var i = 0 ; i < tamArr; i++)
           htmlSelectEstado += "<option value='"+data[i].code+"'>"+data[i].name+"</option>";
     
       $(".select-estado").html(htmlSelectEstado);
       if(boolFiltro)
        $(".select-estado").val(data[1].code);
       else
        $(".select-estado").val($(".atb.estado").val());
    
    });
    
}