/* 
 OBS.: o solicitante é a própria pessoa 
 */


$(document).ready(function () {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    var arquivo = [];

    // CONFIGURAÇÕES INICIAIS DA TELA
    $(".esconder-tl-cadastro").hide();
    
    $(".div-produto").removeClass("col-md-2");
    $(".div-categoria").removeClass("col-md-2");
    $(".div-localizacao").removeClass("col-md-6");
    $(".div-versao, .div-tipo, .div-prioridade").removeClass("col-md-3");
    $(".div-solicitante").removeClass("col-md-3");

    // col-md-3 div-solicitante

    $(".div-localizacao").addClass("col-md-4");
    $(".div-produto").addClass("col-md-4");
    $(".div-categoria").addClass("col-md-4");
    $(".div-versao, .div-tipo, .div-prioridade").addClass("col-md-4");
    $(".div-solicitante").addClass("col-md-4");
    
    $("#area-versao-tl-cadastro").html($("#area-versao").html());
    $("#area-versao").html("");
    $(".txt-descricao").attr('rows', '4');

    $(".arquivo").change(function (e) {
        arquivo = e.target.files;
        var htmlList = "<ul>";
        for (var i = 0; i < arquivo.length; i++){
            htmlList += "<li>"+arquivo[i].name+"</li>";
        }
        htmlList += "</ul>";
        $(".lista-arquivos").html(htmlList);
    });

    $(".btn.salvar").click(function (e) {
        $(this).html("Cadastrando dados...");
        $(this).prop("disabled", true);

        var formData = new FormData();
        
        var categoria = "6"; // cod gestao de mudança
        if($(".select-tipo-project").val() == "142" || $(".select-tipo-project").val() == "4"){ // 142 4
            categoria = "1";
        }
        
        formData.append("opForm", true);
        formData.append("state", $(".input-estado").val());
        formData.append("stateCode", $(".input-estado-estado").val());
        formData.append("stakeholderSelected", $(".select-fonte").val());
        formData.append("codProduto", $(".select-produto").val());
        formData.append("nomeProduto", $(".select-produto :selected").text());
        formData.append("data", $(".input-data-registro").val());
        formData.append("status", $("input.input-situacao").text());
        formData.append("statusCode", $(".input-situacao-cod").val());
        formData.append("selectRelease", $(".select-release").val());
        formData.append("nomeRelease", $(".select-release :selected").text());
        formData.append("selectTipoProject", $(".select-tipo-project").val());
        formData.append("txtFaseDescoberta", $(".txt-fase-descoberta").val());
        formData.append("txtDescricao", $(".txt-descricao").val());
        formData.append("selectSolicitante", $(".select-solicitante").val());
        formData.append("selectPrioridade", $(".select-prioridade").val());
        formData.append("txtJustificativa", $(".txt-descricao").val());
        formData.append("selectCliente", $(".select-cliente").val());
        formData.append("nomeSelectCliente", $(".select-cliente :selected").text());
        formData.append("selectFabricaResponsavel", $(".select-fabrica-responsavel").val());
        formData.append("nomeFabricaResponsavel", $(".select-fabrica-responsavel :selected").text());
        formData.append("attendanceLevelCode", $(".attendanceLevelCode").val());
        formData.append("nomeCliente", $(".select-cliente :selected").text());
        formData.append("txtAtendimento", $(".nvl-atendimento").val());
        formData.append("txtObservacao", $(".txt-observacao").val());
        formData.append("textSolicitante", $(".select-solicitante :selected").text());
        formData.append("txtLocalization", $(".select-localizacao :selected").text());
        formData.append("selectComplexidade", $(".select-complexidade").val());
        formData.append("selectCategoria", categoria);
        formData.append("textCode", $(".text-code").val());
        formData.append("responsavel", $(".input-responsavel").val());
        formData.append("selectResponsavelAtendimento", $(".select-responsavel-atendimento").val());
        formData.append("solicitanteFinal", $(".txt-solicitante-final").val());

        $.ajax({
            url: urlContext + 'solicitacao/inserirsmc',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader(header, token);
            },
            success: function (data) {

                if (arquivo.length > 0) {
                    // REQUISIÇÃO AJAX PARA ANEXAR ARQUIVO A CRC
                    var formDataFile = new FormData();
                    for (var i = 0; i < arquivo.length; i++){
                        formDataFile.append("file", arquivo[i]);
                    }
                    formDataFile.append("crc", data);
                    formDataFile.append("codProduto", $(".select-produto").val());

                    $.ajax({
                        dataType: 'json',
                        url: urlContext + 'solicitacao/subirarquivo',
                        type: 'POST',
                        data: formDataFile,
                        enctype: 'multipart/form-data',
                        contentType: false,
                        processData: false,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader(header, token);
                        },
                        success: function (data2) {
                            alert("O ticket gerado é: " + data);
                            window.location.href = urlContext + "solicitacao/cadastrar";
                            $(".btn.salvar").html("Salvar");
                            $(".btn.salvar").prop("disabled", false);
                        },
                        error: function (data2) {
                            alert("ticket salvo, mas infelismente houve um erro ao anexar arquivo ao ticket");
                            $(".btn.salvar").html("Salvar");
                            $(".btn.salvar").prop("disabled", false);
                        }
                    });//fim ajax post                            

                } else {
                    alert("O ticket gerado é: " + data);
                    window.location.href = urlContext + "solicitacao/cadastrar";
                    $(".btn.salvar").html("Salvar");
                    $(".btn.salvar").prop("disabled", false);
                }

            },
            error: function (data) {
                alert("Infelismente houve um erro no cadastro do ticket");
                $(".btn.salvar").html("Salvar");
                $(".btn.salvar").prop("disabled", false);
            }
        });//fim ajax post        

    });

});