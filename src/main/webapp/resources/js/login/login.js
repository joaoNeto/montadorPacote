/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*****************************************************************************
 * 
 * Funcoes Eventos dos (DOM) da pagina.
 * 
 *****************************************************************************/

$(document).ready(function () {
    //protecao csrf
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    onClickEntrar(header, token);

});


/*****************************************************************************
 * 
 * Funcoes Eventos dos componentes da pagina.
 * 
 *****************************************************************************/
function onClickEntrar(header, token) {
    $("#btn-entrar").click(function (e) {
        e.preventDefault();
        var cliente =  $("#cliente").val();
        var username = $("#usuario").val();
        var password = $("#senha").val();
        
        username = cliente +';'+username;
        console.log(username);
        $("#username").val(username);
        $("#hidden-hash").val(password);        
//        $.ajax({
//            url: urlContext + '/login',
//            type: 'POST',
//            data: {
//                'username': username,
//                'password': password},
//            beforeSend: function (xhr) {
//                xhr.setRequestHeader(header, token);
//            },
//            success: function (data) {
//                console.log(data);
//            },
//            error: function (data) {
//                console.log(data);
//            }
//        });//fim ajax post
         $("#loginForm").submit();
    });//fim envento click
}

/*****************************************************************************
 * 
 * Funcoes comuns utilizadas nesta pagina.
 * 
 *****************************************************************************/


/*****************************************************************************
 * 
 * Funcoes de callback utilizadas nesta pagina.
 * 
 *****************************************************************************/