var TableLoadObj = (function(){
    
    var elemObj = {
        paginacao: "full_numbers",
        fixedColumns: 0,
        deferLoading: 10,
        cache: true,
         esconderColunas: true,
        corrigirPaginacao : true
    };

    return {
        openDialog: function(m, opts){
            var newModel = $("#"+m);
            newModel.on('show.bs.modal', function (event) {
                var modal = $(this);
                modal.find('.modal-title').text(opts.title ? opts.title : 'Modal');
            });
            newModel.modal('toggle');
            
            $("#btnSaveModal").on("click", function(e){
                e.preventDefault();

                var newObj = {}, dataItem = $("#formTypeOfRestrict").serializeObject();

                if(dataItem.tipo == ""){
                    setTimeout(marcarErro($("#tipo"), 'Esse campo é obrigatório'));

                    return false;
                }

                if (dataItem.tipo != 0) {
                    if(dataItem.quantidade <= 0){
                        setTimeout(marcarErro($("#quantidade"), 'Esse campo precisa ter um valor maior que 0'));

                        return false;
                    }
                }

                newObj.tipo = dataItem.tipo;
                newObj.motivo = dataItem.motivo;
                newObj.quantidade = dataItem.quantidade;
                newObj.items = opts.values;
                
                $.ajax({
                    url: BASE_URL+"cadastro/bloqueiodecompra/sendblock",
                    type: "POST",
                    async: true,
                    data: newObj,
                    dataType:'json',
                    beforeSend: function(){
                      $("#btnSaveModal").hide();
                      $(".enviando").show();
                    },
                    error: function(txt){
                        $(".enviando").hide();
                        $("#btnSaveModal").show();
                        msgAlerta(txt.message, function(){
                            window.location.reload();
                        });
                    },
                    success: function(data,status,xhr){
                        $(".enviando").hide();
                        $("#btnSaveModal").show();
                        msgAlerta(data.message, function(){
                            window.location.reload();
                        });
                    }
                });
            });
        },        
        makeTable: function(v,f){
            if(v.colunas){
                if(!$('#'+v.id+" thead").length){
                    var content = [], contentHtml = [], contentFoot = [];
                    jQuery.each(v.colunas, function(key, item){
                        var tmp = [
                          '<th class="',item.className,'" title="',(item.title ? item.title : item.targets),'">',item.name,'</th>'  
                        ];
                        content.push(tmp.join(""));
                    });
                    contentHtml = [
                        '<thead>',
                        '<tr class="totalizacao">',
                        content.join(""),
                        '</tr>',
                        '</thead>',
                        '<tbody>',
                        '<tr>',
                        '<td colspan="',v.colunas.length,'">',
                        '<h4 class="carregando"><span>Carregando... </span><img src="',BASE_URL_EXTERNO,'images/aguarde.gif"></h4>',
                        '</td>',
                        '</tr>',
                        '</tbody>'
                    ];
                    
                    if(f.active){
                        jQuery.each(v.colunas, function(key, item){
                            var tmp = [
                              '<th>',(key === 2 ? "<button class=\"btn btn-info btn-xs btn-seguir\" id=\"btnActionTable\">"+f.action.label+"</button>" : ''),'</th>'  
                            ];
                            contentFoot.push(tmp.join(""));
                        });
                        $("#"+v.id).append(contentHtml.join("")).append("<tfoot><tr>"+contentFoot.join("")+"</tr></tfoot>");
                        $("#btnActionTable").click(function(){
                            f.action.fnc();
                        });
                    } else {
                        $("#"+v.id).append(contentHtml.join(""));
                    }
                }
            } else {
                alert("ERROR");
            }
        },
        load: function(v,a){
            elemObj.ordenacao = v.ordenacao;
            elemObj.colunasTotal = v.colunas;
            elemObj.colunas = v.colunas;
            elemObj.orderPrincipal = v.orderPrincipal;
            elemObj.title = v.title;
            elemObj.filtroConfigurado = $.cookie("windowFilterName") ? true : false; //Engranagem fica azul para o usuário indicando está aplicado um filtro. by Filipe Falcão CRC70828
            elemObj.objAcaoConfiguracaoTabela = {
                enabled: v.filter,
                load: true,
                target: BASE_URL+"cadastro/bloqueiodecompra/filter",
                title: "Filtros",
                id: uniqID()
            };
            if(a){
                elemObj.dataSet = v.data;
                iniciarDetalhesAjaxMultinivel(v.id, elemObj);
            } else {
                $.post(BASE_URL + v.link, function (arrRetorno) {
                    elemObj.dataSet = arrRetorno.data;
                    iniciarDetalhesAjaxMultinivel(v.id, elemObj);
                }, "json");
            }
        },
        switch: function(v){
            var tableConfig = {}, me = this;
            switch (v.table) {
                case "tabela-detalhe-produtos":
                    tableConfig = {
                        filter: true,
                        id: v.table,
                        link: v.link,
                        data: null,
                        ordenacao: [
                            {orderable: false, targets: [0,1,4]},
                            {type: 'dinheiro', targets: [5,6,10,11,12,13,14,15,16,18]}
                        ],
                        colunas: [
                            {targets: 0, name: "", title:"Configurações da tabela.", className: "center col-0 coluna-fixa nao-filtra nao-arrasta configurar-coluna"},
                            {targets: 1, name: "SMF", title:"Semáforo Indicativo no NSE do produto.", className: "center   col-1 coluna-fixa nao-filtra nao-arrasta"},
                            {targets: 2, name: "DESCRIÇÃO", title:"Descrição do Produto.", className: "col-lg-2 col-2 coluna-fixa nao-filtra nao-arrasta"},
                            {targets: 3, name: "APRESENTAÇÃO", title:"Apresentação do Produto.", className: "col-lg-2 col-3 coluna-fixa nao-filtra nao-arrasta"},
                            {targets: 4, name: '<input type="checkbox" class="selecionarTodos">', title:"Indicador de seleção para bloqueio.", className: "center col-4 coluna-fixa nao-filtra nao-arrasta"},
                            {targets: 5, format: "0" ,name: "NSE-%", title:"Nível de serviço de estoque do produto na rede", className: "col-sm-1 col-5 numero"},
                            {targets: 6, format: "0,0", name: "GIRO", title:"Giro do estoque da rede: CMV-R$ dos últimos 30 dias dividido pelo Estoque-R$.", className: "col-sm-1 col-6 numero"},
                            {targets: 7, name: "CM", title:"Curva ABC da Margem de Contribuição do Produto na Rede.", className: "col-sm-1 col-7"},
                            {targets: 8, name: "C$", title:"Curva ABC Financeira do Produto na Rede.", className: "col-sm-1 col-8"},
                            {targets: 9, name: "CF", title:"Curva ABC Física do Produto na Rede.", className: "col-sm-1 col-9"},
                            {targets: 10, format :"0", name: "LOJAS", title:"Quantidade de lojas em que o produto é comercializado.", className: "col-sm-1 col-10 numero"},
                            {targets: 11, format: "0", name: "LJS BLQ", title:"Quantidade de lojas onde o produto está bloqueado",  className: "col-sm-1 col-11 numero"},
                            {targets: 12, totalizar: true, format: "0", name: "FCN TOTAL",  title:"Total somado do Facing de Referência do produto em todas as lojas em que o mesmo possua bloqueio de compra.",  className: "col-sm-1 col-12 numero"},
                            {targets: 13, totalizar: true, format: "0", name: "ESTOQUE-UN", title:"Quantidade total em estoque disponível nas lojas com bloqueio.", className: "col-sm-1 col-13 numero"},
                            {targets: 14, totalizar: true, format: "0", name: "EXCESSO-UN", title:"Quantidade do produto em excesso baseado no Facing das lojas com bloqueio.", className: "col-sm-1 col-14 numero"},
                            {targets: 15, totalizar: true, format: "0", name: "FALTA-UN",   title:"Quantidade do produto em falta baseado no facing das lojas com bloqueio.", className: "col-sm-1 col-15 numero"},
                            {targets: 16, totalizar: true, format: "0", name: "OC PDT-UN",  title:"Quantidade do produto em OC pendente considerando as lojas  com bloqueio.", className: "col-sm-1 col-16 numero"},
                            {targets: 17, name: "FABRICANTE", title:"Código e nome da Indústria do Produto.", className: "col-sm-1 col-17 "},
                            {targets: 18, format: "0",name: "CÓDIGO", title:"Código do Produto.",  className: "col-sm-1 col-18 numero"}
                        ],
                        orderPrincipal: [[5, "asc"]],
                        title: "Clique para exibir o NSE do produto",
                        modal: {
                            strHtml: $(".filtroentidades").html(),
                            bolPrimeiro: false
                        }
                    };
                    me.makeTable(tableConfig, {
                        active: true,
                        action: {
                            fnc: function(){
                                var items = [];
                                $(".arrObjProduto").each(function(key, obj){
                                    if($(obj).is(":checked")){
                                        items.push($(obj).data());
                                    }
                                });
                                
                                if(items.length === 0){
                                    msgAlerta("Para seguir é necessário selecionar ao menos um produto.");
                                } else {
                                    me.openDialog('modalRestrict', {
                                        title: "Definir bloqueio de compras",
                                        values: items
                                    });
                                }
                            },
                            label: "Seguir"
                        }
                    });
                    break;
                case "tabela-detalhe-lojaProdutos":
                    tableConfig = {
                        filter: true,
                        id: v.table,
                        link: v.link,
                        data: null,
                        ordenacao: [
                            {orderable: false, targets: [0, 1, 4]},
                            {type: 'dinheiro', targets: [7,8,9]}
                        ],
                        colunas: [
                            {targets: 0, name: "", className: "nao-filtra coluna-fixa col-00"},
                            {targets: 1, name: "SMF", className: "nao-filtra center coluna-fixa col-01"},
                            {targets: 2, format: "0", name: "LOJA", className: "col-sm-1 coluna-fixa numero col-02"},
                            {targets: 3, name: "NOME", className: "col-sm-8 coluna-fixa col-03"},                           
                            {targets: 4, name: '<input type="checkbox" class="selecionarTodos">', className: "col-sm-1 center col-4 coluna-fixa nao-filtra"},
                            {targets: 5, totalizar: true, format: "0", name: "QTD ITENS", className: "col-sm-1 numero col-05"},
                            {targets: 6, totalizar: true, format: "0", name: "ITENS BLQ", className: "col-sm-1 numero col-06"},
                            {targets: 7, totalizar: true, format: "0,0", name: "ESTOQUE-R$", className: "col-sm-1 numero col-07"},
                            {targets: 8, totalizar: true, format: "0,0", name: "EXCESSO-R$", className: "col-sm-1 numero col-08"},
                            {targets: 9, totalizar: true, format: "0,0", name: "FALTA-R$", className: "col-sm-1 numero col-09"},
                            {targets: 10, totalizar: true, format: "0,0", name: "VLR EM OC - R$", className: "col-sm-1 numero col-10"},
                        ],
                        orderPrincipal: [[3, "asc"]],
                        title: "Clique para exibir o NSE do comprador"
                    };
                    me.makeTable(tableConfig, {
                        active: false
                    });
                    break;
                case "tabela-detalhe-industriaProdutos":
                    tableConfig = {
                        filter: true,
                        id: v.table,
                        link: v.link,
                        data: null,
                        ordenacao: [
                            {orderable: false, targets: [0, 1, 4]},
                            {type: 'dinheiro', targets: [7,8,9]}
                        ],
                        colunas: [
                            {targets: 0, name: "", className: "nao-filtra coluna-fixa col-00"},
                            {targets: 1, name: "SMF", className: "nao-filtra center coluna-fixa col-01"},
                            {targets: 2, format: "0", name: "LOJA", className: "col-sm-1 coluna-fixa numero col-02"},
                            {targets: 3, name: "NOME", className: "col-sm-8 coluna-fixa col-03"},                           
                            {targets: 4, name: '<input type="checkbox" class="selecionarTodos">', className: "col-sm-1 center col-4 coluna-fixa nao-filtra"},
                            {targets: 5, totalizar: true, format: "0", name: "QTD ITENS", className: "col-sm-1 numero col-05"},
                            {targets: 6, totalizar: true, format: "0", name: "ITENS BLQ", className: "col-sm-1 numero col-06"},
                            {targets: 7, totalizar: true, format: "0,0", name: "ESTOQUE-R$", className: "col-sm-1 numero col-07"},
                            {targets: 8, totalizar: true, format: "0,0", name: "EXCESSO-R$", className: "col-sm-1 numero col-08"},
                            {targets: 9, totalizar: true, format: "0,0", name: "FALTA-R$", className: "col-sm-1 numero col-09"},
                            {targets: 10, totalizar: true, format: "0,0", name: "VLR EM OC - R$", className: "col-sm-1 numero col-10"},
                        ],
                        orderPrincipal: [[3, "asc"]],
                        title: "Clique para exibir o NSE do Indústria"
                    };
                    me.makeTable(tableConfig, {
                        active: false
                    });
                    break;
                case "tabela-principal-bloqueio-produto":
                    tableConfig = {
                        filter: false,
                        id: v.table,
                        link: v.link,
                        data: null,
                        ordenacao: [
                            {orderable: false, targets: [0,1]},
                            {type: 'dinheiro', targets: [3]}
                        ],
                        colunas: [
                            {targets: 0, name: "", className: "nao-filtra coluna-fixa col-00", title: "Configurações da tabela"},
                            {targets: 1, name: "SMF", className: "nao-filtra center coluna-fixa col-01", title: "Semáforo do NSE para os Produtos da Indústria"},
                            {targets: 2, name: "Indústria", className: "col-02", title: "Nome da indústria do produto"},
                            {targets: 3, name: "Estoque-R$", className: "numero col-03", totalizar: true, format: "0,0", title: "Estoque atual do Mix de produtos da indústria na rede, valorizado pelo último custo compra"},
                            {targets: 4, name: "NSE-%", className: "numero col-04", totalizar: false, format: "0", title: "Nível de Serviço do Estoque do Mix de Produtos da Indústria"},
                            {targets: 5, name: "MIX IND", className: "numero col-05", totalizar: true, format: "0", title: "Mix de produtos da indústria habilitados para venda"},
                            {targets: 6, name: "LJS BLQ", className: "numero col-06", totalizar: true, format: "0", title: "Quantidade de Lojas com Bloqueio de Compras"},
                            {targets: 7, name: "ITENS BLQ", className: "numero col-07", totalizar: true, format: "0", title: "Total de itens da indústria com Bloqueio de Compras"},
                            {targets: 8, name: "CURVA A", className: "numero col-08", totalizar: true, format: "0", title: "Total de itens do Indústria com bloqueio de compra da Curva A <curva de referência da rede>"},
                            {targets: 9, name: "CURVA B", className: "numero col-09", totalizar: true, format: "0", title: "Total de itens do Indústria com bloqueio de compra da Curva B <curva de referência da rede>"},
                            {targets: 10, name: "CURVA C", className: "numero col-10", totalizar: true, format: "0", title: "Total de itens do Indústria com bloqueio de compra da Curva C <curva de referência da rede>"},
                            {targets: 11, name: "CURVA D", className: "numero col-11", totalizar: true, format: "0", title: "Total de itens do Indústria com bloqueio de compra da Curva D <curva de referência da rede>"},
                            {targets: 12, name: "CÓDIGO", className: "center col-12", title: "Código da indústria"},
                        ],
                        orderPrincipal: [[4, "asc"]],
                        title: "Clique para exibir o Bloqueio de Compra"
                    };
                    me.makeTable(tableConfig, {
                        active: false
                    });
                    break;
                default:
                    break;
            };
            me.load(tableConfig, false);
        }
    }

})();

function disableField(o) {
    if(o.value == "0"){
        $("#quantidade").attr("disabled", true);
        $("#motivo").attr("disabled", true);
        $("#motivo").removeClass("obrigatorio");
        $("#quantidade").removeClass("obrigatorio");
    } else {
        $("#quantidade").attr("disabled", false);
        $("#motivo").attr("disabled", false);
        $("#motivo").addClass("obrigatorio");
        $("#quantidade").addClass("obrigatorio");
        
    }
}

$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};