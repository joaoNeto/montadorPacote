/**
 * @author Douglas Lira <douglas.lira.web@gmail.com>
 * @name informata
 *
 * Aqui deve conter todos as ações que são comuns aos sistema. Ao encontrar
 * um código que esteja sendo repetido com frequência, favor implementar aqui
 * e chamado no seu respectivo local. Esse processo faz parte do CLEAN CODE!
 *
 * Como implementar:
 *
 * 1º - Registrar a função na variavel informata
 * Ex.: informata.novaFuncao
 *
 * 2º - Implementar a função registrada
 * Ex.:
 * function novaFuncao(){ ... }
 *
 *
 * MOTIVO:
 *
 * O intuido dessa abordagem é deixar funções comuns em um escopo isolado
 * acessivel apenas atravez da variavel INFORMATA. Sempre que for necessário,
 * tiver duvidas ou não souber onde encontrar, basta acessar no console do
 * browser a palavra "informata." e terá acesso a biblioteca de funcções
 * já registradas.
 *
 */

(function() {

    'use strict';

    window.informata = {};

    /**
     * Method convert string to CamelCase
     *
     * @author Douglas Lira <douglas.lira.web@gmail.com>
     * @param {type} str
     * @returns {unresolved}
     * @example
     *
     *  "test-from-camelcase".toCamelCase();
     */
    String.prototype.toCamelCase = function(str) {
        return str.replace(/\s(.)/g, function($1) {
            return $1.toUpperCase();
        }).replace(/\s/g, '').replace(/^(.)/, function($1) {
            return $1.toLowerCase();
        });
    };
})();

(function(informata) {

    'use strict';

    // Global from scope
    informata.websocketEnabled = false;
    informata.websocket = null;
    informata.dataTableInstance = [];
    informata.orderTable = [];
    informata.clickOpenDrill = {};

    // Actions
    informata.toCamelCase = toCamelCase;
    informata.reloadTopBar = reloadTopBar;
    informata.getCurva = getCurva;
    informata.checkOutputTab = checkOutputTab;
    informata.checkBetweenValue = checkBetweenValue;
    informata.numberFormat = numberFormat;
    informata.mask = mask;
    informata.inRange = inRange;
    informata.indGeralCurvaInit = indGeralCurvaInit;
    informata.uniqID = uniqID;
    informata.pulseAnimate = pulseAnimate;
    informata.stopPropagation = stopPropagation;
    informata.randColor = randColor;
    informata.selectAll = selectAll;

    /**
     * Method to convert string to camelCase
     *
     * @author Douglas Lira <douglas.lira.web@gmail.com>
     * @param {type} str
     * @returns {unresolved}
     */
    function toCamelCase(str) {
        return str.toLowerCase().replace(/[-_]+/g, ' ').replace(/[^\w\s]/g, '').replace(/ (.)/g, function($1) {
            return $1.toUpperCase();
        }).replace(/ /g, '');
    }

    /**
     * Method to list icons at topbar
     *
     * @returns {undefined}
     */
    function reloadTopBar() {

        $.post(BASE_URL + "motordeservico/usuarioindicador/indicadoresdousuariologado?ajax=true", {1: 1}, function(arrRetorno) {
            if (arrRetorno.bolRetorno) {
                var strHtml = "";
                $(arrRetorno.arrIndicadores).each(function(i, objIndicador) {
                    strHtml += "<li>";
                    strHtml += "<a class='indicadorTopo' href='" + BASE_URL + objIndicador.urlIndicador + "' title='" + objIndicador.descricao + "'>";
                    strHtml += "<image src='" + BASE_URL + "images/indicadores/48_azul/" + objIndicador.imagem + "' />";
                    if (objIndicador.total > 0) {
                        strHtml += "<span class='badge bg-important'>" + objIndicador.total + "</span>";
                    }
                    strHtml += "</a>";
                    strHtml += "</li>";
                });
                $(".indicadoresTopo").html(strHtml);
            }
        }, "json");

        $(document).delegate(".indicadorTopo", 'mouseenter mouseleave', function(event) {
            if (event.type === 'mouseover') {
                var imagem = $(this).find("img").attr("src");
                imagem = imagem.replace("_azul", "_cinza");
                $(this).find("img").attr("src", imagem);
            } else {
                var imagem = $(this).find("img").attr("src");
                imagem = imagem.replace("_cinza", "_azul");
                $(this).find("img").attr("src", imagem);
            }
        });
    }


    /*
     * CRC: 71644
     * JOSE GABRIEL
     */

    function getCurva(curva) {
        var strCurva = '';
        if (curva) {
            strCurva = curva.replace(/[^a-zA-Z]/i, '');
            if (strCurva === 'A') {
                strCurva = 'semaforo-texto-verde';
            } else if (strCurva === 'B') {
                strCurva = 'semaforo-texto-amarelo';
            } else if (strCurva === 'C') {
                strCurva = 'semaforo-texto-vermelho';
            } else {
                strCurva = 'semaforo-texto-vermelho';
            }
        }
        return strCurva;
    }

    /**
     * Method to find dynamic tab position at INDICADORES
     *
     * @author Douglas Lira <douglas.lira.web@gmail.com>
     * @param {String} v
     * @returns {Number}
     */
    function checkOutputTab(v) {
        var searchPos = 0;
        $(".steps ul li").each(function(key, item) {
            if ($(item).text().indexOf(v) !== -1) {
                searchPos = key + 1;
                return;
            }
        });
        return searchPos;
    }

    function checkBetweenValue(obj, min, max) {
        var elem = $(obj), infoValue = elem.data();
        $(elem.parent()).find('.mensagem-erro').remove();
        elem.parent().find('.errorIcone').remove();
        elem.parent().find('.certoIcone').remove();
        if ((parseInt(elem.val()) >= parseInt(infoValue.min ? infoValue.min : min)) && (parseInt(elem.val()) <= parseInt(infoValue.max ? infoValue.max : max))) {
            elem.parent().addClass("has-success");
            elem.parent().removeClass("has-error").attr("style", "");
            elem.parent().find(".error").remove();
            elem.parent().append('<span class="certoIcone"><i class="fa fa-check-circle fa-2x"></i></span>');
        } else {
            elem.parent().addClass("has-error");
            elem.parent().append('<span class="errorIcone"><i class="fa fa-times-circle fa-2x"></i></span>');
            elem.parent().append('<span class="mensagem-erro has-error">A quantidade de dias deve ser maior que 0 e/ou menor igual a 7.</span>');
        }
    }



    function indGeralCurvaInit(arrId, objProdutoCabecalho) {

        for (var i = 0; i < arrId.length; i++) {

            /**
             * TODO: REMOVER CÓDIGO DUPLICADO
             */

            switch (arrId[i]) {
                case 'curvaFisicaProduto':
                    $('#' + arrId[i]).removeClass('semaforo-texto-verde').removeClass('semaforo-texto-amarelo').removeClass('semaforo-texto-vermelho');
                    $('#' + arrId[i]).addClass(informata.getCurva(objProdutoCabecalho.curvaFisica));
                    $('#' + arrId[i]).text(objProdutoCabecalho.curvaFisica.replace(/[^a-zA-Z]/i, ''));
                    break;
                case 'curvaFinanceiraProduto':
                    $('#' + arrId[i]).removeClass('semaforo-texto-verde').removeClass('semaforo-texto-amarelo').removeClass('semaforo-texto-vermelho');
                    $('#' + arrId[i]).addClass(informata.getCurva(objProdutoCabecalho.curvaFinanceira));
                    $('#' + arrId[i]).text(objProdutoCabecalho.curvaFinanceira.replace(/[^a-zA-Z]/i, ''));
                    break;
                case 'curvaMargemProduto':
                    $('#' + arrId[i]).removeClass('semaforo-texto-verde').removeClass('semaforo-texto-amarelo').removeClass('semaforo-texto-vermelho');
                    $('#' + arrId[i]).addClass(informata.getCurva(objProdutoCabecalho.curvaMc));
                    var curvaMedia = objProdutoCabecalho.curvaMc.replace(/[^a-zA-Z]/i, '');
                    curvaMedia = (curvaMedia == '-' ? "D" : curvaMedia);
                    $('#' + arrId[i]).text(curvaMedia);
                    break;
            }
            /*antes
             $('#'+arrId[i]).removeClass('semaforo-texto-verde').removeClass('semaforo-texto-amarelo').removeClass('semaforo-texto-vermelho');
             $('#'+arrId[i]).addClass(informata.getCurva(objProdutoCabecalho.curvaFisica));
             $('#'+arrId[i]).text(objProdutoCabecalho.curvaFisica.replace(/[^a-zA-Z]/i, ''));
             */

            $(".semaforo-menor div .valor-indicador.semaforo-texto").css({'font': "bold 1.6em/1.2 'roboto'", width: "35px", height: "35px"});
            $(".semaforo-menor div .valor-indicador.semaforo-texto").css({'line-height': "1.4"});
        }

    }

    /**
     * Method to format number
     *
     * @author Douglas Lira <douglas.lira.web@gmail.com>
     * @param {type} number
     * @param {type} decimals
     * @param {type} decPoint
     * @param {type} thousandsSep
     * @returns {unresolved}
     */
    function numberFormat(number, decimals, decPoint, thousandsSep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number;
        var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals);
        var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep;
        var dec = (typeof decPoint === 'undefined') ? '.' : decPoint;
        var s = '';
        var toFixedFix = function(n, prec) {
            var k = Math.pow(10, prec);
            return '' + (Math.round(n * k) / k).toFixed(prec);
        };
        // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }

    /**
     * Method to apply mask into input text
     *
     * @author Douglas Lira <douglas.lira.web@gmail.com>
     * @param {type} m
     * @param {type} v
     * @returns {@var;tv|String}
     */
    function mask(m, v) {

        var me = this;
        function empty(v) {
            var vclean = "";
            if (v !== null) {
                for (var i = 0; i < 30; i++) {
                    if (v.substr(i, 1) === " ") {
                    } else {
                        vclean = vclean + v.substr(i, 1);
                    }
                }
            }
            return vclean;
        }

        if (m === '###.###.###-##|##.###.###/####-##') {
            if (v.length > 14) {
                return informata.mask('##.###.###/####-##', v);
            } else {
                return informata.mask('###.###.###-##', v);
            }
        }

        if (m === '## ####-####|## #####-####') {
            if (v.length > 12) {
                return informata.mask('## #####-####', v);
            } else {
                return informata.mask('## ####-####', v);
            }
        }

        var tv = "";
        var ret = "";
        var character = "#";
        var separator = "|";
        var maskUse = "";
        var cleanMask = "";
        v = empty(v);
        if (v === "") {
            return v;
        }
        ;
        var temp = m.split(separator);
        var dif = 1000;
        for (var i = 0; i < v.length; i++) {
            if (!isNaN(v.substr(i, 1))) {
                tv = tv + v.substr(i, 1);
            }
        }

        v = tv;
        for (var i = 0; i < temp.length; i++) {
            var mult = "", validate = 0;
            for (var j = 0; j < temp[i].length; j++) {

                if (temp[i].substr(j, 1) === "]") {
                    temp[i] = temp[i].substr(j + 1);
                    break;
                }

                if (validate === 1) {
                    mult = mult + temp[i].substr(j, 1);
                }

                if (temp[i].substr(j, 1) === "[") {
                    validate = 1;
                }
            }
            for (var j = 0; j < v.length; j++) {
                temp[i] = mult + temp[i];
            }
        }

        if (temp.length === 1) {
            maskUse = temp[0];
            cleanMask = "";
            for (var j = 0; j < maskUse.length; j++) {
                if (maskUse.substr(j, 1) === character) {
                    cleanMask = cleanMask + character;
                }
            }
            var tam = cleanMask.length;
        } else {

            for (var i = 0; i < temp.length; i++) {
                cleanMask = "";
                for (var j = 0; j < temp[i].length; j++) {
                    if (temp[i].substr(j, 1) === character) {
                        cleanMask = cleanMask + character;
                    }
                }
                if (v.length > cleanMask.length) {
                    if (dif > (v.length - cleanMask.length)) {
                        dif = v.length - cleanMask.length;
                        maskUse = temp[i];
                        tam = cleanMask.length;
                    }
                } else if (v.length < cleanMask.length) {
                    if (dif > (cleanMask.length - v.length)) {
                        dif = cleanMask.length - v.length;
                        maskUse = temp[i];
                        tam = cleanMask.length;
                    }
                } else {
                    maskUse = temp[i];
                    tam = cleanMask.length;
                    break;
                }
            }
        }

        if (v.length > tam) {
            v = v.substr(0, tam);
        } else if (v.length < tam) {
            var masct = "", j = v.length;
            for (var i = maskUse.length - 1; i >= 0; i--) {
                if (j === 0) {
                    break;
                }
                if (maskUse.substr(i, 1) === character) {
                    j--;
                }
                masct = maskUse.substr(i, 1) + masct;
            }
            maskUse = masct;
        }

        j = maskUse.length - 1;
        for (var i = v.length - 1; i >= 0; i--) {
            if (maskUse.substr(j, 1) !== character) {
                ret = maskUse.substr(j, 1) + ret;
                j--;
            }
            ret = v.substr(i, 1) + ret;
            j--;
        }
        return ret;
    }
    ;
    /**
     * Method to check value in range
     *
     * @author Douglas Lira <douglas.lira.web@gmail.com>
     * @param {type} number
     * @param {type} start
     * @param {type} end
     * @returns {Boolean}
     */
    function inRange(number, start, end) {
        return number > start && number < end;
    }

    /**
     * Method to create uniq id to element
     *
     * @author Douglas Lira <douglas.lira.web@gmail.com>
     * @returns {String}
     */
    function uniqID() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
        return s4() + s4();
    }

    /**
     * Methodo to craete animation during ajax loading
     *
     * @author Douglas Lira <douglas.lira.web@gmail.com>
     * @param {type} elm
     * @param {type} visible
     * @returns {undefined}
     */
    function pulseAnimate(elm, visible) {
        var newElm = $(elm);
        if (visible) {
            if (!newElm.is(":visible") && visible) {
                newElm.show();
            } else {
                newElm.animate({
                    opacity: 0.5
                }, 700, function() {
                    newElm.animate({
                        opacity: 1
                    }, 700, function() {
                        informata.pulseAnimate(elm, visible);
                    });
                });
            }

        } else {
            newElm.fadeOut();
        }
    }

    /**
     * Method to stop propagation event
     *
     * @author Douglas Lira <douglas.lira.web@gmail.com>
     * @param {type} evt
     * @returns {undefined}
     */
    function stopPropagation(evt) {
        if (evt.stopPropagation !== undefined) {
            evt.stopPropagation();
        } else {
            evt.cancelBubble = true;
        }
    }

    /**
     * Method to generate color
     *
     * @author Douglas Lira <douglas.lira.web@gmail.com>
     * @returns {String}
     */
    function randColor() {
        var cssHSL = "hsl(" + 360 * Math.random() + ',' + (25 + 70 * Math.random()) + '%,' + (85 + 10 * Math.random()) + '%)';
        return cssHSL;
    }

    /**
     * Seleciona todos os checkbox filho
     * 
     * @param {*} classePai 
     * @param {*} classeFilho 
     * 
     * informata.selectAll('classe1', 'classe2');
     */
    function selectAll(classePai, classeFilho) {
       
        var checked = false;

        if ($('.' + classePai).is(':checked')) {
            checked = true;
        }

        $('input.' + classeFilho + ':not(:disabled)').prop('checked', checked).trigger('change');
    }

})(informata);

/**
 * Create watch to event show|hide
 *
 * @author Douglas Lira <douglas.lira.web@gmail.com>
 * @param {type} $
 * @returns {undefined}
 * @example
 *
 *   $('#btnShow').click(function(){
 *     $('#foo').show();
 *   });
 *
 *   $('#foo').on('show', function(){ ... });
 *
 */
(function($) {
    $.each(['show', 'hide'], function(i, ev) {
        var el = $.fn[ev];
        $.fn[ev] = function() {
            this.trigger(ev);
            return el.apply(this, arguments);
        };
    });
})(jQuery);
