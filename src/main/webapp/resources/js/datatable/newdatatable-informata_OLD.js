/**
 * @author Douglas Lira <douglas.lira.web@gmail.com>
 * @param {type} informata
 * @returns {undefined}
 * @example
 *
 * window.informata.dataTable('table-main', {
 *   url: "url/to/load-table-main",
 *   drilldown: {
 *     enabled: true
 *   },
 *   columns: [
 *     {data:"A", targets: 0, name: ''},
 *     {data:"B", targets: 1, name: 'Column 1'},
 *     {data:"C", targets: 2, name: 'Column 2'},
 *     {data:"D", targets: 3, name: 'Column 3'},
 *     {data:"E", targets: 4, name: 'Column 4'},
 *     {data:"F", targets: 5, name: 'Column 5', summarize: true},
 *     {data:"G", targets: 6, name: 'Column 6'},
 *     {data:"H", targets: 7, name: 'Column 7'}
 *   ]
 * }).init();
 *
 */
(function (informata) {

    'use strict';

    informata.dataTable = dataTable;

    function dataTable(idTable, settings) {
        console.log(urlContext);
        // Totalizador
        settings.summarize = settings.summarize === undefined ? true : settings.summarize;

        // Pesquisa
        settings.search = settings.search === undefined ? true : settings.search;

        // Ordenação
        settings.order = settings.order === undefined ? [[1, "desc"]] : settings.order;

        // Configurações da tabela
        settings.config = settings.config === undefined ? true : settings.config;
        settings.config_url = settings.config_url === undefined ? urlContext + "configuracaotabela/configuracaodefault" : settings.config_url;

        // QuantidadePorPagina
        settings.qtdItens = settings.qtdItens === undefined ? 50 : settings.qtdItens;

        var clickTemp = null;
        var tabelaSalvaNoBanco = [];

        return {

            /**
             * Method to adjusts columns
             *
             * @param {type} idElemento
             * @param {type} obj
             * @returns {undefined}
             */
            adjustColumns: function (idElemento, obj) {
                var tabela = document.getElementById(idElemento);
                var rowFilter = document.querySelectorAll('#row-search-' + obj);
                $("#" + idElemento + "_wrapper").find("table").css({
                    "width": "100%"
                });
                if (tabela.parentNode.className === 'dataTables_scrollBody') {
                    Array.prototype.forEach.call(rowFilter, function (el, i) {
                        var larguraCelulaFiltroCabecalho = tabela.parentNode.previousElementSibling.firstElementChild.firstElementChild.firstElementChild.firstElementChild.children[i].style.width;
                        tabela.parentNode.previousElementSibling.firstElementChild.firstElementChild.firstElementChild.firstElementChild.nextElementSibling.children[i].style.cssText += 'min-width: ' + larguraCelulaFiltroCabecalho + '; max-width: ' + larguraCelulaFiltroCabecalho;
                        el.style.cssText += 'min-width: ' + larguraCelulaFiltroCabecalho + '; max-width: ' + larguraCelulaFiltroCabecalho;
                        el.parentNode.nextElementSibling.children[i].style.cssText += 'min-width: ' + larguraCelulaFiltroCabecalho + '; max-width: ' + larguraCelulaFiltroCabecalho;
                    });
                }
            },

            /**
             * Method return instance of DataTable
             *
             * @param {type} v
             * @returns {Boolean}
             */
            getInstance: function (v) {
                return window.informata.dataTableInstance[v] ? window.informata.dataTableInstance[v] : false;
            },

            /**
             *
             * @param {type} v
             * @returns {window.informata.orderTable|Window.informata.orderTable}
             */
            getOrder: function (v) {
                return window.informata.orderTable[v];
            },

            /**
             * Method set custom order
             *
             * @param {type} v
             * @returns {undefined}
             */
            setOrder: function (instance, v) {
                window.informata.orderTable[instance] = v;
            },

            /**
             * Method to notification
             *
             * @param {type} title
             * @param {type} text
             * @returns {undefined}
             */
            openNotification: function (title, text) {
                if (title && text) {
                    var uniqID = informata.uniqID();
                    var contentDialog = ['<div id="', uniqID, '">', text, '</div>'];
                    $(contentDialog.join("")).prependTo("#" + idTable).dialog({
                        title: title,
                        modal: true,
                        show: {
                            effect: 'fade',
                            duration: 250
                        },
                        hide: {
                            effect: 'fade',
                            duration: 250
                        },
                        buttons: [{
                            text: "Fechar",
                            class: "btn btn-primary",
                            click: function () {
                                $(this).dialog('destroy').remove();
                            }
                        }],
                        close: function () {
                            $(this).dialog('destroy').remove();
                        }
                    });
                }
            },

            /**
             *
             * @param {type} instanceTable
             * @param {type} instanceName
             * @returns {undefined}
             */
            searchColumn: function (instanceTable, instanceName) {
                var me = this;
                var orderCol = me.getOrder(instanceName + "-order");
                jQuery.each(orderCol, function (key, i) {
                    if (!settings.drilldown) {
                        if ($("#" + instanceName + "-field-search-" + i).val() !== "" && settings.columns[i].search) {
                            $("#" + instanceName + "-header-" + key + ' i').removeClass('fa-search').addClass('fa-filter');

                        }
                    } else {
                        if (settings.drilldown && i !== 0) {
                            if ($("#" + instanceName + "-field-search-" + i).val() !== "" && settings.columns[i].search) {
                                $("#" + instanceName + "-header-" + key + ' i').removeClass('fa-search').addClass('fa-filter');
                            }
                        }
                    }
                });
            },

            /**
             * Method to summarize column
             *
             * @param {type} instanceTable
             * @param {type} instanceTableName
             * @returns {undefined}
             *
             * TODO: Improve this method
             */
            summarizeColumn: function (instanceTable, instanceName) {
                var me = this;
                var orderCol = me.getOrder(instanceName + "-order");
                var keyTMP = {};

                jQuery.each(settings.columns, function (i, c) {
                    if (c.summarize) {
                        keyTMP.colName = c.name;
                        keyTMP.colTarget = c.targets;
                        keyTMP.colType = c.type;
                    }

                    jQuery.each(orderCol, function (key, pos) {
                        if (settings.columns[pos].name === keyTMP.colName) {
                            keyTMP.colSum = instanceTable.column(key, {
                                search: 'applied'
                            }).data().sum();
                            keyTMP.colPosActual = key;

                            var data = 0;

                            if (keyTMP.colType == "number") {
                                data = keyTMP.colSum.toLocaleString('pt-br', { style: 'decimal' });
                            }
                            else if (keyTMP.colType == "currency") {
                                data = keyTMP.colSum.toLocaleString('pt-br', { minimumFractionDigits: 2, style: 'decimal' });
                            }

                            $("#" + instanceName + "-summarize-" + keyTMP.colTarget).empty().html(data);
                        }
                    });
                });
            },

            /**
             *
             * @param {type} instanceTable
             * @param {type} instanceName
             * @returns {undefined}
             */
            renderFieldColumn: function (instanceTable, instanceName) {
                var me = this;
                var orderCol = me.getOrder(instanceName + "-order");
                var keyTMP = {};
                jQuery.each(settings.columns, function (i, c) {
                    if (c.field) {
                        keyTMP.colName = c.name;
                        keyTMP.colTarget = c.targets;
                        keyTMP.field = c.field;
                    }

                    jQuery.each(orderCol, function (key, pos) {
                        if (settings.columns[pos].name === keyTMP.colName) {
                            keyTMP.colPosActual = key;
                            var field = [];
                            if (keyTMP.field.type === "text") {
                                field = ['<input class="form-control input-sm" id="', instanceName, '-field-', keyTMP.colTarget, '-summarize" type="', keyTMP.field.type, '" value="', keyTMP.field.value, '">'];
                            } else if (keyTMP.field.type === "select") {
                                field = ['<select class="form-control input-sm" id="', instanceName, '-field-', keyTMP.colTarget, '-summarize"></select>'];
                            } else if (keyTMP.field.type === "checkbox") {
                                field = ['<input class="input-sm ', keyTMP.field.class, '" id="', instanceName, '-field-', keyTMP.colTarget, '-summarize" type="', keyTMP.field.type, '" value="', keyTMP.field.value, '">'];
                            } else if (keyTMP.field.type === "icon") {
                                field = ['<i class="fa ', keyTMP.field.class, '" title="', keyTMP.field.title, '"></i>'];
                            }

                            if (keyTMP.field.inHead) {
                                $("#" + instanceName + "-header-" + keyTMP.colTarget).empty().html(field.join(""));
                                var elementEvent = $('#' + instanceName + '-field-' + keyTMP.colTarget + '-header');
                            } else {
                                $("#" + instanceName + "-summarize-" + keyTMP.colTarget).empty().html(field.join(""));
                                var elementEvent = $('#' + instanceName + '-field-' + keyTMP.colTarget + '-summarize');
                            }

                            if (keyTMP.field.actions) {
                                jQuery.each(keyTMP.field.actions, function (a, b) {
                                    elementEvent.on(a, function (e) {
                                        b(e);
                                    });
                                });
                            }
                        }
                    });

                });
            },

            /**
             * Method to render value in header
             *
             * @param {type} obj
             * @returns {undefined}
             */
            renderSummarize: function (obj) {
                var countRows = $('#' + idTable + ' thead tr th').length;
                var tt = ["<tr class=\"text-right\" style=\"background-color: #999; border: 1px solid #FFFF; color: #FFF\">"];
                for (var i = 0; i < countRows; i++) {
                    tt.push("<td><div class=\"linhaTotalizadora\" id=\"" + obj + "-summarize-" + i + "\"></div></td>");
                }
                tt.push("</tr>");
                $('#' + idTable + ' thead tr:last').after(tt.join(""));
            },

            makeTable: function (obj) {

                var me = this;
                var keyTMP = {};

                jQuery.each(settings.columns, function (i, c) {

                    switch (c.type) {
                        case 'currency':
                            c.render = function (data, type, row) {
                                return (data != null) ? data.toLocaleString('pt-br', { minimumFractionDigits: 2, style: 'decimal' }) : 0;
                            }
                            break;

                        case 'number':
                            c.render = function (data, type, row) {
                                return (data != null) ? data.toLocaleString('pt-br', { style: 'decimal' }) : 0;
                            }
                            break;
                        default:
                            c.render = function (data, type, row) {

                                if (!data) {
                                    data = '';
                                }

                                return String(data);
                            }
                    }
                });
            },

            /**
             * Method to render inputs in header
             *
             * @param {type} obj
             * @returns {undefined}
             */
            renderSearchField: function (obj) {
                var countRows = $('#' + idTable + ' thead tr th').length;
                var tt = ["<tr style=\"display: none\" id=\"row-search-" + obj + "\">"];
                for (var i = 0; i < countRows; i++) {

                    settings.columns[i].search = settings.columns[i].search === undefined ? true : settings.columns[i].search;

                    if (!settings.drilldown) {
                        tt.push("<td>" + (settings.columns[i].search ? "<input type=\"text\" id=\"" + obj + "-field-search-" + i + "\" data-instance=\"" + obj + "\" data-column=\"" + i + "\" class=\"form-control input-sm " + obj + "-field-search\" placeholder=\"Pesquisar...\" />" : "") + "</td>");
                    } else {
                        if (settings.drilldown && i !== 0) {
                            tt.push("<td>" + (settings.columns[i].search ? "<input type=\"text\" id=\"" + obj + "-field-search-" + i + "\" data-instance=\"" + obj + "\" data-column=\"" + i + "\" class=\"form-control input-sm " + obj + "-field-search\" placeholder=\"Pesquisar...\" />" : "") + "</td>");
                        } else {
                            tt.push("<td style=\"text-align:center;\"><a href=\"javascript:void(0);\" id=\"" + obj + "-field-search-close\" > X <i class=\"fa fa-window-close  text-muted\" rel=\"popover\"></i></a></td>");
                        }
                    }
                }
                tt.push("</tr>");
                $('#' + idTable + ' thead tr:last').after(tt.join(""));
                $("#" + obj + "-field-search-close").on("click", function () {
                    $("#row-search-" + obj).hide();
                });
                return;
            },

            /**
             * Method to render icons and labels in header
             *
             * @param {type} instanceTable
             * @param {type} instanceName
             * @param {type} labelText
             * @param {type} i
             * @returns {undefined}
             */
            renderHeader: function (instanceTable, instanceName, labelText, i) {

                var me = this;
                var objColumn = $(instanceTable.column(i).header());
                var contentButtonSearch = [];
                var contentButtonConfig = [];
                var contentBasic = [
                    '<div id="', instanceName, '-header-', i, '">', labelText, '</div>'
                ];
                objColumn.empty().html(contentBasic.join(""));
                contentButtonSearch = me.renderButtonSearch(instanceName + "-search-" + i);

                if (settings.config) {
                    contentButtonConfig = me.renderButtonConfig(instanceName + "-config-" + i);
                }

                if (!settings.drilldown) {

                    if (settings.columns[i].search) {
                        $("#" + instanceName + "-header-" + i + "").append(contentButtonSearch.join(""));
                    }

                    if (i === 0) {
                        $("#" + instanceName + "-header-" + i + "").append(contentButtonConfig.join(""));
                    }

                } else {
                    if (settings.drilldown && i !== 0) {
                        if (settings.columns[i].search) {
                            $("#" + instanceName + "-header-" + i + "").append(contentButtonSearch.join(""));
                        }
                    } else {
                        $("#" + instanceName + "-header-" + i + "").append(contentButtonConfig.join(""));
                    }
                }

                if (settings.columns[i].search) {

                    $("#" + instanceName + '-header-' + i).addClass('header-search');

                    $("#" + instanceName + "-search-" + i).on("click", function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        $("." + idTable + "-tempRmv").remove();
                        me.openRowSearch(instanceTable, instanceName, labelText, i);
                    });
                }

                $("#" + instanceName + "-config-" + i).on("click", function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $("." + idTable + "-tempRmv").remove();
                    me.openConfig();
                });
            },

            /**
             * Method to enable row to search
             *
             * @param {type} instanceTable
             * @param {type} instanceName
             * @param {type} labelText
             * @param {type} i
             * @returns {undefined}
             */
            openRowSearch: function (instanceTable, instanceName, labelText, i) {
                $("#row-search-" + instanceName).show();
                var instanceActual = "",
                    keyTMP = {};
                jQuery.each(settings.columns, function (x, c) {
                    if (c.name === labelText) {
                        keyTMP.colName = c.name;
                        keyTMP.colTarget = c.targets;
                    }
                });
                $("#" + instanceName + "-field-search-" + keyTMP.colTarget).focus();
                $("." + instanceName + "-field-search").off().on("keyup change", function (e) {
                    $("." + idTable + "-tempRmv").remove();
                    instanceActual = $(this).data().instance;
                    window.informata.dataTableInstance[instanceActual].api().column($(this).parent().index() + ':visible').search(this.value).draw();
                    //$("#" + instanceName + "-header-" + i).append("<i style=\"float:right\" class=\"fa fa-filter\" aria-hidden=\"true\"></i>");
                    var code = e.keyCode || e.which;
                    if (code === 27 || code === 13) {
                        $("#row-search-" + instanceActual).hide();
                    }
                });

            },

            /**
             * TODO: Implements to enable filters
             * @returns {undefined}
             */
            openConfig: function () {
                var uniqID = informata.uniqID();
                var contentDialog = ['<div id="', uniqID, '" class="row"></div>'];
                var conteudoConfiguracao = $(contentDialog.join("")).prependTo("#" + idTable).load(settings.config_url, "", function () {

                    var tabela = $("#" + idTable).DataTable();
                    var colunas = tabela.columns();
                    var quantidadeColunas = colunas[0].length;
                    var strHtml = '<ol></ol>';
                    var strColuna = '<div class="col-md-6 container-itens-list"></div>';
                    var itensAtivos = 0;

                    $('.configuracao-default').append(strHtml);
                    $('.configuracao-default ol:last').append(strColuna);



                    for (var i = 0; i < quantidadeColunas; i++) {

                        var th = $(tabela.column(i).header());

                        if (i == 0) {
                            th.addClass('desativar-visibilidade');
                        }

                        if (!th.hasClass('desativar-visibilidade')) {
                            itensAtivos++
                        }

                        if (itensAtivos > 0 && (itensAtivos % 11 == 0)) { $('.configuracao-default ol:last').append(strColuna); }

                        var coluna = tabela.column(i);
                        var strHtmlLi = "";
                        var visible = tabela.column(i).visible() == true ? 'checked=checked' : '';
                        var exibir = th.hasClass('desativar-visibilidade');
                        var texto = (th.find(".linhaCabecalho").size() > 0) ? th.find(".linhaCabecalho").text().replace(/<\/?[^>]+(>|$)/g, "").trim() : th.text().replace(/<\/?[^>]+(>|$)/g, "").trim();
                        var strTitutlo = "";
                        var checkTypeIsCheckbox = th.find(".linhaCabecalho input[type=checkbox]").is(':checkbox');

                        if (th.find(".linhaCabecalho").text().length === 0 && i === 0) {
                            strTitutlo = "<i class=\"fa fa-cog\"></i>&nbsp;CONFIGURAÇÕES";
                        } else {
                            strTitutlo = th.attr('aria-label');
                        }

                        strHtmlLi += '<li class="text-muted item-configuracao">';
                        strHtmlLi += '<input type="checkbox" class="check-visible" data-index="' + i + '" style="margin-right: 10px; float: left" ' + visible + '>';
                        strHtmlLi += strTitutlo.split(':')[0];
                        strHtmlLi += '</li>';

                        if (!exibir) {
                            $('.configuracao-default .container-itens-list:last').append(strHtmlLi);
                        }
                    }

                    $('.check-visible').on('click', function () {
                        var index = $(this).data('index');
                        var status = $(this).prop('checked');
                        tabela.column(index).visible(status).state.save();
                        tabela.draw();
                    });

                    $(document).off('click', '.excel');
                    $(document).on('click', '.excel', function () {
                        $("." + idTable + '-excel').trigger("click");
                        $('.ui-dialog-titlebar-close').trigger('click');
                    });

                    $(document).off('click', '.imprimir');
                    $(document).on('click', '.imprimir', function () {
                        $("." + idTable + '-pdf').trigger("click");
                        $('.ui-dialog-titlebar-close').trigger('click');
                    });

                    $(".datatables-configuracoes").steps({
                        headerTag: "h3",
                        bodyTag: "section",
                        transitionEffect: "slideLeft",
                        autoFocus: true,
                        // Disables the finish button (required if pagination is enabled)
                        enableFinishButton: false,
                        // Disables the next and previous buttons (optional)
                        enablePagination: false,
                        // Enables all steps from the begining
                        enableAllSteps: true,
                        // Removes the number from the title
                        titleTemplate: "#title#"
                    });
                });

                var dialog = bootbox.dialog({
                    title: 'Configuração',
                    message: '<p><i class="fa fa-spin fa-spinner"></i> Carregando configuração...</p>',
                    size: 'large',
                    className: 'modal',
                    buttons: {
                        cancel: {
                            label: "Fechar",
                            className: 'btn btn-info'
                        },
                        noclose: {
                            label: "Marcar todos",
                            className: 'btn btn-info marcar-todos',
                            callback: function () {
                                $('.check-visible').not(':checked').trigger('click');
                                return false;
                            }
                        },
                        ok: {
                            label: "Restaurar padrão",
                            className: 'btn btn-info restaurar',
                            callback: function () {
                                $('#' + idTable).DataTable().state.clear();
                                dialog.modal('hide');
                                window.location.reload();
                            }
                        },
                    }
                });

                dialog.init(function () {

                    var btnExcel = '<button data-bb-handler="Excel" type="button" class="btn btn-default btn-xs excel pull-right"></button>';
                    var btnPdf = '<button data-bb-handler="Imprimir" type="button" style="margin-right: 10px" class="btn btn-default btn-xs imprimir pull-right"></button>';

                    dialog.find('.bootbox-body').css('padding', '0px 20px').html(conteudoConfiguracao);
                    dialog.find('.modal-header').append(btnExcel);
                    dialog.find('.modal-header').append(btnPdf);
                });
            },


            /**
             * Method to render buttons search
             *
             * @param {type} idBtn
             * @returns {Array}
             */
            renderButtonSearch: function (idBtn, instanceName) {
                return [
                    '<div style="float:left;">',
                    '<a title="Pesquisar" id="', idBtn, '" data-instance="', instanceName, '" href="javascript:void(0)">',
                    '<i class="fa fa-search text-muted buscar-tabela-detalhes" rel="popover"></i>',
                    '</a>',
                    '</div>'
                ];
            },

            /**
             * Method to render config button
             *
             * @param {type} idBtn
             * @returns {Array}
             */
            renderButtonConfig: function (idBtn) {
                return [
                    '<div style="display:inline-block; margin-right: 3px;">',
                    '<a class="btConfigurarJanela text-center" title="Configurar colunas da tabela" id="', idBtn, '" href="javascript:void(0)">',
                    '<i class="fa fa-cog configColunasJanela text-muted" rel="popover"></i>',
                    '</a>',
                    '</div>'
                ];
            },

            /**
             * Method to render exports button
             *
             */
            renderButtonsExport: function () {
                return [
                    {
                        extend: 'excel',
                        text: 'Excel',
                        title: "MD-LOG Web",
                        className: idTable + '-excel',
                        exportOptions: {
                            columns: ':visible:not(.no-print)',
                            stripHtml: false
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        title: "*",
                        className: idTable + '-pdf',
                        orientation: 'landscape',
                        exportOptions: {
                            columns: ':visible:not(.no-print)'
                        },
                        customize: function (doc) {

                            var dt = new Date();
                            var dia = dt.getDate();
                            var mes = dt.getMonth() + 1;
                            var ano = dt.getFullYear();

                            if (mes < 10) {
                                mes = '0' + mes;
                            }

                            doc.pageMargins = [10, 25, 10, 15];
                            doc.defaultStyle.fontSize = 7;
                            doc.content[0].text = '';
                            doc.content[0].margin = [0, 0, 0, 0];
                            doc.content[1].table.widths = '*';
                            doc.styles.tableHeader.alignment = 'left';
                            doc.styles.tableHeader.fontSize = 7;
                            doc.header = {
                                text: $.trim($('ul.breadcrumb li:last').text()) + ' - ' + dia + "/" + mes + "/" + ano,
                                alignment: 'right',
                                margin: [0, 10, 10, 0]
                            }

                            // RODAPÉ
                            doc.footer = function (currentPage, pageCount) {

                                var cols = [];
                                var objFooter = {};

                                cols[0] = { text: 'www.informata.com.br', alignment: 'left', margin: [10] };
                                cols[1] = { text: 'Página ' + currentPage.toString() + ' de ' + pageCount, alignment: 'right', margin: [0, 0, 10] };

                                objFooter['alignment'] = 'center';
                                objFooter['columns'] = cols;

                                return objFooter;
                            }

                            doc.content.splice(1, 0, {
                                margin: [0, 0, 0, 12],
                                alignment: 'left',
                                fit: [200, 150],
                                image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANoAAAAxCAIAAAAz/CDNAAAN00lEQVR4Ae2caVQUV/rGFbsbERRBoxLNP/5zZhLPmZnEJToxHyZxsswoi3HBRAXZF2RBQBNwx0UTI2piHFQWOrg4MSpLs4AsNIsiI4m4xCVOXOIyKi6o0EtXV9c89E16Olwtu/vQdH8oznM4ZdWt28WtXz3v+95bba/XJ88TJMhOJOAoSMBRkCABxydKkICjIEECjoIEHCdFrvPbc9Z3W92bHyUII24zCTi++WGC77baqAaOKLKO9Vl7QBh0QTbA8Y0dF0KqVBG1TEStBiBG1rPYhmbsu/T+uqLXpwQIoy+oJ3B8P2lbYP71oAqFV8HjoEpVZL0uUu+O2AipVE7ffyOo/PGc3NOTwlcLUAqyIo5vBa+YndMSWcuE16jC69gZZWqvwvbAGm1oLRteqw2uVE3edXlGXmtojSa8Rh1Zq535lXyib1w3fLQgAcfxU+ZN8AzUK2Di9PnTttZOzLoUBtTkDIwQCq/T+VWzXjKFT8HDqQdbvb+56V/WEVrDRNSxOIrA7VfY+ueNR71W7x/vGTT+WU753alzzSefLO/ARFMuWN743dN6MOvjiPbmle3YfQgK/3gduYBuF30Nc6KXCjh21cpNGSyrU2s0SqWqQ6HcU3UioepBuFw957DKR9YeVM0YyhcQ6Xno/l/33Zm0/75noSK0Fpj+cgiZ5Qff3vQrbgutVEbmXcmWHTnafKqx+TR0vOUsPe7c03/25Zebcs08PdDtOTN/bt5qBaDdyyX9KRGfrBNwfILOXbwMIhkt+3Obuuzio+QjyigUK/U6/2p2SqFi7mFlSA0zq6T93b3/8a/o3A6Ra/2q2PcKNR9VaINrtB/m35m85+q88o4wWGkdu/yYuvKK6lqbWsvq0O2lq9fN4uNRe8ekmZHPdBpr4Ej/yA7X4WJ6FEcBx9TNGSq1RsWwLbdUm1s0ixrZ2CO6SL0dIll85+DDd/5552/f3p17WBUq1yI0E6ecV6V9L18xafeNv39zO6BSFVzDgODoBi6xUbf1pObMbWWHWvtYodqQnmsuHzA/nquF16JNz+BInNLyqCrgaIHe9AluOvHDDzcfrvme3XORg1L+xQE7/wqVT94Dn7z7vqWKD0o1nsXM3EpthB5HWKbPoXvTCh/OLOk85F2i8avCIW7ZcW73BW3m8TvLi3+8fLe9rL7ZNyL5mffmx5+udiGA52plFfX/a3m71TIcgYJBaTv27Nydh275DbuHcBRwROURkJD6VcP1l79hxxyCvXFFV3VrmpQfye4jKYQjhtWyobVwSp13CTNZpoRTTs27H1ilxqHQXw/NrmAyzrL55x/57z79h7XycZ/Wlp644hmSTN9I+t6gLumyZ+GqL9CMFnozbgaGLMPxiZ0jWXwalDVHm3sOR6Gynjg19LUvz/TLZp1yOPdd3KQiVnq6XXpOu7KZXdDIRR/hojDRWKOdVar4y77Wtw88mlWuDpZ3OuX8Bi7pGLf+e3bfWcWGhjvjNjQMX1zmFC8btKjk/RV735gRbcq9gTl18Tm6QCZCyy7UdiOORKDkiUQiZNscRzyNG7fvxgwAqc1JJMEGclzsNLH2wh+CxnjASA9kYmFl2k7yQOKQsZA42WKixzPwuXUnxVk6cRbnmM25ZGs90m/7VrBbz3AHL3Grmln/8g444rSCtqBqTbCcnV7OTC1lEKDXn2AP/luzUX5tSvrxkZ9955JUKoktdIgucIjOfz5w8xjvcBNxhLrsfOLgPm5XGBpgG3u6HUcI4Rtt6Iu0IY4EFKQNz6y90JKHZpD3tHMBKP14o2q0AY5YU3kh7dyInPa+2TpJFifOYPpuuTUoV/fHA9zcak56jtl2UrWgXr2kSZt9ns04xyU3cR8fY6XnNJuO3Jq8vXnU6pohyWVuK471iS3qNT9fEl80LKX8hfnZo73CTMQRg4iNLoGYf34HZ1kJRwhuTd8YW+FIU2JuLUiAhpXynwjc7QFHKMB9TUu/DOUL0g43qdYxkxFvviXK7DTLfjncmD0P1zZr5De4H9u4u0quTc2dvqervaqIL7z82ueNA5KKxbGFvaML+i09Koov7p9U6rG4Ah7pMS9tjMk4khqFv4BAxUPZp7VwpNPZCz9dtQmOiK00JQQUIhIx6OTblNkxnEt3YhfuCBwlmaxjFuuWrRqSrRi49ZajPnbDLAfteDA8V7Woifu5nVOznIbl7imY2dLvX1zVMDClWqyPzqJYmfvKY4MXVw1IKhHFdAbrof5pprsjGXd6v3FKR3un9XCkkwf89DyOeCDBIl35GUdktKGvFmehDU/6Af6wk+rEPnAcNyXAbU2L3g51INI5Q+X65c3BUo1LDmK3znnbA5dM1bsl3MWHHMNyrI77qbV9/Gdy15QaUWKFQ0xhv6RytyVyt5VNjguKCZ0I2UP8zMORfoIRXGi7MtxLq+KIxSH6/hk3mOwXz69uwRFhl54UM3FpALRRgYUacyom2BeOoiyuUxmMZPMtpyxm6NfqEbu1Lv944LhThai99kRnpL7boYna1+KRUua0qEqysNIj9Yj70jpJQqnL8mMOcUUAEbIMRzpJwv0geQ89RlbFER/Bf2Pu3HvAk4St+zKnW3DEU0EPiIkTAmDL3BFAJ/YSrH/3xcWBWSrnHJbgiNxRksXCGp0yGY+d910zOuCUI/Zy8Q1MzLc/jEqtHJBUOmBpw+CVjZIFpX1iZZDzskYHfSnjGF/UP7F4RMJ+s3JHuoYgY0rfFdwSq+JIZjfp0qrncQQNJp5FpzqILdQhymLtEkdSypx0z+wYnK1ylzIoZeCOxCwRvvtvuwccB0mZYbtY33LNnNxTr6xrGJBc3W9xQ9+Pq/vEyVDHkFJGHF/suqjMdWGpS0LRMH8zShme8ARrNGYU22hmVRxBHv+8Y4/hCG5MPAv7aebMhQwt7aiUEWeyfbNY9yzVi9LHbun3ASKJ3cgdJTuUOARSO18Lz780cqVcsqDEIbESuWPvmEKC43OrmoYvk/dPKEJZ3SemYIiZpYzBmbpUebBGurFVcKSeBx478Ytdxq9uwREPBl0ym0gSzuU3Tp6/3fa5o7uhlIEy1K5pVwZ/zTj/WsqId6hwyEWqgzvOzj35/OJyh1hZ74QKUeJhlDJ9E0rdl9QOWFoviisSxaCUAZ35luBIVbX0D3i1Eo6wYUwjW2FJxnIc6YoYiygmhnWwZe6noxM7cUeUMicNpYx4p0aSdr1/NoMA7S7Vum3vLGWwH9XM9DLNh9KWYcllyBEdEipcUuSuyVUI3E5JqGwqkUH+tpQJNRdHqnDhSeAsxxGv2RqEZTeyaMY/q9ztOOJzDddAC08dGQp6RhBX+8xpKZyFc2nO6DeV6AUbu8gdB6Uel2xXijL1pcxORpR2A7EbTokFw6Fbb7hltMMmjXGEKfZfUu+WUuO8sFwUJ0PIliSW47eexbzeUYeG+n0+xjPEAhzpIMWzcmjVF8xwU7uBRYuuAe7FkzyQpWryFGGb53T+t/LIgjXVie1xRLwO+v/oHOctN8Uoq7crgCNZlemkMO2a687Hw3KZgVLdLH0pM3JF9ZAVDShlUFaL42QOMZhoLBAnlCFG947Kcw7JfmlmCqxx7JRAy3AEdvxDY0Ucqdlm2+JIzM/iRUKzOkGKbCc4EgWM8Ynw+KTEaf158RetvzglUsmN1yTp7U5Z2uG7mHmV6pD9F0atPwoQRQurRXHFvefn9epUPrgUR+5/fvbaV70ixk4JIn1ahiOJL3QW3wM4whFRPFHJoi1xJJWK8YDwJDM8jxAyUTr0G0+b276ypj1yrHfYqDnLB684KtmhFmdo4ZHAUZzeIfmllFHPzj2FUgbBGqUM3pnorQ/NDpEHhvht+pN31GjPELA4bnIA/3Sasein2XADDIJXmdIV9Mw2tMgrRbiM7qWQvgbTRa6EHhM8KjhqbGOGP8EUL0eOSHogc2dkzdrw+NGJAbq1BY5UoT3WK+z3IVtcPz2PmtpxwxVRukKkzx1n6HNHD30p02vBYdQuDlEHBwZse3la4mteYbQp2r8E8aTsNnjfkUdjvUJGRuf0Sz0hSe/om/2bUkYc0znX2De24KWZi+GI9jCagki0tfg7pXTeAje1Hxxhk4FjPUNGT416JbXKp1jxdqF2boU6aO8pf2nz6NSS//Pf8Kp3JMpnNBNQsHMckRpiVjUpdYvpE644xU7/y6gJPmGZJU111zUVVzXHf35Yfeb63E82TfggguSIdi0BR6qyJt9tAIL4jfc4+ad47RFHfL1rasiitnaVQqPrUGnK64+/5Rv1hnewQEC3C16F8sIYI3BjnN6BOVKUYD+ZgEScNcYRdQnhDBOK6M2C2SIUN/b+/zuCyOojzUq1Bq92hiStnugTPMHTKtYoCEkbYY5sowomVJFXjcg2imtAg220RAMyC0ZwBM3YwCHynTizcMQMBvloe8cR8o9b8aDt0X5ZpVdAggCN9QQmQA/BC9jBEUkaB+YIXtjAfmMHBYLG7mj8wp6xd5KWROjBsE3muejCxa5xhMjfJsjag0zwgv9BXf5J+EOMQkQmQsiGnpg7EtSE/xvccgkia6TE22B1JEbjN/wMvkhwxDbgM5aAo7UkCCAiQOO3IXyTl80M9km/s8gfrC2XgKMgBGWEY0OFCxbBH6zOmDPDl/zBn7E7InzDSslZFk5lCzgKopfswZnx7I+xzwEylDjklQhDZY32JNfEHlImkx4EHO1XggQcBQk4ChIk4ChI0H8BhWw1i7jsHXMAAAAASUVORK5CYII='
                            });
                        }
                    },
                ]
            },

            /**
             * Method to start DataTable
             *
             * @param {type} obj
             * @param {type} data
             * @returns {undefined}
             */
            startTable: function (obj, data) {
                var me = this,
                    tableColReorder;
                me.makeTable();

                if (settings.summarize) {
                    me.renderSummarize(obj);
                }

                if (settings.search) {
                    me.renderSearchField(obj);
                }

                window.informata.dataTableInstance[obj] = $("#" + idTable).dataTable({
                    data: data,
                    columns: settings.columns,
                    colReorder: true,
                    stateSave: true,
                    stateDuration: 60 * 60 * 720,
                    orderCellsTop: true,
                    autoWidth: false,
                    pageLength: settings.qtdItens,
                    order: settings.order,
                    paging: settings.paginate !== undefined ? settings.paginate : true,
                    pagingType: "full_numbers",
                    dom: 'Bfrtip',
                    buttons: me.renderButtonsExport(),
                    language: {
                        lengthMenu: "Mostrar _MENU_",
                        zeroRecords: "Nenhum conteúdo encontrado!",
                        info: "Mostrando página _PAGE_ de _PAGES_",
                        infoEmpty: "Sem linhas",
                        infoFiltered: "(filtrado de _MAX_ total linhas)",
                        search: "Filtrar:",
                        paginate: {
                            first: "Primeiro",
                            last: "Último",
                            next: "Próximo",
                            previous: "Anterior"
                        },
                        loadingRecords: "Carregando...",
                        processing: "Processando..."
                    },
                    drawCallback: function () {

                        $(document).on('click', '.reset-grid', function (e) {
                            $('.drilldown-aberto').find('.btn-drilldown').trigger('click');
                        });

                        if (settings.drilldown) {

                            $(document).off('click', '.btn-drilldown');
                            $(document).on('click', '.btn-drilldown', function (e) {

                                var img = $(this);
                                var td = img.parent('td');
                                var tr = td.parent('tr');
                                var tabela = tr.parents('table');
                                var identificador = idTable + '-' + tabela.attr('id');

                                // Verifica se o drilldown está aberto ou fechado
                                if (tr.hasClass('drilldown-aberto')) {

                                    // Fecha drilldown
                                    img.attr("src", urlContext + "images/details_open.png");
                                    tr.removeClass('drilldown-aberto');
                                    tr.siblings('.tr-temporario').hide().remove();

                                } else {

                                    // Abre o drilldown
                                    img.attr("src", urlContext + "images/details_close.png");
                                    tr.addClass('drilldown-aberto');
                                    tr.siblings('.drilldown-aberto').find('.btn-drilldown').trigger('click');

                                    var objDataClick = img.data();
                                    var countRows = $('#' + tabela.attr('id') + ' thead tr th').length;
                                    objDataClick.close = false;

                                    td.each(function (key, value) {
                                        tr.after('<tr id="tr-temporario-' + identificador + '" class="tr-temporario" style="background-color: #FFFFFF"><td class="td-temporario-' + identificador + '" colspan="' + countRows + '"></td></tr>');
                                        $('.td-temporario-' + identificador).empty().html('Aguarde...');
                                        $('.td-temporario-' + identificador).load(objDataClick.url, '', function () {
                                            console.log('Tabela carregada...');
                                        });
                                    });
                                }
                            });
                        }
                    },
                    initComplete: function (settings, json) {
                        var table = this.api();
                        var larguraJanela = $('#' + idTable + '_wrapper').width() - 25;
                        $('#' + idTable + '_wrapper .dataTables_paginate').width(larguraJanela);
                        table.columns.adjust().draw();
                        //me.adjustColumns(idTable, obj);
                    },
                    stateSaveCallback: function (settings, callback) {
//                        $.ajax({
//                            url: BASE_URL + "configuracaotabela/salvartabela?ajax=true&idTable=" + obj,
//                            data: callback,
//                            dataType: "json",
//                            type: "POST"
//                        });                        
                    },
                    stateLoadCallback: function (settings, data) {
                        var table = this.api();

//                        $.ajax({
//                            url: BASE_URL + "configuracaotabela?ajax=true&idTable=" + obj,
//                            dataType: "text json",
//                            success: function (response) {
//                                if (response.data) {
//                                    var colunas = table.columns();
//                                    var quantidadeColunas = colunas[0].length;
//
//                                    //Recarregando a visibilidade da coluna
//                                    if (response.data.length > 0) {
//                                        for (var i = 0; i < quantidadeColunas; i++) {
//                                            var visibilidade = (response.data.columns[i].visible == 'true') ? true : false;
//                                            table.column(i).visible(visibilidade);
//                                        }
//
//                                        table.state.save();
//
//                                        var orderCols = response.data.ColReorder;
//                                        // THE MAGIC!!!! ///////////////////////////
//                                        tableColReorder = new $.fn.dataTable.ColReorder(window.informata.dataTableInstance[obj]);
//                                        tableColReorder.fnOrder(orderCols);
//                                        ////////////////////////////////////////////
//
//                                        // Recarregando a ordem
//                                        table.order(response.data.order).draw();
//                                    }
//                                }
//                            }
//                        });
                    },
                    headerCallback: function (nHead, aData, iStart, iEnd, aiDisplay) {
                        var table = this.api();
                        var orderCol = sessionStorage.getItem(obj + "-order") === null ? table.columns()[0] : sessionStorage.getItem(obj + "-order").split(',').map(Number);
                        me.setOrder(obj + "-order", orderCol);
                        // CUSTOM HEADER ///////////////////////////////////////
                        var colunas = table.columns();
                        var quantidadeColunas = colunas[0].length;

                        for (var i = 0; i < quantidadeColunas; i++) {
                            var th = $(table.column(i).header());
                            me.renderHeader(table, obj, th.text(), i);
                        }

                        if (settings.summarize) {
                            me.summarizeColumn(table, obj);
                        }
                        me.renderFieldColumn(table, obj);
                        me.searchColumn(table, obj);
                        ////////////////////////////////////////////////////////
                    }
                });

                new $.fn.dataTable.FixedColumns(window.informata.dataTableInstance[obj], {
                    leftColumns: 1
                });

            },
            /**
             * Method return list of instance
             *
             * @returns {Array}
             */
            instance: function () {
                return window.informata.dataTableInstance;
            },
            /**
             * Method initialize custom DataTable
             *
             * @returns {undefined}
             */
            init: function () {
                var me = this;
                $.ajax({
                    url: settings.url,
                    type: "GET",
                    async: true,
                    dataType: "text json",
                    beforeSend: function () {
                        console.log("LOADING...");
                    },
                    error: function (txt) {
                        console.log("ERROR: ", txt);
                    },
                    success: function (response, status, xhr) {
                        var newInstance = informata.toCamelCase(idTable);
                        //                        if (window.informata.dataTableInstance[newInstance]) {
                        //                            var tableObj = window.informata.dataTableInstance[newInstance].api().settings();
                        //                            var colReorder = new $.fn.dataTable.ColReorder(window.informata.dataTableInstance[newInstance]);
                        //                            tableObj.clear();
                        //                            tableObj.rows.add(response.data);
                        //                            tableObj.draw();
                        //                        } else {
                        me.startTable(newInstance, response.data);
                        //                        }
                    }
                });
            },
            initPage: function () {
                var obj = informata.toCamelCase(idTable);
                var me = this,
                    tableColReorder;
                me.makeTable();
                if (settings.summarize) {
                    me.renderSummarize(obj);
                }
                if (settings.search) {
                    me.renderSearchField(obj);
                }

                window.informata.dataTableInstance[obj] = $("#" + idTable).dataTable({
                    columns: settings.columns,
                    colReorder: true,
                    stateSave: true,
                    stateDuration: 60 * 60 * 720,
                    orderCellsTop: true,
                    autoWidth: false,
                    pageLength: settings.qtdItens,
                    order: settings.order,
                    paging: settings.paginate !== undefined ? settings.paginate : true,
                    pagingType: "full_numbers",
                    dom: 'Bfrtip',
                    ajax: settings.ajax !== undefined ? settings.ajax : false,
                    processing: settings.processing !== undefined ? settings.processing : false,
                    serverSide: settings.serverSide !== undefined ? settings.serverSide : false,
                    buttons: me.renderButtonsExport(),
                    language: {
                        lengthMenu: "Mostrar _MENU_",
                        zeroRecords: "Nenhum conteúdo encontrado!",
                        info: "Mostrando página _PAGE_ de _PAGES_",
                        infoEmpty: "Sem linhas",
                        infoFiltered: "(filtrado de _MAX_ total linhas)",
                        search: "Filtrar:",
                        paginate: {
                            first: "Primeiro",
                            last: "Último",
                            next: "Próximo",
                            previous: "Anterior"
                        },
                        loadingRecords: "Carregando...",
                        processing: "Processando..."
                    },
                    drawCallback: function () {

                        $(document).on('click', '.reset-grid', function (e) {
                            $('.drilldown-aberto').find('.btn-drilldown').trigger('click');
                        });

                        if (settings.drilldown) {

                            $(document).off('click', '.btn-drilldown');
                            $(document).on('click', '.btn-drilldown', function (e) {

                                var img = $(this);
                                var td = img.parent('td');
                                var tr = td.parent('tr');
                                var tabela = tr.parents('table');
                                var identificador = idTable + '-' + tabela.attr('id');

                                // Verifica se o drilldown está aberto ou fechado
                                if (tr.hasClass('drilldown-aberto')) {

                                    // Fecha drilldown
                                    img.attr("src", urlContext + "images/details_open.png");
                                    tr.removeClass('drilldown-aberto');
                                    tr.siblings('.tr-temporario').hide().remove();

                                } else {

                                    // Abre o drilldown
                                    img.attr("src", urlContext + "images/details_close.png");
                                    tr.addClass('drilldown-aberto');
                                    tr.siblings('.drilldown-aberto').find('.btn-drilldown').trigger('click');

                                    var objDataClick = img.data();
                                    var countRows = $('#' + tabela.attr('id') + ' thead tr th').length;
                                    objDataClick.close = false;

                                    td.each(function (key, value) {
                                        tr.after('<tr id="tr-temporario-' + identificador + '" class="tr-temporario" style="background-color: #FFFFFF"><td class="td-temporario-' + identificador + '" colspan="' + countRows + '"></td></tr>');
                                        $('.td-temporario-' + identificador).empty().html('Aguarde...');
                                        $('.td-temporario-' + identificador).load(objDataClick.url, '', function () {
                                            console.log('Tabela carregada...');
                                        });
                                    });
                                }
                            });
                        }
                    },
                    initComplete: function (settings, json) {
                        var table = this.api();
                        var larguraJanela = $('#' + idTable + '_wrapper').width() - 25;
                        $('#' + idTable + '_wrapper .dataTables_paginate').width(larguraJanela);
                        table.columns.adjust().draw();
                        //me.adjustColumns(idTable, obj);
                    },
                    stateSaveCallback: function (settings, callback) {
//                        $.ajax({
//                            url: BASE_URL + "configuracaotabela/salvartabela?ajax=true&idTable=" + obj,
//                            data: callback,
//                            dataType: "json",
//                            type: "POST"
//                        });
                    },
                    stateLoadCallback: function (settings, data) {
                        var table = this.api();

//                        $.ajax({
//                            url: BASE_URL + "configuracaotabela?ajax=true&idTable=" + obj,
//                            dataType: "text json",
//                            success: function (response) {
//
//                                if (response.data) {
//                                    var colunas = table.columns();
//                                    var quantidadeColunas = colunas[0].length;
//
//                                    //Recarregando a visibilidade da coluna
//                                    if (response.data.length > 0) {
//                                        for (var i = 0; i < quantidadeColunas; i++) {
//                                            var visibilidade = (response.data.columns[i].visible == 'true') ? true : false;
//                                            table.column(i).visible(visibilidade);
//                                        }
//
//                                        table.state.save();
//
//                                        var orderCols = response.data.ColReorder;
//                                        // THE MAGIC!!!! ///////////////////////////
//                                        tableColReorder = new $.fn.dataTable.ColReorder(window.informata.dataTableInstance[obj]);
//                                        tableColReorder.fnOrder(orderCols);
//                                        ////////////////////////////////////////////
//
//                                        // Recarregando a ordem
//                                        table.order(response.data.order).draw();
//                                    }
//                                }
//                            }
//                        });
                    },
                    headerCallback: function (nHead, aData, iStart, iEnd, aiDisplay) {
                        var table = this.api();
                        var json = table.ajax.json();
                        var orderCol = sessionStorage.getItem(obj + "-order") === null ? table.columns()[0] : sessionStorage.getItem(obj + "-order").split(',').map(Number);
                        me.setOrder(obj + "-order", orderCol);
                        // CUSTOM HEADER ///////////////////////////////////////
                        var colunas = table.columns();
                        var quantidadeColunas = colunas[0].length;

                        for (var i = 0; i < quantidadeColunas; i++) {
                            var th = $(table.column(i).header());
                            me.renderHeaderPage(table, obj, th.text(), i);
                        }

                        if (settings.summarize) {
                            me.summarizeColumnPage(table, obj, json);
                        }
                        me.renderFieldColumn(table, obj);
                        me.searchColumn(table, obj);
                        ////////////////////////////////////////////////////////
                    }
                });

                new $.fn.dataTable.FixedColumns(window.informata.dataTableInstance[obj], {
                    leftColumns: 1
                });

            },
            renderHeaderPage: function (instanceTable, instanceName, labelText, i) {

                var me = this;
                var objColumn = $(instanceTable.column(i).header());
                var contentButtonSearch = [];
                var contentButtonConfig = [];
                var contentBasic = [
                    '<div id="', instanceName, '-header-', i, '">', labelText, '</div>'
                ];
                objColumn.empty().html(contentBasic.join(""));
                contentButtonSearch = me.renderButtonSearch(instanceName + "-search-" + i);

                if (settings.config) {
                    contentButtonConfig = me.renderButtonConfig(instanceName + "-config-" + i);
                }

                if (!settings.drilldown) {

                    if (settings.columns[i].search) {
                        $("#" + instanceName + "-header-" + i + "").append(contentButtonSearch.join(""));
                    }

                    if (i === 0) {
                        $("#" + instanceName + "-header-" + i + "").append(contentButtonConfig.join(""));
                    }

                } else {
                    if (settings.drilldown && i !== 0) {
                        if (settings.columns[i].search) {
                            $("#" + instanceName + "-header-" + i + "").append(contentButtonSearch.join(""));
                        }
                    } else {
                        $("#" + instanceName + "-header-" + i + "").append(contentButtonConfig.join(""));
                    }
                }

                if (settings.columns[i].search) {

                    $("#" + instanceName + '-header-' + i).addClass('header-search');

                    $("#" + instanceName + "-search-" + i).on("click", function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        $("." + idTable + "-tempRmv").remove();
                        me.openRowSearchPage(instanceTable, instanceName, labelText, i);
                    });
                }

                $("#" + instanceName + "-config-" + i).on("click", function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $("." + idTable + "-tempRmv").remove();
                    me.openConfig();
                });
            },
            objetsTable: new Object(),
            objectColumns: new Array(),
            openRowSearchPage: function (instanceTable, instanceName, labelText, i) {
                var me = this;
                $("#row-search-" + instanceName).show();
                var instanceActual = "",
                    keyTMP = {};
                jQuery.each(settings.columns, function (x, c) {
                    if (c.name === labelText) {
                        keyTMP.colName = c.name;
                        keyTMP.colTarget = c.targets;
                    }
                });
                $("#" + instanceName + "-field-search-" + keyTMP.colTarget).focus();
                $("." + instanceName + "-field-search").off().on("keyup", function (e) {
                    //if( e.which == 13  ){
                    $("." + idTable + "-tempRmv").remove();
                    instanceActual = $(this).data().instance;
                    var result = ""
                    $("." + instanceName + "-field-search").each(function (key, obj) {
                        me.objectColumns[parseInt($(obj).data('column'))] = $(obj).val();
                    });
                    me.objetsTable.instanceName = me.objectColumns;
                    result = JSON.stringify(me.objetsTable.instanceName);
                    window.informata.dataTableInstance[instanceActual].api().search(result).draw();
                    //window.informata.dataTableInstance[instanceActual].api().column($(this).parent().index() + ':visible').search(this.value).draw();
                    //$("#" + instanceName + "-header-" + i).append("<i style=\"float:right\" class=\"fa fa-filter\" aria-hidden=\"true\"></i>");
                    var code = e.keyCode || e.which;
                    if (code === 27 || code === 13) {
                        $("#row-search-" + instanceActual).hide();
                    }
                    //}
                });

            },
            summarizeColumnPage: function (instanceTable, instanceName, json) {
                var me = this;
                var orderCol = me.getOrder(instanceName + "-order");
                var keyTMP = {};
                jQuery.each(settings.columns, function (i, c) {
                    if (c.summarize) {
                        keyTMP.colName = c.name;
                        keyTMP.colTarget = c.targets;
                        keyTMP.colFormat = c.format;
                        keyTMP.data = c.data;
                        //console.log('#'+instanceName+'-summarize-'+c.data);
                        var data = 0;
                        if (keyTMP.colFormat == "0") {
                            data = window.informata.numberFormat(json.totaLizadores[c.data], 0, "", ".")
                        } else {
                            data = window.informata.numberFormat(json.totaLizadores[c.data], 2, ",", ".")
                        }
                        $('#' + instanceName + '-summarize-' + c.data).html(data);
                    }

                    /*jQuery.each(orderCol, function (key, pos) {
                        if (settings.columns[pos].name === keyTMP.colName) {
                            keyTMP.colSum = instanceTable.column(key, {
                                search: 'applied'
                            }).data().sum();
                            keyTMP.colPosActual = key;

                            var data = 0;

                            if (keyTMP.colFormat == "0") {
                                data = window.informata.numberFormat(keyTMP.colSum, 0, "", ".")
                            } else {
                                data = window.informata.numberFormat(keyTMP.colSum, 2, ",", ".")
                            }

                            $("#" + instanceName + "-summarize-" + keyTMP.colTarget).empty().html(data);
                        }
                    });*/
                });
            }

        };
    }

    // PLUGINS FROM DATATABLE //////////////////////////////////////////////////
    jQuery.fn.dataTable.Api.register('sum()', function () {
        return this.flatten().reduce(function (a, b) {
            if (typeof a === 'string') {
                a = a.replace(/[^\d.-]/g, '') * 1;
            }
            if (typeof b === 'string') {
                b = b.replace(/[^\d.-]/g, '') * 1;
            }
            return a + b;
        }, 0);
    });

})(informata);
