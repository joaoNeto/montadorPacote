/**
 * Arquivo que conterá os detalhamantos multinível via ajax
 * 
 * @author Alberto Medeiros
 */
var data;
var oTable = [];
var qtdRegistrosPagina = [];
arrColunas = [];
var corrigirPaginacao;
var semaforo = true;
var semaforoDetalhe = true;
var arrObjTable = []; // terá os objetos e as tabelas para serem configuradas
$(document).ready(function() {
    if ($("#tabela-detalhe-multinivel").size() > 0)
        iniciarDetalhesAjaxMultinivel("tabela-detalhe-multinivel", {});
});

var BASE_URL = "http://localhost:8080/viewmonitor/";


/**
 * Método que irá configurar a largura das colunas
 * @param {string} idElemento
 * @returns {void}
 */

function ajustarLarguraDasColunas(idElemento) {
    $("#" + idElemento + ' thead tr.filtroTopo td').each(function(i) {
        //var index = i;
        $(this).css({
            "min-width": function() {
                return $(this).closest('.dataTables_scroll').find('.dataTables_scrollHeadInner table thead tr.filtroTopo td').eq(i).css('width');
            },
            "max-width": function() {
                return $(this).closest('.dataTables_scroll').find('.dataTables_scrollHeadInner table thead tr.filtroTopo td').eq(i).css('width');
            }
        });
    });
}

/**
 * Método que irá adicionar a linha de detalhe na table
 * 
 * @param {DataTable} oTable
 * @param {object} nTr
 * @returns {void}
 */
function fnFormatDetailsAjaxMultinivel(oTable, nTr, idElemento) {
    var objTual = nTr;
    var idTableFilho = $(objTual).find("img." + idElemento).attr("id");
    var strUrl = $(objTual).find("img." + idElemento).attr("rel-url");
    var sOut = "";
    var tamanhoPai;


    if ($(".panel-body .adv-table").length == 1) {
        tamanhoPai = $('#' + idElemento).width();
    } else {
        tamanhoPai = $('#' + idElemento).parent().width();
    }


    //altura minima da tabela interna
    var indiceAlturaPorLinha = 28.2;
    var alturaMinima = indiceAlturaPorLinha * qtdRegistrosPagina[idElemento];


    $("." + idTableFilho).load(BASE_URL + strUrl, { idTableFilho: idTableFilho },
        function() {
            //var tamanhoPaiDepois = parseInt($('#' + idElemento).css("width").replace("px", ""));
            //tamanhoPai = (tamanhoPaiDepois < tamanhoPai) ? tamanhoPai : tamanhoPaiDepois;

            // Recuperando o html carregado
            sOut = $("." + idTableFilho).html();
            // Caso não tenha detalhamento
            if (sOut == undefined || sOut == "")
                sOut = "<h4 style='color: #767676'>Não existe detalhamento!</h4>";
            else {
                $("." + idTableFilho).html("");
                // Setando na próxima tr td.details
                $(objTual).next().find(".details").html(sOut);
                // setando o tamanho correto
                $(objTual).next().find(".adv-table").attr("style", "min-height: " + alturaMinima + "px; overflow: auto; width: " + (parseInt(tamanhoPai) - 28) + "px");

                paginacao = false;
                dataSet = null;
                deferRender = false;
                configuracao = { paginacao: false, butons: false, dataSet: dataSet, deferRender: deferRender, scroller: false };
                if (typeof getConfiguracao == "function") {
                    configuracao = getConfiguracao();
                }
                if (typeof getDadaSet == "function") {
                    dataSet = getDadaSet();
                    if ($(dataSet).size() > 2000)
                        deferRender = true;
                    configuracao.dataSet = dataSet;
                    configuracao.deferRender = deferRender;
                }
                // Iniciando a tabela filha
                iniciarDetalhesAjaxMultinivel($(objTual).next().find(".adv-table").find("table").attr("id"), configuracao);

                $(".panel-body > .adv-table .adv-table").each(function(i) {
                    // 3º nível do drilldown ou superior aberto
                    if (i > 0) {
                        $(objTual).next().find(".adv-table").addClass('sub-drilldown');
                        $(objTual).closest(".adv-table").addClass('drilldown');
                    }
                });



                var larguraCabecalho = $("#" + idElemento).parent().prev().find(".dataTables_scrollHeadInner").width();
                $("#" + idElemento).attr("style", "width:" + larguraCabecalho + "px");


                ajustarLarguraDasColunas(idElemento);

            }
            // focando no elemento atual
            focusElemento(objTual);
            return sOut;
        }
    );
}


/**
 * Método que irá iniciar a tabela
 * @param {string} idElemento
 * @param {object} configuracao
 * @returns {void}
 */
function iniciarDetalhesAjaxMultinivel(idElemento, configuracao) {

    var dataSet = new Array();
    htmlCabecalho = $('#' + idElemento + ' thead tr').html();
    // Configuração da tabela
    var ajax = (configuracao.ajax !== undefined) ? configuracao.ajax : null;
    var paginacaoTipo = (configuracao.paginacaoTipo != undefined) ? configuracao.paginacaoTipo : "full_numbers";
    var paginacao = (configuracao.paginacao != undefined) ? configuracao.paginacao : true;
    var buton = (configuracao.butons != undefined) ? configuracao.butons : false;
    //    dataSet = (configuracao.dataSet != undefined) ? configuracao.dataSet : false;
    var autoWidth = (configuracao.autoWidth != undefined) ? configuracao.autoWidth : false;
    var deferRender = (configuracao.deferRender != undefined) ? configuracao.deferRender : true;
    var scrollY = (configuracao.scrollY != undefined) ? configuracao.scrollY : false;
    var scrollX = (configuracao.scrollX != undefined) ? configuracao.scrollX : true;
    var scroller = (configuracao.scroller != undefined) ? configuracao.scroller : false;
    var scrollCollapse = (configuracao.scrollCollapse != undefined) ? configuracao.scrollCollapse : false;
    var info = (configuracao.info != undefined) ? configuracao.info : false;
    var leftColumns = (configuracao.fixedColumns != undefined) ? configuracao.fixedColumns : false;
    var ordenacao = (configuracao.ordenacao != undefined) ? configuracao.ordenacao : [{ orderable: false, targets: 0 }];
    var colunas = (configuracao.colunas != undefined) ? configuracao.colunas : null;
    var colunasTotal = (configuracao.colunasTotal != undefined) ? configuracao.colunasTotal : null;
    var filter = (configuracao.filtro != undefined) ? configuracao.filtro : true;
    var salvarFiltro = (configuracao.salvarFiltro != undefined) ? configuracao.salvarFiltro : true;
    var aaSortingFixed = (configuracao.aaSortingFixed != undefined) ? configuracao.aaSortingFixed : true;
    var esconderColunas = (configuracao.esconderColunas != undefined) ? configuracao.esconderColunas : true;
    var bolMostrarTabela = (configuracao.bolMostrarTabela != undefined) ? configuracao.bolMostrarTabela : true;
    qtdRegistrosPagina[idElemento] = (configuracao.qtdRegistrosPagina != undefined) ? configuracao.qtdRegistrosPagina : 50;
    corrigirPaginacao = (configuracao.corrigirPaginacao != undefined) ? configuracao.corrigirPaginacao : false;
    var orderPri = (configuracao.orderPrincipal != undefined) ? configuracao.orderPrincipal : [
        [2, "asc"]
    ];
    var title = (configuracao.title != undefined) ? configuracao.title : "";
    var callbackDblClick = (configuracao.callbackDblClick != undefined) ? configuracao.callbackDblClick : undefined;
    var cacheCheck = _.isEqual(configuracao.cache, true) ? true : false;

    // Criando array 
    if (configuracao.dataSet != undefined) {
        $(configuracao.dataSet).each(function(i, arr) {
            var arrVarlo = new Array();
            $(arr).each(function(j, valor) {
                arrVarlo[j] = valor;
            });
            dataSet[i] = arrVarlo;
        });
    } else if (dataSet.length == 0) {
        dataSet = false;
    }

    // Salvando as configurações e html da tabela
    arrObjTable[idElemento] = { strHtml: $('#' + idElemento).parent().html(), configuracao: configuracao };
    // Criando os botões
    butons = [{
            extend: 'excel',
            text: 'Excel',
            title: "iMD-Log",
            className: idElemento + '-excel'
        },
        {
            extend: 'print',
            text: '',
            title: "iMD-Log",
            className: idElemento + '-pdf'
        }
    ];
    //  Caso seja filtro
    if (filter) {
        strHtml = "<tr class='filtroTopo'>";
        $('#' + idElemento + ' thead th').each(function(i, value) {
            estiloANterior = "";
            if (!$(this).hasClass("nao-filtra")) {
                primeiro = (i == 0) ? 'configurar-filtro-coluna' : '';
                classeFiltro = "";
                if (colunas != null)
                    classeFiltro = (colunas[i] != undefined) ? colunas[i].className : "";
                strHtml += '<td class="' + classeFiltro + '"><i title="Clique para realizar a busca" class="fa fa-search btFiltrarColuna ' + primeiro + '"></i><input style="width: 75%" coluna=' + i + ' class="campoFiltrar" type="text" placeholder="' + $(this).text() + '" ></td>';
            } else
                strHtml += '<td></td>';
        });
        strHtml += "</tr>";
        $('#' + idElemento + ' thead').prepend(strHtml);
    }
    // Criando a tabela
    oTable[idElemento] = $('#' + idElemento).DataTable({
        ajax: ajax,
        data: dataSet,
        cache: cacheCheck,
        dom: 'Bfrtip',
        buttons: butons,
        deferRender: deferRender,
        pagingType: paginacaoTipo,
        paging: paginacao,
        scrollX: scrollX,
        scrollY: scrollY,
        scrollCollapse: scrollCollapse,
        info: info,
        columns: colunas,
        columnDefs: ordenacao,
        "autoWidth": true,
        iDisplayLength: qtdRegistrosPagina[idElemento],
        stateSave: true,
        stateDuration: 60 * 60 * 720,
        destroy: true,
        aaSortingFixed: aaSortingFixed,
        order: orderPri,
        fixedColumns: {
            leftColumns: leftColumns
        },
        language: {
            "lengthMenu": "Mostrar _MENU_",
            "zeroRecords": "Nenhum conteúdo encontrado!",
            "info": "Mostrando págína _PAGE_ de _PAGES_",
            "infoEmpty": "Sem linhas",
            "infoFiltered": "(filtrado de _MAX_ total linhas)",
            "search": "Filtrar:",
            "zeroRecords": "Nenhuma Linha encontrada",
            "paginate": {
                "first": "Primeiro",
                "last": "Último",
                "next": "Próximo",
                "previous": "Anterior"
            },
            "loadingRecords": "Carregando...",
            "processing": "Processando...",
        },
        stateLoadParams: function(settings, data) {
            // Caso tenha colunas salvas
            if (data.columns != undefined) {
                // Caso seja filtro
                if (filter) {
                    atualizarFiltroDetalhesAjaxMultinivel(data, "", salvarFiltro, idElemento);
                }
            }
        },
        colReorder: {
            reorderCallback: function() {
                // Caso seja filtro
                if (filter) {
                    refatorarFiltroDetalhesAjaxMultinivel(oTable, idElemento);
                }
                $("#" + idElemento).find("tfoot.search-detalhe").hide(200);
            }
        },
//        "stateSaveCallback": function(settings, data) {
//            ajustarLarguraDasColunas(idElemento);
//            if (semaforo) {
//                // Caso não seja pra salvar filtro, limpo os campos de pesquisa antes de salvar
//                if (!salvarFiltro) {
//                    $(data.columns).each(function(i, val) {
//                        data.columns[i].search.search = "";
//                    });
//
//                }
//                semaforo = false;
//                // Send an Ajax request to the server with the state object
//                $.ajax({
//                    "url": BASE_URL + "configuracaotabela/salvartabela?ajax=true&idTable=" + idElemento,
//                    "data": data,
//                    "async": true,
//                    "dataType": "json",
//                    "type": "POST",
//                    "success": function(json) {
//                        semaforo = true;
//                    }
//                });
//            }
//        },
//        "stateLoadCallback": function(settings) {
//            $.ajax({
//                "url": BASE_URL + "configuracaotabela?ajax=true&idTable=" + idElemento,
//                "async": false,
//                "dataType": "json",
//                "success": function(json) {
//                    setTimeout(function() {
//                        if (data != null && data != undefined) {
//                            $(data.ColReorder).each(function(i, object) {
//                                visible = (data.columns[object].visible == "true") ? true : false;
//                                column = oTable[idElemento].column(i);
//                                column.visible(visible);
//                            });
//                        }
//                    }, 100);
//                    data = json.data;
//                }
//            });
//
//            ajustarLarguraDasColunas(idElemento);
//            return data;
//        },
        "headerCallback": function(row, data, start, end, display) {
            $("td.detalhe-table").remove();
            $('#' + idElemento + " tbody tr td").each(function() {
                if ($(this).find("img").attr("data-status") != undefined && $(this).find("img").attr("data-status") == "aberto") {
                    $(this).find("img").attr("src", BASE_URL + "images/details_open.png");
                    $(this).find("img").attr("data-status", "fechado");
                    $('#' + idElemento + ' tbody tr').show();
                    $('#' + idElemento + '_paginate').show();
                }
            });
        },
        "footerCallback": function(row, data, start, end, display) {
            var api = this.api(),
                data;
            if (colunasTotal != null) {
                $(colunasTotal).each(function(e, obj) {
                    if ((obj.totalizar != undefined) && obj.totalizar) {
                        // Caso seja o mesmo nome
                        pageTotalSemFiltro = api
                            .column(e)
                            .data()
                            .reduce(function(a, b) {
                                if (obj.format == "00:00") {
                                    return somarHora(a, b)
                                }
                                return (intVal(a) + intVal(b));
                            }, 0);
                        pageTotal = api
                            .column(e, { search: 'applied' })
                            .data()
                            .reduce(function(a, b) {
                                if (obj.format == "00:00") {
                                    return somarHora(a, b)
                                }
                                return (intVal(a) + intVal(b));
                            }, 0);
                        // Se for 
                        if (obj.format == "00:00") {
                            pageTotal = compararHora(pageTotalSemFiltro, pageTotal) ? pageTotal : pageTotalSemFiltro;
                        } else {
                            pageTotal = isNaN(pageTotal) ? 0 : pageTotal;
                            pageTotalSemFiltro = isNaN(pageTotalSemFiltro) ? 0 : pageTotalSemFiltro;
                            pageTotal = ((pageTotalSemFiltro > pageTotal) && (pageTotal > 0)) ? pageTotal : pageTotalSemFiltro;
                        }

                        // Update footer
                        $(api.column(e).header()).html(
                            "<div class='linhaCabecalho'>" + obj.name + "</div>" +
                            "<div class='linhaTotalizadora'>" + formatarNumero(pageTotal, ((obj.format != undefined) ? obj.format : "")) + "</div>"
                        );
                    } else {
                        intPosicao = e;
                        $(api.column(e).header()).html(
                            "<div class='linhaCabecalho'>" + obj.name + "</div>" +
                            "<div class='linhaTotalizadora'></div>"
                        );
                        //                        $("#" + idElemento + "_wrapper .dataTables_scrollHead table thead th:nth").();
                    }
                });
            }
            if ($(api.column(0).header()).find('.configColunas').size() == 0 && bolMostrarTabela == true) {
                float = ($(api.column(0).header()).text() == '') ? '' : 'style="float: left"';
                btConfigColuna = '<a title="Configurar colunas da tabela" ' + float + ' href="javascript:void(0)"><i class="fa fa-cog configColunas" rel="popover"></i></a><div id="menu-configuracao" style="display: none"><a href="#"><i class="fa fa-table configColunasJanela" aria-hidden="true"></i></a><a href="#"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a><a href="#"><i class="fa fa-print" aria-hidden="true"></i></a></div>';
                if ($(api.column(0).header()).find('.linhaCabecalho').length != 0) {
                    $(api.column(0).header()).addClass('configurar-coluna').find('.linhaCabecalho').prepend(btConfigColuna);
                } else {
                    $(api.column(0).header()).addClass('configurar-coluna').prepend(btConfigColuna);
                }

            }

            $('.configColunas').popover({
                html: true,
                content: function() {
                    return $('#menu-configuracao').html();
                }
            });


        },
    });

    setTimeout(function() {
        objInput = $("#" + idElemento + " .com-filtro").parent().find(".input")
        var that = oTable[idElemento].column($(objInput).attr("coluna"));
        pesquisatDadoTabela(that, $(objInput));
    }, 1500);

    centralizaPaginacao(idElemento);

    if (corrigirPaginacao) {
        $(".dataTables_length#" + idElemento + "_length").find("select").val(qtdRegistrosPagina[idElemento]).trigger("change");
    }

    ajustarLarguraDasColunas(idElemento);

    /**
     * Focus out dos campos de pesquisa
     */
    $(document).delegate(".campoFiltrar", "blur", function(e) {
        if ($(this).val() == "")
            $(this).parent().find("i").removeClass("com-filtro");
        else
            $(this).parent().find("i").addClass("com-filtro");

        $(this).hide("slow");
    });

    /**
     * Mostrar evento de clique que mostrará o campo de busca
     */
    $(document).delegate(".btFiltrarColuna", "click", function(e) {
        $(this).parent().find("input").show("slow").trigger("focus");
    });

    $(document).delegate(".resetarOrder", "click", function(e) {
        oTable.state.clear();
        window.location.reload();
    });

    if (esconderColunas) {
        arrColunas[idElemento] = [];
        arrColunasElemento = [];
        $("#" + idElemento + "_wrapper .dataTables_scrollHead table thead th").each(function(i, n) {
            if ($(this).text() != "" && !$(this).hasClass("coluna-fixa")) {
                coluna = (i == 0) ? 0 : i - 1;
                texto = ($(this).find(".linhaCabecalho").size() > 0) ? $(this).find(".linhaCabecalho").text() : $(this).text();
                arrColunasElemento[coluna] = texto.replace(/<\/?[^>]+(>|$)/g, "").trim();
            }
        });
        arrColunas[idElemento] = arrColunasElemento;
        $("#" + idElemento + "_wrapper .dataTables_scrollHead").on('click', ".configColunasJanela", function() {
            msgTratamentoColunas(montarHtml(idElemento, oTable), oTable[idElemento], idElemento);
            reordenarColunaAutomaticamente(idElemento);
        });
    }

    if (title != "") {
        $("#" + idElemento + " tbody").attr("title", title);
    }

    // Caso seja filtro
    if (filter) {
        // Apply the search
        $("#" + idElemento + "_wrapper tr.filtroTopo td input[type='text']").on('keyup', function() {
            pesquisatDadoTabela(idElemento, $(this));
        });
    }

    // Amplia o datagrid
    $(document).on("click", "#btnExpandirFechar.abrir", function() {
        $(this).find("#imgBtnExpandirFechar").removeClass("fa-expand").addClass("fa-compress");
        $(this).removeClass("abrir").addClass("fechar");


        $(".grafico").slideUp(500);
        $("#" + idElemento + "_length select").val(100).trigger("change");

        reordenarColunaAutomaticamente(idElemento);
    });

    // Diminuir datagrid
    $(document).on("click", "#btnExpandirFechar.fechar", function() {
        $(this).find("#imgBtnExpandirFechar").removeClass("fa-compress").addClass("fa-expand");
        $(this).removeClass("fechar").addClass("abrir");

        $(".grafico").slideDown(500);
        $("#" + idElemento + "_length select").val(50).trigger("change");

        reordenarColunaAutomaticamente(idElemento);
    });

    /**
     * Destruindo o click pra evitar erro
     */
    $(document).off('click', 'img.' + idElemento);

    /**
     * Dois clickes na linha
     */
    $(document).on('dblclick', '#' + idElemento + ' > tbody tr', function() {
        $(this).find('img.' + idElemento).trigger("click");
    });

    /**
     * Evento de click no detalhe modal
     */
    $(document).on('click.' + idElemento, 'img.' + idElemento, function() {
        var nTr = $(this).parents('tr')[0];

        if ($(this).attr("data-status") != undefined && $(this).attr("data-status") == "aberto") {
            this.src = BASE_URL + "images/details_open.png";
            var objTual = nTr;
            var idTableFilho = $(objTual).attr("id");

            $(objTual).next().remove();
            $('#' + idElemento + ' tbody tr').not(nTr).show();
            $('#' + idElemento + '_paginate').show();
            $('#' + idElemento + '_wrapper .dataTables_scrollFoot').show();

            $('#' + idElemento + '_wrapper .dataTables_scrollHeadInner thead th:not(.sorting)').trigger("click").trigger("click");

            $(this).attr("data-status", "fechado");
        } else {
            $('#' + idElemento + ' tbody tr').not(nTr).hide();
            $('#' + idElemento + '_paginate').hide();
            $('#' + idElemento + '_wrapper .dataTables_scrollFoot').hide();

            $(this).attr("data-status", "aberto");
            /* Open this row */
            this.src = BASE_URL + "images/details_close.png";

            html = fnFormatDetailsAjaxMultinivel(oTable, nTr, idElemento);
            // Total de colunas
            colunas = $('#' + idElemento + ' thead tr th').length;
            html = "<tr class='detalhe-table' title=''><td colspan='" + colunas + "' class='details'>" + html + "</td></tr>";
            $(html).insertAfter(nTr);

            var objClick = $(this);
            var objTual = nTr;
            var idTableFilho = $(objTual).attr("id");
            $(objTual).next().find(".details").html('<h4 class="carregando"><span>Carregando... </span><img src="' + BASE_URL + 'images/aguarde.gif"> </h4>');

            //$(objTual).next().find(".details").attr("style", "max-width: 80% !important");
            focusElemento(objTual);
        }

        if (typeof callbackDblClick == "function") {
            callbackDblClick($(this));
        }
    });

}

/**
 * Irá realizar a busca na tabela
 * 
 * @param {string} idElemento
 * @param {object} elemteno
 * @returns {void}
 */
function pesquisatDadoTabela(idElemento, elemteno) {
//    var that = oTable[idElemento].column($(elemteno).attr("coluna"));
//    if (that.search() !== elemteno.value) {
//        that.search(elemteno.value).draw();
//    }
}

/**
 * Irá esconder as colunas que o usuáiro configurou
 * 
 * @returns {undefined}
 */
function esconderColunasTable(qtdRegistrosPagina) {
    if (data != null && data != undefined) {
        $(data.ColReorder).each(function(i, object) {
            visible = (data.columns[object].visible == "true") ? true : false;
            column = oTable.column(i);
            column.visible(visible);
        });
    }
}

/**
 * 
 * @param {string} strHtml
 * @param {object} oTable
 * @returns {String}
 */
function montarHtml(idElemento, oTable) {
    strHtml = "<div class='row removerColunasPopUp'>";
    $(arrColunas[idElemento]).each(function(i, value) {
        if (value != "" && value != undefined) {
            coluna = i + 1;
            column = oTable[idElemento].column(coluna);
            strSelect = (column.visible()) ? "checked='checked'" : "";
            strHtml += '<div class="col-sm-6 col-md-6"><input ' + strSelect + ' style="float: left" class="colunasCheck" type="checkbox" value="' + coluna + '" id="' + coluna + '" /> <label class="col-lg-8" for="' + coluna + '">' + value + '</label></div>';
        }
    })
    strHtml += "</div>";
    return strHtml;
}

/**
 * Mensagem de confirm para operações
 * 
 * @param {string}  msg
 * @param {fuction} callbackOk
 * @param {string}  idElemento
 * @returns {undefined}
 * @requires js/bootbox/bootbox.min.js
 */
function msgTratamentoColunas(msg, oTable, idElemento) {
    var marcar = false;
    bootbox.dialog({
        message: msg,
        title: "Configurar Tabela",
        onEscape: function() {},
        show: true,
        backdrop: true,
        closeButton: true,
        animate: true,
        className: "my-modal",
        buttons: {
            "Marcar Todos": {
                className: "btn-info marcar-todos",
                callback: function() {
                    $(".colunasCheck").prop("checked", marcar);
                    reordenarColunaAutomaticamente(idElemento);
                    return false;
                }
            },
            "Restaurar Padrão": {
                className: "btn-info restaurar",
                callback: function() {
                    // Limpando as configurações
                    oTable.state.clear();

                    setTimeout(function() {
                        // Limpando a tabela
                        $('#' + idElemento + '_wrapper').html(arrObjTable[idElemento].strHtml);
                        // Reiniciando a tabela
                        iniciarDetalhesAjaxMultinivel(idElemento, arrObjTable[idElemento].configuracao);
                    }, 900);
                }
            },
            "Excel": {
                className: "btn-default btn-xs excel",
                callback: function() {
                    $("." + idElemento + '-excel').trigger("click");
                    reordenarColunaAutomaticamente(idElemento);
                }
            },
            "Imprimir": {
                className: "btn-default btn-xs imprimir",
                callback: function() {
                    $("." + idElemento + '-pdf').trigger("click");
                    reordenarColunaAutomaticamente(idElemento);
                }
            },
            "Cancelar": {
                className: "btn-default cancelar",
                callback: function() {
                    reordenarColunaAutomaticamente(idElemento);
                }
            },
            success: {
                label: "Salvar",
                className: "btn-info salvar",
                callback: function() {
                    $(".colunasCheck").each(function() {
                        var column = oTable.column($(this).val());
                        semaforo = true;
                        if ($(this).is(':checked'))
                            column.visible(true);
                        else
                            column.visible(false);
                    });

                    reordenarColunaAutomaticamente(idElemento)
                }
            }
        }

    });
}

/**
 * irá reordenar a coluna de forma automática
 * 
 * @param {string} idElemento
 * @returns {undefined}
 */
function reordenarColunaAutomaticamente(idElemento) {
    // Reordenando a coluna
    setTimeout(function() {
        // Reordenando a coluna
        $('#' + idElemento + '_wrapper .dataTables_scrollHeadInner thead th:not(.sorting)').trigger("click").trigger("click");
    }, 500);
}

var intVal = function(i) {
    return typeof i === 'string' ?
        i.replace(/[^0-9\,]/g, '').replace(/[,]/g, '.') * 1 :
        typeof i === 'number' ?
        i : 0;
};

/**
 * Método que irá recriar os filtros caso o usuário reorganize as colunas
 * 
 * @param {object} oTable
 * @param {string} idElemento
 * @returns {void}
 */
function refatorarFiltroDetalhesAjaxMultinivel(oTable, idElemento) {
    if (oTable[idElemento] != undefined) {
        $("#" + idElemento + "_wrapper tr.filtroTopo td").each(function(i, value) {
            $(this).find("input").attr("coluna", i);
        });
    }
}

/**
 * Método que irá atualizar os filtros com a ultima busca realizada
 * 
 * @param {object} data
 * @param {string} strClass
 * @param {boolean} salvarFilro
 * @param {string} idElemento
 * @returns {void}
 */
function atualizarFiltroDetalhesAjaxMultinivel(data, strClass, salvarFilro, idElemento) {
    // Recuperando as colunas 
    colunas = data.columns;
    // Para cada coluna
    $(colunas).each(function(chave, valor) {
        if (salvarFilro) { // Se houver filtro salvo e a conficuração da tabele permitir
            if (valor.search.search != undefined && valor.search.search != "") {
                strSeletor = '#' + idElemento + ' thead .filtroTopo td:nth-child(' + (chave + 1) + ')';
                $(strSeletor).find("input").val(valor.search.search);
                $(strSeletor).find("i").addClass("com-filtro");
            }
        } else { // caso não limpo o filtro
            strSeletor = '#' + idElemento + ' thead .filtroTopo td:nth-child(' + (chave + 1) + ')';
            $(strSeletor).find("input").val("");
            $(strSeletor).find("i").removeClass("com-filtro");
        }
    });
}

function focusElemento(objTual) {
    $("html, body").animate({
        scrollTop: $(objTual).offset().top - 80
    }, 500);
}

function printIt(printThis) {
    var win = window.open();
    self.focus();
    win.document.open();
    win.document.write('<' + 'html' + '><' + 'body' + '>');
    win.document.write(printThis);
    win.document.write('<' + '/body' + '><' + '/html' + '>');
    win.document.close();
    win.print();
    win.close();
}

/*
 * Centraliza a paginação em relação à largura da janela de visualização do navegador.
 * 
 * @param {string} idElemento
 * @returns {undefined}
 */
function centralizaPaginacao(idElemento) {
    var larguraJanela = $('#' + idElemento + '_wrapper').width() - 25;
    $('#' + idElemento + '_wrapper .dataTables_paginate').width(larguraJanela);
}