var idleTime = 1800000, idleCount = 0;

// Interceptando eventos de click
document.onclick = function() {
    idleCount = 0;
};

// Interceptando eventos do mouse
document.onmousemove = function() {
    idleCount = 0;
};

// Interceptando eventos do teclado
document.onkeypress = function() {
    idleCount = 0;
};

// Core
function CheckIdleTime() {
    idleCount++;
    if (idleCount >= idleTime) {
        window.location.href= BASE_URL+"usuario/logout";
    }
}

// Setando intervalo de verificação
window.setInterval(CheckIdleTime, 1000);