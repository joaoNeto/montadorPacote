var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");

$(document).ready(function () {

    listarArquivo();

    /**
     * upload dos arquivos via file
     */
    $(".upload-arquivo").on("change", function (e) {
        uploadArquivo(e);
    });

    /**
     * zippar arquivo unwrap
     */
    $(".zippar-arquivo-unwrap").on("click", function () {
        $(this).prop("disabled",true);
        $(this).html(" <img src='"+pathResources+"/img/aguarde.gif' style='height: 1em;'> Zippando... ");        
        setTimeout(function(){
            zipparArquivo("unwrap");
        },100);        
    });
    
    /**
     * zippar arquivo wrap
     */
    $(".zippar-arquivo-wrap").on("click", function () {
        $(this).prop("disabled",true);
        $(this).html(" <img src='"+pathResources+"/img/aguarde.gif' style='height: 1em;'> Zippando... ");        
        setTimeout(function(){
            zipparArquivo("wrap");
        },100);        
    });    
    
    $(".btn-text-wrap-unwrap").on("click", function(){
        var text = $(".text-wrap-unwrap").val();
        // isWrap é um boolean que retorna true se o texto ja esta wrapeado
        var isWrap = text.indexOf("wrapped") != -1;
        if(isWrap){
            // link para descriptografar texto
            
        }else{
            // link para criptografar texto
            //$.get(urlContext + 'wrap/criptografarTexto?texto='+text, function(data){
            //    $(".text-wrap-unwrap").val(data);
            //});
            
            $.ajax({
                url: urlContext + 'wrap/criptografarTexto',
                type: 'POST',
                data: {"texto":text},
                beforeSend: function (xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data) {
                    $(".text-wrap-unwrap").val(data);
                },
                error: function (data) {
                    $(".text-wrap-unwrap").val(data);
                }
            });            
            
        }
    });
    
});

function uploadArquivo(e) {
    var arquivo = [];
    arquivo = e.target.files;
    var formDataFile = new FormData();
    for (var i = 0; i < arquivo.length; i++) {
        formDataFile.append("file", arquivo[i]);
    }

    $.ajax({
        url: urlContext + 'wrap/subirArquivo',
        type: 'POST',
        data: formDataFile,
        enctype: 'multipart/form-data',
        contentType: false,
        processData: false,
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        success: function (data) {
            listarArquivo();
        },
        error: function (data) {
            listarArquivo();
        }
    });

}

function listarArquivo() {
    // listar arquivos
    $.get(urlContext + "wrap/listarArquivo", function (data) {
        var htmlListaFileWrapeado = "", htmlListaFileUnwrapeado = "";
        data = JSON.parse(data);
        var arrayArquivosWrapeado = JSON.parse(data.arquivosWrap.data);
        var arrayArquivosUnwrapeado = JSON.parse(data.arquivosUnwrap.data);
        
        for (var i = 0; i < arrayArquivosWrapeado.arrFiles.length; i++) {
            htmlListaFileWrapeado += "<li class='clearfix item-arquivo'> ";
            htmlListaFileWrapeado += "  <p class='todo-title'>";
            htmlListaFileWrapeado += arrayArquivosWrapeado.arrFiles[i];
            htmlListaFileWrapeado += "  </p>";
            htmlListaFileWrapeado += "  <div class='todo-actionlist pull-right clearfix'>";
            htmlListaFileWrapeado += "      <i class='ico-close deletar-arquivo todo-remove' style='cursor:pointer'></i>";
            htmlListaFileWrapeado += "  </div>";
            htmlListaFileWrapeado += "</li>";
        }

        for (var i = 0; i < arrayArquivosUnwrapeado.arrFiles.length; i++) {
            htmlListaFileUnwrapeado += "<li class='clearfix item-arquivo'> ";
            htmlListaFileUnwrapeado += "  <p class='todo-title'>";
            htmlListaFileUnwrapeado += arrayArquivosUnwrapeado.arrFiles[i];
            htmlListaFileUnwrapeado += "  </p>";
            htmlListaFileUnwrapeado += "  <div class='todo-actionlist pull-right clearfix'>";
            htmlListaFileUnwrapeado += "      <i class='ico-close deletar-arquivo todo-remove' style='cursor:pointer'></i>";
            htmlListaFileUnwrapeado += "  </div>";
            htmlListaFileUnwrapeado += "</li>";
        }

        $(".lista-file-uncripto").html(htmlListaFileUnwrapeado);
        $(".lista-file-cripto").html(htmlListaFileWrapeado);
        
        $(".item-arquivo .deletar-arquivo").on("click", function(){
            var nomeArquivo = $(this).parent().parent().find("p").html();
            var diretorioArquivo = $(this).parent().parent().parent().attr("diretorio");
            $.get(urlContext+"wrap/deletarArquivo?arquivo="+diretorioArquivo+"/"+nomeArquivo,function(data){
                data = jsonConverter(data);
                listarArquivo();
            });
        });
        
    });

}

function zipparArquivo(op) {
    $.ajax({
        url: urlContext + "wrap/zipparArquivoUnwrap?op="+op,
        type: 'GET',
        contentType: false,
        processData: false,
        async: false,
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        }
    }).done(function (data) {
        var link = jsonConverter(data);
        window.location = urlContext + "resources/sessao/" + link;
        //window.open(urlContext + "resources/sessao/" + link, "minhaJanela", "height=200,width=200");
    }).fail(function (data) {
        console.log('failou');
    }).always(function (data) {
        $(".zippar-arquivo-unwrap").prop("disabled",false);
        $(".zippar-arquivo-unwrap").html("<i class='fa fa-cloud-download'></i> Baixar zip do arquivo");
        $(".zippar-arquivo-wrap").prop("disabled",false);
        $(".zippar-arquivo-wrap").html("<i class='fa fa-cloud-download'></i> Baixar zip do arquivo");
    });
}