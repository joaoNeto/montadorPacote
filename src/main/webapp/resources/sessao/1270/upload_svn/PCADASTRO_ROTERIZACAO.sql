CREATE OR REPLACE PROCEDURE I_MDLOG.PCADASTRO_ROTERIZACAO(PI_UNIDADE                   NUMBER,
															PI_CLIENTE                 NUMBER,
															PI_ROTA                    NUMBER,
															PI_SUBROTA                 NUMBER,
															PI_DATA_ALTERACAO_ROTA     DATE,
															PI_DATA_ALTERACAO_SUBROTA  DATE,
															PI_CNPJ_CLIENTE            VARCHAR2,
															PI_OPERACAO                VARCHAR2,
															STATUS OUT VARCHAR2
															   ) IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  
  
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE :	PCADASTRO_ROTERIZACAO
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DO ROTERIZACAO	.
  AUTOR     : Luis Eduardo Pinheiro Pedrosa
  DATA      : 214/11/2016
  CRC       : 
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN	

STATUS     := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT INTO DBAMDATA.T164_ROTA_CLIENTE(
		  T164_UNIDADE_IE,
		  T164_CLIENTE_IE.
		  T164_ROTA_IE,
		  T164_SUBROTA,
		  T164_DATA_ALTERACAO_ROTA,
		  T164_DATA_ALTERACAO_SUBROTA,
		  T164_CNPJ_CLIENTE
		  )
		VALUES (
		PI_UNIDADE,
		PI_CLIENTE,
		PI_ROTA,
		PI_SUBROTA,
		PI_DATA_ALTERACAO_ROTA,
		PI_DATA_ALTERACAO_SUBROTA,
		PI_CNPJ_CLIENTE,
		);
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  
  
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
  
    BEGIN
      UPDATE DBAMDATA.T164_ROTA_CLIENTE 
         SET 
		 T164_CLIENTE_IE = PI_CLIENTE,
		 T164_ROTA_IE = PI_ROTA,
		 T164_SUBROTA = PI_SUBROTA,
		 T164_DATA_ALTERACAO_ROTA = PI_DATA_ALTERACAO_ROTA,
		 T164_DATA_ALTERACAO_SUBROTA = PI_DATA_ALTERACAO_SUBROTA,
		 T164_CNPJ_CLIENTE = PI_CNPJ_CLIENTE
       WHERE T164_UNIDADE_IE = PI_UNIDADE;
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
  
    BEGIN
      DELETE FROM DBAMDATA.T164_ROTA_CLIENTE 
            WHERE T164_UNIDADE_IE   = PI_UNIDADE;
            
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
    
    
    --CONFIRMAR OPERCAO
    
    COMMIT;
    
    
EXCEPTION
	WHEN OTHERS THEN
		ROLLBACK;
		ecode := SQLCODE;
		emesg := SUBSTR(SQLERRM,1, 2048);
		STATUS     := 'ER';
END;