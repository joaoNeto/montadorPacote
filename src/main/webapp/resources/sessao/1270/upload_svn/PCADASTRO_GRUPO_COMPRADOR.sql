CREATE OR REPLACE PROCEDURE I_MDLOG.PCADASTRO_GRUPO_COMPRADOR(PI_CODCOMPRADOR  NUMBER,
														   PI_NOME  VARCHAR2,
														   PI_OPERACAO   VARCHAR2,
														   STATUS OUT VARCHAR2
														   ) IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  
  
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE :	PCADASTRO_GRUPO_COMPRADOR
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DO GRUPO DE COMPRAS	.
  AUTOR     : Luis Eduardo Pinheiro Pedrosa
  DATA      : 214/11/2016
  CRC       : 
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN	

STATUS     := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
  
    BEGIN
      INSERT INTO DBAMDATA.T323_COMPRADOR(T323_COMPRADOR_IU,T323_NOME)
             VALUES (PI_CODCOMPRADOR,PI_NOME);
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  
  
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
  
    BEGIN
      UPDATE DBAMDATA.T323_COMPRADOR 
         SET T323_NOME = PI_NOME
       WHERE T323_COMPRADOR_IU   = PI_CODCOMPRADOR;
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
  
    BEGIN
      DELETE FROM DBAMDATA.T323_COMPRADOR 
            WHERE T323_COMPRADOR_IU   = PI_CODCOMPRADOR;
            
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
    
    
    --CONFIRMAR OPERCAO
    
    COMMIT;
    
    
EXCEPTION
	WHEN OTHERS THEN
		ROLLBACK;
		ecode := SQLCODE;
		emesg := SUBSTR(SQLERRM,1, 2048);
		STATUS     := 'ER';
END;