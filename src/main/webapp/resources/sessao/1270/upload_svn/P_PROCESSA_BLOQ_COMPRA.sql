CREATE OR REPLACE PROCEDURE I_MDLOG.P_PROCESSA_BLOQ_COMPRA(PO_STATUS OUT VARCHAR2,
                                                           PI_FECHA_OS IN NUMBER DEFAULT 0,
                                                           PI_ACEITA   IN NUMBER DEFAULT 0 )
IS

WHERE_I_MDLOG   VARCHAR2(2000);
WHERE_PROD_LJ   VARCHAR2(2000);
UPDATE_I_MDLOG  VARCHAR2(2000);
USUARIO_I_MDLOG VARCHAR2(2000);
USUARIO_ORIGEM  NUMBER(10);
RESULT_BLOQ     NUMBER(10);
QTD_LJ_BLOQ     NUMBER(10);
QTD_A_BLOQ      NUMBER(10);
QTD_B_BLOQ      NUMBER(10);
QTD_C_BLOQ      NUMBER(10);
QTD_D_BLOQ      NUMBER(10);
FABRICANTE      NUMBER(10);
QTD_LJ_BLOQ_FAB NUMBER(10);
VALOR_ITENS_A   NUMBER(35,15);
VALOR_ITENS_B   NUMBER(35,15);
VALOR_ITENS_C   NUMBER(35,15);
VALOR_ITENS_D   NUMBER(35,15);
QTD_ITENS_SV    NUMBER(35,15);
QTD_LJ_SV       NUMBER(35,15);

COUNTADOR       NUMBER(10);
COUNTADOR_1     NUMBER(10);

  --MSG DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  
  vSqlUpdate  VARCHAR2(1000);

  ERRO EXCEPTION;

  CURSOR C_BASE IS
  SELECT PRODUTO,
       LOJA,
       RESTRICAO_TIPO,
       RESTRICAO_QTD,
       RESTRICAO_MOTIVO,
       USUARIO
  FROM I_MDLOG.TEMP_BLOQUEIO_COMPRA;
 
  PROCEDURE P_ATUALIZA_RESTRICAO(pUnidade       NUMBER,
                                 pProduto       NUMBER,
                                 pTipoRestricao T077_PRODUTO_UNIDADE.T077_TIPORESTR_COMPRA%TYPE,
                                 pQtdRestricao  T077_PRODUTO_UNIDADE.T077_VALORRESTR_COMPRA%TYPE,
                                 pUsuario       T077_PRODUTO_UNIDADE.T077_USUARIO_BLOQUEIO_E%TYPE,
                                 pMotivo        T077_PRODUTO_UNIDADE.T077_OBSRESTR_COMPRA%TYPE)
  IS
    vAux  NUMBER;
  BEGIN

    UPDATE T077_PRODUTO_UNIDADE 
       SET T077_TIPORESTR_COMPRA   = pTipoRestricao,
           T077_VALORRESTR_COMPRA  = pQtdRestricao,
           T077_USUARIO_BLOQUEIO_E = pUsuario,
           T077_DATA_BLOQUEIO      = SYSDATE,
           T077_OBSRESTR_COMPRA    = pMotivo
     WHERE T077_UNIDADE_IE = pUnidade
       AND T077_PRODUTO_IE = pProduto;
       
    vAux := sql%rowcount;       
       
    RESULT_BLOQ := RESULT_BLOQ + vAux;      

  END;

BEGIN
  PO_STATUS       := 'NP';
  WHERE_I_MDLOG   := '';
  USUARIO_I_MDLOG :='';
  COUNTADOR_1 := 1;
  RESULT_BLOQ := 0;
  
  FOR V_PROD IN C_BASE LOOP
   
    IF PI_ACEITA = 0 THEN
   
      select T007_USUARIO_ORIGEM_IU 
        INTO USUARIO_ORIGEM 
        from I_MDLOG.T007_USUARIO_ORIGEM 
       where T007_USUARIO_IE = V_PROD.USUARIO and T007_SISTEMA_ORIGEM_IU = 2; 
        
      select NOME 
       INTO USUARIO_I_MDLOG 
       from i_mdlog.VIEW_DADOS_USUARIO 
       where USUARIO = USUARIO_ORIGEM;
      
      IF V_PROD.PRODUTO > 0 THEN
        WHERE_I_MDLOG := WHERE_I_MDLOG||' T101_PRODUTO = '||V_PROD.PRODUTO;
      END IF;
      
      IF V_PROD.LOJA > 0 THEN
        WHERE_I_MDLOG := WHERE_I_MDLOG||' AND T101_LOJA = '||V_PROD.LOJA;
      END IF;
            
            
      COUNTADOR := 1;
      IF V_PROD.LOJA > 0 THEN  
        P_ATUALIZA_RESTRICAO(V_PROD.LOJA,
                             V_PROD.PRODUTO,
                             V_PROD.RESTRICAO_TIPO,
                             V_PROD.RESTRICAO_QTD,
                             USUARIO_ORIGEM,
                             V_PROD.RESTRICAO_MOTIVO);
      ELSE 
            
        FOR VC_LOG IN (SELECT T101_LOJA FROM I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA WHERE T101_PRODUTO = V_PROD.PRODUTO) 
        LOOP
          P_ATUALIZA_RESTRICAO(VC_LOG.T101_LOJA,
                             V_PROD.PRODUTO,
                             V_PROD.RESTRICAO_TIPO,
                             V_PROD.RESTRICAO_QTD,
                             USUARIO_ORIGEM,
                             V_PROD.RESTRICAO_MOTIVO);
          COUNTADOR := COUNTADOR+1;
        END LOOP;
             
      END IF;
            
      UPDATE_I_MDLOG := 'UPDATE I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA  SET
      T101_BLOQUEIO_COMPRA          = :C1 ,
      T101_VALOR_BLOQ_COMP          = :C2 ,
      T101_MOTIVO_BLOQ_COMP         = :C3  ,
      T101_USUARIO_BLOQ_COMP        = :C4 ,
      T101_USUARIO_NOME_BLOQ_COMP   = :C5  ,
      T101_DATA_BLOQ_COMP           = :C6 
      WHERE '||WHERE_I_MDLOG;
        
      EXECUTE IMMEDIATE UPDATE_I_MDLOG USING 
      V_PROD.RESTRICAO_TIPO,
      V_PROD.RESTRICAO_QTD,
      V_PROD.RESTRICAO_MOTIVO,
      V_PROD.USUARIO,
      USUARIO_I_MDLOG,
      sysdate;
  
      SELECT COUNT(*) QTD INTO QTD_LJ_BLOQ FROM I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA 
       WHERE T101_BLOQUEIO_COMPRA > 0
         AND T101_PRODUTO =  V_PROD.PRODUTO;
     
      UPDATE I_MDLOG.T100_ENTIDADE_PRODUTO 
         SET T100_QTD_LOJAS_BLOQ = QTD_LJ_BLOQ
       WHERE T100_PRODUTO  = V_PROD.PRODUTO;
       
      --// ATUALIZA VISAO PRINCIPAL DO INDICADOR
      
     --//VERIFICA SE A LOJA FOI ESPECIFICADA
      
      IF V_PROD.LOJA > 0 THEN
      
        SELECT T101_FABRICANTE INTO FABRICANTE FROM I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA
         WHERE T101_PRODUTO = V_PROD.PRODUTO
         AND T101_LOJA    =  V_PROD.LOJA;
         
      ELSE
      
        SELECT T101_FABRICANTE INTO FABRICANTE FROM I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA
         WHERE T101_PRODUTO = V_PROD.PRODUTO
         AND ROWNUM <= 1;
         
      END IF;
      
       
      SELECT COUNT(DISTINCT(T101_PRODUTO)) QTD INTO QTD_LJ_BLOQ_FAB FROM I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA 
      JOIN
             I_MDLOG.M02_009_ORDEM_SERVICO
             ON T101_PRODUTO||','||T101_LOJA = M02_009_ID_ORIGEM
             AND M02_009_STATUS = '1'
             AND M02_009_ENTIDADE_IE = 17  
             AND M02_009_USUARIO_IE = V_PROD.USUARIO
       WHERE T101_BLOQUEIO_COMPRA > 0
         AND T101_FABRICANTE =  FABRICANTE;
      
      SELECT
      NVL(COUNT(DISTINCT(PR.T101_PRODUTO)),0) ITENS_A into QTD_A_BLOQ
      FROM I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA PR JOIN I_MDLOG.VIEW_CURVA_PARAMETRO V
           ON V.UNIDADE = PR.T101_LOJA
           JOIN
             I_MDLOG.M02_009_ORDEM_SERVICO
             ON T101_PRODUTO||','||T101_LOJA = M02_009_ID_ORIGEM
             AND M02_009_STATUS = '1'
             AND M02_009_ENTIDADE_IE = 17  
             AND M02_009_USUARIO_IE = V_PROD.USUARIO
      WHERE PR.T101_BLOQUEIO_COMPRA > 0
      AND PR.T101_FABRICANTE = FABRICANTE
      AND SUBSTR(TRIM(I_MDLOG.F_RETORNA_CURVA_PRODUTO(TRIM(NVL(PR.T101_CURVA_ABC_FISICA,'D3')),
                                            TRIM(NVL(PR.T101_CURVA_ABC_FINANCEIRA,'D3')),
                                            TRIM(NVL(PR.T101_CURVA_MARGEM,'D3')),
                                            V.TIPO_CURVA)),1,1) = 'A';
     
      SELECT
      NVL(COUNT(DISTINCT(PR.T101_PRODUTO)),0) ITENS_A into QTD_B_BLOQ
      FROM I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA PR JOIN I_MDLOG.VIEW_CURVA_PARAMETRO V
           ON V.UNIDADE = PR.T101_LOJA
           JOIN
             I_MDLOG.M02_009_ORDEM_SERVICO
             ON T101_PRODUTO||','||T101_LOJA = M02_009_ID_ORIGEM
             AND M02_009_STATUS = '1'
             AND M02_009_ENTIDADE_IE = 17  
             AND M02_009_USUARIO_IE = V_PROD.USUARIO
      WHERE PR.T101_BLOQUEIO_COMPRA > 0
      AND PR.T101_FABRICANTE = FABRICANTE
      AND SUBSTR(TRIM(I_MDLOG.F_RETORNA_CURVA_PRODUTO(TRIM(NVL(PR.T101_CURVA_ABC_FISICA,'D3')),
                                            TRIM(NVL(PR.T101_CURVA_ABC_FINANCEIRA,'D3')),
                                            TRIM(NVL(PR.T101_CURVA_MARGEM,'D3')),
                                            V.TIPO_CURVA)),1,1) = 'B';
                                            
      SELECT
      NVL(COUNT(DISTINCT(PR.T101_PRODUTO)),0) ITENS_A into QTD_C_BLOQ
      FROM I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA PR JOIN I_MDLOG.VIEW_CURVA_PARAMETRO V
           ON V.UNIDADE = PR.T101_LOJA
           JOIN
             I_MDLOG.M02_009_ORDEM_SERVICO
             ON T101_PRODUTO||','||T101_LOJA = M02_009_ID_ORIGEM
             AND M02_009_STATUS = '1'
             AND M02_009_ENTIDADE_IE = 17  
             AND M02_009_USUARIO_IE = V_PROD.USUARIO
      WHERE PR.T101_BLOQUEIO_COMPRA > 0
      AND PR.T101_FABRICANTE = FABRICANTE
      AND SUBSTR(TRIM(I_MDLOG.F_RETORNA_CURVA_PRODUTO(TRIM(NVL(PR.T101_CURVA_ABC_FISICA,'D3')),
                                            TRIM(NVL(PR.T101_CURVA_ABC_FINANCEIRA,'D3')),
                                            TRIM(NVL(PR.T101_CURVA_MARGEM,'D3')),
                                            V.TIPO_CURVA)),1,1) = 'C';
                                            
      SELECT
      NVL(COUNT(DISTINCT(PR.T101_PRODUTO)),0) ITENS_A into QTD_D_BLOQ
      FROM I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA PR JOIN I_MDLOG.VIEW_CURVA_PARAMETRO V
           ON V.UNIDADE = PR.T101_LOJA
           JOIN
             I_MDLOG.M02_009_ORDEM_SERVICO
             ON T101_PRODUTO||','||T101_LOJA = M02_009_ID_ORIGEM
             AND M02_009_STATUS = '1'
             AND M02_009_ENTIDADE_IE = 17
             AND M02_009_USUARIO_IE = V_PROD.USUARIO
      WHERE PR.T101_BLOQUEIO_COMPRA > 0
      AND PR.T101_FABRICANTE = FABRICANTE
      AND SUBSTR(TRIM(I_MDLOG.F_RETORNA_CURVA_PRODUTO(TRIM(NVL(PR.T101_CURVA_ABC_FISICA,'D3')),
                                            TRIM(NVL(PR.T101_CURVA_ABC_FINANCEIRA,'D3')),
                                            TRIM(NVL(PR.T101_CURVA_MARGEM,'D3')),
                                            V.TIPO_CURVA)),1,1) = 'D';
      
      UPDATE I_MDLOG.T106_IND_PROD_BLOQ_VF 
         SET T106_QTD_CURVA_A  = QTD_A_BLOQ,
             T106_QTD_CURVA_B  = QTD_B_BLOQ,
             T106_QTD_CURVA_C  = QTD_C_BLOQ,
             T106_QTD_CURVA_D  = QTD_D_BLOQ, 
             T106_QTD_ITENS_BLOQ = QTD_LJ_BLOQ_FAB
       WHERE T106_FABRICANTE = FABRICANTE
         AND T106_USUARIO    = V_PROD.USUARIO  ;
       
       
      --//PRODUTO SEM VENDA
      SELECT COUNT(DISTINCT(NVL(T101_LOJA,0))) QTD_LJ_SV 
              INTO QTD_LJ_SV
        FROM I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA JOIN
             I_MDLOG.M02_009_ORDEM_SERVICO
             ON T101_PRODUTO||','||T101_LOJA = M02_009_ID_ORIGEM
             AND M02_009_STATUS = '1'
             AND M02_009_ENTIDADE_IE = 18  
             AND M02_009_USUARIO_IE = V_PROD.USUARIO
             AND T101_FABRICANTE = FABRICANTE; 
             
      SELECT
            SUM( T101_PRECO_FAB_FORN_PRIO ) VALOR_ITENS_A
            INTO VALOR_ITENS_A
            FROM I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA PR JOIN I_MDLOG.VIEW_CURVA_PARAMETRO V
                 ON V.UNIDADE = PR.T101_LOJA
            JOIN
                 I_MDLOG.M02_009_ORDEM_SERVICO
                 ON T101_PRODUTO||','||T101_LOJA = M02_009_ID_ORIGEM
                 AND M02_009_STATUS = '1'
                 AND M02_009_ENTIDADE_IE = 18
            AND SUBSTR(TRIM(I_MDLOG.F_RETORNA_CURVA_PRODUTO(TRIM(NVL(PR.T101_CURVA_ABC_FISICA,'D3')),
                                                  TRIM(NVL(PR.T101_CURVA_ABC_FINANCEIRA,'D3')),
                                                  TRIM(NVL(PR.T101_CURVA_MARGEM,'D3')),
                                                  V.TIPO_CURVA)),1,1) = 'A'
            AND T101_FABRICANTE = FABRICANTE
            AND M02_009_USUARIO_IE = V_PROD.USUARIO;
    
      SELECT
            SUM( T101_PRECO_FAB_FORN_PRIO ) VALOR_ITENS_B
            INTO VALOR_ITENS_B
            FROM I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA PR JOIN I_MDLOG.VIEW_CURVA_PARAMETRO V
                 ON V.UNIDADE = PR.T101_LOJA
            JOIN
                 I_MDLOG.M02_009_ORDEM_SERVICO
                 ON T101_PRODUTO||','||T101_LOJA = M02_009_ID_ORIGEM
                 AND M02_009_STATUS = '1'
                 AND M02_009_ENTIDADE_IE = 18
            AND SUBSTR(TRIM(I_MDLOG.F_RETORNA_CURVA_PRODUTO(TRIM(NVL(PR.T101_CURVA_ABC_FISICA,'D3')),
                                                  TRIM(NVL(PR.T101_CURVA_ABC_FINANCEIRA,'D3')),
                                                  TRIM(NVL(PR.T101_CURVA_MARGEM,'D3')),
                                                  V.TIPO_CURVA)),1,1) = 'B'
            AND T101_FABRICANTE = FABRICANTE
            AND M02_009_USUARIO_IE = V_PROD.USUARIO;
            
      SELECT
            SUM( T101_PRECO_FAB_FORN_PRIO ) VALOR_ITENS_C
            INTO VALOR_ITENS_C
            FROM I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA PR JOIN I_MDLOG.VIEW_CURVA_PARAMETRO V
                 ON V.UNIDADE = PR.T101_LOJA
            JOIN
                 I_MDLOG.M02_009_ORDEM_SERVICO
                 ON T101_PRODUTO||','||T101_LOJA = M02_009_ID_ORIGEM
                 AND M02_009_STATUS = '1'
                 AND M02_009_ENTIDADE_IE = 18
            AND SUBSTR(TRIM(I_MDLOG.F_RETORNA_CURVA_PRODUTO(TRIM(NVL(PR.T101_CURVA_ABC_FISICA,'D3')),
                                                  TRIM(NVL(PR.T101_CURVA_ABC_FINANCEIRA,'D3')),
                                                  TRIM(NVL(PR.T101_CURVA_MARGEM,'D3')),
                                                  V.TIPO_CURVA)),1,1) = 'C'
            AND T101_FABRICANTE = FABRICANTE
            AND M02_009_USUARIO_IE = V_PROD.USUARIO;
      
      SELECT
            SUM( T101_PRECO_FAB_FORN_PRIO ) VALOR_ITENS_D
            INTO VALOR_ITENS_D
            FROM I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA PR JOIN I_MDLOG.VIEW_CURVA_PARAMETRO V
                 ON V.UNIDADE = PR.T101_LOJA
            JOIN
                 I_MDLOG.M02_009_ORDEM_SERVICO
                 ON T101_PRODUTO||','||T101_LOJA = M02_009_ID_ORIGEM
                 AND M02_009_STATUS = '1'
                 AND M02_009_ENTIDADE_IE = 18
            AND SUBSTR(TRIM(I_MDLOG.F_RETORNA_CURVA_PRODUTO(TRIM(NVL(PR.T101_CURVA_ABC_FISICA,'D3')),
                                                  TRIM(NVL(PR.T101_CURVA_ABC_FINANCEIRA,'D3')),
                                                  TRIM(NVL(PR.T101_CURVA_MARGEM,'D3')),
                                                  V.TIPO_CURVA)),1,1) = 'D'
            AND T101_FABRICANTE = FABRICANTE
            AND M02_009_USUARIO_IE = V_PROD.USUARIO;
            
            
      SELECT COUNT(DISTINCT(NVL(T101_PRODUTO,0))) QTD_ITENS_SV
             INTO QTD_ITENS_SV
        FROM I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA JOIN
             I_MDLOG.M02_009_ORDEM_SERVICO
             ON T101_PRODUTO||','||T101_LOJA = M02_009_ID_ORIGEM
             AND M02_009_STATUS = '1'
             AND M02_009_ENTIDADE_IE = 18
             AND M02_009_USUARIO_IE = V_PROD.USUARIO
             AND T101_FABRICANTE= FABRICANTE;
        

      UPDATE I_MDLOG.T107_IND_PROD_SEM_VENDA_VF
         SET T107_QTD_CURVA_A  = VALOR_ITENS_A,
             T107_QTD_CURVA_B  = VALOR_ITENS_B,
             T107_QTD_CURVA_C  = VALOR_ITENS_C,
             T107_QTD_CURVA_D  = VALOR_ITENS_D,
             T107_QTD_ITENS_SV = QTD_ITENS_SV,
             T107_QTD_LOJA_SV  = QTD_LJ_SV
      WHERE  T107_FABRICANTE   = FABRICANTE
        AND  T107_USUARIO      = V_PROD.USUARIO ;
             
             
    END IF;
     
    IF PI_FECHA_OS = 1 THEN
    
      IF V_PROD.LOJA = 0  THEN
      
        UPDATE I_MDLOG.M02_009_ORDEM_SERVICO 
          SET  
          M02_009_DATA_FINALIZACAO = SYSDATE,
          M02_009_STATUS = '2'
          WHERE M02_009_ENTIDADE_IE  in (17,18,19)
            AND M02_009_STATUS = '1'
            AND M02_009_ID_ORIGEM  IN (SELECT V_PROD.PRODUTO||','||T101_LOJA
                                           FROM I_MDLOG.T101_ENTIDADE_PRODUTO_LOJA 
                                            WHERE T101_PRODUTO = V_PROD.PRODUTO);
        
        IF PI_ACEITA = 1  THEN
          RESULT_BLOQ := 1;
        END IF;
        
      ELSE
      
        UPDATE I_MDLOG.M02_009_ORDEM_SERVICO 
          SET  
          M02_009_DATA_FINALIZACAO = SYSDATE,
          M02_009_STATUS = 2
          WHERE M02_009_ENTIDADE_IE  in (17,18,19)
            AND M02_009_ID_ORIGEM    = V_PROD.PRODUTO||','||V_PROD.LOJA;
            
      END IF; 
    END IF;
     
  
    IF RESULT_BLOQ >0 THEN
        PO_STATUS := 'PR';
        COMMIT;
    ELSE
    
      RAISE ERRO;
      
    END IF;
  
    WHERE_I_MDLOG := '';
    COUNTADOR_1 := COUNTADOR_1+1;
  END LOOP;
  
EXCEPTION WHEN ERRO THEN

  ROLLBACK;
  PO_STATUS := 'ER';
  
  ecode      := SQLCODE;
  emesg      := SUBSTR(SQLERRM,1, 2048);
WHEN OTHERS THEN

  ROLLBACK;
  PO_STATUS := 'ER';
  
  ecode      := SQLCODE;
  emesg      := SUBSTR(SQLERRM,1, 2048);
END;
/
