/******************************************************************************
Schema    : I_MDLOG
Analista  : ALBERTO MEDEIROS
Data      : 08/07/2016
CRC       : 69541
Descricao : RETORNAR os clusters do sistema
******************************************************************************/
CREATE OR REPLACE VIEW I_MDLOG.VIEW_CLUSTER_SISTEMA as
  SELECT T1200_CLUSTER_IU CLUSTER_IU,
         T1200_DESCRICAO DESCRICAO,
         T1200_TIPO_CLUSTER TIPO_CLUSTER_E
  FROM T1200_CLUSTER ;
  