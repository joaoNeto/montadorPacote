CREATE OR REPLACE PROCEDURE I_MDLOG.PCADASTRO_GRUPO_RESULTADO(
    PI_GRUPO_RESULTADO NUMBER,
    PI_DESCRICAO       VARCHAR2,
    PI_TIPO_ACUMULADOR NUMBER,
    PI_OPERACAO        VARCHAR2,
    STATUS OUT VARCHAR2 )
IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_GRUPO_RESULTADO
  OBJETIVO  : ROTINA PARA OPERAÇÕES DE CADASTRO DO GRUPO RESULTADO.
  AUTOR     : FILIPE FALCÃO
  DATA      : 01/11/2016
  CRC       : CRC70542
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN
	
   STATUS := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT
      INTO DBAMDATA.T251_GRUPO_RESULTADO
        (
          T251_GRUPO_RESULTADO_IU,
          T251_DESCRICAO,
          T251_TIPO_ACUMULADOR
        )
        VALUES
        (
          PI_GRUPO_RESULTADO,
          PI_DESCRICAO,
          PI_TIPO_ACUMULADOR
        );
    EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      STATUS := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
      UPDATE DBAMDATA.T251_GRUPO_RESULTADO
      SET T251_DESCRICAO            = PI_DESCRICAO,
        T251_TIPO_ACUMULADOR        = PI_TIPO_ACUMULADOR
      WHERE T251_GRUPO_RESULTADO_IU = PI_GRUPO_RESULTADO;
    EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      STATUS := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE
      FROM DBAMDATA.T251_GRUPO_RESULTADO
      WHERE T251_GRUPO_RESULTADO_IU = PI_GRUPO_RESULTADO;
    EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      STATUS := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
  --CONFIRMAR OPERCAO
  COMMIT;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  ecode  := SQLCODE;
  emesg  := SUBSTR(SQLERRM,1, 2048);
  STATUS := 'ER';
END;
/