CREATE OR REPLACE FUNCTION I_MDLOG.F_CALC_QTDE_EMB(PI_UNIDADE            NUMBER,
                                                   PI_PRODUTO            NUMBER,
                                                   PI_FORNECEDOR         NUMBER,
                                                   PI_DATA               DATE,
                                                   PI_QTDE_PEDIDA        NUMBER,
                                                   PI_ATUALIZA_REGISTROS NUMBER,
                                                   PI_OC_ESPECIAL        VARCHAR2,
                                                   PI_OS                 VARCHAR2,
                                                   PI_LOJA_CROSSDOCKING  NUMBER DEFAULT 0)
RETURN NUMBER
IS
  vM03_012_EMBALAGEM_COMPRA     M03_012_SUG_COMPRA_DIA.M03_012_EMBALAGEM_COMPRA%TYPE;     
  vM03_012_PERC_ARRED_EMB_PROD  M03_012_SUG_COMPRA_DIA.M03_012_PERC_ARRED_EMB_PROD%TYPE;
  vM03_012_ALIQ_IPI             M03_012_SUG_COMPRA_DIA.M03_012_ALIQ_IPI%TYPE;           
  vM03_012_MVA_ST               M03_012_SUG_COMPRA_DIA.M03_012_MVA_ST%TYPE;             
  vM03_012_ALIQ_ST              M03_012_SUG_COMPRA_DIA.M03_012_ALIQ_ST%TYPE;             
  vM03_012_ALIQ_II              M03_012_SUG_COMPRA_DIA.M03_012_ALIQ_II%TYPE;             
  vM03_012_ALIQ_PIS_IMP         M03_012_SUG_COMPRA_DIA.M03_012_ALIQ_PIS_IMP%TYPE;       
  vM03_012_ALIQ_COF_IMP         M03_012_SUG_COMPRA_DIA.M03_012_ALIQ_COF_IMP%TYPE;       
  vM03_012_TAXA_VENDOR          M03_012_SUG_COMPRA_DIA.M03_012_TAXA_VENDOR%TYPE;
  vM03_012_PRECO_SUGESTAO       M03_012_SUG_COMPRA_DIA.M03_012_PRECO_SUGESTAO%TYPE;
  vM03_012_FORMA_ENTREGA        M03_012_SUG_COMPRA_DIA.M03_012_FORMA_ENTREGA%TYPE;
  
  vM03_012_TIPO_RESTRICAO       M03_012_SUG_COMPRA_DIA.M03_012_TIPO_RESTRICAO%TYPE;
  vM03_012_VALOR_RESTRICAO      M03_012_SUG_COMPRA_DIA.M03_012_VALOR_RESTRICAO%TYPE;
  vM03_012_SALDO_ESTOQUE_DISP   M03_012_SUG_COMPRA_DIA.M03_012_SALDO_ESTOQUE_DISP%TYPE;
  vM03_012_SALDO_PENDENCIA      M03_012_SUG_COMPRA_DIA.M03_012_SALDO_PENDENCIA%TYPE;
  vM03_012_MEDIA_VENDA_DIA      M03_012_SUG_COMPRA_DIA.M03_012_MEDIA_VENDA_DIA%TYPE;
  
  vDescontos                    NUMBER;
  
  vQtdeFinal   NUMBER;
  vQtdeAux     NUMBER;
  
  vValorFinal  NUMBER;
  vValorAux    NUMBER;
  vPercRateio  NUMBER;
  
  vTotalSug       NUMBER;
  vTotalRep       NUMBER;
  vTotalProporcao NUMBER;
  vQtdUnidades    NUMBER;
  
  vGrupoTributario VARCHAR2(2);
  vUFOrigem  VARCHAR2(2);
  vUFDestino VARCHAR2(2);
  vFornecedorCD   NUMBER;
  vUnidadeCD   NUMBER;
  
  PROCEDURE P_CARREGA_DADOS_FORNEC_LOJA(P_FORNEC NUMBER,
                                        P_UNID   NUMBER)
  IS
  BEGIN
    SELECT TRIM(TO_CHAR(T054_TIPO_NATUREZA_E,'00')),
           T002_UF_E
      INTO vGrupoTributario,
           vUFDestino
    FROM T019_FORNECEDOR, 
         T002_MUNICIPIO,
         T054_FORNECEDOR_UNIDADE
    WHERE T002_CEP_IU = T019_CEP_E
      AND T019_FORNECEDOR_IU = P_FORNEC
      AND T054_UNIDADE_IE    = P_UNID
      AND T054_FORNECEDOR_IE = T019_FORNECEDOR_IU;   
      
    SELECT T003_UF_E
       INTO vUFOrigem
      FROM T003_UNIDADE
      WHERE T003_UNIDADE_IU = PI_UNIDADE;  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      vGrupoTributario := '01';  
      vUFDestino := 'XX';
      vUFOrigem := 'XX';
  END;
  
  FUNCTION F_CALC_QTDE_EMB(P_QDTE_ORIGINAL      NUMBER,
                           P_EMB_FORNEC         NUMBER,
                           P_PERC_ARRED         NUMBER,
                           P_TIPO_RESTRICAO     NUMBER,
                           P_VALOR_RESTRICAO    NUMBER,
                           P_SALDO_ESTOQUE_DISP NUMBER,
                           P_SALDO_PENDENCIA    NUMBER,
                           P_MEDIA_VENDA_DIA    NUMBER
                           
                           )
  RETURN NUMBER
  IS
    vQtdeRet  NUMBER;
    
    vQtdeEmbTrunc  NUMBER;
    vQtdeEmb       NUMBER;
    vFracao        NUMBER;
    vFatorEmb      NUMBER;
  BEGIN
    vQtdeEmb      := (P_QDTE_ORIGINAL / P_EMB_FORNEC);
    vQtdeEmbTrunc := trunc(P_QDTE_ORIGINAL / P_EMB_FORNEC);    
    
    if (vQtdeEmb = vQtdeEmbTrunc) THEN
      
      vQtdeRet := P_QDTE_ORIGINAL;
      
    ELSE
      
      vFracao := trunc((vQtdeEmb - vQtdeEmbTrunc) * 100);
      
      if (vFracao > P_PERC_ARRED) THEN
        vFatorEmb := 1;
      ELSE
        vFatorEmb := 0;
      END IF;
      
      vQtdeRet := (vQtdeEmbTrunc + vFatorEmb) * P_EMB_FORNEC;        
    END IF;
    
    --RESTRICAO POR QUANTIDADE
    if ((P_TIPO_RESTRICAO = 2) and (vQtdeRet > P_VALOR_RESTRICAO)) then
      vQtdeRet := P_VALOR_RESTRICAO;
    --RESTRIÇÃO POR Dias De Estoque
    elsif ((P_TIPO_RESTRICAO = 1) and (P_VALOR_RESTRICAO < ((vQtdeRet + P_SALDO_ESTOQUE_DISP + P_SALDO_PENDENCIA)/P_MEDIA_VENDA_DIA))) then    
      vQtdeRet := P_VALOR_RESTRICAO;
    end if;
    
    return vQtdeRet;      
    
  END;
  
  FUNCTION F_CALC_VALOR(P_QTDE         NUMBER,
                        P_PRC          NUMBER,
                        P_DESC         NUMBER,
                        P_TAXA_VENDOR  NUMBER,                        
                        P_MVA_ST       NUMBER,
                        P_ALIQ_ST      NUMBER,
                        P_ALIQ_IPI     NUMBER,
                        P_ALIQ_II      NUMBER, 
                        P_ALIQ_PIS_IMP NUMBER,
                        P_ALIQ_COF_IMP NUMBER)  
  RETURN NUMBER
  IS    
    pValorRet         NUMBER;    
    po_nValorIpi      NUMBER;
    po_nAliqIpi       NUMBER;
    po_nValor_IcmsSt  NUMBER;
    po_MVA_ST         NUMBER;
    po_AlqIcmsST      NUMBER;
                                                       
        
  BEGIN
    pValorRet := P_QTDE * P_PRC;
    
    pValorRet := pValorRet - (pValorRet * P_DESC/100);
    
    I_MDLOG.P_CALC_IMPOSTOS(
    PI_UNIDADE,
    PI_FORNECEDOR,
    PI_PRODUTO,
    trim(to_char(vGrupoTributario,'00')),
    vUFOrigem,
    vUFDestino,
    P_QTDE,
    P_PRC,
    P_DESC,
    po_nValorIpi,
    po_nAliqIpi  ,
    po_nValor_IcmsSt,
    po_MVA_ST       ,
    po_AlqIcmsST
    ) ;
    
    po_nValorIpi := pValorRet * (P_ALIQ_IPI/100);
    
    pValorRet := pValorRet + po_nValorIpi + po_nValor_IcmsSt;
    
    pValorRet := pValorRet * (1 + (P_TAXA_VENDOR/100));
    
    return pValorRet;
    
  END;
  
  PROCEDURE P_ATUALIZA_QTDE(pQtde     NUMBER,
                            pValor    NUMBER,
                            pUnidade  NUMBER)
  IS
  BEGIN    
    IF PI_OC_ESPECIAL = 'N' THEN   
      UPDATE M03_012_SUG_COMPRA_DIA
      SET M03_012_REPOSICAO_UND   = pQtde,
          M03_012_REPOSICAO_FINAL = pValor
      WHERE M03_012_DATA_SUG_IE   = PI_DATA
        AND M03_012_PRODUTO_IE    = PI_PRODUTO
        AND M03_012_FORNECEDOR_IE = PI_FORNECEDOR
        AND M03_012_UNIDADE_IE    = pUnidade
        AND M03_012_ORDEM_SERVICO_IE = PI_OS; 
    ELSE 
      UPDATE M03_016_OC_ESPECIAL
      SET M03_016_REPOSICAO_UND   = pQtde,
          M03_016_REPOSICAO_FINAL = pValor
      WHERE M03_016_DATA_SUG_IE   = PI_DATA
        AND M03_016_PRODUTO_IE    = PI_PRODUTO
        AND M03_016_FORNECEDOR_IE = PI_FORNECEDOR
        AND M03_016_UNIDADE_IE    = pUnidade
        AND M03_016_ORDEM_SERVICO_IE = PI_OS; 
    END IF;
  END;  
  
  PROCEDURE P_ATUALIZA_QTDE_CROSS_DOCKING(P_PRODUTO     NUMBER,
                                          P_FORNECEDOR  NUMBER,
                                          P_UNIDADE     NUMBER,
                                          P_DATA        DATE,
                                          P_QTDE        NUMBER)
  IS
    vFornecedor  NUMBER;    
    vTotalSug       NUMBER;
    vTotalRep       NUMBER;
    vPercRat     NUMBER;
    vQtdeRat     NUMBER;
    vQtde        NUMBER;
    vTotalLojas  NUMBER;
    vValorTotalLojas  NUMBER;
  BEGIN
    vTotalLojas := 0;

    SELECT T908_FORNECEDOR_EMPRESA_E
      INTO vFornecedor
    FROM T908_PARAM_EMPRESA
    WHERE T908_UNIDADE_IE = P_UNIDADE;
    
    IF P_QTDE = 0  THEN
      IF PI_OC_ESPECIAL = 'N' THEN  
        UPDATE M003_003_PRODUTO_SUGESTAO
           SET M003_003_SUGESTAO_FINAL = 0,
               M003_003_VALOR_SUGESTAO_FINAL = 0
        WHERE M003_003_PRODUTO_IE    = P_PRODUTO
         AND M003_003_FORNECEDOR_IE = vFornecedor
         AND M003_003_DATA_RA_IE    = P_DATA;
      ELSE
        UPDATE M03_016_OC_ESPECIAL
        SET M03_016_REPOSICAO_UND   = 0,
            M03_016_REPOSICAO_FINAL = 0
        WHERE M03_016_DATA_SUG_IE   = PI_DATA
          AND M03_016_PRODUTO_IE    = PI_PRODUTO
          AND M03_016_FORNECEDOR_IE = vFornecedor
          AND M03_016_ORDEM_SERVICO_IE = PI_OS;
      END IF;
  
    ELSE
      vTotalRep := 0;
      vTotalSug := 0;
      
      IF PI_OC_ESPECIAL = 'N' THEN  
        SELECT sum(NVL(M003_003_SUGESTAO_FINAL,0)), SUM(NVL(M003_003_SUGESTAO_REAL,0))
          INTO vTotalRep, vTotalSug
        FROM M003_003_PRODUTO_SUGESTAO
        WHERE M003_003_UNIDADE_IE    <> P_UNIDADE
          AND M003_003_PRODUTO_IE    = P_PRODUTO
          AND M003_003_FORNECEDOR_IE = vFornecedor
          AND M003_003_DATA_RA_IE    = P_DATA;
  
        SELECT count(distinct (M003_003_UNIDADE_IE))
          into vQtdUnidades
        FROM M003_003_PRODUTO_SUGESTAO
        WHERE M003_003_UNIDADE_IE    <> P_UNIDADE
          AND M003_003_PRODUTO_IE    = P_PRODUTO
          AND M003_003_FORNECEDOR_IE = vFornecedor
          AND M003_003_DATA_RA_IE    = P_DATA;   
      ELSE
      
        SELECT NVL(SUM(M03_016_REPOSICAO_UND),0), NVL(SUM(M03_016_SUGESTAO_FINAL),0)
          INTO vTotalRep, vTotalSug
        FROM M03_016_OC_ESPECIAL
        WHERE M03_016_DATA_SUG_IE   = P_DATA
          AND M03_016_UNIDADE_IE   <> P_UNIDADE
          AND M03_016_PRODUTO_IE    = P_PRODUTO
          AND M03_016_FORNECEDOR_IE = vFornecedor
          AND M03_016_ORDEM_SERVICO_IE = PI_OS;      
          
        SELECT COUNT(DISTINCT(M03_016_UNIDADE_IE))
          INTO vQtdUnidades
        FROM M03_016_OC_ESPECIAL
        WHERE M03_016_DATA_SUG_IE   = PI_DATA
          AND M03_016_PRODUTO_IE    = PI_PRODUTO
          AND M03_016_FORNECEDOR_IE = vFornecedor
          AND M03_016_ORDEM_SERVICO_IE = PI_OS;          
        
      END IF;
      
      IF PI_OC_ESPECIAL = 'N' THEN  
        for vRegLojasCrossDocking in (SELECT M003_003_UNIDADE_IE,
                                             M003_003_PRODUTO_IE,
                                             case when vTotalRep = 0 then M003_003_SUGESTAO_REAL else M003_003_SUGESTAO_FINAL end M003_003_SUGESTAO_FINAL,
                                             M003_003_SUGESTAO_REAL,
                                             M003_003_EMBALAGEM_COMPRA,
                                             M003_003_PERC_ARRED_EMB_PROD,
                                             M003_003_PRECO_SUGESTAO,
                                             NVL(M003_003_DESCONTO_ITEM,0) + NVL(M003_003_DESCONTO_REPASSE,0) + NVL(M003_003_DESCONTO_COMERCIAL,0) DESCONTOS,
                                             M003_003_TAXA_VENDOR,                        
                                             M003_003_MVA_ST,
                                             M003_003_ALIQ_ST,
                                             M003_003_ALIQ_IPI,
                                             M003_003_ALIQ_II, 
                                             M003_003_ALIQ_PIS_IMP,
                                             M003_003_ALIQ_COF_IMP,
                                             M003_003_TIPO_RESTRICAO,
                                             M003_003_VALOR_RESTRICAO,
                                             M003_003_SALDO_ESTOQUE_DISP,
                                             M003_003_SALDO_PENDENCIA,
                                             M003_003_MEDIA_VENDA_DIA
                                      FROM M003_003_PRODUTO_SUGESTAO
                                      WHERE M003_003_UNIDADE_IE    <> P_UNIDADE
                                        AND M003_003_PRODUTO_IE    = P_PRODUTO
                                        AND M003_003_FORNECEDOR_IE = vFornecedor
                                        AND M003_003_DATA_RA_IE    = P_DATA)
        LOOP
          if vTotalRep > 0 then
            vPercRat := (vRegLojasCrossDocking.M003_003_SUGESTAO_FINAL / vTotalRep); 
          elsif vTotalSug > 0 then      
            vPercRat := (vRegLojasCrossDocking.M003_003_SUGESTAO_FINAL / vTotalSug); 
          else 
            vPercRat := 1 / vQtdUnidades;
          end if;
    
          vQtdeRat := vPercRat * P_QTDE;
    
          vQtde    := F_CALC_QTDE_EMB(vQtdeRat,
                                      vRegLojasCrossDocking.M003_003_EMBALAGEM_COMPRA,
                                      vRegLojasCrossDocking.M003_003_PERC_ARRED_EMB_PROD,
                                      vRegLojasCrossDocking.M003_003_TIPO_RESTRICAO,
                                      vRegLojasCrossDocking.M003_003_VALOR_RESTRICAO,
                                      vRegLojasCrossDocking.M003_003_SALDO_ESTOQUE_DISP,
                                      vRegLojasCrossDocking.M003_003_SALDO_PENDENCIA,
                                      vRegLojasCrossDocking.M003_003_MEDIA_VENDA_DIA
                                      );
                                      
          vValorFinal := F_CALC_VALOR( vQtde,
                                       vRegLojasCrossDocking.M003_003_PRECO_SUGESTAO,
                                       vRegLojasCrossDocking.DESCONTOS,
                                       vRegLojasCrossDocking.M003_003_TAXA_VENDOR,
                                       vRegLojasCrossDocking.M003_003_MVA_ST,
                                       vRegLojasCrossDocking.M003_003_ALIQ_ST,
                                       vRegLojasCrossDocking.M003_003_ALIQ_IPI,
                                       vRegLojasCrossDocking.M003_003_ALIQ_II,
                                       vRegLojasCrossDocking.M003_003_ALIQ_PIS_IMP,
                                       vRegLojasCrossDocking.M003_003_ALIQ_COF_IMP);
    
          IF PI_ATUALIZA_REGISTROS = 1 THEN
    
            UPDATE M003_003_PRODUTO_SUGESTAO
            SET M003_003_SUGESTAO_FINAL = vQtde,
                M003_003_VALOR_SUGESTAO_FINAL = vValorFinal
            WHERE M003_003_UNIDADE_IE    = vRegLojasCrossDocking.M003_003_UNIDADE_IE
              AND M003_003_PRODUTO_IE    = P_PRODUTO
              AND M003_003_FORNECEDOR_IE = vFornecedor
              AND M003_003_DATA_RA_IE    = P_DATA;
    
          END IF;
    
          vTotalLojas := vTotalLojas + vQtde;
          vValorTotalLojas := vValorTotalLojas + vValorFinal;
    
        END LOOP;
        
      ELSE
        FOR vRegLojasCrossDocking IN ( SELECT M03_016_UNIDADE_IE,
                               M03_016_EMBALAGEM_COMPRA, 
                               NVL(M03_016_PERC_ARRED_EMB_PROD,0) M03_016_PERC_ARRED_EMB_PROD, 
                               NVL(M03_016_ALIQ_IPI,0) M03_016_ALIQ_IPI, 
                               NVL(M03_016_MVA_ST, 0) M03_016_MVA_ST, 
                               NVL(M03_016_ALIQ_ST,0) M03_016_ALIQ_ST, 
                               NVL(M03_016_ALIQ_II,0) M03_016_ALIQ_II, 
                               NVL(M03_016_ALIQ_PIS_IMP,0) M03_016_ALIQ_PIS_IMP, 
                               NVL(M03_016_ALIQ_COF_IMP,0) M03_016_ALIQ_COF_IMP, 
                               NVL(M03_016_TAXA_VENDOR,0) M03_016_TAXA_VENDOR,
                               NVL(M03_016_PRECO_SUGESTAO,0) M03_016_PRECO_SUGESTAO,
                               case when vTotalRep = 0 then M03_016_SUGESTAO_FINAL else M03_016_REPOSICAO_UND end M03_016_REPOSICAO_UND,                               
                               NVL(M03_016_DESCONTO_ITEM,0) + NVL(M03_016_DESCONTO_REPASSE,0) + NVL(M03_016_DESCONTO_COMERCIAL,0) DESCONTOS,
                               M03_016_TIPO_RESTRICAO,
                               M03_016_VALOR_RESTRICAO,
                               M03_016_SALDO_ESTOQUE_DISP,
                               M03_016_SALDO_PENDENCIA,
                               M03_016_MEDIA_VENDA_DIA
                          FROM M03_016_OC_ESPECIAL
                          WHERE M03_016_DATA_SUG_IE   = P_DATA
                            AND M03_016_PRODUTO_IE    = P_PRODUTO
                            AND M03_016_FORNECEDOR_IE = vFornecedor
                            AND M03_016_ORDEM_SERVICO_IE = PI_OS
                            AND M03_016_UNIDADE_IE <> P_UNIDADE
        )
        LOOP      
          if vTotalRep > 0 then
            vPercRat := (vRegLojasCrossDocking.M03_016_REPOSICAO_UND / vTotalRep); 
          elsif vTotalSug > 0 then      
            vPercRat := (vRegLojasCrossDocking.M03_016_REPOSICAO_UND / vTotalSug); 
          else 
            vPercRat := 1 / vQtdUnidades;
          end if;
          
          IF vPercRat < 0 THEN
            vPercRat := 0;
          END IF;
    
          vQtdeRat := vPercRat * P_QTDE;
    
          vQtde    := F_CALC_QTDE_EMB(vQtdeRat,
                                      vRegLojasCrossDocking.M03_016_EMBALAGEM_COMPRA,
                                      vRegLojasCrossDocking.M03_016_PERC_ARRED_EMB_PROD,                                      
                                      vRegLojasCrossDocking.M03_016_TIPO_RESTRICAO,
                                      vRegLojasCrossDocking.M03_016_VALOR_RESTRICAO,
                                      vRegLojasCrossDocking.M03_016_SALDO_ESTOQUE_DISP,
                                      vRegLojasCrossDocking.M03_016_SALDO_PENDENCIA,
                                      vRegLojasCrossDocking.M03_016_MEDIA_VENDA_DIA
                                      );
                                      
          vValorFinal := F_CALC_VALOR( vQtde,
                                       vRegLojasCrossDocking.M03_016_PRECO_SUGESTAO,
                                       vRegLojasCrossDocking.DESCONTOS,
                                       vRegLojasCrossDocking.M03_016_TAXA_VENDOR,
                                       vRegLojasCrossDocking.M03_016_MVA_ST,
                                       vRegLojasCrossDocking.M03_016_ALIQ_ST,
                                       vRegLojasCrossDocking.M03_016_ALIQ_IPI,
                                       vRegLojasCrossDocking.M03_016_ALIQ_II,
                                       vRegLojasCrossDocking.M03_016_ALIQ_PIS_IMP,
                                       vRegLojasCrossDocking.M03_016_ALIQ_COF_IMP);
    
          IF PI_ATUALIZA_REGISTROS = 1 THEN
    
            UPDATE M03_016_OC_ESPECIAL
            SET M03_016_REPOSICAO_UND = vQtde,
                M03_016_REPOSICAO_FINAL = vValorFinal
            WHERE M03_016_UNIDADE_IE    = vRegLojasCrossDocking.M03_016_UNIDADE_IE
              AND M03_016_PRODUTO_IE    = P_PRODUTO
              AND M03_016_FORNECEDOR_IE = vFornecedor
              AND M03_016_DATA_SUG_IE   = P_DATA
              AND M03_016_ORDEM_SERVICO_IE = PI_OS;
    
          END IF;
    
          vTotalLojas := vTotalLojas + vQtde;
          vValorTotalLojas := vValorTotalLojas + vValorFinal;        
        END LOOP;
        
      END IF;
  
      P_ATUALIZA_QTDE( vTotalLojas,
                       vValorTotalLojas,
                       P_UNIDADE);
    END IF;

  END;

BEGIN
  vValorFinal := 0;
  
  --//Atualização das Lojas do CrossDocking
  IF PI_LOJA_CROSSDOCKING = 1 THEN
    
    IF PI_OC_ESPECIAL = 'N' THEN
    
      SELECT M003_003_EMBALAGEM_COMPRA,
             M003_003_PERC_ARRED_EMB_PROD,
             M003_003_FORNECEDOR_IE,
             M003_003_TIPO_RESTRICAO,
             M003_003_VALOR_RESTRICAO,
             M003_003_SALDO_ESTOQUE_DISP,
             M003_003_SALDO_PENDENCIA,
             M003_003_MEDIA_VENDA_DIA
        INTO vM03_012_EMBALAGEM_COMPRA,
             vM03_012_PERC_ARRED_EMB_PROD,
             vFornecedorCD,
             vM03_012_TIPO_RESTRICAO,
             vM03_012_VALOR_RESTRICAO,
             vM03_012_SALDO_ESTOQUE_DISP,
             vM03_012_SALDO_PENDENCIA,
             vM03_012_MEDIA_VENDA_DIA
      FROM M003_003_PRODUTO_SUGESTAO    
       WHERE M003_003_PRODUTO_IE = PI_PRODUTO
         AND M003_003_UNIDADE_IE = PI_UNIDADE
         AND M003_003_DATA_RA_IE = PI_DATA; 
    ELSE
      SELECT M03_016_EMBALAGEM_COMPRA,
             M03_016_PERC_ARRED_EMB_PROD,
             M03_016_FORNECEDOR_IE,
             M03_016_TIPO_RESTRICAO,
             M03_016_VALOR_RESTRICAO,
             M03_016_SALDO_ESTOQUE_DISP,
             M03_016_SALDO_PENDENCIA,
             M03_016_MEDIA_VENDA_DIA
        INTO vM03_012_EMBALAGEM_COMPRA,
             vM03_012_PERC_ARRED_EMB_PROD,
             vFornecedorCD,
             vM03_012_TIPO_RESTRICAO,
             vM03_012_VALOR_RESTRICAO,
             vM03_012_SALDO_ESTOQUE_DISP,
             vM03_012_SALDO_PENDENCIA,
             vM03_012_MEDIA_VENDA_DIA
      FROM M03_016_OC_ESPECIAL
       WHERE M03_016_PRODUTO_IE       = PI_PRODUTO
         AND M03_016_UNIDADE_IE       = PI_UNIDADE
         AND M03_016_DATA_SUG_IE      = PI_DATA
         AND M03_016_ORDEM_SERVICO_IE = PI_OS;    
    END IF;
  
    vQtdeFinal := F_CALC_QTDE_EMB(PI_QTDE_PEDIDA,
                                  vM03_012_EMBALAGEM_COMPRA,
                                  vM03_012_PERC_ARRED_EMB_PROD,                                  
                                  vM03_012_TIPO_RESTRICAO,
                                  vM03_012_VALOR_RESTRICAO,
                                  vM03_012_SALDO_ESTOQUE_DISP,
                                  vM03_012_SALDO_PENDENCIA,
                                  vM03_012_MEDIA_VENDA_DIA
                                  );
    IF PI_OC_ESPECIAL = 'N' THEN
      UPDATE M003_003_PRODUTO_SUGESTAO
         SET M003_003_SUGESTAO_FINAL = vQtdeFinal
       WHERE M003_003_PRODUTO_IE = PI_PRODUTO 
         AND M003_003_UNIDADE_IE = PI_UNIDADE 
         AND M003_003_DATA_RA_IE = PI_DATA;
    ELSE
      UPDATE M03_016_OC_ESPECIAL
      SET M03_016_REPOSICAO_UND = vQtdeFinal
       WHERE M03_016_PRODUTO_IE       = PI_PRODUTO
         AND M03_016_UNIDADE_IE       = PI_UNIDADE
         AND M03_016_DATA_SUG_IE      = PI_DATA
         AND M03_016_ORDEM_SERVICO_IE = PI_OS;
    END IF;
       
    --PI_FORNECEDOR
    SELECT T908_UNIDADE_IE 
      INTO vUnidadeCD
    FROM T908_PARAM_EMPRESA 
    WHERE T908_FORNECEDOR_EMPRESA_E = vFornecedorCD;

    IF PI_OC_ESPECIAL = 'N' THEN
      SELECT sum(M003_003_SUGESTAO_FINAL)
        INTO vQtdeFinal
      FROM M003_003_PRODUTO_SUGESTAO
      WHERE M003_003_PRODUTO_IE = PI_PRODUTO
         AND M003_003_UNIDADE_IE <> vUnidadeCD
         AND M003_003_DATA_RA_IE = PI_DATA;
    ELSE
      SELECT sum(M03_016_REPOSICAO_UND)
        INTO vQtdeFinal
      FROM M03_016_OC_ESPECIAL
      WHERE M03_016_PRODUTO_IE       = PI_PRODUTO
        AND M03_016_UNIDADE_IE       <> vUnidadeCD
        AND M03_016_DATA_SUG_IE      = PI_DATA
        AND M03_016_ORDEM_SERVICO_IE = PI_OS; 
    END IF;
    
    vValorFinal := I_MDLOG.F_CALC_QTDE_EMB(vUnidadeCD,
                            PI_PRODUTO,
                            PI_FORNECEDOR,
                            PI_DATA,
                            vQtdeFinal,
                            PI_ATUALIZA_REGISTROS,
                            PI_OC_ESPECIAL,
                            PI_OS,
                            0);
  ELSE 
  
    IF NVL(PI_UNIDADE,0) > 0 THEN
      
      P_CARREGA_DADOS_FORNEC_LOJA(PI_FORNECEDOR, PI_UNIDADE);
      
      IF PI_OC_ESPECIAL = 'N' THEN                   
    
        SELECT M03_012_EMBALAGEM_COMPRA, 
               M03_012_PERC_ARRED_EMB_PROD, 
               NVL(M03_012_ALIQ_IPI,0), 
               NVL(M03_012_MVA_ST,0), 
               NVL(M03_012_ALIQ_ST,0),
               NVL(M03_012_ALIQ_II,0), 
               NVL(M03_012_ALIQ_PIS_IMP,0),  
               NVL(M03_012_ALIQ_COF_IMP,0),  
               NVL(M03_012_TAXA_VENDOR,0), 
               NVL(M03_012_PRECO_SUGESTAO,0),
               NVL(M03_012_DESCONTO_ITEM,0) + NVL(M03_012_DESCONTO_REPASSE,0) + NVL(M03_012_DESCONTO_COMERCIAL,0),
               M03_012_FORMA_ENTREGA,
               M03_012_TIPO_RESTRICAO,
               M03_012_VALOR_RESTRICAO,
               M03_012_SALDO_ESTOQUE_DISP,
               M03_012_SALDO_PENDENCIA,
               M03_012_MEDIA_VENDA_DIA
          INTO vM03_012_EMBALAGEM_COMPRA, 
               vM03_012_PERC_ARRED_EMB_PROD, 
               vM03_012_ALIQ_IPI, 
               vM03_012_MVA_ST, 
               vM03_012_ALIQ_ST, 
               vM03_012_ALIQ_II, 
               vM03_012_ALIQ_PIS_IMP, 
               vM03_012_ALIQ_COF_IMP, 
               vM03_012_TAXA_VENDOR,
               vM03_012_PRECO_SUGESTAO,
               vDescontos,
               vM03_012_FORMA_ENTREGA,
               vM03_012_TIPO_RESTRICAO,
               vM03_012_VALOR_RESTRICAO,
               vM03_012_SALDO_ESTOQUE_DISP,
               vM03_012_SALDO_PENDENCIA,
               vM03_012_MEDIA_VENDA_DIA
        FROM M03_012_SUG_COMPRA_DIA
        WHERE M03_012_DATA_SUG_IE   = PI_DATA
          AND M03_012_UNIDADE_IE    = PI_UNIDADE
          AND M03_012_PRODUTO_IE    = PI_PRODUTO
          AND M03_012_FORNECEDOR_IE = PI_FORNECEDOR
          AND M03_012_ORDEM_SERVICO_IE = PI_OS;
      
      ELSE
      
        SELECT M03_016_EMBALAGEM_COMPRA, 
               M03_016_PERC_ARRED_EMB_PROD, 
               NVL(M03_016_ALIQ_IPI,0), 
               NVL(M03_016_MVA_ST,0), 
               NVL(M03_016_ALIQ_ST,0),
               NVL(M03_016_ALIQ_II,0), 
               NVL(M03_016_ALIQ_PIS_IMP,0),  
               NVL(M03_016_ALIQ_COF_IMP,0),  
               NVL(M03_016_TAXA_VENDOR,0), 
               NVL(M03_016_PRECO_SUGESTAO,0),
               NVL(M03_016_DESCONTO_ITEM,0) + NVL(M03_016_DESCONTO_REPASSE,0) + NVL(M03_016_DESCONTO_COMERCIAL,0),
               M03_016_FORMA_ENTREGA,               
               M03_016_TIPO_RESTRICAO,
               M03_016_VALOR_RESTRICAO,
               M03_016_SALDO_ESTOQUE_DISP,
               M03_016_SALDO_PENDENCIA,
               M03_016_MEDIA_VENDA_DIA
          INTO vM03_012_EMBALAGEM_COMPRA, 
               vM03_012_PERC_ARRED_EMB_PROD, 
               vM03_012_ALIQ_IPI, 
               vM03_012_MVA_ST, 
               vM03_012_ALIQ_ST, 
               vM03_012_ALIQ_II, 
               vM03_012_ALIQ_PIS_IMP, 
               vM03_012_ALIQ_COF_IMP, 
               vM03_012_TAXA_VENDOR,
               vM03_012_PRECO_SUGESTAO,
               vDescontos,
               vM03_012_FORMA_ENTREGA,
               vM03_012_TIPO_RESTRICAO,
               vM03_012_VALOR_RESTRICAO,
               vM03_012_SALDO_ESTOQUE_DISP,
               vM03_012_SALDO_PENDENCIA,
               vM03_012_MEDIA_VENDA_DIA
        FROM M03_016_OC_ESPECIAL
        WHERE M03_016_DATA_SUG_IE   = PI_DATA
          AND M03_016_UNIDADE_IE    = PI_UNIDADE
          AND M03_016_PRODUTO_IE    = PI_PRODUTO
          AND M03_016_FORNECEDOR_IE = PI_FORNECEDOR
          AND M03_016_ORDEM_SERVICO_IE = PI_OS;
      
      END IF;        
      
      vQtdeFinal := F_CALC_QTDE_EMB(PI_QTDE_PEDIDA,
                                    vM03_012_EMBALAGEM_COMPRA,
                                    vM03_012_PERC_ARRED_EMB_PROD,
                                    vM03_012_TIPO_RESTRICAO,
                                    vM03_012_VALOR_RESTRICAO,
                                    vM03_012_SALDO_ESTOQUE_DISP,
                                    vM03_012_SALDO_PENDENCIA,
                                    vM03_012_MEDIA_VENDA_DIA
                                    );
  
      vValorFinal := F_CALC_VALOR( vQtdeFinal,
                                   vM03_012_PRECO_SUGESTAO,
                                   vDescontos,
                                   vM03_012_TAXA_VENDOR,                        
                                   vM03_012_MVA_ST,
                                   vM03_012_ALIQ_ST,
                                   vM03_012_ALIQ_IPI,
                                   vM03_012_ALIQ_II, 
                                   vM03_012_ALIQ_PIS_IMP,
                                   vM03_012_ALIQ_COF_IMP);
      IF PI_ATUALIZA_REGISTROS = 1 THEN
        P_ATUALIZA_QTDE(vQtdeFinal,
                        vValorFinal,
                        PI_UNIDADE);   
        IF vM03_012_FORMA_ENTREGA = 'C' THEN
          P_ATUALIZA_QTDE_CROSS_DOCKING(PI_PRODUTO,
                                        PI_FORNECEDOR,
                                        PI_UNIDADE,
                                        PI_DATA,
                                        vQtdeFinal);
        END IF;
      END IF;           

    ELSE

      IF PI_OC_ESPECIAL = 'N' THEN 

        SELECT NVL(SUM(M03_012_SUGESTAO_FINAL),0), NVL(SUM(M03_012_REPOSICAO_UND),0)
          INTO vTotalSug, vTotalRep
        FROM M03_012_SUG_COMPRA_DIA
        WHERE M03_012_DATA_SUG_IE   = PI_DATA
          AND M03_012_PRODUTO_IE    = PI_PRODUTO
          AND M03_012_FORNECEDOR_IE = PI_FORNECEDOR          
          AND M03_012_ORDEM_SERVICO_IE = PI_OS;      
        
        vTotalProporcao := vTotalRep;
        if vTotalProporcao <= 0 then
          vTotalProporcao := vTotalSug;
          if vTotalProporcao <= 0 then
            vTotalProporcao := 0;
          end if;
        end if;

        IF vTotalProporcao > 0 THEN
          FOR VREG IN ( SELECT M03_012_UNIDADE_IE,
                               M03_012_EMBALAGEM_COMPRA, 
                               NVL(M03_012_PERC_ARRED_EMB_PROD,0) M03_012_PERC_ARRED_EMB_PROD, 
                               NVL(M03_012_ALIQ_IPI,0) M03_012_ALIQ_IPI, 
                               NVL(M03_012_MVA_ST, 0) M03_012_MVA_ST, 
                               NVL(M03_012_ALIQ_ST,0) M03_012_ALIQ_ST, 
                               NVL(M03_012_ALIQ_II,0) M03_012_ALIQ_II, 
                               NVL(M03_012_ALIQ_PIS_IMP,0) M03_012_ALIQ_PIS_IMP, 
                               NVL(M03_012_ALIQ_COF_IMP,0) M03_012_ALIQ_COF_IMP, 
                               NVL(M03_012_TAXA_VENDOR,0) M03_012_TAXA_VENDOR,
                               NVL(M03_012_PRECO_SUGESTAO,0) M03_012_PRECO_SUGESTAO,
                               NVL(M03_012_SUGESTAO_FINAL,0) M03_012_SUGESTAO_FINAL,
                               NVL(M03_012_REPOSICAO_UND,0) M03_012_REPOSICAO_UND,
                               NVL(M03_012_DESCONTO_ITEM,0) + NVL(M03_012_DESCONTO_REPASSE,0) + NVL(M03_012_DESCONTO_COMERCIAL,0) DESCONTOS,
                               M03_012_FORMA_ENTREGA,
                               M03_012_TIPO_RESTRICAO,
                               M03_012_VALOR_RESTRICAO,
                               M03_012_SALDO_ESTOQUE_DISP,
                               M03_012_SALDO_PENDENCIA,
                               M03_012_MEDIA_VENDA_DIA
                          FROM M03_012_SUG_COMPRA_DIA
                          WHERE M03_012_DATA_SUG_IE   = PI_DATA
                            AND M03_012_PRODUTO_IE    = PI_PRODUTO
                            AND M03_012_FORNECEDOR_IE = PI_FORNECEDOR                            
                            AND M03_012_ORDEM_SERVICO_IE = PI_OS
          )
          LOOP
            
            P_CARREGA_DADOS_FORNEC_LOJA(PI_FORNECEDOR, VREG.M03_012_UNIDADE_IE);              
            
            if vTotalRep > 0 then
              vPercRateio := (VREG.M03_012_REPOSICAO_UND / vTotalProporcao); 
            else            
              vPercRateio := (VREG.M03_012_SUGESTAO_FINAL / vTotalProporcao); 
            end if;
            
            vQtdeAux    := vPercRateio * PI_QTDE_PEDIDA;
            
            vQtdeFinal  := F_CALC_QTDE_EMB(vQtdeAux,
                                           VREG.M03_012_EMBALAGEM_COMPRA,
                                           VREG.M03_012_PERC_ARRED_EMB_PROD,                                           
                                           VREG.M03_012_TIPO_RESTRICAO,
                                           VREG.M03_012_VALOR_RESTRICAO,
                                           VREG.M03_012_SALDO_ESTOQUE_DISP,
                                           VREG.M03_012_SALDO_PENDENCIA,
                                           VREG.M03_012_MEDIA_VENDA_DIA
                                           );
  
            vValorAux := F_CALC_VALOR( vQtdeFinal,
                                       VREG.M03_012_PRECO_SUGESTAO,
                                       VREG.DESCONTOS,
                                       NVL(VREG.M03_012_TAXA_VENDOR,0),                        
                                       NVL(VREG.M03_012_MVA_ST,0),
                                       NVL(VREG.M03_012_ALIQ_ST,0),
                                       NVL(VREG.M03_012_ALIQ_IPI,0),
                                       NVL(VREG.M03_012_ALIQ_II,0), 
                                       NVL(VREG.M03_012_ALIQ_PIS_IMP,0),
                                       NVL(VREG.M03_012_ALIQ_COF_IMP,0));
                                       
            vValorFinal := vValorFinal + vValorAux;                                
            IF PI_ATUALIZA_REGISTROS = 1 THEN
              
              P_ATUALIZA_QTDE(vQtdeFinal,
                              vValorAux,
                              VREG.M03_012_UNIDADE_IE);
            
              if VREG.M03_012_FORMA_ENTREGA = 'C' then          
                P_ATUALIZA_QTDE_CROSS_DOCKING(PI_PRODUTO,
                                              PI_FORNECEDOR,
                                              VREG.M03_012_UNIDADE_IE,
                                              PI_DATA,
                                              vQtdeFinal);
              END IF;
  
            END IF;
  
          END LOOP;
        
        ELSE
        
          SELECT COUNT(DISTINCT(M03_012_UNIDADE_IE))
            INTO vQtdUnidades
          FROM M03_012_SUG_COMPRA_DIA
          WHERE M03_012_DATA_SUG_IE   = PI_DATA
            AND M03_012_PRODUTO_IE    = PI_PRODUTO
            AND M03_012_FORNECEDOR_IE = PI_FORNECEDOR
            AND M03_012_ORDEM_SERVICO_IE = PI_OS;       
  
          FOR VREG IN ( SELECT M03_012_UNIDADE_IE,
                               M03_012_EMBALAGEM_COMPRA, 
                               NVL(M03_012_PERC_ARRED_EMB_PROD,0) M03_012_PERC_ARRED_EMB_PROD, 
                               NVL(M03_012_ALIQ_IPI,0) M03_012_ALIQ_IPI, 
                               NVL(M03_012_MVA_ST, 0) M03_012_MVA_ST, 
                               NVL(M03_012_ALIQ_ST,0) M03_012_ALIQ_ST, 
                               NVL(M03_012_ALIQ_II,0) M03_012_ALIQ_II, 
                               NVL(M03_012_ALIQ_PIS_IMP,0) M03_012_ALIQ_PIS_IMP, 
                               NVL(M03_012_ALIQ_COF_IMP,0) M03_012_ALIQ_COF_IMP, 
                               NVL(M03_012_TAXA_VENDOR,0) M03_012_TAXA_VENDOR,
                               NVL(M03_012_PRECO_SUGESTAO,0) M03_012_PRECO_SUGESTAO,
                               NVL(M03_012_SUGESTAO_FINAL,0) M03_012_SUGESTAO_FINAL,
                               NVL(M03_012_REPOSICAO_UND,0) M03_012_REPOSICAO_UND,
                               NVL(M03_012_DESCONTO_ITEM,0) + NVL(M03_012_DESCONTO_REPASSE,0) + NVL(M03_012_DESCONTO_COMERCIAL,0) DESCONTOS,
                               M03_012_FORMA_ENTREGA,
                               M03_012_TIPO_RESTRICAO,
                               M03_012_VALOR_RESTRICAO,
                               M03_012_SALDO_ESTOQUE_DISP,
                               M03_012_SALDO_PENDENCIA,
                               M03_012_MEDIA_VENDA_DIA
                          FROM M03_012_SUG_COMPRA_DIA
                          WHERE M03_012_DATA_SUG_IE   = PI_DATA
                            AND M03_012_PRODUTO_IE    = PI_PRODUTO
                            AND M03_012_FORNECEDOR_IE = PI_FORNECEDOR
                            AND M03_012_ORDEM_SERVICO_IE = PI_OS
          )
          LOOP
            
            P_CARREGA_DADOS_FORNEC_LOJA(PI_FORNECEDOR, VREG.M03_012_UNIDADE_IE);      
            
            vQtdeAux    :=  PI_QTDE_PEDIDA / vQtdUnidades;
            
            vQtdeFinal  := F_CALC_QTDE_EMB(vQtdeAux,
                                           VREG.M03_012_EMBALAGEM_COMPRA,
                                           VREG.M03_012_PERC_ARRED_EMB_PROD,
                                           VREG.M03_012_TIPO_RESTRICAO,
                                           VREG.M03_012_VALOR_RESTRICAO,
                                           VREG.M03_012_SALDO_ESTOQUE_DISP,
                                           VREG.M03_012_SALDO_PENDENCIA,
                                           VREG.M03_012_MEDIA_VENDA_DIA);
  
            vValorAux := F_CALC_VALOR( vQtdeFinal,
                                       VREG.M03_012_PRECO_SUGESTAO,
                                       VREG.DESCONTOS,
                                       NVL(VREG.M03_012_TAXA_VENDOR,0),                        
                                       NVL(VREG.M03_012_MVA_ST,0),
                                       NVL(VREG.M03_012_ALIQ_ST,0),
                                       NVL(VREG.M03_012_ALIQ_IPI,0),
                                       NVL(VREG.M03_012_ALIQ_II,0), 
                                       NVL(VREG.M03_012_ALIQ_PIS_IMP,0),
                                       NVL(VREG.M03_012_ALIQ_COF_IMP,0));
                                       
            vValorFinal := vValorFinal + ROUND(vValorAux,2);                                
            IF PI_ATUALIZA_REGISTROS = 1 THEN
              P_ATUALIZA_QTDE(vQtdeFinal,
                              vValorFinal,
                              VREG.M03_012_UNIDADE_IE);      
              if VREG.M03_012_FORMA_ENTREGA = 'C' then          
                P_ATUALIZA_QTDE_CROSS_DOCKING(PI_PRODUTO,
                                              PI_FORNECEDOR,
                                              VREG.M03_012_UNIDADE_IE,
                                              PI_DATA,
                                              vQtdeFinal);
              END IF;                          
            END IF;
  
          END LOOP;      
        
        END IF;
         
      ELSE--//OC ESPECIAL...

        SELECT NVL(SUM(M03_016_SUGESTAO_FINAL),0), NVL(SUM(M03_016_REPOSICAO_UND),0)
          INTO vTotalSug, vTotalRep
        FROM M03_016_OC_ESPECIAL
        WHERE M03_016_DATA_SUG_IE   = PI_DATA
          AND M03_016_PRODUTO_IE    = PI_PRODUTO
          AND M03_016_FORNECEDOR_IE = PI_FORNECEDOR          
          AND M03_016_ORDEM_SERVICO_IE = PI_OS;      
        
        vTotalProporcao := vTotalRep;
        if vTotalProporcao <= 0 then
          vTotalProporcao := vTotalSug;
          if vTotalProporcao <= 0 then
            vTotalProporcao := 0;
          end if;
        end if;          
          

        IF vTotalProporcao > 0 THEN
          FOR VREG IN ( SELECT M03_016_UNIDADE_IE M03_012_UNIDADE_IE,
                               M03_016_EMBALAGEM_COMPRA M03_012_EMBALAGEM_COMPRA, 
                               NVL(M03_016_PERC_ARRED_EMB_PROD,0) M03_012_PERC_ARRED_EMB_PROD, 
                               NVL(M03_016_ALIQ_IPI,0) M03_012_ALIQ_IPI, 
                               NVL(M03_016_MVA_ST, 0) M03_012_MVA_ST, 
                               NVL(M03_016_ALIQ_ST,0) M03_012_ALIQ_ST, 
                               NVL(M03_016_ALIQ_II,0) M03_012_ALIQ_II, 
                               NVL(M03_016_ALIQ_PIS_IMP,0) M03_012_ALIQ_PIS_IMP, 
                               NVL(M03_016_ALIQ_COF_IMP,0) M03_012_ALIQ_COF_IMP, 
                               NVL(M03_016_TAXA_VENDOR,0) M03_012_TAXA_VENDOR,
                               NVL(M03_016_PRECO_SUGESTAO,0) M03_012_PRECO_SUGESTAO,
                               NVL(M03_016_SUGESTAO_FINAL,0) M03_012_SUGESTAO_FINAL,
                               NVL(M03_016_REPOSICAO_UND,0) M03_012_REPOSICAO_UND,
                               NVL(M03_016_DESCONTO_ITEM,0) + NVL(M03_016_DESCONTO_REPASSE,0) + NVL(M03_016_DESCONTO_COMERCIAL,0) DESCONTOS,
                               M03_016_FORMA_ENTREGA,
                               M03_016_TIPO_RESTRICAO M03_012_TIPO_RESTRICAO,
                               M03_016_VALOR_RESTRICAO M03_012_VALOR_RESTRICAO,
                               M03_016_SALDO_ESTOQUE_DISP M03_012_SALDO_ESTOQUE_DISP,
                               M03_016_SALDO_PENDENCIA M03_012_SALDO_PENDENCIA,
                               M03_016_MEDIA_VENDA_DIA M03_012_MEDIA_VENDA_DIA
                          FROM M03_016_OC_ESPECIAL
                          WHERE M03_016_DATA_SUG_IE   = PI_DATA
                            AND M03_016_PRODUTO_IE    = PI_PRODUTO
                            AND M03_016_FORNECEDOR_IE = PI_FORNECEDOR                            
                            AND M03_016_ORDEM_SERVICO_IE = PI_OS
          )
          LOOP

            P_CARREGA_DADOS_FORNEC_LOJA(PI_FORNECEDOR, VREG.M03_012_UNIDADE_IE);

            if vTotalRep > 0 then
              vPercRateio := (VREG.M03_012_REPOSICAO_UND / vTotalProporcao); 
            else            
              vPercRateio := (VREG.M03_012_SUGESTAO_FINAL / vTotalProporcao); 
            end if;

            vQtdeAux    := vPercRateio * PI_QTDE_PEDIDA;

            vQtdeFinal  := F_CALC_QTDE_EMB(vQtdeAux,
                                           VREG.M03_012_EMBALAGEM_COMPRA,
                                           VREG.M03_012_PERC_ARRED_EMB_PROD,
                                           VREG.M03_012_TIPO_RESTRICAO,
                                           VREG.M03_012_VALOR_RESTRICAO,
                                           VREG.M03_012_SALDO_ESTOQUE_DISP,
                                           VREG.M03_012_SALDO_PENDENCIA,
                                           VREG.M03_012_MEDIA_VENDA_DIA);

            vValorAux := F_CALC_VALOR( vQtdeFinal,
                                       VREG.M03_012_PRECO_SUGESTAO,
                                       VREG.DESCONTOS,
                                       NVL(VREG.M03_012_TAXA_VENDOR,0),                        
                                       NVL(VREG.M03_012_MVA_ST,0),
                                       NVL(VREG.M03_012_ALIQ_ST,0),
                                       NVL(VREG.M03_012_ALIQ_IPI,0),
                                       NVL(VREG.M03_012_ALIQ_II,0), 
                                       NVL(VREG.M03_012_ALIQ_PIS_IMP,0),
                                       NVL(VREG.M03_012_ALIQ_COF_IMP,0));

            vValorFinal := vValorFinal + vValorAux;                                
            IF PI_ATUALIZA_REGISTROS = 1 THEN
              UPDATE M03_016_OC_ESPECIAL
              SET M03_016_REPOSICAO_UND   = vQtdeFinal,
                  M03_016_REPOSICAO_FINAL = vValorFinal
              WHERE M03_016_DATA_SUG_IE   = PI_DATA
                AND M03_016_PRODUTO_IE    = PI_PRODUTO
                AND M03_016_FORNECEDOR_IE = PI_FORNECEDOR
                AND M03_016_UNIDADE_IE    = VREG.M03_012_UNIDADE_IE;            
            END IF;

            IF PI_ATUALIZA_REGISTROS = 1 THEN
              P_ATUALIZA_QTDE(vQtdeFinal,
                              vValorFinal,
                              VREG.M03_012_UNIDADE_IE);      
              if VREG.M03_016_FORMA_ENTREGA = 'C' then          
                P_ATUALIZA_QTDE_CROSS_DOCKING(PI_PRODUTO,
                                              PI_FORNECEDOR,
                                              VREG.M03_012_UNIDADE_IE,
                                              PI_DATA,
                                              vQtdeFinal);
              END IF;                          
            END IF;            

          END LOOP;

        ELSE

          SELECT COUNT(DISTINCT(M03_016_UNIDADE_IE))
            INTO vQtdUnidades
          FROM M03_016_OC_ESPECIAL
          WHERE M03_016_DATA_SUG_IE   = PI_DATA
            AND M03_016_PRODUTO_IE    = PI_PRODUTO
            AND M03_016_FORNECEDOR_IE = PI_FORNECEDOR
            AND M03_016_ORDEM_SERVICO_IE = PI_OS;
          
          FOR VREG IN ( SELECT M03_016_UNIDADE_IE M03_012_UNIDADE_IE,
                               M03_016_EMBALAGEM_COMPRA M03_012_EMBALAGEM_COMPRA, 
                               NVL(M03_016_PERC_ARRED_EMB_PROD,0) M03_012_PERC_ARRED_EMB_PROD, 
                               NVL(M03_016_ALIQ_IPI,0) M03_012_ALIQ_IPI, 
                               NVL(M03_016_MVA_ST, 0) M03_012_MVA_ST, 
                               NVL(M03_016_ALIQ_ST,0) M03_012_ALIQ_ST, 
                               NVL(M03_016_ALIQ_II,0) M03_012_ALIQ_II, 
                               NVL(M03_016_ALIQ_PIS_IMP,0) M03_012_ALIQ_PIS_IMP, 
                               NVL(M03_016_ALIQ_COF_IMP,0) M03_012_ALIQ_COF_IMP, 
                               NVL(M03_016_TAXA_VENDOR,0) M03_012_TAXA_VENDOR,
                               NVL(M03_016_PRECO_SUGESTAO,0) M03_012_PRECO_SUGESTAO,
                               NVL(M03_016_SUGESTAO_FINAL,0) M03_012_SUGESTAO_FINAL,
                               NVL(M03_016_REPOSICAO_UND,0) M03_012_REPOSICAO_UND,
                               NVL(M03_016_DESCONTO_ITEM,0) + NVL(M03_016_DESCONTO_REPASSE,0) + NVL(M03_016_DESCONTO_COMERCIAL,0) DESCONTOS,
                               M03_016_FORMA_ENTREGA,
                               M03_016_TIPO_RESTRICAO M03_012_TIPO_RESTRICAO,
                               M03_016_VALOR_RESTRICAO M03_012_VALOR_RESTRICAO,
                               M03_016_SALDO_ESTOQUE_DISP M03_012_SALDO_ESTOQUE_DISP,
                               M03_016_SALDO_PENDENCIA M03_012_SALDO_PENDENCIA,
                               M03_016_MEDIA_VENDA_DIA M03_012_MEDIA_VENDA_DIA
                          FROM M03_016_OC_ESPECIAL
                          WHERE M03_016_DATA_SUG_IE   = PI_DATA
                            AND M03_016_PRODUTO_IE    = PI_PRODUTO
                            AND M03_016_FORNECEDOR_IE = PI_FORNECEDOR
                            AND M03_016_ORDEM_SERVICO_IE = PI_OS
          )
          LOOP
            P_CARREGA_DADOS_FORNEC_LOJA(PI_FORNECEDOR, VREG.M03_012_UNIDADE_IE);                   
          
            vQtdeAux    :=  PI_QTDE_PEDIDA / vQtdUnidades;
            
            vQtdeFinal  := F_CALC_QTDE_EMB(vQtdeAux,
                                           VREG.M03_012_EMBALAGEM_COMPRA,
                                           VREG.M03_012_PERC_ARRED_EMB_PROD,
                                           VREG.M03_012_TIPO_RESTRICAO,
                                           VREG.M03_012_VALOR_RESTRICAO,
                                           VREG.M03_012_SALDO_ESTOQUE_DISP,
                                           VREG.M03_012_SALDO_PENDENCIA,
                                           VREG.M03_012_MEDIA_VENDA_DIA);
  
            vValorAux := F_CALC_VALOR( vQtdeFinal,
                                       VREG.M03_012_PRECO_SUGESTAO,
                                       VREG.DESCONTOS,
                                       NVL(VREG.M03_012_TAXA_VENDOR,0),                        
                                       NVL(VREG.M03_012_MVA_ST,0),
                                       NVL(VREG.M03_012_ALIQ_ST,0),
                                       NVL(VREG.M03_012_ALIQ_IPI,0),
                                       NVL(VREG.M03_012_ALIQ_II,0), 
                                       NVL(VREG.M03_012_ALIQ_PIS_IMP,0),
                                       NVL(VREG.M03_012_ALIQ_COF_IMP,0));
                                       
            vValorFinal := vValorFinal + ROUND(vValorAux,2);            
            
            IF PI_ATUALIZA_REGISTROS = 1 THEN
              P_ATUALIZA_QTDE(vQtdeFinal,
                              vValorFinal,
                              VREG.M03_012_UNIDADE_IE);      
              if VREG.M03_016_FORMA_ENTREGA = 'C' then          
                P_ATUALIZA_QTDE_CROSS_DOCKING(PI_PRODUTO,
                                              PI_FORNECEDOR,
                                              VREG.M03_012_UNIDADE_IE,
                                              PI_DATA,
                                              vQtdeFinal);
              END IF;                          
            END IF;             
  
          END LOOP;      
        
        END IF;    
      
      END IF;           
  
    END IF;
    
  END IF;

  IF PI_ATUALIZA_REGISTROS = 1 THEN  
    COMMIT;
  END IF;
  
  return vValorFinal;

END;
/
