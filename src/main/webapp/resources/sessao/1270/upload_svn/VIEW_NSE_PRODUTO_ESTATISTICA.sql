CREATE OR REPLACE FORCE VIEW I_MDLOG.VIEW_NSE_PRODUTO_ESTATISTICA (PBM,
                                                                   UNIDADE,
                                                                   PRODUTO,
                                                                   ANO_MES,
                                                                   QTD_COMPRA,
                                                                   VALOR_COMPRA,
                                                                   PRECO_COMPRA,
                                                                   QTD_VENDA,
                                                                   VALOR_VENDA,
                                                                   CMV,
                                                                   ANULACOES,
                                                                   MARGEM
                                                                  )
AS
SELECT 0 PBM, UNIDADE, PRODUTO, ANO_MES, NVL (QTD_COMPRA, 0) QTD_COMPRA,
       NVL (VALOR_COMPRA, 0) VALOR_COMPRA, NVL (PRECO_COMPRA, 0) PRECO_COMPRA,
       NVL (QTD_VENDA, 0) QTD_VENDA, NVL (VALOR_VENDA, 0) VALOR_VENDA,
       NVL (CMV, 0) CMV, NVL (ANULACOES, 0) ANULACOES,
       (NVL (VALOR_VENDA, 0) - (NVL (CMV, 0) + NVL (ANULACOES, 0))) MARGEM
  FROM (SELECT T079_UNIDADE_IE UNIDADE, T079_PRODUTO_IE PRODUTO,
               T079_ANOMES_IU ANO_MES, T079_COMPRA_UNIDADE QTD_COMPRA,
               T079_COMPRA_VALOR VALOR_COMPRA,
               CASE
                  WHEN T079_COMPRA_UNIDADE > 0
                     THEN (T079_COMPRA_VALOR / T079_COMPRA_UNIDADE
                          )
                  ELSE 0
               END PRECO_COMPRA,
               T079_VENDA_UNIDADE QTD_VENDA, T079_VENDA_VALOR VALOR_VENDA,
               (SELECT SUM (NVL (T128_CUSTO_COMPRA, 0) * T128_QTDE_VENDIDA)
                  FROM T128_SAIDA_ITENS
                 WHERE T128_UNIDADE_IE = T079_UNIDADE_IE
                   AND T128_PRODUTO_IE = T079_PRODUTO_IE
                   AND T128_TIPO_NOTAFISC_IE = 'A'
                   AND T128_DATA_EMISSAO_IE BETWEEN ('01/'||SUBSTR(T079_ANOMES_IU,5,2)||'/'||SUBSTR(T079_ANOMES_IU,1,4))
                                       AND LAST_DAY ('01/'||SUBSTR(T079_ANOMES_IU,5,2)||'/'||SUBSTR(T079_ANOMES_IU,1,4))) CMV,
                ( SELECT SUM(T078_VALOR_LIQUIDO - (NVL(T078_CUSTO_COMPRA,0)*T078_QTDE_PRODUTO))
                     FROM T078_ENTRADA_ITENS
                     JOIN T013_NATUREZA_OPERACAO ON
                       T013_NATUREZA_OPERACAO_IU = T078_NATUREZA_OPERACAO_E
                   WHERE T013_TIPO_CLASSIFICACAO_FISCAL = 3 --DEVOLU��O
                     AND  T078_UNIDADE_IE = T079_UNIDADE_IE
                     AND T078_PRODUTO_IE  = T079_PRODUTO_IE
                     AND TO_CHAR(T078_DATA_MOVIMENTO,'YYYYMM') = T079_ANOMES_IU              
                 ) ANULACOES
          FROM T079_PRODUTO_UNIDADE_ESTAT, T003_UNIDADE, T076_PRODUTO
         WHERE T079_UNIDADE_IE = T003_UNIDADE_IU
           AND T003_UNIDADE_ATIVA = 'S'
           AND T076_PRODUTO_IU = T079_PRODUTO_IE           
           AND T076_TIPO_UTILIZACAO = 'C' 
           AND T079_ANOMES_IU >= TO_CHAR (ADD_MONTHS (SYSDATE, -13), 'RRRRMM'));
                         