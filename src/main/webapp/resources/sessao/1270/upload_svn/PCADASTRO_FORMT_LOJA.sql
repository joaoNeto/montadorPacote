create or replace PROCEDURE I_MDLOG.PCADASTRO_FORMT_LOJA(PI_CODFORMATO_LOJA          NUMBER, 
													PI_DESCRICAO               VARCHAR2,
													PI_OPERACAO                CHAR,
													STATUS OUT VARCHAR2
													) IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  PI_CONTROLE_ANVISA  NUMBER(10);
  PI_DIGITACOES_VALIDAS  CHAR(4);
  
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_FORMT_LOJA
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DO FORMATO LOJA.
  AUTOR     : Luis Eduardo Pinheiro Pedrosa
  DATA      : 03/11/2016
  CRC       : 
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN
	STATUS     := 'PR';
	PI_CONTROLE_ANVISA := NULL;
	PI_DIGITACOES_VALIDAS := '1234';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT INTO DBAMDATA.T031_FORMATO_LOJA(
		T031_FORMATO_LOJA_IU,
		T031_DESCRICAO_FORMATO_LOJA)
	 VALUES (
		PI_CODFORMATO_LOJA,
		PI_DESCRICAO
	);
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  
  
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
     UPDATE DBAMDATA.T031_FORMATO_LOJA
     SET
		T031_DESCRICAO_FORMATO_LOJA                      = PI_DESCRICAO,
       WHERE T031_FORMATO_LOJA_IU           = PI_CODFORMATO_LOJA;
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE FROM DBAMDATA.T031_FORMATO_LOJA 
            WHERE T031_FORMATO_LOJA_IU   = PI_CODFORMATO_LOJA;
            
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
    
    
    --CONFIRMAR OPERCAO
    
    COMMIT;
    
    
EXCEPTION
	WHEN OTHERS THEN
		ROLLBACK;
		ecode := SQLCODE;
		emesg := SUBSTR(SQLERRM,1, 2048);
		STATUS     := 'ER';
END;