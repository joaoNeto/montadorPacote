create or replace PROCEDURE I_MDLOG.PCADASTRO_MOTIVO_CANC_PEDIDOS(
											PI_CODIGO_MOTIVO            NUMBER, 
											PI_DESCRICAO_MOTIVO         VARCHAR2,
											PI_TIPO_MOTIVO              CHAR,
											PI_ROTINA_PROCESSAMENTO     CHAR, 
											PI_OPERACAO                 CHAR,
											STATUS OUT VARCHAR2
                                            ) IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_LOJA
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DE LOJA.
  AUTOR     : Luis Eduardo Pinheiro Pedrosa
  DATA      : 28/11/2016
  CRC       : 70542
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN
	STATUS     := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT INTO DBAMDATA.T021_MOTIVO_CANC_PEDIDOS(
		T021_CODIGO_MOTIVO_IU ,
		T021_DESCRICAO_MOTIVO,
		T021_TIPO_MOTIVO ,
		T021_ROTINA_PROCESSAMENTO
		)
	 VALUES (
		PI_CODIGO_MOTIVO , 
		PI_DESCRICAO_MOTIVO,
		PI_TIPO_MOTIVO ,
		PI_ROTINA_PROCESSAMENTO
	);
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/

  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
     UPDATE DBAMDATA.T021_MOTIVO_CANC_PEDIDOS
     SET 
		T021_DESCRICAO_MOTIVO           = PI_DESCRICAO_MOTIVO, 
		T021_TIPO_MOTIVO                = PI_TIPO_MOTIVO,
		T021_ROTINA_PROCESSAMENTO       = PI_ROTINA_PROCESSAMENTO, 
		WHERE T021_CODIGO_MOTIVO_IU     = PI_CODIGO_MOTIVO;
		
	
	EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE FROM DBAMDATA.T021_MOTIVO_CANC_PEDIDOS
            WHERE T021_CODIGO_MOTIVO_IU   = PI_CODIGO_MOTIVO;
            
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
    
    
    --CONFIRMAR OPERCAO
    
    COMMIT;
    
    
EXCEPTION
	WHEN OTHERS THEN
		ROLLBACK;
		ecode := SQLCODE;
		emesg := SUBSTR(SQLERRM,1, 2048);
		STATUS     := 'ER';
	END;	