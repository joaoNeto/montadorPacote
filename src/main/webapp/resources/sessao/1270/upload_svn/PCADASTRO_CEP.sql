CREATE OR REPLACE PROCEDURE I_MDLOG.PCADASTRO_CEP(
    PI_CEP VARCHAR2,
    PI_IBGE_E VARCHAR2,
    PI_UF_E VARCHAR2,
    PI_PARTIC_AREA_LIVRE_COMERC CHAR,
    PI_CIDADE VARCHAR2,
    PI_OPERACAO VARCHAR2,
    STATUS OUT VARCHAR2 )
IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_CEP
  OBJETIVO  : ROTINA PARA OPERAÇÕES DE CADASTRO DO CEP.
  AUTOR     : FILIPE FALCÃO
  DATA      : 01/11/2016
  CRC       : CRC70542
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN
	
   STATUS := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT
      INTO DBAMDATA.T002_MUNICIPIO
        (
          T002_CEP_IU,
          T002_IBGE_E,
          T002_UF_E,
          T002_PARTIC_AREA_LIVRE_COMERC,
          T002_CIDADE
        )
        VALUES
        (
          PI_CEP,
          PI_IBGE_E,
          PI_UF_E,
          PI_PARTIC_AREA_LIVRE_COMERC,
          PI_CIDADE
        );
    EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      STATUS := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
      UPDATE DBAMDATA.T002_MUNICIPIO
      SET T002_CEP_IU = PI_CEP,
          T002_IBGE_E = PI_IBGE_E,
          T002_UF_E = PI_UF_E,
          T002_PARTIC_AREA_LIVRE_COMERC = PI_PARTIC_AREA_LIVRE_COMERC,
          T002_CIDADE = PI_CIDADE
      
      WHERE T002_CEP_IU = PI_CEP;
    EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      STATUS := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE
      FROM DBAMDATA.T002_MUNICIPIO
      WHERE T002_CEP_IU = PI_CEP;
    EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      STATUS := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
  --CONFIRMAR OPERCAO
  COMMIT;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  ecode  := SQLCODE;
  emesg  := SUBSTR(SQLERRM,1, 2048);
  STATUS := 'ER';
END;
/