create or replace PROCEDURE I_MDLOG.PCADASTRO_CAD_CLUSTER(
											PI_CODIGO_CLUSTER           NUMBER, 
											PI_DESCRICAO                VARCHAR2,
											PI_TIPO_CLUSTER             NUMBER,
											PI_OPERACAO                 CHAR,
											STATUS OUT VARCHAR2
                                            ) IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  
  ERROLOJA EXCEPTION;
  
  ROW_LOJAS  boolean;
  
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_SEG_CLIENTE
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DOS CLUSTER
  AUTOR     : Luis Eduardo Pinheiro Pedrosa
  DATA      : 16/11/2016
  CRC       : 70542
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
  
  cursor c_lojas is
  SELECT CODCLUSTER,LOJA FROM I_MDLOG.TEMP_CAD_CLUSTER_LOJA;
  
BEGIN
	STATUS     := 'PR';
	ROW_LOJAS  := FALSE;
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT INTO DBAMDATA.T480_CLUSTER(
		T480_CLUSTER_IU,
		T480_DESCRICAO, 
		T480_TIPO_CLUSTER_E
		)
	 VALUES (
		PI_CODIGO_CLUSTER, 
		PI_DESCRICAO,
		PI_TIPO_CLUSTER
	);
	
	
    for vc_lojas in c_lojas loop
			
			ROW_LOJAS := TRUE;
			
			BEGIN
		
				INSERT INTO DBAMDATA.T481_CLUSTER_LOJA(T481_CLUSTER_IE,T481_LOJA_IE)
				VALUES(vc_lojas.CODCLUSTER,vc_lojas.LOJA);
				
			EXCEPTION WHEN OTHERS THEN
				RAISE ERROLOJA;
			END;
			
		end loop;
	
	
	if ROW_LOJAS = FALSE  then
  
    	raise ERROLOJA;		
		
	END IF;
	
	
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  
  
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
     UPDATE DBAMDATA.T480_CLUSTER
     SET
		T480_DESCRICAO         = PI_DESCRICAO, 
		T480_TIPO_CLUSTER_E    = PI_TIPO_CLUSTER
       WHERE T480_CLUSTER_IU   = PI_CODIGO_CLUSTER;
   
		 DELETE FROM DBAMDATA.T481_CLUSTER_LOJA
         WHERE T481_CLUSTER_IE   = PI_CODIGO_CLUSTER;
		 
		 for vc_lojas in c_lojas loop
			
			ROW_LOJAS := TRUE;
			
			BEGIN
		
				INSERT INTO DBAMDATA.T481_CLUSTER_LOJA(T481_CLUSTER_IE,T481_LOJA_IE)
				VALUES(vc_lojas.CODCLUSTER,vc_lojas.LOJA);
				
			EXCEPTION WHEN OTHERS THEN
				RAISE ERROLOJA;
			END;
			
		end loop;
	
	
	if ROW_LOJAS = FALSE  then
  
    	raise ERROLOJA;		
		
	END IF;
		 
		 
	
	EXCEPTION WHEN OTHERS THEN	 
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE FROM DBAMDATA.T480_CLUSTER
            WHERE T480_CLUSTER_IU   = PI_CODIGO_CLUSTER;
	  
	  DELETE FROM DBAMDATA.T481_CLUSTER_LOJA
            WHERE T481_CLUSTER_IE   = PI_CODIGO_CLUSTER;	
            
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
    --CONFIRMAR OPERCAO
    COMMIT;
EXCEPTION
	WHEN ERROLOJA THEN
		ROLLBACK;
		ecode := SQLCODE;
		emesg := SUBSTR(SQLERRM,1, 2048);
		STATUS     := 'ER';
	WHEN OTHERS THEN
		ROLLBACK;
		ecode := SQLCODE;
		emesg := SUBSTR(SQLERRM,1, 2048);
		STATUS     := 'ER';
END;
/