/****************************************************************************** 
Schema : I_MDLOG
Analista : TASSIANO ALENCAR 
Data : 18/12/2017
CRC : 73309
Descricao : VIEW CRIADA PARA GERA��O DO RELAT�RIO EM PDF
******************************************************************************/
CREATE OR REPLACE VIEW I_MDLOG.VIEW_PDF_LISTA_PRODUTOS AS
SELECT T110_PRODUTO_IE  AS PRODUTO,
  T076_CODIGO_BARRA     AS CODIGO_BARRA,
  T082_REFERENC_FABRICA AS REFERENCIA_FABRICA,
  T076_DESCRICAO
  || ' '
  || T076_APRESENTACAO                                                                                                                               AS DESCRICAO_APRESENTACAO,
  null                                                                                                                                               AS MTC,
  T076_NCM_E                                                                                                                                         AS NCM,
  ''                                                                                                                                                 AS CST,
  ''                                                                                                                                                 AS CFOP,
  (SELECT T501_TIPO FROM T501_PRODUTO_EMBALAGEM WHERE T501_MENOR_EMB_VENDA = 'S' AND T076_PRODUTO_IU = T501_PRODUTO_IE AND ROWNUM = 1)               AS UN,
  T110_QTDE_PEDIDO                                                                                                                                   AS QTD_PEDIDO,
  T110_PRECO_ITEM                                                                                                                                    AS PRECO_ITEM,
  ((T110_QTDE_PEDIDO * T110_PRECO_ITEM) - (((T110_QTDE_PEDIDO * T110_PRECO_ITEM)*(NVL(T110_DESCONTO_ITEM,0) + NVL(T110_DESCONTO_COMERCIAL,0)))/100)) AS VALOR_TOTAL ,
  0                                                                                                                                                  AS BASE_ICMS,
  0                                                                                                                                                  AS VALOR_ICMS,
  CASE
    WHEN T054_COBRA_IPI = 'S'
    THEN
      CASE
        WHEN T076_CLASSIFICACAO_IPI = '1'
        THEN nvl(T076_ALIQUOTA_IPI,0)
        ELSE 0
      END
    ELSE 0
  END ALIQUOTA_IPI,
  CASE
    WHEN T054_COBRA_IPI = 'S'
    THEN
      CASE
        WHEN T076_CLASSIFICACAO_IPI = '1'
        THEN (((T110_QTDE_PEDIDO * T110_PRECO_ITEM))*nvl(T076_ALIQUOTA_IPI,0))/100
        ELSE 0
      END
    ELSE 0
  END VALOR_IPI,
  T110_PERC_ALIQUOTAICMS                                       AS ALIQUOTACMS,
  (NVL(T110_DESCONTO_ITEM,0) + NVL(T110_DESCONTO_COMERCIAL,0)) AS DESCONTO_PERCENTUAL,
  T108_UNIDADE_IE       AS UNIDADE,
  T108_NUMERO_PEDIDO_IU AS PEDIDO
  FROM T110_PEDIDOS_PEND_ITENS
    JOIN T108_PEDIDOS_PEND
      ON T108_UNIDADE_IE        = T110_UNIDADE_IE
     AND T108_NUMERO_PEDIDO_IU = T110_NUMERO_PEDIDO_IE
    JOIN T076_PRODUTO
      ON T076_PRODUTO_IU = T110_PRODUTO_IE
    JOIN T054_FORNECEDOR_UNIDADE
      ON T054_UNIDADE_IE     = T108_UNIDADE_IE
     AND T054_FORNECEDOR_IE = T108_FORNECEDOR_E
    LEFT JOIN T082_PRODUTO_FORNECEDOR
      ON T082_UNIDADE_IE          = T110_UNIDADE_IE
     AND T082_PRODUTO_IE         = T110_PRODUTO_IE
     AND T082_FORNECEDOR_IE      = T108_FORNECEDOR_E
  WHERE T110_QTDE_PEDIDO > 0
ORDER BY DESCRICAO_APRESENTACAO ASC;
/