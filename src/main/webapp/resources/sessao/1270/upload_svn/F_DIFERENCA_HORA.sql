create or replace FUNCTION  I_MDLOG.F_DIFERENCA_HORA(PI_INICIO IN DATE,PI_FIM IN DATE)
RETURN NUMBER
IS
DIAS VARCHAR2(4);
HORAS VARCHAR2(4);
MINUTOS VARCHAR2(4);
SEGUNDOS VARCHAR2(4);

BEGIN


   I_MDLOG.DIFERENCA_DATA(PI_INICIO,PI_FIM,DIAS,HORAS,MINUTOS,SEGUNDOS);

   RETURN TO_NUMBER(HORAS);

END F_DIFERENCA_HORA;
/