CREATE OR REPLACE PROCEDURE I_MDLOG.PCADASTRO_TIPO_NATUREZA(PI_CODTIPO_NATUREZA  VARCHAR2,
                                                            PI_DESCRICAO          VARCHAR2,
                                                            PI_OPERACAO           VARCHAR2,
                                                            STATUS OUT            VARCHAR2
                                                            ) IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  
  
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_TIPO_NATUREZA
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DO TIPO NATUREZA(279)
  AUTOR     : H�LIO SALES
  DATA      : 03/11/2016
  CRC       : 
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN  

  STATUS     := 'PR';

  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
  
    BEGIN
      INSERT INTO DBAMDATA.T279_TIPO_NATUREZA(T279_TIPO_NATUREZA_IU, T279_DESCRICAO)
             VALUES (PI_CODTIPO_NATUREZA,PI_DESCRICAO);
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  
  
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
  
    BEGIN
      UPDATE DBAMDATA.T279_TIPO_NATUREZA 
         SET T279_DESCRICAO = PI_DESCRICAO
       WHERE T279_TIPO_NATUREZA_IU = PI_CODTIPO_NATUREZA;
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
  
    BEGIN
      DELETE FROM DBAMDATA.T279_TIPO_NATUREZA 
            WHERE T279_TIPO_NATUREZA_IU = PI_CODTIPO_NATUREZA;
            
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
    
    
    --CONFIRMAR OPERCAO
    
    COMMIT;
    
    
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    ecode := SQLCODE;
    emesg := SUBSTR(SQLERRM,1, 2048);
    STATUS     := 'ER';
END;
/