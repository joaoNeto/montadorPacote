
CREATE OR REPLACE VIEW I_MDLOG.VM03_003_FECHAMENTO_OC_RESUMO AS 
SELECT 
  M03_012_FORNECEDOR_IE FORNECEDOR, 
  RAZAO_SOCIAL,
  CGC,
  CIDADE,
  UF,  
  M03_012_ORDEM_SERVICO_IE OS, 
  MAX(M03_012_DATA_SUG_IE) DATA_SUG,
  
  sum(M03_012_SUGESTAO_FINAL * M03_012_PRECO_SUGESTAO) SUG_TOTAL_PRODUTOS,
  nvl(SUM((M03_012_ALIQ_IPI / 100) * (1-(M03_012_DES_ITEM_SUG / 100)) * (M03_012_PRECO_SUGESTAO * M03_012_SUGESTAO_FINAL)),0) SUG_VALOR_IPI,
  nvl(SUM((M03_012_DES_ITEM_SUG / 100) * (M03_012_PRECO_SUGESTAO * M03_012_SUGESTAO_FINAL)),0) SUG_VALOR_DESCONTO,  
  nvl(SUM((M03_012_ALIQ_ST / 100) * ((M03_012_PRECO_SUGESTAO * M03_012_SUGESTAO_FINAL) + ((M03_012_PRECO_SUGESTAO * M03_012_SUGESTAO_FINAL) * (M03_012_MVA_ST / 100)))),0) SUG_VALOR_ICMS_ST,  
  nvl(SUM((M03_012_TAXA_VENDOR / 100) * (1-(M03_012_DES_ITEM_SUG / 100)) * (M03_012_PRECO_SUGESTAO * M03_012_SUGESTAO_FINAL)),0) SUG_VALOR_OUTRAS_DESP,  
  
  sum(M03_012_REPOSICAO_UND * M03_012_PRECO_REPOSICAO) REP_TOTAL_PRODUTOS,
  nvl(SUM((M03_012_ALIQ_IPI / 100) * (1 - (M03_012_DESCONTO_ITEM / 100)) * (M03_012_PRECO_REPOSICAO * M03_012_REPOSICAO_UND)),0) REP_VALOR_IPI,  
  nvl(SUM((M03_012_DESCONTO_ITEM / 100) * (M03_012_REPOSICAO_UND * M03_012_PRECO_REPOSICAO)),0) REP_VALOR_DESCONTO,  
  nvl(SUM((M03_012_ALIQ_ST / 100) * ((M03_012_PRECO_REPOSICAO * M03_012_REPOSICAO_UND) + ((M03_012_PRECO_REPOSICAO * M03_012_REPOSICAO_UND) * (M03_012_MVA_ST / 100)))),0) REP_VALOR_ICMS_ST,  
  nvl(SUM((M03_012_TAXA_VENDOR / 100) * (1 - (M03_012_DESCONTO_ITEM / 100)) *  (M03_012_REPOSICAO_UND * M03_012_PRECO_REPOSICAO)),0) REP_VALOR_OUTRAS_DESP,  
  
  count(distinct (case when M03_012_REPOSICAO_UND > 0 then M03_012_PRODUTO_IE else null end)) QTD_ITENS,
  nvl(SUM(M03_012_REPOSICAO_UND),0) REP_VOLUME,
  nvl(SUM(M03_012_REPOSICAO_UND * M03_012_PESO_BRUTO),0) REP_PESO_BRUTO,
  nvl(SUM(M03_012_REPOSICAO_UND * M03_012_PESO_LIQUIDO),0) REP_PESO_LIQUIDO,
  nvl(SUM((M03_012_REPOSICAO_UND * M03_012_CUBAGEM) / 1000000),0) REP_CUBAGEM      
FROM I_MDLOG.M03_012_SUG_COMPRA_DIA A2
     join I_MDLOG.VIEW_M02_FORNECEDOR on
        M03_012_FORNECEDOR_IE = FORNECEDOR_IU   
WHERE
  M03_012_DATA_SUG_IE = (SELECT MAX(M03_012_DATA_SUG_IE) FROM I_MDLOG.M03_012_SUG_COMPRA_DIA A1 WHERE A1.M03_012_FORNECEDOR_IE = A2.M03_012_FORNECEDOR_IE AND A1.M03_012_UNIDADE_IE = A2.M03_012_UNIDADE_IE AND A1.M03_012_ORDEM_SERVICO_IE = A2.M03_012_ORDEM_SERVICO_IE)     
  
GROUP BY   
  M03_012_FORNECEDOR_IE, 
  RAZAO_SOCIAL,
  CGC,
  CIDADE,
  UF,
  M03_012_ORDEM_SERVICO_IE;
         
GRANT SELECT ON I_MDLOG.VM03_003_FECHAMENTO_OC_RESUMO TO INTEGRA; 
