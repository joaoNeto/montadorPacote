CREATE OR REPLACE FUNCTION I_MDLOG.FCOM_STATUS_PEDIDO_COMPRA(P_UNIDADE NUMBER,
                                                             P_PEDIDO  NUMBER) RETURN number
IS
  vSumT110_QTDE_PEDIDO       NUMBER; 
  vSumT110_QTDE_NOTAFISCAL   NUMBER;
  vSumT110_QTDE_NOTAFISCLIB  NUMBER;
  vLotes                     NUMBER;
  vT108_SITUACAO_LOTE_PEDIDO T108_PEDIDOS_PEND.T108_SITUACAO_LOTE_PEDIDO%type;
  vT108_TIPO_BLOQUEIO        T108_PEDIDOS_PEND.T108_TIPO_BLOQUEIO%type;
  vT108_DATA_LIBERACAO       T108_PEDIDOS_PEND.T108_DATA_LIBERACAO%type;
/*******************************************************************************
 Nome        : FCOM_STATUS_PEDIDO_COMPRA
 Chamador    :
 Local       : Functions
 Descric�o   : Define o Status do Pedido, de acordo com Regra da RV para adequar ao MDLogWeb
 Analista	 : H�lio Sales
 Programador : 

 Parametros  : P_UNIDADE NUMBER
               P_PEDIDO  NUMBER
********************************************************************************/  
            

BEGIN
  SELECT T108_SITUACAO_LOTE_PEDIDO,
         T108_TIPO_BLOQUEIO, 
         T108_DATA_LIBERACAO, 
         (select count(*) from T209_LOTESTOQUE where T209_UNIDADE_IE = T108_UNIDADE_IE AND T209_NUMERO_PEDIDO = T108_NUMERO_PEDIDO_IU),
         SUM(T110_QTDE_PEDIDO), 
         SUM(T110_QTDE_NOTAFISCAL), 
         SUM(T110_QTDE_NOTAFISCLIB)
    INTO vT108_SITUACAO_LOTE_PEDIDO, 
         vT108_TIPO_BLOQUEIO,
         vT108_DATA_LIBERACAO, 
         vLotes,
         vSumT110_QTDE_PEDIDO, 
         vSumT110_QTDE_NOTAFISCAL, 
         vSumT110_QTDE_NOTAFISCLIB 
  FROM T108_PEDIDOS_PEND
    JOIN T110_PEDIDOS_PEND_ITENS ON
      T108_UNIDADE_IE       = T110_UNIDADE_IE AND
      T108_NUMERO_PEDIDO_IU = T110_NUMERO_PEDIDO_IE
  WHERE T108_UNIDADE_IE       = P_UNIDADE
    AND T108_NUMERO_PEDIDO_IU = P_PEDIDO    
  GROUP BY T108_SITUACAO_LOTE_PEDIDO, T108_TIPO_BLOQUEIO, T108_DATA_LIBERACAO,
           T108_UNIDADE_IE,T108_NUMERO_PEDIDO_IU;
  
  IF (vT108_TIPO_BLOQUEIO IS NOT NULL AND vT108_DATA_LIBERACAO IS NULL) THEN
    RETURN 2; --//BLOQUEADO
  ELSIF (vLotes > 0) AND (NVL(vT108_SITUACAO_LOTE_PEDIDO,0) = 0) THEN  
    RETURN 6;
  ELSIF NVL(vT108_SITUACAO_LOTE_PEDIDO,0) = 0 THEN
    RETURN 1; --//Liberado
  ELSIF vT108_SITUACAO_LOTE_PEDIDO = 1 THEN
    RETURN 3; --//Em recebimento
  ELSIF vSumT110_QTDE_NOTAFISCAL = 0 THEN
    RETURN 1; --//Liberado
  ELSIF vSumT110_QTDE_PEDIDO > vSumT110_QTDE_NOTAFISCAL THEN
    RETURN 6; --//Recebido Parcial
  ELSIF vSumT110_QTDE_PEDIDO = vSumT110_QTDE_NOTAFISCAL THEN
    RETURN 5; --//Totalmente Recebido
  ELSE
    RETURN 1; --//Liberado  
  END IF;
            
END;                                                    
/
