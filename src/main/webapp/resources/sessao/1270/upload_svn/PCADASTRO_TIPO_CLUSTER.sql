create or replace PROCEDURE I_MDLOG.PCADASTRO_TIPO_CLUSTER(
    PI_TIPOCLUSTER NUMBER,
    PI_DESCRICAO VARCHAR2,
    PI_OPERACAO VARCHAR2,
    STATUS OUT VARCHAR2 )
IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_TIPO_CLUSTER
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DO TIPO CLUSTER.
  AUTOR     : FILIPE FALC�O
  DATA      : 23/11/2016
  CRC       : CRC70542
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN
	
   STATUS := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT
      INTO DBAMDATA.T060_TIPO_CLUSTER
        (
          T060_TIPO_CLUSTER_IU,
          T060_DESCRICAO_TIPO_CLUSTER
        )
        VALUES
        (
          PI_TIPOCLUSTER,
          PI_DESCRICAO
        );
    EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      STATUS := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
      UPDATE DBAMDATA.T060_TIPO_CLUSTER
      SET T060_DESCRICAO_TIPO_CLUSTER = PI_DESCRICAO
          
      WHERE T060_TIPO_CLUSTER_IU = PI_TIPOCLUSTER;
    EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      STATUS := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE
      FROM DBAMDATA.T060_TIPO_CLUSTER
      WHERE T060_TIPO_CLUSTER_IU = PI_TIPOCLUSTER;
    EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      STATUS := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
  --CONFIRMAR OPERCAO
  COMMIT;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  ecode  := SQLCODE;
  emesg  := SUBSTR(SQLERRM,1, 2048);
  STATUS := 'ER';
END;