create or replace PROCEDURE PCADASTRO_CLUSTER_LOJA(
    PI_CODCLUSTER NUMBER,
    PI_LOJA NUMBER,
    PI_OPERACAO VARCHAR2,
    STATUS OUT VARCHAR2 )
IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_CEP
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DO CLUSTER LOJA.
  AUTOR     : FILIPE FALC�O
  DATA      : 22/11/2016
  CRC       : CRC70542
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN
	
   STATUS := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT
      INTO DBAMDATA.T481_CLUSTER_LOJA
        (
          T481_CLUSTER_IE,
          T481_LOJA_IE
        )
        VALUES
        (
          PI_CODCLUSTER,
          PI_LOJA
        );
    EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      STATUS := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
      UPDATE DBAMDATA.T481_CLUSTER_LOJA
      SET T481_LOJA_IE = PI_LOJA
          
      WHERE T481_CLUSTER_IE = PI_CODCLUSTER;
    EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      STATUS := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE
      FROM DBAMDATA.T481_CLUSTER_LOJA
      WHERE T481_CLUSTER_IE = PI_CODCLUSTER;
    EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      STATUS := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
  --CONFIRMAR OPERCAO
  COMMIT;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  ecode  := SQLCODE;
  emesg  := SUBSTR(SQLERRM,1, 2048);
  STATUS := 'ER';
END;