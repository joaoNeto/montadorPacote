/******************************************************************************
Schema    : I_MDLOG
Analista  : Luis Eduardo Pinheiro Pedrosa	
Data      : 04/01/2017
CRC       : 70542
Descricao : Cadastro de CLUSTER LOJA
******************************************************************************/
CREATE OR REPLACE FORCE VIEW I_MDLOG.VIEW_CLUSTER_LOJA
AS
SELECT 
  T1201_CLUSTER_IE as codCluster, 
  T1201_UNIDADE_IE as loja , 
  T003_RAZAO_SOCIAL AS loja_desc
FROM T1201_CLUSTER_UNIDADE
  JOIN T003_UNIDADE ON
    T003_UNIDADE_IU = T1201_UNIDADE_IE;
    
