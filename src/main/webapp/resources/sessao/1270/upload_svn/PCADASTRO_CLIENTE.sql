CREATE OR REPLACE PROCEDURE I_MDLOG.PCADASTRO_CLIENTE(  PI_OPERACAO VARCHAR2,
                                                        STATUS OUT VARCHAR2 )
IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_CLIENTE
  OBJETIVO  : ROTINA PARA OPERAÃƒÆ’Ã¢â‚¬Â¡ÃƒÆ’Ã¢â‚¬Â¢ES DE CADASTRO DO CLIENTE.
  AUTOR     : JOSE GABRIEL
  DATA      : 25/11/2016
  CRC       : CRC70542
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
  
  
  V_T014_ENDERECO_COBRANCA DBAMDATA.T014_CLIENTE.T014_ENDERECO_COBRANCA%TYPE;
  V_T014_BLOQUEIO_CREDITO  DBAMDATA.T014_CLIENTE.T014_BLOQUEIO_CREDITO%TYPE;
  V_T014_CONDICOES_PRAZO   DBAMDATA.T014_CLIENTE.T014_CONDICOES_PRAZO%TYPE;
  V_T014_CRITICA_FATURAMIN DBAMDATA.T014_CLIENTE.T014_CRITICA_FATURAMIN%TYPE;
  V_T014_EMITE_COBRANCA    DBAMDATA.T014_CLIENTE.T014_EMITE_COBRANCA%TYPE;
  V_T014_COMPRA_PSI        DBAMDATA.T014_CLIENTE.T014_COMPRA_PSI%TYPE;
  V_T014_REPASSE           DBAMDATA.T014_CLIENTE.T014_REPASSE%TYPE;
  V_T014_RETEM_ICM         DBAMDATA.T014_CLIENTE.T014_RETEM_ICM%TYPE;
  V_T014_DESCONTO_REPASSE  DBAMDATA.T014_CLIENTE.T014_DESCONTO_REPASSE%TYPE;
  V_T014_TIPO_ENTREGA      DBAMDATA.T014_CLIENTE.T014_TIPO_ENTREGA%TYPE;
  
  V_T014_RAZAO_SOCIAL      DBAMDATA.T014_CLIENTE.T014_RAZAO_SOCIAL%TYPE;
  V_T014_GRUPO_CLIENTE_E   DBAMDATA.T014_CLIENTE.T014_GRUPO_CLIENTE_E%TYPE;
  V_T014_LOJA_E            DBAMDATA.T014_CLIENTE.T014_LOJA_E%TYPE;
  V_T014_ACOMPANHA_SCI     DBAMDATA.T014_CLIENTE.T014_ACOMPANHA_SCI%TYPE;
  
  
  --VALORES DEFAULTS
  V_T014_TIPO_SEGMENTO      DBAMDATA.T014_CLIENTE.T014_TIPO_SEGMENTO%TYPE;
  V_T014_VIGIADO            DBAMDATA.T014_CLIENTE.T014_VIGIADO%TYPE;
  V_T014_CLIENTE_ESPECIAL   DBAMDATA.T014_CLIENTE.T014_CLIENTE_ESPECIAL%TYPE;
  V_T014_COMPRA_TODAS_CATEG DBAMDATA.T014_CLIENTE.T014_COMPRA_TODAS_CATEGORIAS%TYPE;
  V_T014_DATA_CADASTRO      DBAMDATA.T014_CLIENTE.T014_DATA_CADASTRO%TYPE;
  V_T014_EMPRESA_COLIGADA   DBAMDATA.T014_CLIENTE.T014_EMPRESA_COLIGADA%TYPE;
  V_T014_CLASSIFICACAO      DBAMDATA.T014_CLIENTE.T014_CLASSIFICACAO%TYPE;
  
  
  
  CURSOR C_CLIENTE IS
  SELECT COD_CLIENTE
        ,PESSOA_FISJUR
        ,SEXO
        ,RAZAO_SOCIAL
        ,NOME
        ,CEP
        ,ENDERECO
        ,BAIRRO
        ,NUMERO
        ,COMPLEMENTO
        ,CIDADE
        ,ESTADO
        ,TIPO_CLIENTE
        ,INSC_ESTADUCAL_RG
        ,CGC_CPF
        ,CONSUMIDOR_FINAL
        ,TELEFONE_1
        ,COD_INTEGRACAO
        ,SEGMENTO_NEGOC
        ,RETEM_ICMS
        ,REPASSE_ICMS
        ,ALIQUOTA_REPASSE
        ,CFOP_DEFAULT
        ,DATA_NASCIMENTO
		,ESTRANGEIRO
		,CANAL_ORIGEM
  FROM  I_MDLOG.TEMP_CAD_CLIENTE; 
  
BEGIN
	
   STATUS := 'NP';
  /******************************BEGIN - CADASTRAR*****************************/
  
  FOR VC_CLIENTE IN C_CLIENTE LOOP
    
    --VALORES DEFAULTS
    V_T014_TIPO_SEGMENTO         := 'A';
    V_T014_VIGIADO               := 'N';
    V_T014_CLIENTE_ESPECIAL      := 'N';
    V_T014_COMPRA_TODAS_CATEG    := 'S';
    V_T014_DATA_CADASTRO         :=  SYSDATE;
    V_T014_EMPRESA_COLIGADA      := 'N';
    V_T014_CLASSIFICACAO         := 'E';
    V_T014_ENDERECO_COBRANCA     := 'S';
    V_T014_BLOQUEIO_CREDITO      := 'B'; 
    V_T014_CRITICA_FATURAMIN     := 'N';
    V_T014_COMPRA_PSI            := 'N';
    V_T014_EMITE_COBRANCA        := 'N';
    V_T014_ACOMPANHA_SCI        := 'N';

    
    IF VC_CLIENTE.PESSOA_FISJUR = 'F' THEN
      
      
      V_T014_CONDICOES_PRAZO     := 'F';
      V_T014_REPASSE             := 'N';
      V_T014_RETEM_ICM           := 'S';
      V_T014_DESCONTO_REPASSE    :=  0;
      V_T014_TIPO_ENTREGA        :=  0;
      
    ELSE
      
      V_T014_CONDICOES_PRAZO      := 'P';
      V_T014_GRUPO_CLIENTE_E      := '';
      V_T014_LOJA_E               := '';
      V_T014_TIPO_ENTREGA         := '1';
      V_T014_DESCONTO_REPASSE     := VC_CLIENTE.ALIQUOTA_REPASSE;
      
    END IF;
    
    
    
    IF PI_OPERACAO = 'C' THEN
      BEGIN
        INSERT
        INTO DBAMDATA.T014_CLIENTE
          (
            T014_CLIENTE_IU        ,
            T014_ENDERECO_COBRANCA ,
            T014_BLOQUEIO_CREDITO  ,
            T014_CONDICOES_PRAZO   ,
            T014_CRITICA_FATURAMIN ,
            T014_EMITE_COBRANCA    ,
            T014_COMPRA_PSI        ,
            T014_REPASSE           ,
            T014_RETEM_ICM         ,
            T014_DESCONTO_REPASSE  ,
            T014_TIPO_ENTREGA      ,
            T014_RAZAO_SOCIAL      ,
            T014_NOME              ,
            T014_GRUPO_CLIENTE_E   ,
            T014_LOJA_E            ,
            T014_ACOMPANHA_SCI     ,
            T014_PESSOA_FISJUR     ,
            T014_SEXO              ,
            T014_CEP_LOGRAD        ,
            T014_CEP_E             ,
            T014_ENDERECO          ,
            T014_BAIRRO            ,
            T014_NUMERO            ,
            T014_COMPLEMENTO       ,
            T014_TIPO_CLIENTE      ,
            T014_INSCRICAO_ESTADUAL,
            T014_CGC_CPF           ,
            T014_CONSUMIDOR_FINAL  ,
            T014_DDD1              ,
            T014_FONE1             ,
            T014_CODIGO_INTEGRACAO ,
            --T014_GRUPO_CLIENTE_E   ,
            --T014_RETEM_ICM         ,
            --T014_REPASSE           ,
            --T014_DESCONTO_REPASSE  ,
            T014_NAT_OPERACAO_E    ,
            T014_DATA_NASCIMENTO   ,
            T014_TIPO_SEGMENTO     ,
            T014_VIGIADO           ,
            T014_CLIENTE_ESPECIAL  ,
            T014_COMPRA_TODAS_CATEGORIAS,
            T014_DATA_CADASTRO          ,
            T014_EMPRESA_COLIGADA       ,
            T014_CLASSIFICACAO,
			T014_ESTRANGEIRO,
			T014_CANAL_ORIGEM
          )
          VALUES
          (
            I_MDLOG.F_NEXT_USUARIO() ,
            V_T014_ENDERECO_COBRANCA ,
            V_T014_BLOQUEIO_CREDITO  ,
            V_T014_CONDICOES_PRAZO   ,
            V_T014_CRITICA_FATURAMIN ,
            V_T014_EMITE_COBRANCA    ,
            V_T014_COMPRA_PSI        ,
            V_T014_REPASSE           ,
            V_T014_RETEM_ICM         ,
            V_T014_DESCONTO_REPASSE  ,
            V_T014_TIPO_ENTREGA      ,
            VC_CLIENTE.RAZAO_SOCIAL  ,
            VC_CLIENTE.NOME          ,
            V_T014_GRUPO_CLIENTE_E   ,
            V_T014_LOJA_E            ,
            V_T014_ACOMPANHA_SCI     ,
            VC_CLIENTE.PESSOA_FISJUR ,
            VC_CLIENTE.SEXO          ,
            VC_CLIENTE.CEP           ,
            VC_CLIENTE.CEP           ,
            VC_CLIENTE.ENDERECO      ,
            VC_CLIENTE.BAIRRO        ,
            VC_CLIENTE.NUMERO        ,
            VC_CLIENTE.COMPLEMENTO   ,
            VC_CLIENTE.TIPO_CLIENTE   ,
        VC_CLIENTE.INSC_ESTADUCAL_RG ,
            VC_CLIENTE.CGC_CPF       ,
        VC_CLIENTE.CONSUMIDOR_FINAL  ,
        substr(VC_CLIENTE.TELEFONE_1,1,2),
        substr(VC_CLIENTE.TELEFONE_1,3,length(VC_CLIENTE.TELEFONE_1)),
        VC_CLIENTE.COD_INTEGRACAO    ,
        --VC_CLIENTE.SEGMENTO_NEGOC    ,
        --VC_CLIENTE.RETEM_ICMS        ,
        --VC_CLIENTE.REPASSE_ICMS      ,
        --VC_CLIENTE.ALIQUOTA_REPASSE  ,
        VC_CLIENTE.CFOP_DEFAULT      ,
        VC_CLIENTE.DATA_NASCIMENTO   ,
        V_T014_TIPO_SEGMENTO         ,
        V_T014_VIGIADO               ,
        V_T014_CLIENTE_ESPECIAL      ,
        V_T014_COMPRA_TODAS_CATEG    ,
        V_T014_DATA_CADASTRO         ,
        V_T014_EMPRESA_COLIGADA      ,
        V_T014_CLASSIFICACAO,
		VC_CLIENTE.ESTRANGEIRO,
		VC_CLIENTE.CANAL_ORIGEM
          );
          STATUS := 'PR';
      EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        ecode  := SQLCODE;
        emesg  := SUBSTR(SQLERRM,1, 2048);
        STATUS := emesg;
      END;
    END IF;
    /********************************END - CADASTRAR*****************************/
    /********************************BEGIN EDITAR********************************/
    IF PI_OPERACAO = 'U' THEN
      BEGIN
        UPDATE DBAMDATA.T014_CLIENTE
        SET 
            T014_ENDERECO_COBRANCA        =    V_T014_ENDERECO_COBRANCA ,
            T014_BLOQUEIO_CREDITO         =    V_T014_BLOQUEIO_CREDITO  ,
            T014_CONDICOES_PRAZO          =    V_T014_CONDICOES_PRAZO   ,
            T014_CRITICA_FATURAMIN        =    V_T014_CRITICA_FATURAMIN ,
            T014_EMITE_COBRANCA           =    V_T014_EMITE_COBRANCA    ,
            T014_COMPRA_PSI               =    V_T014_COMPRA_PSI        ,
            T014_REPASSE                  =    V_T014_REPASSE           ,
            T014_RETEM_ICM                =    V_T014_RETEM_ICM         ,
            T014_DESCONTO_REPASSE         =    V_T014_DESCONTO_REPASSE  ,
            T014_TIPO_ENTREGA             =    V_T014_TIPO_ENTREGA      ,
            T014_RAZAO_SOCIAL             =    VC_CLIENTE.RAZAO_SOCIAL  ,
            T014_NOME                     =    VC_CLIENTE.NOME          ,
            T014_GRUPO_CLIENTE_E          =    V_T014_GRUPO_CLIENTE_E   ,
            T014_LOJA_E                   =    V_T014_LOJA_E            ,
            T014_ACOMPANHA_SCI            =    V_T014_ACOMPANHA_SCI     ,
            T014_PESSOA_FISJUR            =    VC_CLIENTE.PESSOA_FISJUR ,
            T014_SEXO                     =    VC_CLIENTE.SEXO          ,
            T014_CEP_LOGRAD               =    VC_CLIENTE.CEP           ,
            T014_CEP_E                    =    VC_CLIENTE.CEP           ,
            T014_ENDERECO                 =    VC_CLIENTE.ENDERECO      ,
            T014_BAIRRO                   =    VC_CLIENTE.BAIRRO        ,
            T014_NUMERO                   =    VC_CLIENTE.NUMERO        ,
            T014_COMPLEMENTO              =    VC_CLIENTE.COMPLEMENTO   ,
            T014_TIPO_CLIENTE             =    VC_CLIENTE.TIPO_CLIENTE   ,
            T014_INSCRICAO_ESTADUAL       =    VC_CLIENTE.INSC_ESTADUCAL_RG ,
            T014_CGC_CPF                  =    VC_CLIENTE.CGC_CPF       ,
            T014_CONSUMIDOR_FINAL         =    VC_CLIENTE.CONSUMIDOR_FINAL  ,
            T014_DDD1                     =    substr(VC_CLIENTE.TELEFONE_1,1,2),
            T014_FONE1                    =    substr(VC_CLIENTE.TELEFONE_1,3,length(VC_CLIENTE.TELEFONE_1)),
            T014_CODIGO_INTEGRACAO        =    VC_CLIENTE.COD_INTEGRACAO    ,
            T014_NAT_OPERACAO_E           =    VC_CLIENTE.CFOP_DEFAULT      ,
            T014_DATA_NASCIMENTO          =    VC_CLIENTE.DATA_NASCIMENTO   ,
            T014_TIPO_SEGMENTO            =    V_T014_TIPO_SEGMENTO         ,
            T014_VIGIADO                  =    V_T014_VIGIADO               ,
            T014_CLIENTE_ESPECIAL         =    V_T014_CLIENTE_ESPECIAL      ,
            T014_COMPRA_TODAS_CATEGORIAS  =    V_T014_COMPRA_TODAS_CATEG    ,
            T014_DATA_CADASTRO            =    V_T014_DATA_CADASTRO         ,
            T014_EMPRESA_COLIGADA         =    V_T014_EMPRESA_COLIGADA      ,
            T014_CLASSIFICACAO            =    V_T014_CLASSIFICACAO			,
            T014_ESTRANGEIRO              =    VC_CLIENTE.ESTRANGEIRO		,
            T014_CANAL_ORIGEM             =    VC_CLIENTE.CANAL_ORIGEM
        WHERE T014_CLIENTE_IU = VC_CLIENTE.COD_CLIENTE;
           STATUS := 'PR';
      EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        ecode  := SQLCODE;
        emesg  := SUBSTR(SQLERRM,1, 2048);
        STATUS := 'ER';
      END;
    END IF;
    /********************************END EDITAR********************************/
    /********************************BEGIN EXCLUIR********************************/
    IF PI_OPERACAO = 'D' THEN
      BEGIN
        DELETE
        FROM DBAMDATA.T014_CLIENTE
        WHERE T014_CLIENTE_IU = VC_CLIENTE.COD_CLIENTE;
        STATUS := 'PR';
      EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        ecode  := SQLCODE;
        emesg  := SUBSTR(SQLERRM,1, 2048);
        STATUS := 'ER';
      END;
    END IF;
    /********************************END EXCLUIR********************************/
    --CONFIRMAR OPERCAO
    COMMIT;
    
    END LOOP;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  ecode  := SQLCODE;
  emesg  := SUBSTR(SQLERRM,1, 2048);
  STATUS := 'ER';
END;
/