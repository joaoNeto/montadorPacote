create or replace FUNCTION I_MDLOG.F_EXISTE_LINHA_PROD(PI_PRODUTO NUMBER)
RETURN NUMBER
IS
  LINHA NUMBER(10);
  /*******************************************************************************
  SCHEMA    : I_MDLOG
  FUNCTION  : F_EXISTE_LINHA_PROD
  OBJETIVO  : Ir�? VALIDAR SE O PRODUTO TEM LINHA DE COMPRADOR
  AUTOR     : JOSE GABRIEL
  DATA      : 05/04/2017
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *******************************************************************************/
BEGIN

    LINHA := 0;
    
    
    SELECT NVL(COUNT(*),0) LINHAS INTO LINHA
      FROM T201_LINHA_PRODUTO 
     WHERE T201_PRODUTO_IE = PI_PRODUTO;


    RETURN LINHA;

  
END;
/