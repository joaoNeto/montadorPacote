/****************************************************************************** 
Schema : I_MDLOG
Analista : TASSIANO ALENCAR 
Data : 14/12/2017
CRC : 73309
Descricao : SEQUENCE PARA CADASTRO DA LINHA
******************************************************************************/
CREATE SEQUENCE  "I_MDLOG"."S001_T200_LINHA" MINVALUE 1 MAXVALUE 9999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER CYCLE;