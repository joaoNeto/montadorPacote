CREATE OR REPLACE FUNCTION I_MDLOG.F_DEFINE_QTDE_DIAS_CURVA(P_UNIDADE  number,
							     P_CURVA_FISICA varchar2,
                                 P_CURVA_FINANCEIRA  varchar2, 
                                 P_CURVA_MARGEM      varchar2, 
                                 P_TIPO_CURVA_PARAM  varchar2
                                                  ) RETURN NUMBER
IS
 vQTDE_DIAS_CURVA  NUMBER;
 vCurvaProduto     VARCHAR2(2);
BEGIN

 vCurvaProduto := I_MDLOG.F_RETORNA_CURVA_PRODUTO(P_CURVA_FISICA,
                                                  P_CURVA_FINANCEIRA,
                                                  P_CURVA_MARGEM,
                                                  P_TIPO_CURVA_PARAM);
 SELECT
   CASE SUBSTR(vCurvaProduto,1,1)
     WHEN 'A' THEN M03_002_CURVA_A
     WHEN 'B' THEN M03_002_CURVA_B
     WHEN 'C' THEN M03_002_CURVA_C
     WHEN 'D' THEN M03_002_CURVA_D
   END
     INTO vQTDE_DIAS_CURVA
 FROM I_MDLOG.M03_002_PARAMETROS_UNIDADES
 WHERE M03_002_UNIDADE_IE = P_UNIDADE;

 RETURN vQTDE_DIAS_CURVA;
EXCEPTION
 WHEN NO_DATA_FOUND THEN
   return 0;
END;
