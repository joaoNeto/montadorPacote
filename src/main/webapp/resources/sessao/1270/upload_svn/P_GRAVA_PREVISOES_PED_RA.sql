CREATE OR REPLACE PROCEDURE I_MDLOG.P_GRAVA_PREVISOES_PED_RA(pi_nUnidade      IN NUMBER,
                                                             pi_nNumeroPedido IN NUMBER)
IS
  --// Identificador de existir Produtos com Prazo Dif. no Pedido
  vvPossuiPrazoDif               VARCHAR2(1);

  --// Valores Totais dos Itens
  vnValorTotalPrazo              NUMBER(15,2);
  vnPercDescFinancMedio          NUMBER(6,4); --// Desconto Financeiro Medio requer 4 casas decimais
  vnValorTotalLegenda            NUMBER(15,2);

  --// Variaveis para Rateio do Valor das Parcelas
  TotalPorParcela                T110_PEDIDOS_PEND_ITENS.T110_PRECO_ITEM%TYPE;
  i                              INTEGER;
  Parcela                        T110_PEDIDOS_PEND_ITENS.T110_PRECO_ITEM%TYPE;
  TotaisItens                    NUMBER;
  Resto                          NUMBER;
  QtdeParcelas                   NUMBER;

  --// Nova Seq. Titulo Pagar
  ult_tit_pagar                  T900_PARAM_NUMFISC.T900_ULT_TITULO_PAGAR%TYPE;
  --// Parametro da Duplicata
  vnDATABASE_CALC_VENC_NF_CMP T925_PARAM_GERAIS_UNIDADES.T925_DATABASE_CALC_VENC_NF_CMP%TYPE;
  --// Data Limite da Duplicata
  d_t057_data_limite          T057_TITULO_PAGAR.T057_DATA_LIMITE%TYPE;
  --// Data Prevista Fatura e Data da Entrega do Pedido
  d_t108_dataprevista_fatura  T108_PEDIDOS_PEND.T108_DATAPREVISTA_FATURA%TYPE;
  d_t108_data_entrega         T108_PEDIDOS_PEND.T108_DATA_ENTREGA%TYPE;
  --// Tipo de Pedido
  n_t108_tipo_pedido          T108_PEDIDOS_PEND.T108_TIPO_PEDIDO%TYPE;
  nT108_FORNECEDOR_E          T108_PEDIDOS_PEND.T108_FORNECEDOR_E%TYPE;       

 /**********************
  Declaracao de Cursores
  **********************/

  --// Cursor de Prazos Distintos existentes nos Itens do Pedido
  CURSOR c_Prazos IS
    SELECT T110_PRAZO
      FROM T110_PEDIDOS_PEND_ITENS
     WHERE (T110_UNIDADE_IE = pi_nUnidade)
       AND (T110_NUMERO_PEDIDO_IE = pi_nNumeroPedido)
       AND (NVL(T110_STATUS_ITEM,0) = 0 OR
            NVL(T110_STATUS_ITEM,0) = 6)
     GROUP BY T110_PRAZO;
  
/*********************************
 INICIO DO PROCESSAMENTO PRINCIPAL
**********************************/
BEGIN

  --// Pesquisa Parametro da Duplicata
  SELECT T925_DATABASE_CALC_VENC_NF_CMP
    INTO vnDATABASE_CALC_VENC_NF_CMP
    FROM T925_PARAM_GERAIS_UNIDADES;

  --// Pesquisa Informacoes do Pedido
  BEGIN
    SELECT T108_DATAPREVISTA_FATURA,
           T108_DATA_ENTREGA,
           T108_TIPO_PEDIDO,
           T108_FORNECEDOR_E
      INTO d_t108_dataprevista_fatura,
           d_t108_data_entrega,
           n_t108_tipo_pedido,
           nT108_FORNECEDOR_E
      FROM T108_PEDIDOS_PEND
     WHERE (T108_UNIDADE_IE       = pi_nUnidade)
       AND (T108_NUMERO_PEDIDO_IU = pi_nNumeroPedido);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      d_t108_dataprevista_fatura := NULL;
      d_t108_data_entrega        := NULL;
      n_t108_tipo_pedido         := NULL;
  END;

 /************************************
  Se Pedido diferente de Transferencia
  ************************************/
  IF (NVL(n_t108_tipo_pedido,0) <> 8) THEN

   /*********************************************************
    Cursor de Prazos Distintos existentes nos Itens do Pedido
    *********************************************************/
    FOR vc_Prazos IN c_Prazos LOOP

     /***********************************************
      Obtem o Percentual de Desconto Financeiro Medio
               e o Valor Total do Prazo
      ***********************************************/
      PCOM_OBTEM_DESC_FIN_MEDIO_RV(0, -->> Chamado da Geracao dos Pedidos de Compra
                                   pi_nUnidade,
                                   nT108_FORNECEDOR_E,
                                   pi_nNumeroPedido,
                                   NULL, -->> Nesta Chamada nao precisa passar o Numero do Lote
                                   vc_Prazos.T110_PRAZO,
                                   NULL, -->> Nesta Chamada nao precisa passar a Nota Fiscal
                                   NULL, -->> Nesta Chamada nao precisa passar a Serie da NF
                                   NULL, -->> Nesta Chamada nao precisa passar a Data Emissao da NF
                                   NULL, -->> Nesta Chamada nao precisa passar o Tipo Emitente da NF
                                   NULL, -->> Nesta Chamada nao precisa passar o Codigo Emitente da NF
                                   vvPossuiPrazoDif,
                                   vnValorTotalPrazo,
                                   vnPercDescFinancMedio,
                                   vnValorTotalLegenda);

     
      --// Verifica a Qtde. de Parcelas cadastrada na Tabela de Prazos do Fornecedor
      SELECT COUNT(*)
        INTO QtdeParcelas
        FROM M003_016_TEMP_PRAZOS_PEDIDOS
       WHERE M003_016_UNIDADE    = pi_nUNIDADE
         AND M003_016_FORNECEDOR = nT108_FORNECEDOR_E;

      --// Inicializa Variavel para Controle da Parcela
      i := 0;

      FOR vc_PrazosFornecedor IN (SELECT M003_016_PRAZO_DIAS,   
                                         NVL(M003_016_DESCONTO,0) M003_016_DESCONTO 
                                   FROM M003_016_TEMP_PRAZOS_PEDIDOS
                                  WHERE M003_016_UNIDADE    = pi_nUNIDADE
                                    AND M003_016_FORNECEDOR = nT108_FORNECEDOR_E
                                 ORDER BY M003_016_PRAZO_DIAS) 
      LOOP

        --// Calcula Valor da Parcela
        i               := i + 1;
        TotaisItens     := TRUNC(vnValorTotalPrazo,2);
        TotalPorParcela := TRUNC((TotaisItens / QtdeParcelas),2);
        Resto           := TotaisItens - (TotalPorParcela * QtdeParcelas);
        IF i = 1 THEN
          Parcela       := (TotalPorParcela + Resto);
        ELSE
          Parcela       := (TotalPorParcela);
        END IF;

        --// Nova Seq. de Titulo a Pagar
        ult_tit_pagar := FSIS_INCREMENTANCAMPOS_RV('T900_PARAM_NUMFISC',
                                                   'T900_ULT_TITULO_PAGAR',
                                                   'WHERE T900_UNIDADE_IE = ' || TO_CHAR(pi_nUnidade),1);

        --// Gera a previs�o de duplicata por fornecedor.
        INSERT INTO T109_PEDIDOS_PEND_PRAZO (
                    T109_UNIDADE_IE,
                    T109_NUMERO_PEDIDO_IE,
                    T109_NUMERO_TITULO_IE,
                    T109_TIPO_DOCUMENTO_IE,
                    T109_PRAZO_PAG_IU,
                    T109_DESCONTO_LIMITE)
            VALUES (pi_nUNIDADE,
                    pi_nNumeroPedido,
                    ult_tit_pagar,
                    'DP',
                    vc_PrazosFornecedor.M003_016_PRAZO_DIAS,            -->> PRAZO DO CADASTRO DO FORNECEDOR
                    NVL(vc_PrazosFornecedor.M003_016_DESCONTO ,0)); -->> DESCONTO FINANCEIRO DE PRAZOS DO CADASTRO DO FORNECEDOR

        --// Data base for data de emissao
        IF vnDATABASE_CALC_VENC_NF_CMP = 1 THEN
          d_t057_data_limite := d_t108_dataprevista_fatura + NVL(vc_PrazosFornecedor.M003_016_PRAZO_DIAS,0); -->> APLICA PRAZO DO CADASTRO DO FORNECEDOR
        ELSE
          d_t057_data_limite := d_t108_data_entrega + NVL(vc_PrazosFornecedor.M003_016_PRAZO_DIAS,0);        -->> APLICA PRAZO DO CADASTRO DO FORNECEDOR
        END IF;
        -- CRC31308 - Roll-Out/S�o Bento
        BEGIN
        --// Insere Previsao de Duplicata
        INSERT INTO T057_TITULO_PAGAR(
                    T057_UNIDADE_IE,
                    T057_NUMERO_TITULO_IU,
                    T057_TIPO_DOCUMENTO_IU,
                    T057_FORNECEDOR_E,
                    T057_DATA_EMISSAO,
                    T057_DATA_RECEB,
                    T057_DATA_LIMITE,
                    T057_LOCAL_PAGAMENTO,
                    T057_SITUACAO_TITULO,
                    T057_VALOR_BRUTO,
                    T057_BASE_DESCONTO,
                    T057_CHEQUE_IMPRESSO,
                    T057_TIPO_INCLUSAO,
                    T057_DESCONTO_LIMITE,
                    T057_NUMERO_PEDIDO_E)
            VALUES (pi_nUNIDADE,
                    ult_tit_pagar,
                    'DP',
                    nT108_FORNECEDOR_E,
                    d_t108_dataprevista_fatura,
                    d_t108_data_entrega,
                    d_t057_data_limite,
                    'CAR',
                    'P',
                    Parcela,
                    Parcela,
                    'N',
                    'A',
                    NVL(vc_PrazosFornecedor.M003_016_DESCONTO ,0), -->> DESCONTO FINANCEIRO DE PRAZOS DO CADASTRO DO FORNECEDOR
                    pi_nNumeroPedido);
        EXCEPTION
          WHEN DUP_VAL_ON_INDEX THEN

          UPDATE T057_TITULO_PAGAR
             SET T057_FORNECEDOR_E      = nT108_FORNECEDOR_E,
                 T057_DATA_EMISSAO      = d_t108_dataprevista_fatura,
                 T057_DATA_RECEB        = d_t108_data_entrega,
                 T057_DATA_LIMITE       = d_t057_data_limite,
                 T057_LOCAL_PAGAMENTO   = 'CAR',
                 T057_SITUACAO_TITULO   = 'P',
                 T057_VALOR_BRUTO       = Parcela,
                 T057_BASE_DESCONTO     = Parcela,
                 T057_CHEQUE_IMPRESSO   = 'N',
                 T057_TIPO_INCLUSAO     = 'A',
                 T057_DESCONTO_LIMITE   = NVL(vc_PrazosFornecedor.M003_016_DESCONTO ,0),
                 T057_NUMERO_PEDIDO_E   = pi_nNumeroPedido
           WHERE T057_UNIDADE_IE = pi_nUNIDADE
             AND T057_NUMERO_TITULO_IU = ult_tit_pagar
             AND T057_TIPO_DOCUMENTO_IU = 'DP';

        END;

      END LOOP;

    END LOOP;

  END IF;

END;
/
