/*******************************************************************************
PROCEDURE : PCADASTRO_UNIDADE_MEDIDA
AUTOR     : TASSIANO ALENCAR
DATA      : 12/04/2017
CRC       : CRC70542
ALTERACAO : NOME - 00/00/0000 - CRC99999
DESCRICAO DA ALTERACAO.
*******************************************************************************/
CREATE OR REPLACE PROCEDURE I_MDLOG.PCADASTRO_UNIDADE_MEDIDA (
  PI_OPERACAO           VARCHAR2,
  PI_UNIDADE_MEDIDA     VARCHAR2,
  PI_UNIDADE_DESCRICAO  VARCHAR2,
  PO_STATUS OUT         VARCHAR2,
  PO_MSG    OUT         VARCHAR2
) IS

  -- Variaveis de erro
  ecode NUMBER(10);
  emesg VARCHAR2(2000);

BEGIN
  
  PO_STATUS := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT INTO T098_UNIDADE_MEDIDA
        (T098_UNIDADE_MEDIDA_IU, T098_DESCRICAO)
      VALUES
        (PI_UNIDADE_MEDIDA, PI_UNIDADE_DESCRICAO);
      EXCEPTION
      WHEN OTHERS THEN 
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      PO_STATUS := 'ER';
      PO_MSG := ecode||'-'||emesg;
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  
  /******************************BEGIN - EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
      UPDATE T098_UNIDADE_MEDIDA
      SET
        T098_DESCRICAO = PI_UNIDADE_DESCRICAO
      WHERE
        T098_UNIDADE_MEDIDA_IU = PI_UNIDADE_MEDIDA;
      EXCEPTION
      WHEN OTHERS THEN 
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      PO_STATUS := 'ER';
      PO_MSG := ecode||'-'||emesg;
    END;
  END IF;
  /********************************END - EDITAR********************************/
  
  /******************************BEGIN - EXCLUIR*******************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE FROM T098_UNIDADE_MEDIDA
      WHERE
        T098_UNIDADE_MEDIDA_IU = PI_UNIDADE_MEDIDA;
      EXCEPTION
      WHEN OTHERS THEN 
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      PO_STATUS := 'ER';
      PO_MSG := ecode||'-'||emesg;
    END;
  END IF;
  /********************************END - EXCLUIR*******************************/
END;