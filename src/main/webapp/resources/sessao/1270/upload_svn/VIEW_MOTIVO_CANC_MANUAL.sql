/**
* CRC  : CRC66980 - CRIA��O DE VIEW MOTIVO DE CANCELAMENTO MANUAL
*  AUTOR: ALBERTO MEDEIROS
*  DATA : 05/11/2015
*/
CREATE OR REPLACE VIEW I_MDLOG.VIEW_MOTIVO_CANC_MANUAL(CODIGO,DESCRICAO,TIPO,ROTINA)
AS
  SELECT T021_CODIGO_MOTIVO_IU,
    T021_DESCRICAO_MOTIVO,
    T021_TIPO_MOTIVO,
    T021_ROTINA_PROCESSAMENTO
  FROM DBAMDATA.T021_MOTIVO_CANC_PEDIDOS
  WHERE T021_TIPO_MOTIVO = 'M';
  
  
  