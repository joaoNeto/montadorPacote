/****************************************************************************** 
Schema : I_MDLOG
Analista : TASSIANO ALENCAR 
Data : 31/01/2018
CRC : 73309
Descricao : VIEW RESPONSÁVEL POR EXIBIR AS INFORMAÇÕES DO DRILLDOWN DA FICHA
            DE PEDIDOS
******************************************************************************/
DROP VIEW I_MDLOG.VIEW_ITENS_PEDIDO_CROSS
;
/