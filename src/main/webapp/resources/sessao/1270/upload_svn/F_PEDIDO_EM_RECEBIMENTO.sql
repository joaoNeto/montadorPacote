CREATE OR REPLACE FUNCTION I_MDLOG.F_PEDIDO_EM_RECEBIMENTO(P_UNIDADE NUMBER,
                                                           P_PEDIDO  NUMBER) RETURN BOOLEAN
IS
  vRet   BOOLEAN;
  vAux   INTEGER;
BEGIN

  vRet := TRUE;
  
  FOR VREG IN (SELECT T111_NUMERO_CARGA, 
                       TRUNC(T111_DATA_INICIO_RECEB_NF) T111_DATA_INICIO_RECEB_NF, 
                       T111_USUARIO_INICIO_RECEB_NF ||  ' - ' || T024_NOME AS T111_USUARIO_INICIO_RECEB_NF, 
                       T111_NOTA_FISCAL_IU 
                FROM T111_PEDIDOS_PEND_ITENS_NF, T024_USUARIO 
                WHERE T111_UNIDADE_IE       = P_UNIDADE
                  AND T111_NUMERO_PEDIDO_IE = P_PEDIDO 
                  AND T024_USUARIO_IU(+) = T111_USUARIO_INICIO_RECEB_NF 
                GROUP BY 
                  T111_NUMERO_CARGA, 
                  TRUNC(T111_DATA_INICIO_RECEB_NF), 
                  T111_USUARIO_INICIO_RECEB_NF, 
                  T024_NOME, 
                  T111_NOTA_FISCAL_IU 
                ORDER BY 
                  T111_NUMERO_CARGA, 
                  TRUNC(T111_DATA_INICIO_RECEB_NF), 
                  T111_USUARIO_INICIO_RECEB_NF, 
                  T024_NOME, 
                  T111_NOTA_FISCAL_IU )
  LOOP
    IF VREG.T111_NUMERO_CARGA IS NULL THEN
      vRet := FALSE;
    ELSE
      
      SELECT COUNT(*)
        INTO vAux
        FROM T209_LOTESTOQUE 
      WHERE T209_UNIDADE_IE     = P_UNIDADE
        AND T209_NUMERO_PEDIDO  = P_PEDIDO
        AND T209_CARGA          = VREG.T111_NUMERO_CARGA
        AND T209_DATA_LIBERACAO IS NOT NULL;
      
      IF vAux > 0 THEN
        vRet := FALSE; 
      END IF;
            
    END IF;
  
  END LOOP;
  
  RETURN vRet;          
  
END;
/
                                                  