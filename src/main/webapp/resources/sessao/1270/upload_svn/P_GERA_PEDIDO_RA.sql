create or replace PROCEDURE I_MDLOG.P_GERA_PEDIDO_RA(PI_TIPO_PEDIDO            NUMBER,
                                                     PI_FORNECEDOR             NUMBER,
                                                     PI_UNIDADE_CENTRALIZADORA NUMBER,
                                                     PI_TIPO_FRETE             NUMBER,
                                                     PI_TRANSPORTADORA         NUMBER,
                                                     PI_OBSERVACAO             VARCHAR2,
                                                     PI_DATA_PREV_FATURA       DATE,
                                                     PI_DATA_PREV_ENTREGA      DATE,
                                                     PI_NUMERO_OS              VARCHAR2,
                                                     PO_PEDIDOS_GERADOS        OUT VARCHAR2,
                                                     PI_OC_ESPECIAL            VARCHAR2 DEFAULT 'N',
                                                     PI_TIPO_BONIFICACAO       NUMBER DEFAULT NULL
                                                     )
/*******************************************************************************
  NOME      : P_GERA_PEDIDO_RA
  DESCRICAO : Procedure para gera��o do Pedido atrav�s do RA do MDLOGWeb
  ANALISTA  : H�lio Sales

              PI_TIPO_PEDIDO : 1 - Direto Loja
                               2 - Pedido �nico
                               3 - Cross-Docking Fiscal - NEGR�O UTILIZA APENAS ESSA OP��O DE CROSS DOCKING
                               4 - Cross-Docking F�sico 

*******************************************************************************/
IS
--123456798012345679801234567980
  nSUGESTAO                      NUMBER;
  poPedidosGerados               VARCHAR2(1000);
  poPedidosGeradosLojasCrossDock VARCHAR2(1000);
  vPedidoCrossDockingUnidCD      VARCHAR2(10);
  vUnidadeDoPedido               NUMBER;
  vTotalReposicaoOutrasUnidades  NUMBER;
  
  vUsuarioOrigem                 I_MDLOG.T007_USUARIO_ORIGEM.T007_USUARIO_ORIGEM_IU%TYPE;
  
  vM03_012_PRECO_SUGESTAO        I_MDLOG.M03_012_SUG_COMPRA_DIA.M03_012_PRECO_SUGESTAO%TYPE;
  vM03_012_PRECO_REPOSICAO       I_MDLOG.M03_012_SUG_COMPRA_DIA.M03_012_PRECO_REPOSICAO%TYPE;
  vM03_012_REPOSICAO_UND         I_MDLOG.M03_012_SUG_COMPRA_DIA.M03_012_REPOSICAO_UND%TYPE;
  vM03_012_DESCONTO_ITEM         I_MDLOG.M03_012_SUG_COMPRA_DIA.M03_012_DESCONTO_ITEM%TYPE;
  vM03_012_DESCONTO_REPASSE      I_MDLOG.M03_012_SUG_COMPRA_DIA.M03_012_DESCONTO_REPASSE%TYPE;
  vM03_012_DESCONTO_COMERCIAL    I_MDLOG.M03_012_SUG_COMPRA_DIA.M03_012_DESCONTO_COMERCIAL%TYPE;
  vM03_012_EMBALAGEM_COMPRA      I_MDLOG.M03_012_SUG_COMPRA_DIA.M03_012_EMBALAGEM_COMPRA%TYPE;
  vM03_012_TAXA_VENDOR           I_MDLOG.M03_012_SUG_COMPRA_DIA.M03_012_TAXA_VENDOR%TYPE;
  vM03_012_PERC_ARRED_EMB_PROD   I_MDLOG.M03_012_SUG_COMPRA_DIA.M03_012_PERC_ARRED_EMB_PROD%TYPE;
  vPRODUTO_NOVO                  VARCHAR2(1);
  vNovaQtde                      NUMBER;
  vDataAtual                     DATE;  
  vQtdeItem                      NUMBER;
  vQtdeItemNoPedido              NUMBER;
  vM03_015_FORMA_ENTREGA         VARCHAR2(1);
  
  tT190_SUGESTAO_COMPRA          T190_SUGESTAO_COMPRA%ROWTYPE;  
  tT191_SUGESTAO_COMPRA_ITEM     T191_SUGESTAO_COMPRA_ITEM%ROWTYPE;
  vForncedorCD                   NUMBER;
  vAux                           INTEGER;
  
  FUNCTION F_CALC_QTDE_EMB(P_QDTE_ORIGINAL NUMBER,
                           P_EMB_FORNEC    NUMBER,
                           P_PERC_ARRED    NUMBER)
  RETURN NUMBER
  IS
    vQtdeRet  NUMBER;
    
    vQtdeEmbTrunc  NUMBER;
    vQtdeEmb       NUMBER;
    vFracao        NUMBER;
    vFatorEmb      NUMBER;
  BEGIN
    vQtdeEmb      := (P_QDTE_ORIGINAL / P_EMB_FORNEC);
    vQtdeEmbTrunc := trunc(P_QDTE_ORIGINAL / P_EMB_FORNEC);    
    
    if (vQtdeEmb = vQtdeEmbTrunc) THEN
      
      vQtdeRet := vQtdeEmb;
      
    ELSE
      
      vFracao := trunc((vQtdeEmb - vQtdeEmbTrunc) * 100);
      
      if (vFracao > P_PERC_ARRED) THEN
        vFatorEmb := 1;
      ELSE
        vFatorEmb := 0;
      END IF;
      
      vQtdeRet := (vQtdeEmbTrunc + vFatorEmb) * P_EMB_FORNEC;        
    END IF;   
    
    return vQtdeRet;      
  END;

  PROCEDURE P_ATUAL_TIPO_FRETE_TRANS_OBSER(PI_PEDIDOS  IN VARCHAR2)
  IS
    vT903_MENSAGEM_ORDEM_COMPRA T903_PARAM_COMPRAS.T903_MENSAGEM_ORDEM_COMPRA%type;
  BEGIN
  
    FOR vPedidos IN (select regexp_substr(PI_PEDIDOS,'[^,]+', 1, level) PEDIDO from dual
                     connect by regexp_substr(PI_PEDIDOS, '[^,]+', 1, level) is not null
                    )
    LOOP

      SELECT T903_MENSAGEM_ORDEM_COMPRA
        into vT903_MENSAGEM_ORDEM_COMPRA
      FROM T903_PARAM_COMPRAS
      WHERE T903_UNIDADE_IE = vUnidadeDoPedido;
    
      IF PI_OC_ESPECIAL = 'N' THEN

        UPDATE T108_PEDIDOS_PEND
        SET T108_TRANSPORTADORA_E = CASE WHEN PI_TRANSPORTADORA = 0 THEN NULL ELSE PI_TRANSPORTADORA END, 
            T108_TIPO_FRETE       = CASE WHEN PI_TIPO_FRETE = 1 THEN 'F' ELSE 'C' END,
            T108_OBSERVACAO       = vT903_MENSAGEM_ORDEM_COMPRA || '.' || PI_OBSERVACAO ||  '. OS: ' || PI_NUMERO_OS,
            T108_TIPO_PEDIDO      = 'P'
        WHERE T108_UNIDADE_IE       = vUnidadeDoPedido
          AND T108_NUMERO_PEDIDO_IU = vPedidos.PEDIDO;

      ELSE

        UPDATE T108_PEDIDOS_PEND
        SET T108_TRANSPORTADORA_E = CASE WHEN PI_TRANSPORTADORA = 0 THEN NULL ELSE PI_TRANSPORTADORA END, 
            T108_TIPO_FRETE       = CASE WHEN PI_TIPO_FRETE = 1 THEN 'F' ELSE 'C' END,
            T108_OBSERVACAO       = vT903_MENSAGEM_ORDEM_COMPRA || '.' || PI_OBSERVACAO ||  '. OS: ' || PI_NUMERO_OS,
            --T108_TIPO_BONIFICACAO_E = CASE WHEN PI_TIPO_BONIFICACAO > 0 THEN PI_TIPO_BONIFICACAO ELSE NULL END,
            T108_TIPO_PEDIDO      = 'P'
        WHERE T108_UNIDADE_IE       = vUnidadeDoPedido
          AND T108_NUMERO_PEDIDO_IU = vPedidos.PEDIDO;      

      END IF; 
      
      --I_MDLOG.P_GRAVA_PREVISOES_PED_RA(vUnidadeDoPedido,
      --                                 vPedidos.PEDIDO);

      UPDATE T108_PEDIDOS_PEND
      SET T108_DATAPREVISTA_FATURA= PI_DATA_PREV_FATURA, 
          T108_DATA_ENTREGA       = PI_DATA_PREV_ENTREGA
      WHERE  T108_UNIDADE_IE      = vUnidadeDoPedido
        AND T108_NUMERO_PEDIDO_IU = vPedidos.PEDIDO;      
        
      PO_PEDIDOS_GERADOS := PO_PEDIDOS_GERADOS || vUnidadeDoPedido ||','||vPedidos.PEDIDO ||';'; 

    END LOOP;  

  END;

  PROCEDURE P_GERA_CABECALHO(ptT190_SUGESTAO_COMPRA IN T190_SUGESTAO_COMPRA%ROWTYPE)
  IS
  BEGIN
    INSERT INTO dbamdata.T190_SUGESTAO_COMPRA (
         T190_UNIDADE_IE,          
         T190_SUGESTAO_COMPRA_IU,  
         T190_DATA_SUGESTAO,      
         T190_USUARIO_E,
         T190_TIPOPENDFOREC,       
         T190_TIPOSUGESTAO,
         T190_TIPO_GERACAO,
         T190_STATUS_MANIPULACAO) 
       VALUES (
         ptT190_SUGESTAO_COMPRA.T190_UNIDADE_IE,
         ptT190_SUGESTAO_COMPRA.T190_SUGESTAO_COMPRA_IU,
         TRUNC(SYSDATE),
         ptT190_SUGESTAO_COMPRA.T190_USUARIO_E,
         'S',                      
         0,                      
         'C',
         9);  
  END;
  
  PROCEDURE P_GERA_ITENS(ptT191_SUGESTAO_COMPRA_ITEM IN T191_SUGESTAO_COMPRA_ITEM%ROWTYPE)
  IS
  BEGIN
  
    INSERT INTO T191_SUGESTAO_COMPRA_ITEM(
             T191_UNIDADE_SUGESTAO_IE, 
             T191_SUGESTAO_COMPRA_IE,   
             T191_UNIDADE_FATURA_IE,    
             T191_PRODUTO_IE,
             T191_ESTPAD_FACING_LOJA,  
             T191_SALDO_ESTOQUE,        
             T191_SUGESTAO_ORIGINAL,    
             T191_MEDIA_VENDASDIA,
             T191_PENDENCIA,           
             T191_SUGESTAO_CXFECHADA,   
             T191_SUGESTAO_UNITARIA,    
             T191_DIAS_ESTOQUE,        
             T191_EMBALAGEM_PADRAO,     
             T191_FORNECEDOR_E,         
             T191_OBSERVACAO,
             T191_PRODUTO_NOVO,        
             T191_ESTQ_PAD_FACING_TEMP, 
             T191_CURVA_PRODUTO,        
             T191_ESTQ_PAD_FACING_SUGESTAO,
             T191_PRECO_UNITARIO,      
             T191_DESCONTO_ITEM,        
             T191_SUGESTAO_PALETT,
             T191_PERC_EMBALAGEM,
             T191_PERC_DESPACESS  ) 
          VALUES(
             ptT191_SUGESTAO_COMPRA_ITEM.T191_UNIDADE_SUGESTAO_IE,             
             ptT191_SUGESTAO_COMPRA_ITEM.T191_SUGESTAO_COMPRA_IE,                 
             ptT191_SUGESTAO_COMPRA_ITEM.T191_UNIDADE_FATURA_IE,        
             ptT191_SUGESTAO_COMPRA_ITEM.T191_PRODUTO_IE,
             NULL, --vnEstoqueFacingNormal,    
             NULL, --VN_SALDO_ESTOQUE,          
             ptT191_SUGESTAO_COMPRA_ITEM.T191_SUGESTAO_UNITARIA,      
             NULL,--VN_T077_MEDIA_VENDADIA,
             NULL,--VN_PENDENCIA_FORNECEDOR,  
             NULL,--VN_SUGESTAO_CAIXA,         
             ptT191_SUGESTAO_COMPRA_ITEM.T191_SUGESTAO_UNITARIA,        
             NULL,--VN_DIASESTOQUE,           
             ptT191_SUGESTAO_COMPRA_ITEM.T191_EMBALAGEM_PADRAO,  
             ptT191_SUGESTAO_COMPRA_ITEM.T191_FORNECEDOR_E,
             null,
             ptT191_SUGESTAO_COMPRA_ITEM.T191_PRODUTO_NOVO,
             NULL,--vnEstoqueFacingTemporario, 
             NULL,--VV_T079_CURVAABC_UNIDADE,  
             NULL,--vnEstoqueFacingSugestao,
             ptT191_SUGESTAO_COMPRA_ITEM.T191_PRECO_UNITARIO,         
             ptT191_SUGESTAO_COMPRA_ITEM.T191_DESCONTO_ITEM,                         
             NULL,
             0,
			       ptT191_SUGESTAO_COMPRA_ITEM.T191_PERC_DESPACESS
             );
  END;
  
  FUNCTION F_RETORNA_QTDE_DEFINIDA_ITEM(P_UNIDADE NUMBER,
                                        P_PEDIDO  NUMBER,
                                        P_PRODUTO NUMBER) RETURN NUMBER
  IS
    vRetorno  NUMBER;
  BEGIN
    
    SELECT T110_QTDE_PEDIDO
      INTO  vRetorno  
    FROM T110_PEDIDOS_PEND_ITENS
    WHERE T110_UNIDADE_IE       = P_UNIDADE
      AND T110_NUMERO_PEDIDO_IE = P_PEDIDO
      AND T110_PRODUTO_IE       = P_PRODUTO;
      
    RETURN vRetorno;     
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 0;
  END;
  
  /*
  PROCEDURE P_ATUALIZA_PRAZOS_PED_CROSS(P_UNIDADE NUMBER,
                                        P_PEDIDO  NUMBER)
                                        
  IS
  BEGIN
  
    FOR vPedidos IN (
                     SELECT DISTINCT T140_UNIDADE_IE, T140_NUMERO_PEDIDO_IE,
                            NVL(T054_PRAZO_ENTREGA_CD_LOJA,0) PRAZOS
                     FROM T140_PEDIDOSPEN_CROSSDOCK_CD
                       JOIN T108_PEDIDOS_PEND ON
                         T108_UNIDADE_IE = T140_UNIDADE_IE AND 
                         T108_NUMERO_PEDIDO_IU = T140_NUMERO_PEDIDO_IE
                       JOIN T054_FORNECEDOR_UNIDADE ON 
                         T054_UNIDADE_IE = T108_UNIDADE_IE AND 
                         T054_FORNECEDOR_IE = T108_FORNECEDOR_E                         
                     WHERE T140_UNIDADE_CD_IE      = P_UNIDADE
                      AND T140_NUMERO_PEDIDO_CD_IE = P_PEDIDO
                    )
    LOOP
      --//PI_DATA_PREV_ENTREGA � o prazo do pedido Pai (CrossDocking), logo os filhos receber�o na T108_DATAPREVISTA_FATURA
      --//a DATA_PREV_ENTREGA do pedido Pai, e a T108_DATA_ENTREGA dos filhos ser� a PI_DATA_PREV_ENTREGA(Pai) mais os prazos do cadastro.
      UPDATE T108_PEDIDOS_PEND
      SET T108_DATAPREVISTA_FATURA = PI_DATA_PREV_ENTREGA,
          T108_DATA_ENTREGA        = PI_DATA_PREV_ENTREGA + vPedidos.PRAZOS 
      WHERE  T108_UNIDADE_IE      = vPedidos.T140_UNIDADE_IE
        AND T108_NUMERO_PEDIDO_IU = vPedidos.T140_NUMERO_PEDIDO_IE;

    END LOOP;  

  END;
  */

BEGIN

  PO_PEDIDOS_GERADOS := '';
  IF PI_OC_ESPECIAL = 'N' THEN
    SELECT MAX(M03_012_DATA_SUG_IE)
      INTO vDataAtual  
    FROM I_MDLOG.M03_012_SUG_COMPRA_DIA
    WHERE M03_012_REPOSICAO_UND > 0
      AND M03_012_FORNECEDOR_IE = PI_FORNECEDOR
      AND M03_012_ORDEM_SERVICO_IE = PI_NUMERO_OS;
  ELSE
    SELECT MAX(M03_016_DATA_SUG_IE)
      INTO vDataAtual  
    FROM I_MDLOG.M03_016_OC_ESPECIAL
    WHERE M03_016_REPOSICAO_UND > 0
      AND M03_016_FORNECEDOR_IE = PI_FORNECEDOR
      AND M03_016_ORDEM_SERVICO_IE = PI_NUMERO_OS;  
  END IF;


  --//DIRETO LOJA/CD  OU   CROSS DOCIKING FISCAL
  IF PI_TIPO_PEDIDO IN (1, 3) THEN
    IF PI_OC_ESPECIAL = 'N' THEN
      FOR vReg IN (SELECT DISTINCT M03_012_UNIDADE_IE, M03_012_FORNECEDOR_IE, M03_012_DATA_SUG_IE, T007_USUARIO_ORIGEM_IU
                   FROM I_MDLOG.M03_012_SUG_COMPRA_DIA,
                        I_MDLOG.T007_USUARIO_ORIGEM
                   WHERE M03_012_COMPRADOR_IE  = T007_USUARIO_IE
                     and M03_012_FORNECEDOR_IE = PI_FORNECEDOR
                     AND M03_012_REPOSICAO_UND > 0
                     AND M03_012_DATA_SUG_IE = vDataAtual
                     AND M03_012_ORDEM_SERVICO_IE = PI_NUMERO_OS
                   )
      LOOP
        if PI_TIPO_PEDIDO = 1 then--//DIRETO LOJA ASSUME A LOJA COMO A UNIDADE DO PEDIDO
          vUnidadeDoPedido := vReg.M03_012_UNIDADE_IE;
        else--//CROSS DOCIKING ASSUME A UNIDADE CENTRALIZDARA COMO A UNIDADE DO PEDIDO
          vUnidadeDoPedido := PI_UNIDADE_CENTRALIZADORA;
        end if;
        
        nSUGESTAO := FSIS_INCREMENTACAMPO('T900_PARAM_NUMFISC',
                                          'T900_ULT_SUGESTAO_COMPRA',
                                          'WHERE T900_UNIDADE_IE = ' || TO_CHAR(vUnidadeDoPedido));
        
        tT190_SUGESTAO_COMPRA.T190_UNIDADE_IE         := vUnidadeDoPedido;
        tT190_SUGESTAO_COMPRA.T190_SUGESTAO_COMPRA_IU := nSUGESTAO;
        tT190_SUGESTAO_COMPRA.T190_USUARIO_E          := vReg.T007_USUARIO_ORIGEM_IU;
        P_GERA_CABECALHO(tT190_SUGESTAO_COMPRA);      
        
        FOR vRegItens IN (SELECT M03_012_PRODUTO_IE, 
                                 M03_012_PRECO_REPOSICAO, 
                                 M03_012_REPOSICAO_UND,
                                 M03_012_DESCONTO_ITEM, 
                                 M03_012_DESCONTO_REPASSE, 
                                 M03_012_DESCONTO_COMERCIAL,
                                 M03_012_EMBALAGEM_COMPRA,
                                 CASE WHEN M03_012_PRODUTO_NOVO = 1 THEN 'S' ELSE 'N' END PRODUTO_NOVO,
                                 M03_012_TAXA_VENDOR,
                                 M03_012_PERC_ARRED_EMB_PROD,
                                 M03_012_PRODUTO_NOVO
                          FROM I_MDLOG.M03_012_SUG_COMPRA_DIA
                          WHERE M03_012_REPOSICAO_UND > 0
                            and M03_012_FORNECEDOR_IE = PI_FORNECEDOR
                            AND M03_012_UNIDADE_IE    = vReg.M03_012_UNIDADE_IE
                            AND M03_012_DATA_SUG_IE = vDataAtual
                            AND M03_012_ORDEM_SERVICO_IE = PI_NUMERO_OS)
        LOOP
        
          tT191_SUGESTAO_COMPRA_ITEM.T191_UNIDADE_SUGESTAO_IE := vUnidadeDoPedido;     
          tT191_SUGESTAO_COMPRA_ITEM.T191_SUGESTAO_COMPRA_IE  := nSUGESTAO;
          tT191_SUGESTAO_COMPRA_ITEM.T191_UNIDADE_FATURA_IE   := vUnidadeDoPedido;
          tT191_SUGESTAO_COMPRA_ITEM.T191_PRODUTO_IE          := vRegItens.M03_012_PRODUTO_IE;
          tT191_SUGESTAO_COMPRA_ITEM.T191_SUGESTAO_ORIGINAL   := vRegItens.M03_012_REPOSICAO_UND;
          tT191_SUGESTAO_COMPRA_ITEM.T191_SUGESTAO_UNITARIA   := vRegItens.M03_012_REPOSICAO_UND;
          tT191_SUGESTAO_COMPRA_ITEM.T191_FORNECEDOR_E        := vReg.M03_012_FORNECEDOR_IE;
          tT191_SUGESTAO_COMPRA_ITEM.T191_PRECO_UNITARIO      := vRegItens.M03_012_PRECO_REPOSICAO;
          tT191_SUGESTAO_COMPRA_ITEM.T191_DESCONTO_ITEM       := vRegItens.M03_012_DESCONTO_ITEM;
          tT191_SUGESTAO_COMPRA_ITEM.T191_PERC_DESPACESS      := vRegItens.M03_012_TAXA_VENDOR;        
          tT191_SUGESTAO_COMPRA_ITEM.T191_EMBALAGEM_PADRAO    := vRegItens.M03_012_EMBALAGEM_COMPRA;
          tT191_SUGESTAO_COMPRA_ITEM.T191_PRODUTO_NOVO        := vRegItens.M03_012_PRODUTO_NOVO;
          tT191_SUGESTAO_COMPRA_ITEM.T191_PERC_EMBALAGEM      := 0;
          
          P_GERA_ITENS(tT191_SUGESTAO_COMPRA_ITEM); 
  
        END LOOP;
        
        I_MDLOG.P_GRAVA_PEDIDO_RA_ERP(vUnidadeDoPedido,
                                      nSUGESTAO,
                                      poPedidosGerados); 
                                      
        IF PI_TIPO_PEDIDO = 3 THEN
          
          IF vReg.M03_012_UNIDADE_IE <> PI_UNIDADE_CENTRALIZADORA THEN
            
            FOR vPedidos IN (select regexp_substr(poPedidosGerados,'[^,]+', 1, level) PEDIDO from dual
                             connect by regexp_substr(poPedidosGerados, '[^,]+', 1, level) is not null
                            )
            LOOP
              UPDATE T108_PEDIDOS_PEND
              SET T108_FLAG_CROSS_DOCKING = 'S'
              WHERE T108_UNIDADE_IE       = PI_UNIDADE_CENTRALIZADORA
                AND T108_NUMERO_PEDIDO_IU = vPedidos.PEDIDO;
                
              INSERT INTO T668_PEDIDOS_PEND_UNID_CRDOCK
              (T668_UNIDADE_IE, T668_NUMERO_PEDIDO_IE, T668_UNID_CROSSDOCKING)
              values
              (PI_UNIDADE_CENTRALIZADORA, vPedidos.PEDIDO, vReg.M03_012_UNIDADE_IE);
  
            END LOOP; 
  
          END IF;
  
        END IF;  
        
        P_ATUAL_TIPO_FRETE_TRANS_OBSER(poPedidosGerados);                                                  
  
      END LOOP;
    
    
    ELSE--//OC Especial
    
      FOR vReg IN (  SELECT DISTINCT M03_016_UNIDADE_IE, M03_016_FORNECEDOR_IE, M03_016_DATA_SUG_IE, T007_USUARIO_ORIGEM_IU
                   FROM I_MDLOG.M03_016_OC_ESPECIAL,
                        I_MDLOG.T007_USUARIO_ORIGEM
                   WHERE M03_016_COMPRADOR_IE  = T007_USUARIO_IE
                     and M03_016_FORNECEDOR_IE = PI_FORNECEDOR
                     AND M03_016_REPOSICAO_UND > 0
                     AND M03_016_DATA_SUG_IE = vDataAtual
                     AND M03_016_ORDEM_SERVICO_IE = PI_NUMERO_OS
                   )
      LOOP
        if PI_TIPO_PEDIDO = 1 then--//DIRETO LOJA ASSUME A LOJA COMO A UNIDADE DO PEDIDO
          vUnidadeDoPedido := vReg.M03_016_UNIDADE_IE;
        else--//CROSS DOCIKING ASSUME A UNIDADE CENTRALIZDARA COMO A UNIDADE DO PEDIDO
          vUnidadeDoPedido := PI_UNIDADE_CENTRALIZADORA;
        end if;
        
        nSUGESTAO := FSIS_INCREMENTACAMPO('T900_PARAM_NUMFISC',
                                          'T900_ULT_SUGESTAO_COMPRA',
                                          'WHERE T900_UNIDADE_IE = ' || TO_CHAR(vUnidadeDoPedido));
        
        tT190_SUGESTAO_COMPRA.T190_UNIDADE_IE         := vUnidadeDoPedido;
        tT190_SUGESTAO_COMPRA.T190_SUGESTAO_COMPRA_IU := nSUGESTAO;
        tT190_SUGESTAO_COMPRA.T190_USUARIO_E          := vReg.T007_USUARIO_ORIGEM_IU;
        P_GERA_CABECALHO(tT190_SUGESTAO_COMPRA);      
        
        FOR vRegItens IN (SELECT M03_016_PRODUTO_IE, 
                                 M03_016_PRECO_REPOSICAO,
                                 M03_016_REPOSICAO_UND,
                                 M03_016_DESCONTO_ITEM, 
                                 M03_016_DESCONTO_REPASSE, 
                                 M03_016_DESCONTO_COMERCIAL,
                                 M03_016_EMBALAGEM_COMPRA,
                                 CASE WHEN M03_016_PRODUTO_NOVO = 1 THEN 'S' ELSE 'N' END PRODUTO_NOVO,
                                 M03_016_TAXA_VENDOR,
                                 M03_016_PERC_ARRED_EMB_PROD
                          FROM I_MDLOG.M03_016_OC_ESPECIAL
                          WHERE M03_016_REPOSICAO_UND > 0
                            and M03_016_FORNECEDOR_IE    = PI_FORNECEDOR
                            AND M03_016_UNIDADE_IE       = vReg.M03_016_UNIDADE_IE
                            AND M03_016_DATA_SUG_IE      = vDataAtual
                            AND M03_016_ORDEM_SERVICO_IE = PI_NUMERO_OS)
        LOOP
          tT191_SUGESTAO_COMPRA_ITEM.T191_UNIDADE_SUGESTAO_IE := vUnidadeDoPedido;     
          tT191_SUGESTAO_COMPRA_ITEM.T191_SUGESTAO_COMPRA_IE  := nSUGESTAO;
          tT191_SUGESTAO_COMPRA_ITEM.T191_UNIDADE_FATURA_IE   := vUnidadeDoPedido;
          tT191_SUGESTAO_COMPRA_ITEM.T191_PRODUTO_IE          := vRegItens.M03_016_PRODUTO_IE;
          tT191_SUGESTAO_COMPRA_ITEM.T191_SUGESTAO_ORIGINAL   := vRegItens.M03_016_REPOSICAO_UND;
          tT191_SUGESTAO_COMPRA_ITEM.T191_SUGESTAO_UNITARIA   := vRegItens.M03_016_REPOSICAO_UND;
          tT191_SUGESTAO_COMPRA_ITEM.T191_FORNECEDOR_E        := vReg.M03_016_FORNECEDOR_IE;
          tT191_SUGESTAO_COMPRA_ITEM.T191_PRECO_UNITARIO      := vRegItens.M03_016_PRECO_REPOSICAO;
          tT191_SUGESTAO_COMPRA_ITEM.T191_DESCONTO_ITEM       := vRegItens.M03_016_DESCONTO_ITEM;
          tT191_SUGESTAO_COMPRA_ITEM.T191_PERC_DESPACESS      := vRegItens.M03_016_TAXA_VENDOR;          
          tT191_SUGESTAO_COMPRA_ITEM.T191_EMBALAGEM_PADRAO    := vRegItens.M03_016_EMBALAGEM_COMPRA;
          tT191_SUGESTAO_COMPRA_ITEM.T191_PERC_EMBALAGEM      := 0;          
          P_GERA_ITENS(tT191_SUGESTAO_COMPRA_ITEM);
        END LOOP;
        
        I_MDLOG.P_GRAVA_PEDIDO_RA_ERP(vUnidadeDoPedido,
                                      nSUGESTAO,
                                      poPedidosGerados); 
                                      
        IF PI_TIPO_PEDIDO = 3 THEN
          
          IF vReg.M03_016_UNIDADE_IE <> PI_UNIDADE_CENTRALIZADORA THEN
            
            FOR vPedidos IN (select regexp_substr(poPedidosGerados,'[^,]+', 1, level) PEDIDO from dual
                             connect by regexp_substr(poPedidosGerados, '[^,]+', 1, level) is not null
                            )
            LOOP
              UPDATE T108_PEDIDOS_PEND
              SET T108_FLAG_CROSS_DOCKING = 'S'
              WHERE T108_UNIDADE_IE       = PI_UNIDADE_CENTRALIZADORA
                AND T108_NUMERO_PEDIDO_IU = vPedidos.PEDIDO;
                
              INSERT INTO T668_PEDIDOS_PEND_UNID_CRDOCK
              (T668_UNIDADE_IE, T668_NUMERO_PEDIDO_IE, T668_UNID_CROSSDOCKING)
              values
              (PI_UNIDADE_CENTRALIZADORA, vPedidos.PEDIDO, vReg.M03_016_UNIDADE_IE);
  
            END LOOP; 
  
          END IF;
  
        END IF; 
        
        P_ATUAL_TIPO_FRETE_TRANS_OBSER(poPedidosGerados); 
        
      END LOOP;   
      
    END IF;

  --/PEDIDO UNICO 
  ELSIF PI_TIPO_PEDIDO = 2 THEN
    vUnidadeDoPedido := PI_UNIDADE_CENTRALIZADORA;
    nSUGESTAO := FSIS_INCREMENTACAMPO('T900_PARAM_NUMFISC',
                                        'T900_ULT_SUGESTAO_COMPRA',
                                        'WHERE T900_UNIDADE_IE = ' || TO_CHAR(PI_UNIDADE_CENTRALIZADORA));
                                        
    SELECT DISTINCT T007_USUARIO_ORIGEM_IU 
    into vUsuarioOrigem       
    FROM I_MDLOG.M03_012_SUG_COMPRA_DIA,
         I_MDLOG.T007_USUARIO_ORIGEM     
    WHERE M03_012_COMPRADOR_IE  = T007_USUARIO_IE
      and M03_012_FORNECEDOR_IE = PI_FORNECEDOR
      and M03_012_UNIDADE_IE    = PI_UNIDADE_CENTRALIZADORA
      AND M03_012_DATA_SUG_IE   = vDataAtual
      AND M03_012_ORDEM_SERVICO_IE = PI_NUMERO_OS
      and rownum <= 1;  
    
    tT190_SUGESTAO_COMPRA.T190_UNIDADE_IE         := PI_UNIDADE_CENTRALIZADORA;
    tT190_SUGESTAO_COMPRA.T190_SUGESTAO_COMPRA_IU := nSUGESTAO;
    tT190_SUGESTAO_COMPRA.T190_USUARIO_E          := vUsuarioOrigem;    
    P_GERA_CABECALHO(tT190_SUGESTAO_COMPRA);
         
    FOR vRegItens IN (SELECT M03_012_PRODUTO_IE,
                             SUM(M03_012_REPOSICAO_UND) QTDE_REPOSICAO
                      FROM I_MDLOG.M03_012_SUG_COMPRA_DIA
                      WHERE M03_012_REPOSICAO_UND > 0
                        AND M03_012_FORNECEDOR_IE = PI_FORNECEDOR
                        AND M03_012_DATA_SUG_IE = vDataAtual
                        AND M03_012_ORDEM_SERVICO_IE = PI_NUMERO_OS
                      GROUP BY M03_012_PRODUTO_IE)
    LOOP
    
      begin
        --//PARA CADA PRODUTO, BUSCAR AS INFORMA��ES DO PRODUTO NA UNIDADE CENTRALIZADORA
        SELECT distinct M03_012_PRECO_SUGESTAO, 
               M03_012_PRECO_REPOSICAO,
               M03_012_REPOSICAO_UND,
               M03_012_DESCONTO_ITEM, 
               M03_012_DESCONTO_REPASSE, 
               M03_012_DESCONTO_COMERCIAL,
               M03_012_EMBALAGEM_COMPRA,
               CASE WHEN M03_012_PRODUTO_NOVO = 1 THEN 'S' ELSE 'N' END PRODUTO_NOVO,
               M03_012_TAXA_VENDOR,
               M03_012_PERC_ARRED_EMB_PROD
          INTO vM03_012_PRECO_SUGESTAO,
               vM03_012_PRECO_REPOSICAO,
               vM03_012_REPOSICAO_UND,
               vM03_012_DESCONTO_ITEM, 
               vM03_012_DESCONTO_REPASSE, 
               vM03_012_DESCONTO_COMERCIAL,
               vM03_012_EMBALAGEM_COMPRA,
               vPRODUTO_NOVO,
               vM03_012_TAXA_VENDOR,
               vM03_012_PERC_ARRED_EMB_PROD         
        FROM I_MDLOG.M03_012_SUG_COMPRA_DIA
        WHERE M03_012_REPOSICAO_UND > 0
          AND M03_012_UNIDADE_IE    = PI_UNIDADE_CENTRALIZADORA 
          AND M03_012_FORNECEDOR_IE = PI_FORNECEDOR
          AND M03_012_PRODUTO_IE    = vRegItens.M03_012_PRODUTO_IE
          and rownum <= 1;
      exception 
        when others then
        begin
          --//PARA CADA PRODUTO, BUSCAR AS INFORMA��ES DO PRODUTO NA UNIDADE CENTRALIZADORA
          SELECT distinct M03_012_PRECO_SUGESTAO,
                 M03_012_PRECO_REPOSICAO,
                 M03_012_REPOSICAO_UND,
                 M03_012_DESCONTO_ITEM, 
                 M03_012_DESCONTO_REPASSE, 
                 M03_012_DESCONTO_COMERCIAL,
                 M03_012_EMBALAGEM_COMPRA,
                 CASE WHEN M03_012_PRODUTO_NOVO = 1 THEN 'S' ELSE 'N' END PRODUTO_NOVO,
                 M03_012_TAXA_VENDOR,
                 M03_012_PERC_ARRED_EMB_PROD
            INTO vM03_012_PRECO_SUGESTAO,
                 vM03_012_PRECO_REPOSICAO,
                 vM03_012_REPOSICAO_UND,
                 vM03_012_DESCONTO_ITEM, 
                 vM03_012_DESCONTO_REPASSE, 
                 vM03_012_DESCONTO_COMERCIAL,
                 vM03_012_EMBALAGEM_COMPRA,
                 vPRODUTO_NOVO,
                 vM03_012_TAXA_VENDOR,
                 vM03_012_PERC_ARRED_EMB_PROD         
          FROM I_MDLOG.M03_012_SUG_COMPRA_DIA
          WHERE M03_012_REPOSICAO_UND > 0
            AND M03_012_FORNECEDOR_IE = PI_FORNECEDOR
            AND M03_012_PRODUTO_IE    = vRegItens.M03_012_PRODUTO_IE
            AND M03_012_DATA_SUG_IE = vDataAtual
            and rownum <= 1;
        end;
      end;
                
      vNovaQtde := F_CALC_QTDE_EMB(vRegItens.QTDE_REPOSICAO,vM03_012_EMBALAGEM_COMPRA,vM03_012_PERC_ARRED_EMB_PROD);      
      
      tT191_SUGESTAO_COMPRA_ITEM.T191_UNIDADE_SUGESTAO_IE := PI_UNIDADE_CENTRALIZADORA;     
      tT191_SUGESTAO_COMPRA_ITEM.T191_SUGESTAO_COMPRA_IE  := nSUGESTAO;
      tT191_SUGESTAO_COMPRA_ITEM.T191_UNIDADE_FATURA_IE   := PI_UNIDADE_CENTRALIZADORA;
      tT191_SUGESTAO_COMPRA_ITEM.T191_PRODUTO_IE          := vRegItens.M03_012_PRODUTO_IE;
      tT191_SUGESTAO_COMPRA_ITEM.T191_SUGESTAO_ORIGINAL   := vNovaQtde;
      tT191_SUGESTAO_COMPRA_ITEM.T191_SUGESTAO_UNITARIA   := vNovaQtde;
      tT191_SUGESTAO_COMPRA_ITEM.T191_FORNECEDOR_E        := PI_FORNECEDOR;
      tT191_SUGESTAO_COMPRA_ITEM.T191_PRECO_UNITARIO      := vM03_012_PRECO_REPOSICAO;
      tT191_SUGESTAO_COMPRA_ITEM.T191_DESCONTO_ITEM       := vM03_012_DESCONTO_ITEM;
      tT191_SUGESTAO_COMPRA_ITEM.T191_PERC_DESPACESS      := vM03_012_TAXA_VENDOR;        
      tT191_SUGESTAO_COMPRA_ITEM.T191_EMBALAGEM_PADRAO    := vM03_012_EMBALAGEM_COMPRA;
      tT191_SUGESTAO_COMPRA_ITEM.T191_PRODUTO_NOVO        := vPRODUTO_NOVO;
      tT191_SUGESTAO_COMPRA_ITEM.T191_PERC_EMBALAGEM      := 0;
      tT191_SUGESTAO_COMPRA_ITEM.T191_OBSERVACAO          := PI_OBSERVACAO;
      
      P_GERA_ITENS(tT191_SUGESTAO_COMPRA_ITEM); 

    END LOOP;
    
    I_MDLOG.P_GRAVA_PEDIDO_RA_ERP(PI_UNIDADE_CENTRALIZADORA,
                                  nSUGESTAO,
                                  poPedidosGerados);
                                  
    P_ATUAL_TIPO_FRETE_TRANS_OBSER(poPedidosGerados);         
    
  END IF;
  
  
  COMMIT;
  
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('C�digo Oracle: ' || SQLCODE);
    DBMS_OUTPUT.PUT_LINE('Mensagem Oracle: ' || SQLERRM);
    RAISE_APPLICATION_ERROR(-20000, TO_CHAR(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE),TRUE);   
END;