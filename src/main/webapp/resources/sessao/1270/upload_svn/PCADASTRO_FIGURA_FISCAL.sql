/*******************************************************************************
SCHEMA    : DBAMDATA
TABELA    : T425_FIGURA_FISCAL
PROCEDURE : PCADASTRO_FIGURA_FISCAL
AUTOR     : TASSIANO ALENCAR
DATA      : 27/04/2017
CRC       : 70542
ALTERACAO : NOME - 00/00/0000 - CRC99999
DESCRICAO DA ALTERACAO.
*******************************************************************************/
CREATE OR REPLACE PROCEDURE DBAMDATA.PCADASTRO_FIGURA_FISCAL (
  PI_OPERACAO                   VARCHAR2,
  PI_CODIGO	                    NUMBER,
  PI_DESCRICAO	                VARCHAR2,
  PI_OBSERVACAO	                VARCHAR2,
  PI_DATA_EFETIVACAO	          DATE,
  PI_DATA_EXPIRACAO	            DATE,
  PO_STATUS                     OUT VARCHAR2,
  PO_MSG                        OUT VARCHAR2
)
IS
  ecode NUMBER(10);
  emesg VARCHAR2(2000);

BEGIN
  
  PO_STATUS := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT INTO DBAMDATA.T425_FIGURA_FISCAL
        (T425_FIGURA_FISCAL_IU, T425_DESCRICAO, T425_OBSERVACAO, T425_DATA_EFETIVACAO, T425_DATA_EXPIRACAO)
      VALUES
        (PI_CODIGO, PI_DESCRICAO, PI_OBSERVACAO, PI_DATA_EFETIVACAO, PI_DATA_EXPIRACAO);
      EXCEPTION
      WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      PO_STATUS := 'ER';
      PO_MSG := ecode ||'-'|| emesg;
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/

  /******************************BEGIN - EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
      UPDATE DBAMDATA.T425_FIGURA_FISCAL
      SET
        T425_DESCRICAO              = PI_DESCRICAO,
        T425_OBSERVACAO             = PI_OBSERVACAO,
        T425_DATA_EXPIRACAO         = PI_DATA_EXPIRACAO
      WHERE
        T425_FIGURA_FISCAL_IU       = PI_CODIGO;
      EXCEPTION
      WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      PO_STATUS := 'ER';
      PO_MSG := ecode ||'-'|| emesg;
    END;
  END IF;
  /********************************END - EDITAR********************************/
  
  /******************************BEGIN - EXCLUIR*******************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE FROM DBAMDATA.T425_FIGURA_FISCAL
      WHERE
         T425_FIGURA_FISCAL_IU       = PI_CODIGO;
      EXCEPTION
      WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      PO_STATUS := 'ER';
      PO_MSG := ecode ||'-'|| emesg;
    END;
  END IF;
  /********************************END - EXCLUIR*******************************/

END;