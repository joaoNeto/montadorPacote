CREATE OR REPLACE PROCEDURE I_MDLOG.P_GRAVA_PEDIDO_RA_ERP(PI_NUNIDADE            IN NUMBER,
                                                          PI_NSUGESTAO           IN NUMBER,
                                                          PO_NUMEROPEDIDOSGERADO OUT VARCHAR2) IS

/*******************************************************************************
  NOME      : P_GRAVA_PEDIDO_RA_ERP
  DESCRICAO : PORCEDURE PARA GERAR OS PEDIDOS DO RA DO I_MDLOG NO ERP
  ANALISTA  : H�lio Sales                                       
*******************************************************************************/
  /* criar registro do pedido */
  TYPE TPEDIDO IS RECORD (
    UNIDADE           T108_PEDIDOS_PEND.T108_UNIDADE_IE%TYPE,
    PEDIDO            T108_PEDIDOS_PEND.T108_NUMERO_PEDIDO_IU%TYPE,
    FORNECEDOR        T108_PEDIDOS_PEND.T108_FORNECEDOR_E%TYPE,
    COMPRADOR         T108_PEDIDOS_PEND.T108_COMPRADOR%TYPE,
    DATA_FATURA       T108_PEDIDOS_PEND.T108_DATAPREVISTA_FATURA%TYPE,
    TIPO_PEDIDO       T108_PEDIDOS_PEND.T108_TIPO_PEDIDO%TYPE,
    UNIDADE_CD        T108_PEDIDOS_PEND.T108_UNIDADE_IE%TYPE );

  TYPE TNCM IS RECORD (vv_NCM T076_PRODUTO.T076_NCM_E%TYPE);
  
  TYPE TLST_NCM IS TABLE OF TNCM INDEX BY BINARY_INTEGER;  
  
  vt_ListaNCM TLST_NCM; 

  /* Criar lista de Registro de �didos*/
  TYPE TLST_PEDIDO IS TABLE OF TPEDIDO INDEX BY BINARY_INTEGER;
  /* iNSTANCIAR LISTA DE PEDIDOS */
  lst_Pedidos                    TLST_PEDIDO;
  vInd                           INTEGER;
  vResp                          INTEGER;
  vvTipoResp                     VARCHAR2(500);
  vvMsgResp                      VARCHAR2(500);

  vvPedidoTransferencia          CHAR; 
  vv_UfForn                      T002_MUNICIPIO.T002_UF_E%TYPE;
  vUfDestino                     T002_MUNICIPIO.T002_UF_E%TYPE;                
  vn_NaturezaOperacaoEntrada     NUMBER;
  vn_Classificacao_Fiscal        T278_PRODUTO_CLASSFISC_NATUR.T278_CLASSIFIC_FISCAL_E%TYPE;
  vn_T108_TIPO_PEDIDO            T108_PEDIDOS_PEND.T108_TIPO_PEDIDO%TYPE;
  vnT054_PRAZO_ENTREGA           T054_FORNECEDOR_UNIDADE.T054_PRAZO_ENTREGA%TYPE;
  vvT054_REPASSE_ICM             T054_FORNECEDOR_UNIDADE.T054_REPASSE_ICM%TYPE;
  vvT054_DESCONTO_NOTA           T054_FORNECEDOR_UNIDADE.T054_DESCONTO_NOTA%TYPE;
  vnT054_COMPRADOR_E             T054_FORNECEDOR_UNIDADE.T054_COMPRADOR_E%TYPE;
  VNT054_QTDE_DIAS_FAT           T054_FORNECEDOR_UNIDADE.T054_QTDE_DIAS_FATURAMENTO%TYPE;
  vnT003_Unidade                 T003_UNIDADE.T003_UNIDADE_IU%TYPE;
  vcT925_ITEM_NOVO_ZERADO        T925_PARAM_GERAIS_UNIDADES.T925_ITEM_NOVO_ZERADO_REPOSIC%TYPE;
  vc_produto_estocado            T077_PRODUTO_UNIDADE.T077_PRODUTO_ESTOCADO%TYPE;
  vc_BloqueiaProdEstocado        T925_PARAM_GERAIS_UNIDADES.T925_BLOQUEIA_COMPRA_PROD_EST%TYPE; 
  CabecalhoIncluido              BOOLEAN;
  vb_IncluirProduto              BOOLEAN;
  vb_ExecutaCommit               BOOLEAN;
  vn_TipoErro                    NUMBER;

  n_t900_ult_pedido_fornecedor   T900_PARAM_NUMFISC.T900_ULT_PEDIDO_FORNECEDOR%TYPE;
  vd_data_faturamento            T908_PARAM_EMPRESA.T908_DATA_FATURAMENTO%TYPE;
  vn_t076_aliquota_ipi           T076_PRODUTO.T076_ALIQUOTA_IPI%TYPE;
  vn_TipoCalcIPI                 T076_PRODUTO.T076_TIPO_CALC_IPI%TYPE;
  NumeroPedidoInicial            T900_PARAM_NUMFISC.T900_ULT_PEDIDO_FORNECEDOR%TYPE;
  vc_Limita_QTD_Itens            T903_PARAM_COMPRAS.T903_LIMITA_QTDE_ITENS_PEDIDO%TYPE;
  vn_QTD_Prod_Pedido             T903_PARAM_COMPRAS.T903_QTDE_ITENS_PEDIDO%TYPE;
  vb_ObedeceQtdItens             BOOLEAN;
  nQuebraItens                   NUMBER(3);
  vvPrenotaGerada                T119_PRENOTA.T119_NUMERO_PRENOTA_IU%TYPE;
  po_HaOcorrencias               VARCHAR2(1);

  vd_DataPedido                  DATE;

  SubTotalLiq                    T110_PEDIDOS_PEND_ITENS.T110_PRECO_ITEM%TYPE;
  CalcIPI                        T110_PEDIDOS_PEND_ITENS.T110_PRECO_ITEM%TYPE;
  TotaisItens                    T110_PEDIDOS_PEND_ITENS.T110_PRECO_ITEM%TYPE;
  vn_PrecoItem                   T110_PEDIDOS_PEND_ITENS.T110_PRECO_ITEM%TYPE;
  vn_ValorDescItem               T110_PEDIDOS_PEND_ITENS.T110_PRECO_ITEM%TYPE;
  vn_ValorDescRepasse            T110_PEDIDOS_PEND_ITENS.T110_PRECO_ITEM%TYPE;
  vn_ValorDescComercial          T110_PEDIDOS_PEND_ITENS.T110_PRECO_ITEM%TYPE;
  vn_ErroCalc                    T110_PEDIDOS_PEND_ITENS.T110_PRECO_ITEM%TYPE;
  vnT108_FORNECEDOR_E            T108_PEDIDOS_PEND.T108_FORNECEDOR_E%TYPE;
  vn_UndAtual                    T108_PEDIDOS_PEND.T108_UNIDADE_IE%TYPE;
  vdT108_DATA_ENTREGA            T108_PEDIDOS_PEND.T108_DATA_ENTREGA%TYPE;
  vn_ValorST                     T110_PEDIDOS_PEND_ITENS.T110_VALOR_ST%TYPE;
  vn_TotalST                     T110_PEDIDOS_PEND_ITENS.T110_VALOR_ST%TYPE; 
  
  vn_CapContM3                   T925_PARAM_GERAIS_UNIDADES.T925_CAPACIDADE_CONTAINER_M3%TYPE;
  vn_CapContPeso                 T925_PARAM_GERAIS_UNIDADES.T925_CAPACIDADE_CONTAINER_PESO%TYPE;
  vn_MoedaDespAcessImp           T925_PARAM_GERAIS_UNIDADES.T925_PREV_DESP_ACESS_IMP_MOEDA%TYPE;
  vn_MoedaFreteImp               T925_PARAM_GERAIS_UNIDADES.T925_PREVISAO_FRETE_IMP_MOEDA%TYPE;
  vn_MoedaTHCImp                 T925_PARAM_GERAIS_UNIDADES.T925_PREVISAO_THC_IMP_MOEDA%TYPE;
  vn_MoedaDespLogImp             T925_PARAM_GERAIS_UNIDADES.T925_PREV_DESP_LOG_IMP_MOEDA%TYPE;
  vn_PrevDespAcessImp            T925_PARAM_GERAIS_UNIDADES.T925_PREV_DESP_ACESS_IMP%TYPE;
  vn_PrevFreteImp                T925_PARAM_GERAIS_UNIDADES.T925_PREVISAO_FRETE_IMP%TYPE;
  vn_PrevTHCImp                  T925_PARAM_GERAIS_UNIDADES.T925_PREVISAO_THC_IMP%TYPE;
  vn_PrevDespLogImp              T925_PARAM_GERAIS_UNIDADES.T925_PREV_DESP_LOG_IMP%TYPE;
  vn_AdcSegImp                   T925_PARAM_GERAIS_UNIDADES.T925_ADICIONAL_SEGURO_IMP%TYPE;
  vn_PrevAFRMMImp                T925_PARAM_GERAIS_UNIDADES.T925_PREV_AFRMM_IMP%TYPE;
  vn_PrevSegImp                  T925_PARAM_GERAIS_UNIDADES.T925_PREV_SEGURO_IMP%TYPE;

  vn_despesas_acessorias_imp     T108_PEDIDOS_PEND.T108_DESPESAS_ACESSORIAS_IMP%TYPE; 
  vn_despesas_frete_imp          T108_PEDIDOS_PEND.T108_DESPESAS_FRETE_IMP%TYPE;
  vn_despesas_thc_imp            T108_PEDIDOS_PEND.T108_DESPESAS_THC_IMP%TYPE; 
  vn_despesas_log_imp            T108_PEDIDOS_PEND.T108_DESPESAS_LOG_IMP%TYPE; 
  vn_despesas_afrmm_imp          T108_PEDIDOS_PEND.T108_DESPESAS_AFRMM_IMP%TYPE;
  vn_despesas_siscomex_imp       T108_PEDIDOS_PEND.T108_DESPESAS_SISCOMEX_IMP%TYPE;
  vn_despesas_seguro_imp         T108_PEDIDOS_PEND.T108_DESPESAS_SEGURO_IMP%TYPE;      
  
  --// Variaveis utilizadas no Calculo do IPI/ ICMS ST (PMov_Calc_Impostos_Entrada)
  po_nAliquotaIcmsNormal              NUMBER;
  po_nAliquotaIpi                     NUMBER;
  po_nAgregadoCompra                  NUMBER;
  po_Classificacao_Fiscal             NUMBER;
  po_nBaseIcmsNormal                  NUMBER;
  po_nValorIcmsNormal                 NUMBER;
  po_nBaseIcmsSubstituto              NUMBER;
  po_nValorIcmsSubstituto             NUMBER;
  po_nBaseIpi                         NUMBER;
  po_nValorIpi                        NUMBER;
  po_nOutrasIcmsNormal                NUMBER;
  po_nIsentasIcmsNormal               NUMBER;
  po_nOutrasReducao                   NUMBER;
  po_nIsentasReducao                  NUMBER;
  po_nOutrasIpi                       NUMBER;
  po_nIsentasIpi                      NUMBER;
  po_nValorDifa                       NUMBER;
  po_nDescontoItem                    NUMBER;
  po_nDescontoRepasse                 NUMBER;
  po_nDescontoComercial               NUMBER;
  po_ntotaldescontos                  NUMBER;
  po_cTribPis                         VARCHAR2(100);
  po_nBasePiscofinsIsento             NUMBER;
  po_nBasePiscofinsRetido             NUMBER;
  po_nAliquotaPisRetido               NUMBER;
  po_nAliquotaCofinsRetido            NUMBER;
  po_nValorPisRetido                  NUMBER;
  po_nValorCofinsRetido               NUMBER;
  po_nBasePiscofinsNormal             NUMBER;
  po_nAliquotaPisNormal               NUMBER;
  po_nAliquotaCofinsNormal            NUMBER;
  po_nValorPisNormal                  NUMBER;
  po_nValorCofinsNormal               NUMBER;
  po_cmensagemerro                    VARCHAR2(100);
  po_PercAcrescFECEP                  NUMBER;
  po_ValorAcrescFECEP                 NUMBER;
  po_Base_Diferimento_Icms            NUMBER;
  po_Valor_Diferimento_Icms           NUMBER;
  po_Perc_Diferimento_Icms            NUMBER;
  Vn_VlrDescontoItem                  NUMBER(15,2);
  Vn_VlrDescontoRepasse               NUMBER(15,2);
  Vn_VlrDescontoComercial             NUMBER(15,2);
  Vn_ErroCalculo                      NUMBER(1,0);
  pio_ValorMedioPonderado             NUMBER;
  vnAliquotaInterna                   T211_LOTESTOQUE_ITENS.T211_ALIQUOTA_INTERNA%TYPE;        
  vnPercRedBaseIcms                   T211_LOTESTOQUE_ITENS.T211_PERC_REDBASE_ICMS%TYPE;       
  vnPercBaseRedIcmSubst               T211_LOTESTOQUE_ITENS.T211_PERC_REDBASE_ICMSUBST%TYPE;   
  vnValorRedIcmSubst                  T211_LOTESTOQUE_ITENS.T211_VALOR_REDBASE_ICMSUBST%TYPE;
  vc_emite_cobr_separ_imposto         T019_FORNECEDOR.T019_EMITE_COBR_SEPAR_IMPOSTO%TYPE;      
  
  -- cursor de pedidos de importa��o
  CURSOR c_PedImp IS
  SELECT TCOM_UNIDADE,
         TCOM_NUMERO_PEDIDO
    FROM TCOM_INSPECAO_PEDIDO_COMPRA,
         T019_FORNECEDOR 
   WHERE TCOM_FORNECEDOR = T019_FORNECEDOR_IU
     AND NVL(T019_ESTRANGEIRO,'N') = 'S';

  FUNCTION ConverteMoeda(pi_Moeda IN INTEGER) RETURN NUMBER IS

    vn_ValorCotacao T645_MOEDA_COTACAO.T645_VALOR_COTACAO%TYPE;
       
    CURSOR c_Moeda IS
    SELECT T645_VALOR_COTACAO 
      FROM T645_MOEDA_COTACAO   
     WHERE T645_MOEDA_IE = pi_Moeda 
     ORDER BY T645_DATA_IU DESC;   
     
  BEGIN
  
    vn_ValorCotacao := 1;
    
    FOR vc_Moeda IN c_Moeda LOOP
    
      vn_ValorCotacao := vc_Moeda.T645_VALOR_COTACAO; 
      
      EXIT;
            
    END LOOP;
    
    RETURN vn_ValorCotacao;
      
  END ConverteMoeda;   
  
  
  FUNCTION CalculaDespesa(pi_DespBase IN NUMBER, pi_MoedaImp IN INTEGER, pi_NumContainer IN INTEGER) RETURN NUMBER IS
    vn_TotDespesa T108_PEDIDOS_PEND.T108_DESPESAS_ACESSORIAS_IMP%TYPE;
  BEGIN
    -- real
    IF pi_MoedaImp <= 1 THEN    
      vn_TotDespesa := pi_DespBase * pi_NumContainer;
    ELSE
      vn_TotDespesa := pi_DespBase * pi_NumContainer * ConverteMoeda(pi_MoedaImp);
    END IF;
      
    RETURN vn_TotDespesa;
  
  END CalculaDespesa;   
     
  PROCEDURE Verifica_Despesas_Importacao(pi_TotalPedido IN NUMBER) IS
  
    vb_Achou                   BOOLEAN;
    vn_QtdeNCM                 INTEGER;
    vn_PesoTotal               T076_PRODUTO.T076_PESO_BRUTO%TYPE;
    vn_CubagemTotal            T077_PRODUTO_UNIDADE.T077_CUBAGEM_VENDA%TYPE;
    vn_NumContainer            T108_PEDIDOS_PEND.T108_NUM_CONTAINER%TYPE;    
    vn_CubCalcM3               T108_PEDIDOS_PEND.T108_NUM_CONTAINER%TYPE;
    vn_PesoCalc                T108_PEDIDOS_PEND.T108_NUM_CONTAINER%TYPE;
    vn_valor_adicao            T696_SISCOMEX.T696_VALOR_ADICAO%TYPE;   
    vn_TotSegImp               T108_PEDIDOS_PEND.T108_DESPESAS_SEGURO_IMP%TYPE;
        
    CURSOR c_ItemsImp(pi_UnidadeImp IN NUMBER, pi_PedidoImp IN NUMBER) IS
    SELECT T110_PRODUTO_IE,
           T110_QTDE_PEDIDO,
           NVL(T076_PESO_BRUTO,0) T076_PESO_BRUTO,
           T076_NCM_E,
           NVL(T076_CARACTERISTICA_COMPOSTO,'N') T076_CARACTERISTICA_COMPOSTO,
           NVL(T077_CUBAGEM_VENDA,0) T077_CUBAGEM_VENDA 
      FROM T110_PEDIDOS_PEND_ITENS,
           T076_PRODUTO,
           T077_PRODUTO_UNIDADE 
     WHERE T110_UNIDADE_IE = T077_UNIDADE_IE
       AND T110_PRODUTO_IE = T077_PRODUTO_IE
       AND T077_PRODUTO_IE = T076_PRODUTO_IU
       AND T110_UNIDADE_IE = pi_UnidadeImp
       AND T110_NUMERO_PEDIDO_IE = pi_PedidoImp;

  CURSOR c_ProdMat(pi_UnidadeProd IN NUMBER, pi_ProdAcabado IN NUMBER) IS       
  SELECT T546_PRODUTO_MATERIA_PRIMA_IE,    
         NVL(T546_QTDE_MATERIA_PRIMA,1) T546_QTDE_MATERIA_PRIMA,
         NVL(T077_CUBAGEM_VENDA,0) T077_CUBAGEM_VENDA,
         NVL(T076_PESO_BRUTO,0) T076_PESO_BRUTO,                                          
         T076_NCM_E                                                                                        
    FROM T546_MATERIA_PRIMA_PROD_COMP,  
         T077_PRODUTO_UNIDADE,   
         T076_PRODUTO   
   WHERE T077_PRODUTO_IE = T076_PRODUTO_IU  
     AND T077_PRODUTO_IE = T546_PRODUTO_MATERIA_PRIMA_IE  
     AND T077_UNIDADE_IE = pi_UnidadeProd   
     AND T546_PRODUTO_IE = pi_ProdAcabado;              
     
  BEGIN
  
    -- obtem as previs�es de despesas de importa��o
    SELECT NVL(T925_CAPACIDADE_CONTAINER_M3,0),
         NVL(T925_CAPACIDADE_CONTAINER_PESO,0),
         NVL(T925_PREV_DESP_ACESS_IMP_MOEDA,0),
         NVL(T925_PREVISAO_FRETE_IMP_MOEDA,0),
         NVL(T925_PREVISAO_THC_IMP_MOEDA,0),
         NVL(T925_PREV_DESP_LOG_IMP_MOEDA,0),
         NVL(T925_PREV_DESP_ACESS_IMP,0),
         NVL(T925_PREVISAO_FRETE_IMP,0),
         NVL(T925_PREVISAO_THC_IMP,0),
         NVL(T925_PREV_DESP_LOG_IMP,0),
         NVL(T925_ADICIONAL_SEGURO_IMP,0),
         NVL(T925_PREV_AFRMM_IMP,0),
         NVL(T925_PREV_SEGURO_IMP,0)
    INTO vn_CapContM3,                       
         vn_CapContPeso,        
         vn_MoedaDespAcessImp,  
         vn_MoedaFreteImp,      
         vn_MoedaTHCImp,        
         vn_MoedaDespLogImp,    
         vn_PrevDespAcessImp,   
         vn_PrevFreteImp,       
         vn_PrevTHCImp,         
         vn_PrevDespLogImp,     
         vn_AdcSegImp,          
         vn_PrevAFRMMImp,       
         vn_PrevSegImp   
    FROM T925_PARAM_GERAIS_UNIDADES;            
    
    FOR vc_PedImp IN c_PedImp LOOP
        
      vn_PesoTotal := 0;
      vn_CubagemTotal := 0;
      vt_ListaNCM.DELETE;
              
      -- calcula o numero de container        
      IF (vn_CapContM3 > 0) OR (vn_CapContPeso > 0) THEN
      
        FOR vc_ItemsImp IN c_ItemsImp(vc_PedImp.TCOM_UNIDADE, vc_PedImp.TCOM_NUMERO_PEDIDO) LOOP
                                              
          -- Se for produto composto calcula o peso/cubagem total das mat�rias primas
          IF vc_ItemsImp.T076_CARACTERISTICA_COMPOSTO = 'S' THEN
                                        
            FOR vc_ProdMat IN c_ProdMat(vc_PedImp.TCOM_UNIDADE, vc_ItemsImp.T110_PRODUTO_IE) LOOP     
              
              vb_Achou := FALSE;
              
              -- guarda lista de NCM do pedido
              IF vt_ListaNCM.COUNT > 0 THEN
                 
                 FOR i IN vt_ListaNCM.FIRST..vt_ListaNCM.LAST LOOP
                    
                   IF vt_ListaNCM(i).vv_NCM = vc_ProdMat.T076_NCM_E THEN
                     
                     vb_Achou := TRUE;
                     EXIT;
                       
                   END IF;
                       
                 END LOOP;
                         
                -- guarda lista de NCM do pedido
                IF NOT vb_Achou THEN
                  vt_ListaNCM(vt_ListaNCM.COUNT+1).vv_NCM := vc_ProdMat.T076_NCM_E;
                END IF;
                  
              ELSE
                vt_ListaNCM(1).vv_NCM := vc_ProdMat.T076_NCM_E;                
              END IF;    
                
              vn_PesoTotal := vn_PesoTotal + (vc_ProdMat.T076_PESO_BRUTO*vc_ItemsImp.T110_QTDE_PEDIDO*vc_ProdMat.T546_QTDE_MATERIA_PRIMA);
              vn_CubagemTotal := vn_CubagemTotal + ((vc_ProdMat.T077_CUBAGEM_VENDA*vc_ItemsImp.T110_QTDE_PEDIDO*vc_ProdMat.T546_QTDE_MATERIA_PRIMA)/1000000);
                  
            END LOOP;
              
          -- Poduto normal  
          ELSE
            
            vb_Achou := FALSE;
            
            IF vt_ListaNCM.COUNT > 0 THEN
               
               FOR i IN vt_ListaNCM.FIRST..vt_ListaNCM.LAST LOOP
                    
                 IF vt_ListaNCM(i).vv_NCM = vc_ItemsImp.T076_NCM_E THEN
                     
                   vb_Achou := TRUE;
                   EXIT;
                       
                 END IF;
                       
               END LOOP;
                        
              -- guarda lista de NCM do pedido
              IF NOT vb_Achou THEN
                vt_ListaNCM(vt_ListaNCM.COUNT+1).vv_NCM := vc_ItemsImp.T076_NCM_E;
              END IF;
            ELSE
              vt_ListaNCM(1).vv_NCM := vc_ItemsImp.T076_NCM_E;                
            END IF;               
                
            vn_PesoTotal := vn_PesoTotal + (vc_ItemsImp.T076_PESO_BRUTO*vc_ItemsImp.T110_QTDE_PEDIDO);
            vn_CubagemTotal := vn_CubagemTotal + ((vc_ItemsImp.T077_CUBAGEM_VENDA*vc_ItemsImp.T110_QTDE_PEDIDO)/1000000);
              
          END IF;
            
        END LOOP;
                
        IF vn_CapContM3 > 0 THEN
          vn_CubCalcM3 := ROUND(vn_CubagemTotal/vn_CapContM3);
        ELSE
          vn_CubCalcM3 := 0;
        END IF;
          
        IF vn_CapContPeso > 0 THEN
          vn_PesoCalc := ROUND(vn_PesoTotal/vn_CapContPeso);
        ELSE
          vn_PesoCalc := 0;
        END IF;
          
        IF vn_CubCalcM3 > vn_PesoCalc THEN
          vn_NumContainer := vn_CubCalcM3;
        ELSE
          vn_NumContainer := vn_PesoCalc;
        END IF;
          
        IF vn_NumContainer > 999 THEN
          vn_NumContainer := 999;
        END IF;
          
        IF vn_NumContainer <= 0 THEN
          vn_NumContainer := 1;
        END IF;
                
      END IF;
                                
      -- calcula as despesas acess�rias de importa��o
      vn_despesas_acessorias_imp := CalculaDespesa(vn_PrevDespAcessImp, vn_MoedaDespAcessImp, vn_NumContainer);
      -- calcula a despesa de frete de importa��o
      vn_despesas_frete_imp := CalculaDespesa(vn_PrevFreteImp, vn_MoedaFreteImp, vn_NumContainer);
      -- calcula a despesa de THC de importa��o
      vn_despesas_thc_imp := CalculaDespesa(vn_PrevTHCImp, vn_MoedaTHCImp, vn_NumContainer);
      -- calcula a despesa de log�stica de importa��o
      vn_despesas_log_imp := CalculaDespesa(vn_PrevDespLogImp, vn_MoedaDespLogImp, vn_NumContainer);
      -- calcula a despesa de AFRMM de importa��o
      vn_despesas_afrmm_imp := ((vn_despesas_frete_imp * vn_PrevAFRMMImp)/100) * vn_NumContainer;
   
      vn_QtdeNCM := vt_ListaNCM.COUNT;
            
      -- calcula a despesa de SISCOMEX de importa��o    
      BEGIN
        SELECT T696_VALOR_ADICAO
          INTO vn_valor_adicao
          FROM T696_SISCOMEX
         WHERE T696_ADICAO_IU = vn_QtdeNCM;
      EXCEPTION
        WHEN OTHERS THEN
          vn_valor_adicao := 0;
      END;
    
      vn_despesas_siscomex_imp := vn_valor_adicao * vn_NumContainer;
      -- calcula a despesa de Seguro de importa��o
      vn_TotSegImp := (((pi_TotalPedido + vn_despesas_frete_imp + vn_despesas_thc_imp)*vn_AdcSegImp)/100);
      vn_despesas_seguro_imp := ((vn_TotSegImp * vn_PrevSegImp)/100) * vn_NumContainer;
            
      UPDATE T108_PEDIDOS_PEND
         SET T108_NUM_CONTAINER = vn_NumContainer,
             T108_DESPESAS_ACESSORIAS_IMP = vn_despesas_acessorias_imp,
             T108_DESPESAS_FRETE_IMP = vn_despesas_frete_imp, 
             T108_DESPESAS_THC_IMP = vn_despesas_thc_imp,
             T108_DESPESAS_LOG_IMP = vn_despesas_log_imp,
             T108_DESPESAS_AFRMM_IMP = vn_despesas_afrmm_imp,
             T108_DESPESAS_SISCOMEX_IMP = vn_despesas_siscomex_imp,
             T108_DESPESAS_SEGURO_IMP = vn_despesas_seguro_imp
       WHERE T108_UNIDADE_IE = vc_PedImp.TCOM_UNIDADE
         AND T108_NUMERO_PEDIDO_IU = vc_PedImp.TCOM_NUMERO_PEDIDO;               
      
    END LOOP;  
          
  END Verifica_Despesas_Importacao;               
           
  
  FUNCTION OBTEM_NATUREZA_COMPRA(pil_Unidade IN NUMBER,
                                 pil_FORNECEDOR IN NUMBER,
                                 pil_PRODUTO IN NUMBER)
  RETURN NUMBER IS
    vnNatureza NUMBER;
  BEGIN                                       
    vnNatureza := NULL;
    
    --// Primeiro tenta Obter a natureza de opera��o com base em outros lotes deste mesmo fornecedor para este mesmo produto nos ultimos 360 dias
    BEGIN 
      SELECT T209_NATUREZA_OPERACAO_E
        INTO vnNatureza
        FROM T209_LOTESTOQUE
       WHERE T209_UNIDADE_IE = pil_UNIDADE
         AND T209_TIPO_EMITENTE = 'F'
         AND T209_CODIGO_EMITENTE = pil_FORNECEDOR
         AND T209_DATA_MOVIMENTO >= (SYSDATE - 360)
         AND EXISTS(SELECT '1' 
                      FROM T211_LOTESTOQUE_ITENS 
                     WHERE T211_UNIDADE_IE = T209_UNIDADE_IE
                       AND T211_NUMERO_LOTE_IE = T209_NUMERO_LOTE_IU
                       AND T211_PRODUTO_IE = pil_PRODUTO);
    EXCEPTION
      WHEN OTHERS THEN 
        vnNatureza := NULL;
    END;                         

    --// Caso n�o encontre atribui a natureza padr�o de compra. 
    IF vnNatureza IS NULL THEN 
      IF vv_UfForn = vUfDestino THEN  
        vnNatureza := 110201; 
      ELSE
        vnNatureza := 210201;
      END IF;
    END IF;
    
    RETURN vnNatureza;
  END;          
  
  --//Procedimento para garanti que s� ser�o gerados pedidos que atinjam a fatura m�nima estabelecida pelo Fornecedor.
  FUNCTION FCAIU_EM_FATURA_MINIMA(FICF_UNIDADEPEDIDO IN NUMBER,
                                  FICF_NPEDIDO       IN NUMBER,
                                  FICF_NFORNECEDOR   IN NUMBER,
                                  FICF_TOTAL_PEDIDO  IN NUMBER) RETURN BOOLEAN IS
    vnT019_FATURA_MINIMA  T019_FORNECEDOR.T019_FATURA_MINIMA%TYPE;
    RESP                  BOOLEAN;
    vIndex                INTEGER;
  BEGIN
    --//Inicializar como n�o criticando Fatura M�nima
    RESP := FALSE;
    --//verificar a Fatura m�nima do Fornecedor
    SELECT NVL(T019_FATURA_MINIMA,0)
      INTO vnT019_FATURA_MINIMA
      FROM T019_FORNECEDOR
     WHERE T019_FORNECEDOR_IU = FICF_NFORNECEDOR;
    --//configurar resposta em fun��o do Limite M�nimo e do Valor total do Pedido
    RESP := FICF_TOTAL_PEDIDO < vnT019_FATURA_MINIMA;
    --se caiu por fatura m�nima deverei eliminar o pedido
    IF RESP THEN
      --Excluir Itens
      DELETE T110_PEDIDOS_PEND_ITENS
       WHERE T110_UNIDADE_IE = FICF_UNIDADEPEDIDO
         AND T110_NUMERO_PEDIDO_IE = FICF_NPEDIDO;

      DELETE TCOM_INSPECAO_PED_COMPRA_ITENS
       WHERE TCOM_UNIDADE = FICF_UNIDADEPEDIDO
         AND TCOM_NUMERO_PEDIDO = FICF_NPEDIDO;
      --excluir registro de cabe�alho
      DELETE T108_PEDIDOS_PEND
       WHERE T108_UNIDADE_IE = FICF_UNIDADEPEDIDO
         AND T108_NUMERO_PEDIDO_IU = FICF_NPEDIDO;

      DELETE TCOM_INSPECAO_PEDIDO_COMPRA
       WHERE TCOM_UNIDADE = FICF_UNIDADEPEDIDO
         AND TCOM_NUMERO_PEDIDO = FICF_NPEDIDO;
      --//Percorrer a lista com os pedidos para excluir o pedido indicado
      vIndex := -1;
      LOOP
        vIndex := lst_PEDIDOS.NEXT(vIndex);
        EXIT WHEN vIndex IS NULL;
        --ao encontrar o pedido, excluir o mesmo da lista      --Percorrer Lista para eliminar o pedido da lista
        IF ( lst_PEDIDOS(vIndex).UNIDADE = FICF_UNIDADEPEDIDO ) AND
           ( lst_PEDIDOS(vIndex).PEDIDO  = FICF_NPEDIDO ) THEN
          lst_PEDIDOS.DELETE(vIndex);
        END IF;
      END LOOP;
      --//Ocorrencia de Erro 15 Fatura m�nima
      PREP_GRAVA_OCORRENCIA_PRODUTO(PI_NUNIDADE,PI_NSUGESTAO,0,15,vd_DataPedido,FICF_NFORNECEDOR,FICF_UNIDADEPEDIDO,'',NULL,NULL);
    END IF;
    --retornar resultado
    RETURN RESP;
  END;
  --PROCEDURE RESPONSAVEL POR GRAVAR AS PREVIS�ES
  PROCEDURE GRAVA_PREVISOES(pigp_nunidade   IN NUMBER,
                            pigp_nnumeroped IN NUMBER,
                            pigp_ValorTotal IN NUMBER,
                            pigp_TipoPedido IN CHAR,
                            pigp_ValorST    IN NUMBER) IS
      n_t900_ult_titulo_pagar        T900_PARAM_NUMFISC.T900_ULT_TITULO_PAGAR%TYPE;
      n_CodFornecedor                T108_PEDIDOS_PEND.T108_FORNECEDOR_E%TYPE;
      d_DataPrevistaFat              T108_PEDIDOS_PEND.T108_DATAPREVISTA_FATURA%TYPE;
      d_DataEntrega                  T108_PEDIDOS_PEND.T108_DATA_ENTREGA%TYPE;
      d_t057_data_limite             T057_TITULO_PAGAR.T057_DATA_LIMITE%TYPE;
      nResto                         NUMBER;
      N_PrazoPagto                   T132_FORNEC_UNIDADE_PRAZO.T132_PRAZO_PAG_IU%TYPE;
      vn_PrazoImp                    T019_FORNECEDOR.T019_PRAZO_PGMTO_IMPOSTO%TYPE; 
      vn_ValorTotal                  T057_TITULO_PAGAR.T057_VALOR_BRUTO%TYPE;  

  BEGIN
    --//verificar informa��es do pedido pendente
    SELECT T108_FORNECEDOR_E, T108_DATAPREVISTA_FATURA, T108_DATA_ENTREGA,
           NVL(T108_DESPESAS_ACESSORIAS_IMP,0),
           NVL(T108_DESPESAS_FRETE_IMP,0),
           NVL(T108_DESPESAS_THC_IMP,0),
           NVL(T108_DESPESAS_LOG_IMP,0),
           NVL(T108_DESPESAS_AFRMM_IMP,0),
           NVL(T108_DESPESAS_SISCOMEX_IMP,0),
           NVL(T108_DESPESAS_SEGURO_IMP,0)                   
      INTO n_CodFornecedor,   d_DataPrevistaFat,        d_DataEntrega,
           vn_despesas_acessorias_imp,      
           vn_despesas_frete_imp,           
           vn_despesas_thc_imp,             
           vn_despesas_log_imp,             
           vn_despesas_afrmm_imp,           
           vn_despesas_siscomex_imp,        
           vn_despesas_seguro_imp                      
      FROM T108_PEDIDOS_PEND
     WHERE T108_UNIDADE_IE = pigp_nunidade
       AND T108_NUMERO_PEDIDO_IU = pigp_nnumeroped;
       
    -- Soma com as despesas de importa��o   
    vn_ValorTotal := pigp_ValorTotal + (vn_despesas_acessorias_imp+vn_despesas_frete_imp+vn_despesas_thc_imp+vn_despesas_log_imp+vn_despesas_afrmm_imp+vn_despesas_siscomex_imp+vn_despesas_seguro_imp);
       
    --//verificar se caiu em fatura m�nima
    IF (pigp_TipoPedido = '8') OR NOT FCAIU_EM_FATURA_MINIMA(pigp_nunidade, pigp_nnumeroped, n_CodFornecedor, vn_ValorTotal) THEN
      --//Informar o valor total do Pedido
      UPDATE TCOM_INSPECAO_PEDIDO_COMPRA
         SET TCOM_TOTAL_PEDIDO = vn_ValorTotal
       WHERE TCOM_UNIDADE = pigp_nunidade
         AND TCOM_NUMERO_PEDIDO = pigp_nnumeroped;

      d_t057_data_limite   := d_DataPrevistaFat;
      
      SELECT count(M003_016_PRAZO_DIAS)
        INTO N_PrazoPagto
        FROM M003_016_TEMP_PRAZOS_PEDIDOS
       WHERE M003_016_UNIDADE    = pigp_nunidade
         AND M003_016_FORNECEDOR = n_CodFornecedor;        
         
      IF pigp_ValorST > 0 THEN
      
        SELECT NVL(T019_PRAZO_PGMTO_IMPOSTO,1)
          INTO vn_PrazoImp
          FROM T019_FORNECEDOR
         WHERE T019_FORNECEDOR_IU = n_CodFornecedor;
         
        --// Nova Seq. de Titulo a Pagar
        n_t900_ult_titulo_pagar := FSIS_INCREMENTANCAMPOS('T900_PARAM_NUMFISC','T900_ULT_TITULO_PAGAR',
                                                        'WHERE T900_UNIDADE_IE = ' || TO_CHAR(pigp_nUnidade),1);
        --// Gera a previs�o de duplicata por produto (Categoria)
        INSERT INTO T109_PEDIDOS_PEND_PRAZO (
          T109_UNIDADE_IE,        T109_NUMERO_PEDIDO_IE,       T109_NUMERO_TITULO_IE,
          T109_TIPO_DOCUMENTO_IE, T109_PRAZO_PAG_IU,           T109_DESCONTO_LIMITE,
          T109_VALOR_PARCELA,     T109_PARCELA_IMPOSTO_ST) VALUES (
          pigp_nUnidade,          pigp_nnumeroped,             n_t900_ult_titulo_pagar,
          'DP',                   vn_PrazoImp,                 0,
          pigp_ValorST,           'S');        
                       
        d_t057_data_limite   := d_DataPrevistaFat + vn_PrazoImp;
          
        --//Inserir o t�tulo apenas se o pedido nao for de transferencia
        IF (pigp_TipoPedido <> '8') THEN
          INSERT INTO T057_TITULO_PAGAR(
            T057_UNIDADE_IE,        T057_NUMERO_TITULO_IU,       T057_TIPO_DOCUMENTO_IU,
            T057_FORNECEDOR_E,      T057_DATA_EMISSAO,           T057_DATA_RECEB,
            T057_DATA_LIMITE,       T057_LOCAL_PAGAMENTO,        T057_SITUACAO_TITULO,
            T057_VALOR_BRUTO,       T057_BASE_DESCONTO,          T057_CHEQUE_IMPRESSO,
            T057_TIPO_INCLUSAO,     T057_DESCONTO_LIMITE,        T057_NUMERO_PEDIDO_E,
            T057_DATA_ANTECIPACAO) VALUES (
            pigp_nUnidade,          n_t900_ult_titulo_pagar,     'DP',
            n_CodFornecedor,        d_DataPrevistaFat,           d_DataEntrega,
            d_t057_data_limite,     'CAR',                       'P',
            pigp_ValorST,           pigp_ValorST,                'N',
            'A',                    0,                           pigp_nnumeroped,
            d_t057_data_limite);
        END IF;
                
      END IF;   

      nResto := MOD(vn_ValorTotal,N_PrazoPagto);

      FOR vcPRAZOS IN (SELECT M003_016_PRAZO_DIAS N_PrazoPagto,   NVL(M003_016_DESCONTO,0) n_DescLimite
                         FROM M003_016_TEMP_PRAZOS_PEDIDOS
                        WHERE M003_016_UNIDADE    = pigp_nunidade
                          AND M003_016_FORNECEDOR = n_CodFornecedor
                       ORDER BY M003_016_PRAZO_DIAS) LOOP

        d_t057_data_limite   := d_DataPrevistaFat + vcPRAZOS.N_PrazoPagto;
        --// Nova Seq. de Titulo a Pagar
        n_t900_ult_titulo_pagar := FSIS_INCREMENTANCAMPOS('T900_PARAM_NUMFISC','T900_ULT_TITULO_PAGAR',
                                                        'WHERE T900_UNIDADE_IE = ' || TO_CHAR(pigp_nUnidade),1);
        --// Gera a previs�o de duplicata por produto (Categoria)
        INSERT INTO T109_PEDIDOS_PEND_PRAZO (
          T109_UNIDADE_IE,        T109_NUMERO_PEDIDO_IE,       T109_NUMERO_TITULO_IE,
          T109_TIPO_DOCUMENTO_IE, T109_PRAZO_PAG_IU,           T109_DESCONTO_LIMITE,
          T109_VALOR_PARCELA,     T109_PARCELA_IMPOSTO_ST) VALUES (
          pigp_nUnidade,          pigp_nnumeroped,             n_t900_ult_titulo_pagar,
          'DP',                   vcPRAZOS.N_PrazoPagto,       vcPRAZOS.n_DescLimite,
          TRUNC(vn_ValorTotal/N_PrazoPagto)+nResto, 'N');

        --//Inserir o t�tulo apenas se o pedido nao for de transferencia
        IF (pigp_TipoPedido <> '8') THEN
          INSERT INTO T057_TITULO_PAGAR(
            T057_UNIDADE_IE,        T057_NUMERO_TITULO_IU,       T057_TIPO_DOCUMENTO_IU,
            T057_FORNECEDOR_E,      T057_DATA_EMISSAO,           T057_DATA_RECEB,
            T057_DATA_LIMITE,       T057_LOCAL_PAGAMENTO,        T057_SITUACAO_TITULO,
            T057_VALOR_BRUTO,       T057_BASE_DESCONTO,          T057_CHEQUE_IMPRESSO,
            T057_TIPO_INCLUSAO,     T057_DESCONTO_LIMITE,        T057_NUMERO_PEDIDO_E,
            T057_DATA_ANTECIPACAO) VALUES (
            pigp_nUnidade,          n_t900_ult_titulo_pagar,     'DP',
            n_CodFornecedor,        d_DataPrevistaFat,           d_DataEntrega,
            d_t057_data_limite,     'CAR',                       'P',
            TRUNC(vn_ValorTotal/N_PrazoPagto)+nResto, TRUNC(vn_ValorTotal/N_PrazoPagto)+nResto, 'N',
            'A',                    vcPRAZOS.n_DescLimite,                pigp_nnumeroped,
            d_t057_data_limite);
        END IF;

        nResto := 0;
        --//
        INSERT INTO TCOM_INSPECAO_PED_COMPRA_PRAZO (
          TCOM_UNIDADE,           TCOM_NUMERO_PEDIDO,       TCOM_DESCONTO_LIMITE,   TCOM_DESCONTO_ANTECIPACAO,   TCOM_PRAZO_PAG_IU ) VALUES (
          pigp_nUnidade,          pigp_nnumeroped,          vcPRAZOS.n_DescLimite,           0,                           N_PrazoPagto);

      END LOOP;
    END IF;
  END;

  -- CRC:19711 -> VERIFICA SE O FORNECEDOR REPRESENTA UM CD ...
  FUNCTION FCOM_FORNEC_TRANSFERENCIA (PI_FORNECEDOR IN  NUMBER,
                                      PO_NUNIDADE   OUT NUMBER)
  RETURN BOOLEAN IS
  BEGIN
    --//inicializar vari�veis
    PO_NUNIDADE := 0;
    --//Verificar se o fornecedor � unidade
    BEGIN
      SELECT T908_UNIDADE_IE
        INTO PO_NUNIDADE
        FROM T908_PARAM_EMPRESA
       WHERE T908_FORNECEDOR_EMPRESA_E = PI_FORNECEDOR;
      --Se n�o deu erro, retornar positivo, Fornecedor � uma unidade.
      RETURN TRUE;
      -- se nao retornou dados nao � um fornecedor de transferencia ...
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RETURN FALSE;
    END;
  END;
/* *****************************************************************************
          B L O C O   P R I N C I P A L   D E   P R O C E S S A M E N T O
 ******************************************************************************* */
BEGIN
  vb_ExecutaCommit             := TRUE;
  NumeroPedidoInicial          := 0;
  n_t900_ult_pedido_fornecedor := NULL;
  vnAliquotaInterna            := 0;       
  vnPercRedBaseIcms            := 0;
  vnPercBaseRedIcmSubst        := 0;
  vnValorRedIcmSubst           := 0;
  PO_NUMEROPEDIDOSGERADO       := ' ';
  
  -- Verifica se bloqueia produto estocado
  SELECT NVL(T925_BLOQUEIA_COMPRA_PROD_EST,'N')
    INTO vc_BloqueiaProdEstocado 
    FROM T925_PARAM_GERAIS_UNIDADES;   
  
  PSIS_LIMPA_TABELAS('TCOM_INSPECAO_PEDIDO_COMPRA,TCOM_INSPECAO_PED_COMPRA_ITENS,TCOM_INSPECAO_PED_COMPRA_PRAZO,');
  --//Guardar Data Atual
  vd_DataPedido := SYSDATE;
  --//Carregar param�tros Gerais de Unidade
  SELECT T925_ITEM_NOVO_ZERADO_REPOSIC
    INTO vcT925_ITEM_NOVO_ZERADO
    FROM T925_PARAM_GERAIS_UNIDADES;
  --//Loop com as sugest�es
  FOR vSug IN  ( SELECT T190_UNIDADE_IE, T190_SUGESTAO_COMPRA_IU, T190_DATA_SUGESTAO, T190_USUARIO_E
                   FROM T190_SUGESTAO_COMPRA
                  WHERE T190_UNIDADE_IE         = PI_NUNIDADE
                    AND T190_SUGESTAO_COMPRA_IU = PI_NSUGESTAO
                    AND T190_STATUS_MANIPULACAO = 9 ) LOOP -- Pegar apenas as sugest�es com STATUS 9 .
    --//Inicializando vari�veis
    CabecalhoIncluido   := FALSE;
    nQuebraItens        := 0;
    TotaisItens         := 0;
    vn_TotalST          := 0;
    vn_ValorST          := 0;
    vnT108_FORNECEDOR_E := 0;
    vn_UndAtual         := 0;


    --//LOOP Com os produtos da sugest�o
    FOR vItemSug IN ( SELECT T191_UNIDADE_FATURA_IE, T191_PRODUTO_IE,   T191_FORNECEDOR_E,   T191_SUGESTAO_UNITARIA,
                             T191_EMBALAGEM_PADRAO,  T191_PRODUTO_NOVO, T191_PRECO_UNITARIO, T191_PERC_EMBALAGEM,
                             T191_PERC_DESPACESS,    T191_DESCONTO_ITEM
                        FROM T191_SUGESTAO_COMPRA_ITEM
                       WHERE T191_UNIDADE_SUGESTAO_IE = vSug.T190_UNIDADE_IE
                         AND T191_SUGESTAO_COMPRA_IE  = vSug.T190_SUGESTAO_COMPRA_IU
                       ORDER BY T191_UNIDADE_FATURA_IE, T191_FORNECEDOR_E ) LOOP
      --//Configurar o erro inicialmente como ZERO -- Inexistente
      vn_TipoErro := 0;
      --Sugest�o Zerada
      IF vItemSug.T191_SUGESTAO_UNITARIA <= 0 THEN
        vn_TipoErro := 1;
      END IF;
      --Pre�o de Compra Zerado
      IF NVL(vItemSug.T191_PRECO_UNITARIO,0) <=  0 THEN
        vn_TipoErro := 2;
      END IF;
      -- Sugest�o menor que uma embalagem do fornecedor
      IF ( vItemSug.T191_EMBALAGEM_PADRAO > 0 ) THEN
        IF ( ROUND(vItemSug.T191_SUGESTAO_UNITARIA / vItemSug.T191_EMBALAGEM_PADRAO) <= 0 ) THEN
          vn_TipoErro := 3;--Sugest�o menor que uma embalagem padr�o do produto
        END IF;
      ELSE
        vn_TipoErro := 5;--Embalagem Padr�o n�o configurada.
      END IF;
      --Empresa n�o trabalha com sugest�o para itens novos gerados
      --IF (vcT925_ITEM_NOVO_ZERADO = 'N') AND (vItemSug.T191_PRODUTO_NOVO  = 'S') THEN
      --  vn_TipoErro := 4;
      --END IF;
      
      vn_Classificacao_Fiscal := NULL;
      
      -- Obtem UF do fornecedor
      BEGIN
       SELECT T002_UF_E 
         INTO vv_UfForn
         FROM T002_MUNICIPIO,
              T019_FORNECEDOR   
        WHERE T002_CEP_IU = T019_CEP_E           
          AND T019_FORNECEDOR_IU = vItemSug.T191_FORNECEDOR_E;
      EXCEPTION
       WHEN OTHERS THEN
         vv_UfForn := NULL;
      END;         
        
      IF vv_UfForn IS NOT NULL THEN
         
        BEGIN
          SELECT T002_UF_E
            INTO vUfDestino
            FROM T003_UNIDADE,
                 T002_MUNICIPIO
           WHERE T003_CEP_E = T002_CEP_IU
             AND T003_UNIDADE_IU = vItemSug.T191_UNIDADE_FATURA_IE;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            vUfDestino := NULL;
        END;
                  
        vn_NaturezaOperacaoEntrada := OBTEM_NATUREZA_COMPRA(vItemSug.T191_UNIDADE_FATURA_IE,vItemSug.T191_FORNECEDOR_E,vItemSug.T191_PRODUTO_IE);
         
        BEGIN
          SELECT T278_PRODUTO_CLASSFISC_NATUR.T278_CLASSIFIC_FISCAL_E
            INTO vn_Classificacao_Fiscal
            FROM T278_PRODUTO_CLASSFISC_NATUR
           WHERE (T278_PRODUTO_CLASSFISC_NATUR.T278_UNIDADE_IE           = vItemSug.T191_UNIDADE_FATURA_IE) AND
                 (T278_PRODUTO_CLASSFISC_NATUR.T278_PRODUTO_IE           = vItemSug.T191_PRODUTO_IE) AND
                 (T278_PRODUTO_CLASSFISC_NATUR.T278_NATUREZA_OPERACAO_IE = vn_NaturezaOperacaoEntrada) AND
                 (T278_PRODUTO_CLASSFISC_NATUR.T278_UF_IE                = vv_UfForn);
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            BEGIN
              SELECT T084_PRODUTO_CLASS_FISCAL.T084_CLASSIFIC_FISCAL_E
                INTO vn_Classificacao_Fiscal
                FROM T084_PRODUTO_CLASS_FISCAL
               WHERE (T084_PRODUTO_CLASS_FISCAL.T084_UNIDADE_IE = vItemSug.T191_UNIDADE_FATURA_IE) AND
                     (T084_PRODUTO_CLASS_FISCAL.T084_PRODUTO_IE = vItemSug.T191_PRODUTO_IE) AND
                     (T084_PRODUTO_CLASS_FISCAL.T084_UF_IE      = vv_UfForn);
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                vn_Classificacao_Fiscal := NULL;
            END;
        END;
                    
      END IF;
      
      --IF vn_Classificacao_Fiscal IS NULL THEN
      --  vn_TipoErro := 17;
      --END IF;
            
      -- Se o par�metro n�o permitir produto estocado
      IF vc_BloqueiaProdEstocado = 'S' THEN
        -- Verifica se o produto � estocado      
        SELECT NVL(T077_PRODUTO_ESTOCADO,'N') 
          INTO vc_produto_estocado
          FROM T077_PRODUTO_UNIDADE
         WHERE T077_UNIDADE_IE = vItemSug.T191_UNIDADE_FATURA_IE
           AND T077_PRODUTO_IE = vItemSug.T191_PRODUTO_IE;
           
        IF vc_produto_estocado = 'S' THEN
          vn_TipoErro := 18;
        END IF;
           
      END IF;     
            
      --//Se o erro continua zerado, vou poder incluir o produto
      vb_IncluirProduto := vn_TipoErro = 0;
      --//Permitir inserir apenas se pre�o maior que ZERO
      IF vb_IncluirProduto THEN
        --//Carregar informa��es do do Fornecedor na unidade, se ainda n�o carregado
        IF ( vnT108_FORNECEDOR_E <> vItemSug.T191_FORNECEDOR_E ) OR ( vn_UndAtual <> vItemSug.T191_UNIDADE_FATURA_IE ) THEN
          --Se trocou o fornecedor ou unidade, fechar o pedido do fornecedor unidade Anterior
          IF vnT108_FORNECEDOR_E <> 0 THEN
            CabecalhoIncluido := FALSE;
            nQuebraItens := 0;
            -- implantacao piloto WM (05/04/2010).
            --grava As Previs�es De Duplicata(T109)
            Grava_Previsoes(vn_UndAtual, n_t900_ult_pedido_fornecedor, TotaisItens, vn_T108_TIPO_PEDIDO, vn_TotalST);
            --//zerar total do pedido
            TotaisItens := 0;
            vn_TotalST  := 0;
            vn_ValorST := 0;
          END IF;
          --//Carregar par�metros de unidade wm caso de troca da mesma
          IF vn_UndAtual <> vItemSug.T191_UNIDADE_FATURA_IE THEN
            --//Carregar par�metros de compra
            BEGIN
              SELECT T903_LIMITA_QTDE_ITENS_PEDIDO, T903_QTDE_ITENS_PEDIDO
                INTO vc_Limita_QTD_Itens,           vn_QTD_Prod_Pedido
                FROM T903_PARAM_COMPRAS
               WHERE T903_UNIDADE_IE = vItemSug.T191_UNIDADE_FATURA_IE;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                vc_Limita_QTD_Itens := 'N';
            END;
            --//Carregar par�metros de unidade
            BEGIN
              SELECT NVL(T908_DATA_FATURAMENTO,SYSDATE)
                INTO vd_data_faturamento
                FROM T908_PARAM_EMPRESA
               WHERE T908_UNIDADE_IE = vItemSug.T191_UNIDADE_FATURA_IE;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                vd_data_faturamento := SYSDATE;
            END;
            --//Guardar valores de controle de LOOP
            vn_UndAtual := vItemSug.T191_UNIDADE_FATURA_IE;
          END IF;
          --//Dados do Fornecedor
          BEGIN
            SELECT NVL(T054_PRAZO_ENTREGA,0), T054_REPASSE_ICM,   T054_DESCONTO_NOTA,   T054_COMPRADOR_E,   NVL(T054_QTDE_DIAS_FATURAMENTO,0)
              INTO vnT054_PRAZO_ENTREGA,      vvT054_REPASSE_ICM, vvT054_DESCONTO_NOTA, vnT054_COMPRADOR_E, vnT054_QTDE_DIAS_FAT
              FROM T054_FORNECEDOR_UNIDADE
             WHERE T054_UNIDADE_IE    = vItemSug.T191_UNIDADE_FATURA_IE
               AND T054_FORNECEDOR_IE = vItemSug.T191_FORNECEDOR_E;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              vnT054_PRAZO_ENTREGA := 0;
              vvT054_REPASSE_ICM   := 0;
              vvT054_DESCONTO_NOTA := 0;
              vnT054_QTDE_DIAS_FAT := 0;
          END;
          --//Guardar valores de controle de LOOP
          vnT108_FORNECEDOR_E := vItemSug.T191_FORNECEDOR_E;
        END IF;
        --//Dados do produto
        SELECT NVL(T076_ALIQUOTA_IPI,0), T076_TIPO_CALC_IPI
          INTO vn_t076_aliquota_ipi,     vn_TipoCalcIPI
          FROM T076_PRODUTO
         WHERE T076_PRODUTO_IU = vItemSug.T191_PRODUTO_IE;
        --//Se ainda n�o incluiu o cbe�alho, incluir agora
        IF NOT CabecalhoIncluido THEN
          --//Obter novo n�mero de Pedido
          N_T900_ULT_PEDIDO_FORNECEDOR := FSIS_INCREMENTACAMPO('T900_PARAM_NUMFISC',
                                                               'T900_ULT_PEDIDO_FORNECEDOR',
                                                               'WHERE T900_UNIDADE_IE = '|| TO_CHAR(vItemSug.T191_UNIDADE_FATURA_IE));
          --caso tenha pedido gerado guarda o numero do primeiro pedido
          IF NumeroPedidoInicial = 0 THEN
            NumeroPedidoInicial := n_t900_ult_pedido_fornecedor;
          END IF;
          
          PO_NUMEROPEDIDOSGERADO := PO_NUMEROPEDIDOSGERADO || n_t900_ult_pedido_fornecedor || ',';

          --//Configurar tipo e data do pedido
          vn_T108_TIPO_PEDIDO   := '5'; -- Status 5 - Reposi��o Automatica
          vb_ObedeceQtdItens    := FALSE;--Configurar para n�o obedecer o par�metro que limita QTD de Itens no Pedido Pendente
          vvPedidoTransferencia := 'N';
          
          -- VERIFICA SE O FORNECEDOR REPRESENTA UM CD PARA DEFINIR O TIPO DE PEDIDO ...
          IF FCOM_FORNEC_TRANSFERENCIA( vItemSug.T191_FORNECEDOR_E, vnT003_Unidade ) THEN
            vb_ObedeceQtdItens := TRUE;--Se for FORNECEDOR UNIDADE, dever� obedecer o par�metro.
            vn_T108_TIPO_PEDIDO   := '8'; -- Tipo pedido 8 - Transferencia...
            
            vvPedidoTransferencia := 'S';
            IF vnT003_Unidade = 0 THEN
              PREP_GRAVA_OCORRENCIA_PRODUTO(PI_NUNIDADE,PI_NSUGESTAO,0,6,vd_DataPedido,vItemSug.T191_FORNECEDOR_E,vn_UndAtual,'',null,null);
            END IF;
          END IF;
          
          --//configurar data de faturamento
          vd_data_faturamento := vd_data_faturamento + vnT054_QTDE_DIAS_FAT;
          --//Configurar data de Entrega
          vdT108_DATA_ENTREGA := vd_data_faturamento + vnT054_PRAZO_ENTREGA;
          --//Acumular pedido na lista
          vInd := lst_Pedidos.COUNT + 1;

          lst_Pedidos(vInd).UNIDADE     := vItemSug.T191_UNIDADE_FATURA_IE;
          lst_Pedidos(vInd).PEDIDO      := n_t900_ult_pedido_fornecedor;
          lst_Pedidos(vInd).FORNECEDOR  := vItemSug.T191_FORNECEDOR_E;
          lst_Pedidos(vInd).COMPRADOR   := vnT054_COMPRADOR_E;
          lst_Pedidos(vInd).DATA_FATURA := vd_data_faturamento;
          lst_Pedidos(vInd).TIPO_PEDIDO := vn_T108_TIPO_PEDIDO;
          lst_Pedidos(vInd).UNIDADE_CD  := vnT003_Unidade;

          --//inserir o pedido pendente
          INSERT INTO T108_PEDIDOS_PEND(
             T108_UNIDADE_IE,                 T108_NUMERO_PEDIDO_IU,             T108_DATA_PEDIDO,
             T108_DATAPREVISTA_FATURA,        T108_DATA_ENTREGA,                 T108_SITUACAO_PEDIDO,
             T108_TIPO_FRETE,                 T108_FORNECEDOR_E,                 T108_OBSERVACAO,
             T108_TIPO_PEDIDO,                T108_UNIDADE_SUGESTAO,             T108_NUMERO_SUGESTAO,
             T108_REQUIS_DEPOSEXT, T108_CODCOMPRADOR, T108_COMPRADOR )  --// Lenilton 
          VALUES(
             vItemSug.T191_UNIDADE_FATURA_IE, n_t900_ult_pedido_fornecedor,      vd_DataPedido,
             vd_data_faturamento,             vdT108_DATA_ENTREGA,               'P',
             'C',                             vItemSug.T191_FORNECEDOR_E,        'Pedido gerado atrav�s da reposi��o autom�tica.',
             vn_T108_TIPO_PEDIDO,             PI_NUNIDADE,                       PI_NSUGESTAO,
             vvPedidoTransferencia, vSug.T190_USUARIO_E, (SELECT T024_NOME FROM T024_USUARIO WHERE T024_USUARIO_IU = vSug.T190_USUARIO_E));
          --//Inserir Registro para chamada da Al�ada de Compras
          INSERT INTO TCOM_INSPECAO_PEDIDO_COMPRA (
             TCOM_UNIDADE,                    TCOM_NUMERO_PEDIDO,                TCOM_FORNECEDOR,
             TCOM_COMPRADOR,                  TCOM_TAXA_VENDOR,                  TCOM_DATAPREVISTA_FATURA)  VALUES (
             vItemSug.T191_UNIDADE_FATURA_IE, n_t900_ult_pedido_fornecedor,      vItemSug.T191_FORNECEDOR_E,
             vnT054_COMPRADOR_E,              0,                                 vd_data_faturamento);
          --//seta a variavel indicando que o cabecalho do pedido ja foi criado
          CabecalhoIncluido := TRUE;
        END IF;
        
        --// Chama a rotina de calculo
        PMOV_CALC_IMPOSTOS_ENTRADA(vItemSug.T191_UNIDADE_FATURA_IE,
                                   vItemSug.T191_FORNECEDOR_E,
                                   'F',
                                   vItemSug.T191_PRODUTO_IE,
                                   vn_NaturezaOperacaoEntrada,
                                   vv_UfForn ,
                                   vUfDestino,
                                   vItemSug.T191_SUGESTAO_UNITARIA,
                                   vItemSug.T191_PRECO_UNITARIO,
                                   vItemSug.T191_DESCONTO_ITEM,
                                   vvT054_REPASSE_ICM,
                                   vvT054_DESCONTO_NOTA,
                                   vItemSug.T191_PERC_EMBALAGEM,
                                   vItemSug.T191_PERC_DESPACESS,
                                   'W',
                                   0,
                                   po_nAliquotaIcmsNormal   ,
                                   po_nAliquotaIpi          ,
                                   po_nAgregadoCompra       ,
                                   po_Classificacao_Fiscal  ,
                                   po_nBaseIcmsNormal       ,
                                   po_nValorIcmsNormal      ,
                                   po_nBaseIcmsSubstituto   ,
                                   po_nValorIcmsSubstituto  ,
                                   po_nBaseIpi              ,
                                   po_nValorIpi             ,
                                   po_nOutrasIcmsNormal     ,
                                   po_nIsentasIcmsNormal    ,
                                   po_nOutrasReducao        ,
                                   po_nIsentasReducao       ,
                                   po_nOutrasIpi            ,
                                   po_nIsentasIpi           ,
                                   po_nValorDifa            ,
                                   po_nDescontoItem         ,
                                   po_nDescontoRepasse      ,
                                   po_nDescontoComercial    ,
                                   po_ntotaldescontos       ,
                                   po_cTribPis              ,
                                   po_nBasePiscofinsIsento  ,
                                   po_nBasePiscofinsRetido  ,
                                   po_nAliquotaPisRetido    ,
                                   po_nAliquotaCofinsRetido ,
                                   po_nValorPisRetido       ,
                                   po_nValorCofinsRetido    ,
                                   po_nBasePiscofinsNormal  ,
                                   po_nAliquotaPisNormal    ,
                                   po_nAliquotaCofinsNormal ,
                                   po_nValorPisNormal       ,
                                   po_nValorCofinsNormal    ,
                                   po_cmensagemerro         ,
                                   po_PercAcrescFECEP       ,
                                   po_ValorAcrescFECEP      ,
                                   po_Base_Diferimento_Icms ,
                                   po_Valor_Diferimento_Icms,
                                   po_Perc_Diferimento_Icms ,
                                   vnAliquotaInterna,         
                                   vnPercRedBaseIcms,         
                                   vnPercBaseRedIcmSubst,     
                                   vnValorRedIcmSubst,        
                                   pio_ValorMedioPonderado);         
        
        --//Inseri os itens do pedido pendente
        INSERT INTO T110_PEDIDOS_PEND_ITENS(
           T110_UNIDADE_IE,                        T110_NUMERO_PEDIDO_IE,                  T110_PRODUTO_IE,
           T110_QTDE_PEDIDO,                       T110_PERC_REPASSEICM,                   T110_DESCONTO_COMERCIAL,
           T110_PRECO_ITEM,                        T110_DESCONTO_ITEM,                     T110_PERC_DESPACESS,
           T110_PERC_EMBALAGEM,                    T110_ALIQUOTA_IPI,                      T110_OPERACAO_FISCAL,
           T110_TIPO_CALC_IPI,                     T110_DATA_ENTREGA,                      T110_VALOR_ST ) VALUES(
           vItemSug.T191_UNIDADE_FATURA_IE,        n_t900_ult_pedido_fornecedor,           vItemSug.T191_PRODUTO_IE,
           vItemSug.T191_SUGESTAO_UNITARIA,        vvT054_REPASSE_ICM,                     vvT054_DESCONTO_NOTA,
           vItemSug.T191_PRECO_UNITARIO,           vItemSug.T191_DESCONTO_ITEM,            vItemSug.T191_PERC_DESPACESS,
           vItemSug.T191_PERC_EMBALAGEM,           vn_t076_aliquota_ipi,                   '1',
           vn_TipoCalcIPI,                         vdT108_DATA_ENTREGA,                    po_nValorIcmsSubstituto);
        --//Inserir ITENS para Al�ada
        INSERT INTO TCOM_INSPECAO_PED_COMPRA_ITENS (
           TCOM_PRODUTO,                           TCOM_QTDE_PEDIDA,                       TCOM_PRECO_COMPRA,
           TCOM_UNIDADE,                           TCOM_DESCONTO_ITEM,                     TCOM_PERC_REPASSEICM,
           TCOM_DESCONTO_COMERCIAL,                TCOM_PERC_EMBALAGEM,                    TCOM_PERC_DESPACESS,
           TCOM_NUMERO_PEDIDO ) VALUES (
           vItemSug.T191_PRODUTO_IE,               vItemSug.T191_SUGESTAO_UNITARIA,        vItemSug.T191_PRECO_UNITARIO,
           vItemSug.T191_UNIDADE_FATURA_IE,        vItemSug.T191_DESCONTO_ITEM,            vvT054_REPASSE_ICM,
           vvT054_DESCONTO_NOTA,                   vItemSug.T191_PERC_EMBALAGEM,           vItemSug.T191_PERC_DESPACESS,
           n_t900_ult_pedido_fornecedor);
           
        vn_ValorST := NVL(po_nValorIcmsSubstituto,0);   
                
        -- verifica tipo de cobran�a de imposto do fornecedor
        SELECT NVL(T019_EMITE_COBR_SEPAR_IMPOSTO,'N')
          INTO vc_emite_cobr_separ_imposto
          FROM T019_FORNECEDOR
         WHERE T019_FORNECEDOR_IU = vItemSug.T191_FORNECEDOR_E;
              
        --//Acumular o custo total do Pedido
        vn_PrecoItem := TRUNC((vItemSug.T191_SUGESTAO_UNITARIA * vItemSug.T191_PRECO_UNITARIO),2);
        --//Verificar o desconto de entrada
        PMOV_CALC_DESCONTOS_ENTRADA( vItemSug.T191_UNIDADE_FATURA_IE,  vItemSug.T191_FORNECEDOR_E,   'F',
                                     vn_PrecoItem,                     0,                            vvT054_REPASSE_ICM,
                                     vvT054_DESCONTO_NOTA,             vn_ValorDescItem,             vn_ValorDescRepasse,
                                     vn_ValorDescComercial,            vn_ErroCalc);
        --//Acumular o subtotal Liquido
        SubTotalLiq := vn_PrecoItem - (vn_ValorDescItem + vn_ValorDescRepasse + vn_ValorDescComercial);
        CalcIPI     := TRUNC(((vn_PrecoItem * vn_T076_ALIQUOTA_IPI)/100),2);

        -- Soma o total ao ICMS ST 
        IF vc_emite_cobr_separ_imposto = 'N' THEN
          TotaisItens := TotaisItens + SubTotalLiq + CalcIPI + vn_ValorST;
          vn_ValorST := 0;          
        ELSE
          vn_TotalST := vn_TotalST + vn_ValorST; 
          TotaisItens := TotaisItens + SubTotalLiq + CalcIPI;
          vn_ValorST := 0;
        END IF;
          
        --//Guardar informa�oes em caso de limite de itens por pedido
        IF ( vc_Limita_QTD_Itens = 'S' ) and ( vb_ObedeceQtdItens ) THEN
          nQuebraItens := nQuebraItens + 1;
          --//Se atingiu o m�ximo permitido em par�metro, Fechar o pedido para abrir outro
          IF nQuebraItens = vn_QTD_Prod_Pedido THEN
            CabecalhoIncluido := FALSE;
            nQuebraItens := 0;
            -- implantacao piloto WM (05/04/2010).
            --grava As Previs�es De Duplicata(T109)
            Grava_Previsoes(vItemSug.T191_UNIDADE_FATURA_IE, n_t900_ult_pedido_fornecedor, TotaisItens, vn_T108_TIPO_PEDIDO, vn_TotalST);

            --//zerar total do pedido
            TotaisItens := 0;
            vn_ValorST := 0;
            vn_TotalST := 0;
          END IF;
        END IF;
      ELSE --//Se n�o for poss�vel incluir o produto, avisar o problema
        PREP_GRAVA_OCORRENCIA_PRODUTO(PI_NUNIDADE,   PI_NSUGESTAO,               vItemSug.T191_PRODUTO_IE,        vn_TipoErro,
                                      vd_DataPedido, vItemSug.T191_FORNECEDOR_E, vItemSug.T191_UNIDADE_FATURA_IE, '',
                                      null,          null);
      END IF;
    END LOOP;
    
    -- Calcula as despesas de importa��o dos pedidos de importa��o
    Verifica_Despesas_Importacao(TotaisItens);
        
    --grava As Previs�es De Duplicata(T109)
    IF CabecalhoIncluido THEN
      Grava_Previsoes(vn_UndAtual, n_t900_ult_pedido_fornecedor, TotaisItens, vn_T108_TIPO_PEDIDO, vn_TotalST);  --// CRC18646-CR15895
    END IF;
  END LOOP;
 /* Validar o pedido em rela��o � al�ada de Compras, cONSIDERAR O USU�RIO 9999 como requisitante da opera��o. */
  vInd := -1;
  LOOP
    vInd := lst_PEDIDOS.NEXT(vInd);
    EXIT WHEN vInd IS NULL;
    
    --// Lenilton
    IF lst_Pedidos(vInd).TIPO_PEDIDO = '8' THEN  --// Pedido de transfer�ncia entre unidades
      vResp := 0;
    ELSE
      vResp := FCOM_LIBERA_PEDIDOS_BLOQUEADOS( lst_Pedidos(vInd).UNIDADE,     
                                               lst_Pedidos(vInd).PEDIDO,    
                                               lst_Pedidos(vInd).FORNECEDOR,
                                               1,                             
                                               lst_Pedidos(vInd).COMPRADOR, 
                                               9999,
                                               lst_Pedidos(vInd).DATA_FATURA, 
                                               vvTipoResp,                  
                                               vvMsgResp, PI_NSUGESTAO );
    END IF;
    
    --se retornou 1 atualizar erro para o pedido
    IF vResp = 1 THEN
      --//loop para evitar cria��o de vari�veis
      FOR vErro IN ( SELECT TCOM_DATA_HORA_BLOQUEIO, TCOM_TIPO_BLOQUEIO,               TCOM_VALOR_COTA_DISPONIVEL,
                            TCOM_TIPO_COTA,          TCOM_NOME_NEGOCIADOR_GESTOR
                       FROM TCOM_INSPECAO_PEDIDO_COMPRA
                      WHERE TCOM_UNIDADE = lst_Pedidos(vInd).UNIDADE
                        AND TCOM_NUMERO_PEDIDO = lst_Pedidos(vInd).PEDIDO) LOOP
        UPDATE T108_PEDIDOS_PEND
           SET T108_DATA_BLOQUEIO          = vErro.TCOM_DATA_HORA_BLOQUEIO,
               T108_TIPO_BLOQUEIO          = vErro.TCOM_TIPO_BLOQUEIO,
               T108_VALOR_COTA_DISPONIVEL  = vErro.TCOM_VALOR_COTA_DISPONIVEL,
               T108_TIPO_COTA              = vErro.TCOM_TIPO_COTA,
               T108_NOME_NEGOCIADOR_GESTOR = vErro.TCOM_NOME_NEGOCIADOR_GESTOR,
               T108_MENSAGEM_BLOQUEIO      = 'Pedido bloqueado, gerado por sugest�o autom�tica.',
               T108_SITUACAO_LOTE_PEDIDO   = 8
         WHERE T108_UNIDADE_IE       = lst_Pedidos(vInd).UNIDADE
           AND T108_NUMERO_PEDIDO_IU = lst_Pedidos(vInd).PEDIDO;
      END LOOP;

    ELSIF vResp = 0 THEN

      IF lst_Pedidos(vInd).TIPO_PEDIDO = '8' THEN  --// Pedido de transfer�ncia entre unidades
        BEGIN
          PREP_GERA_PRENOTA_PED_AUTO( PI_NUNIDADE, PI_NSUGESTAO, lst_Pedidos(vInd).UNIDADE, lst_Pedidos(vInd).UNIDADE_CD,
                                      9999, lst_Pedidos(vInd).PEDIDO, vvPrenotaGerada,po_HaOcorrencias );
        EXCEPTION
          WHEN OTHERS THEN
            vb_ExecutaCommit := FALSE;
        END;
      END IF;
    END IF;

    --//Excluir o registro ap�s utiliza��o
    DELETE T191_SUGESTAO_COMPRA_ITEM
     WHERE T191_UNIDADE_SUGESTAO_IE = PI_NUNIDADE
       AND T191_SUGESTAO_COMPRA_IE  = PI_NSUGESTAO
       AND T191_UNIDADE_FATURA_IE = lst_Pedidos(vInd).UNIDADE
       AND T191_FORNECEDOR_E = lst_Pedidos(vInd).FORNECEDOR;
  END LOOP;

  --//Verific ar se todas as sugest�es foram eliminadas!!
  SELECT COUNT(DISTINCT T191_FORNECEDOR_E)
    INTO vn_TipoErro
    FROM T191_SUGESTAO_COMPRA_ITEM
   WHERE T191_UNIDADE_SUGESTAO_IE = PI_NUNIDADE
     AND T191_SUGESTAO_COMPRA_IE = PI_NSUGESTAO;
  --//Se n�o encontrou nenhum fornecedor para a sugest�o, todos foram efetivados, ent�o eliminar registro de sugest�o!!
  IF vn_TipoErro = 0 THEN
    -- EXCLUI A REPOSICAO AUTOMATICA
    DELETE T190_SUGESTAO_COMPRA
     WHERE T190_UNIDADE_IE         = PI_NUNIDADE
       AND T190_SUGESTAO_COMPRA_IU = PI_NSUGESTAO;
  ELSE --//Caso contr�rio atualizar registro de sugest�o informando que houve Problemas
    UPDATE T190_SUGESTAO_COMPRA
       SET T190_STATUS_MANIPULACAO = 2
     WHERE T190_UNIDADE_IE         = PI_NUNIDADE
       AND T190_SUGESTAO_COMPRA_IU = PI_NSUGESTAO;
  END IF;
  -- Efetiva Altera��es
  IF vb_ExecutaCommit THEN
    COMMIT;

    IF n_t900_ult_pedido_fornecedor IS NULL THEN
      PO_NUMEROPEDIDOSGERADO := null;
    END IF;
  END IF;

EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    RAISE_APPLICATION_ERROR(-20000, TO_CHAR(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE),TRUE);   
    PREP_GRAVA_OCORRENCIA_PRODUTO(PI_NUNIDADE,PI_NSUGESTAO,0,0,vd_DataPedido,vnT108_FORNECEDOR_E,vn_UndAtual,SQLERRM,null,null);    
END P_GRAVA_PEDIDO_RA_ERP;
/
