CREATE OR REPLACE FUNCTION I_MDLOG.F_PRODUTO_UNIDADE_PERIODO(PI_UNIDADE  NUMBER,
                                                             PI_PRODUTO  NUMBER,
                                                             PI_PERIODO  NUMBER,
                                                             PI_CAMPO_RETORNO  NUMBER) RETURN NUMBER
IS
  vRetorno  NUMBER;
BEGIN
  
  /*
    PI_CAMPO_RETORNO 
      1 - T418_VENDA_UNIDADE_PERIODO
      2 - T418_VENDA_VALOR_PERIODO
      3 - T418_FALTAS_PERIODO
  */

  SELECT CASE PI_CAMPO_RETORNO             
            WHEN 1 THEN T418_VENDA_UNIDADE_PERIODO
            WHEN 2 THEN T418_VENDA_VALOR_PERIODO
            WHEN 3 THEN T418_FALTAS_PERIODO
         END 
       INTO vRetorno
  FROM (
    SELECT T418_VENDA_UNIDADE_PERIODO,
           T418_VENDA_VALOR_PERIODO,
           T418_FALTAS_PERIODO, 
           ROWNUM NUMERO_LINHA
    FROM (
        SELECT T418_VENDA_UNIDADE_PERIODO,
               T418_VENDA_VALOR_PERIODO,
               T418_FALTAS_PERIODO               
          FROM T418_PRODUTO_UNIDADE_PERIODO
         WHERE T418_UNIDADE_IE = PI_UNIDADE
           AND T418_PRODUTO_IE = PI_PRODUTO
         ORDER BY T418_ANOMESDIA_IU DESC
        ) T
    WHERE ROWNUM <= 4
  )
  WHERE NUMERO_LINHA = PI_PERIODO;

  RETURN vRetorno;
END;
/
