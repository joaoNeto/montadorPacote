create or replace PROCEDURE I_MDLOG.PCADASTRO_LOJA(
											PI_CODLOJA             NUMBER, 
											PI_TIPO_UNIDADE        NUMBER,
											PI_REGIME_TRIB         CHAR,
											PI_EMPRESA             VARCHAR2, 
											PI_RAZAO_SOCIAL        VARCHAR2,
											PI_NOME_RESUMIDO       VARCHAR2,
											PI_CEP                 VARCHAR2, 
											PI_ENDERECO            VARCHAR2,
											PI_NUMERO              VARCHAR2,
											PI_COMPLEMENTO         VARCHAR2, 
											PI_BAIRRO              VARCHAR2,
											PI_UF                  CHAR,
											PI_DDD_TELEFONE        VARCHAR2,
											PI_TELEFONE            VARCHAR2, 
											PI_DDD_FAX             VARCHAR2,
											PI_FAX                 VARCHAR2,
											PI_CNPJ                VARCHAR2, 
											PI_INSCRICAO_MUNICIPAL VARCHAR2,
											PI_INSCRICAO_ESTADUAL  VARCHAR2,
											PI_CODIGO_INTEGRACAO   VARCHAR2,
											PI_FORMATO_LOJA        NUMBER, 
											PI_CODIGO_EAN          NUMBER,
											PI_CODIGO_MADRINHA     NUMBER,
											PI_OPERACAO            CHAR,
											STATUS OUT VARCHAR2
                                            ) IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_LOJA
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DE LOJA.
  AUTOR     : Luis Eduardo Pinheiro Pedrosa
  DATA      : 28/11/2016
  CRC       : 70542
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN
	STATUS     := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT INTO DBAMDATA.T003_UNIDADE(
		T003_UNIDADE_IU ,
		T003_TIPO_UNIDADE,
		T003_COD_REGIME_TRIB ,
		T003_EMPRESA ,
		T003_RAZAO_SOCIAL, 
		T003_NOME, 
		T003_CEP_E , 
		T003_ENDERECO,
		T003_NUMERO, 
		T003_COMPLEMENTO, 
		T003_BAIRRO, 
		T003_UF,
		T003_TELEFONE_DDD,
		T003_TELEFONE, 
		T003_FAX_DDD,
		T003_FAX,
		T003_CGC,
		T003_INSCRICAO_MUNICIPAL,
		T003_INSCRICAO_ESTADUAL,
		T003_CODIGO_INTEGRACAO,
		T003_FORMATO_LOJA_E,
		T003_EAN_UNIDADE,
		T003_CODIGO_LOJA_VAREJO	
		)
	 VALUES (
		PI_CODLOJA , 
		PI_TIPO_UNIDADE,
		PI_REGIME_TRIB ,
		PI_EMPRESA, 
		PI_RAZAO_SOCIAL,
		PI_NOME_RESUMIDO,
		PI_CEP, 
		PI_ENDERECO,
		PI_NUMERO,
		PI_COMPLEMENTO, 
		PI_BAIRRO,
		PI_UF ,
		PI_DDD_TELEFONE,
		PI_TELEFONE , 
		PI_DDD_FAX,
		PI_FAX,                 
		PI_CNPJ, 
		PI_INSCRICAO_MUNICIPAL,
		PI_INSCRICAO_ESTADUAL,
		PI_CODIGO_INTEGRACAO,
		PI_FORMATO_LOJA, 
		PI_CODIGO_EAN,
		PI_CODIGO_MADRINHA
	);
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/

  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
     UPDATE DBAMDATA.T003_UNIDADE
     SET
		T003_TIPO_UNIDADE                 = PI_TIPO_UNIDADE, 
		T003_COD_REGIME_TRIB              = PI_REGIME_TRIB, 
		T003_EMPRESA                      = PI_EMPRESA,
		T003_RAZAO_SOCIAL                 = PI_RAZAO_SOCIAL, 
		T003_NOME                         = PI_NOME_RESUMIDO, 
		T003_CEP_E                        = PI_CEP,
		T003_ENDERECO                     = PI_ENDERECO, 
		T003_NUMERO                       = PI_NUMERO, 
		T003_COMPLEMENTO                  = PI_COMPLEMENTO,
		T003_BAIRRO                       = PI_BAIRRO, 
		T003_UF                           = PI_UF, 
		T003_TELEFONE_DDD                 = PI_DDD_TELEFONE,
		T003_TELEFONE                     = PI_TELEFONE, 
		T003_FAX_DDD                      = PI_DDD_FAX, 
		T003_FAX                          = PI_FAX,
		T003_CGC                          = PI_CNPJ, 
		T003_INSCRICAO_MUNICIPAL          = PI_INSCRICAO_MUNICIPAL, 
		T003_INSCRICAO_ESTADUAL           = PI_INSCRICAO_ESTADUAL,
		T003_CODIGO_INTEGRACAO           = PI_CODIGO_INTEGRACAO,
		T003_FORMATO_LOJA_E               = PI_FORMATO_LOJA,
		T003_EAN_UNIDADE                  = PI_CODIGO_EAN,
		T003_CODIGO_LOJA_VAREJO           = PI_CODIGO_MADRINHA
		WHERE T003_UNIDADE_IU             = PI_CODLOJA;
		
	
	EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE FROM DBAMDATA.T003_UNIDADE
            WHERE T003_UNIDADE_IU   = PI_CODLOJA;
            
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
    
    
    --CONFIRMAR OPERCAO
    
    COMMIT;
    
    
EXCEPTION
	WHEN OTHERS THEN
		ROLLBACK;
		ecode := SQLCODE;
		emesg := SUBSTR(SQLERRM,1, 2048);
		STATUS     := 'ER';
END;