create or replace PROCEDURE I_MDLOG.PCADASTRO_CFOP(PI_NATUREZA_OPERACAO          NUMBER, 
											PI_DESCRICAO                 VARCHAR2,
											PI_TIPO                      NUMBER,
											PI_GRUPO_RESULTADO           VARCHAR2, 
											PI_ACUMULA_ESTATISTIC_COMPRA CHAR,
                      PI_GERA_TITULO               NUMBER,
											PI_HABILITA_CALCULO_IPI      CHAR,
											PI_TIPO_CLASSIFICACAO_FISCAL CHAR,
											PI_CFOP_CORRESP              NUMBER,
											PI_CFOP_INVERSA              NUMBER,
											PI_CFOP_ST                   NUMBER,
											PI_OPERACAO                  CHAR,
											STATUS OUT VARCHAR2
                                            ) IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  PI_CONTROLE_ANVISA  NUMBER(10);
  PI_DIGITACOES_VALIDAS  CHAR(4);
  
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_CFOP
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DO CFOP.
  AUTOR     : Luis Eduardo Pinheiro Pedrosa
  DATA      : 03/11/2016
  CRC       : 
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN
	STATUS     := 'PR';
	PI_CONTROLE_ANVISA := NULL;
	PI_DIGITACOES_VALIDAS := '1234';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT INTO DBAMDATA.T013_NATUREZA_OPERACAO(
		T013_NATUREZA_OPERACAO_IU,
		T013_DESCRICAO,
		T013_TIPO,
		T013_GRUPO_RESULTADO_E,
		T013_ACUMULA_ESTATISTIC_COMPRA,
		T013_DIGITACOES_VALIDAS,
		T013_CONTROLE_RDC27_ANVISA_E,
		T013_GERA_TITULO,
		T013_HABILITA_CALCULO_IPI,
		T013_TIPO_CLASSIFICACAO_FISCAL,
		T013_CFOP_CORRESP_E,
		T013_CFOP_INVERSA_IE, 
		T013_CFOP_ST)
	 VALUES (
		PI_NATUREZA_OPERACAO,
		PI_DESCRICAO,
		PI_TIPO,
		PI_GRUPO_RESULTADO,
		PI_ACUMULA_ESTATISTIC_COMPRA,
		PI_DIGITACOES_VALIDAS,
		PI_CONTROLE_ANVISA,
		PI_GERA_TITULO,
		PI_HABILITA_CALCULO_IPI,	
		PI_TIPO_CLASSIFICACAO_FISCAL,
		PI_CFOP_CORRESP,
		PI_CFOP_INVERSA,
		PI_CFOP_ST
	);
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  
  
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
     UPDATE DBAMDATA.T013_NATUREZA_OPERACAO
     SET
		T013_DESCRICAO                 = PI_DESCRICAO,
		T013_TIPO                      = PI_TIPO,
		T013_GRUPO_RESULTADO_E         = PI_GRUPO_RESULTADO,
		T013_ACUMULA_ESTATISTIC_COMPRA = PI_ACUMULA_ESTATISTIC_COMPRA,
		T013_GERA_TITULO               = PI_GERA_TITULO,
		T013_HABILITA_CALCULO_IPI      = PI_HABILITA_CALCULO_IPI,
		T013_TIPO_CLASSIFICACAO_FISCAL = PI_TIPO_CLASSIFICACAO_FISCAL,
		T013_CFOP_CORRESP_E            = PI_CFOP_CORRESP,
		T013_CFOP_INVERSA_IE           = PI_CFOP_INVERSA, 
		T013_CFOP_ST                   = PI_CFOP_ST
       WHERE T013_NATUREZA_OPERACAO_IU   = PI_NATUREZA_OPERACAO;
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE FROM DBAMDATA.T013_NATUREZA_OPERACAO 
            WHERE T013_NATUREZA_OPERACAO_IU   = PI_NATUREZA_OPERACAO;
            
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
    
    
    --CONFIRMAR OPERCAO
    
    COMMIT;
    
    
EXCEPTION
	WHEN OTHERS THEN
		ROLLBACK;
		ecode := SQLCODE;
		emesg := SUBSTR(SQLERRM,1, 2048);
		STATUS     := 'ER';
END;