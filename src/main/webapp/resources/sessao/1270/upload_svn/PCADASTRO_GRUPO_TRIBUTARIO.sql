/*******************************************************************************
SCHEMA    : I_MDLOG
TABELA    : T279_TIPO_NATUREZA
PROCEDURE : PCADASTRO_GRUPO_TRIBUTARIO
AUTOR     : TASSIANO ALENCAR
DATA      : 25/04/2017
CRC       : 70542
ALTERACAO : NOME - 00/00/0000 - CRC99999
DESCRICAO DA ALTERACAO.
*******************************************************************************/
CREATE OR REPLACE PROCEDURE DBAMDATA.PCADASTRO_GRUPO_TRIBUTARIO (
  PI_OPERACAO   VARCHAR2,
  PI_CODIGO     NUMBER,
  PI_DESCRICAO  VARCHAR2,
  PO_STATUS OUT VARCHAR2,
  PO_MSG    OUT VARCHAR2
)
IS
  ecode NUMBER(10);
  emesg VARCHAR2(2000);

BEGIN
  
  PO_STATUS := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT INTO T279_TIPO_NATUREZA
        (T279_TIPO_NATUREZA_IU, T279_DESCRICAO)
      VALUES
        (DBAMDATA.S001_T279_TIPO_NATUREZA.NEXTVAL, PI_DESCRICAO);
      EXCEPTION
      WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      PO_STATUS := 'ER';
      PO_MSG := ecode ||'-'|| emesg;
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  
  /******************************BEGIN - EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
      UPDATE T279_TIPO_NATUREZA
      SET
        T279_DESCRICAO = PI_DESCRICAO
      WHERE
        T279_TIPO_NATUREZA_IU = PI_CODIGO;
      EXCEPTION
      WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      PO_STATUS := 'ER';
      PO_MSG := ecode ||'-'|| emesg;
    END;
  END IF;
  /********************************END - EDITAR********************************/

  /******************************BEGIN - EXCLUIR*******************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE FROM T279_TIPO_NATUREZA
      WHERE
         T279_TIPO_NATUREZA_IU = PI_CODIGO;
      EXCEPTION
      WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      PO_STATUS := 'ER';
      PO_MSG := ecode ||'-'|| emesg;
    END;
  END IF;
  /********************************END - EXCLUIR*******************************/

END;