CREATE OR REPLACE PROCEDURE I_MDLOG.P_GERA_PEDIDO_OC_ESPECIAL
                                                    (PI_TIPO_PEDIDO            NUMBER,
                                                     PI_FORNECEDOR             NUMBER,
                                                     PI_UNIDADE_CENTRALIZADORA NUMBER,
                                                     PI_TIPO_FRETE             NUMBER,
                                                     PI_TRANSPORTADORA         NUMBER,
                                                     PI_OBSERVACAO             VARCHAR2,
													 PO_PEDIDOS_GERADOS        OUT VARCHAR2
													 )
/*******************************************************************************
  NOME      : P_GERA_PEDIDO_RA
  DESCRICAO : Procedure para gera��o do Pedido atrav�s do RA do MDLOGWeb
  ANALISTA  : H�lio Sales

              PI_TIPO_PEDIDO : 1 - Direto Loja
                               2 - Pedido �nico
                               3 - Cross-Docking Fiscal - NEGR�O UTILIZA APENAS ESSA OP��O DE CROSS DOCKING
                               4 - Cross-Docking F�sico 

*******************************************************************************/
IS
--123456798012345679801234567980
  nSUGESTAO                      NUMBER;
  poPedidosGerados               VARCHAR2(1000);
  vUnidadeDoPedido               NUMBER;
  
  vUsuarioOrigem                 I_MDLOG.T007_USUARIO_ORIGEM.T007_USUARIO_ORIGEM_IU%TYPE;
  
  vM03_016_PRECO_SUGESTAO        I_MDLOG.M03_016_OC_ESPECIAL.M03_016_PRECO_SUGESTAO%TYPE;
  vM03_016_REPOSICAO_UND         I_MDLOG.M03_016_OC_ESPECIAL.M03_016_REPOSICAO_UND%TYPE;
  vM03_016_DESCONTO_ITEM         I_MDLOG.M03_016_OC_ESPECIAL.M03_016_DESCONTO_ITEM%TYPE;
  vM03_016_DESCONTO_REPASSE      I_MDLOG.M03_016_OC_ESPECIAL.M03_016_DESCONTO_REPASSE%TYPE;
  vM03_016_DESCONTO_COMERCIAL    I_MDLOG.M03_016_OC_ESPECIAL.M03_016_DESCONTO_COMERCIAL%TYPE;
  vM03_016_EMBALAGEM_COMPRA      I_MDLOG.M03_016_OC_ESPECIAL.M03_016_EMBALAGEM_COMPRA%TYPE;
  vM03_016_TAXA_VENDOR           I_MDLOG.M03_016_OC_ESPECIAL.M03_016_TAXA_VENDOR%TYPE;
  vM03_016_PERC_ARRED_EMB_PROD   I_MDLOG.M03_016_OC_ESPECIAL.M03_016_PERC_ARRED_EMB_PROD%TYPE;
  vPRODUTO_NOVO                  VARCHAR2(1);
  vNovaQtde                      NUMBER;
  vDataAtual                     DATE;  
  
  FUNCTION F_CALC_QTDE_EMB(P_QDTE_ORIGINAL NUMBER,
                           P_EMB_FORNEC    NUMBER,
                           P_PERC_ARRED    NUMBER)
  RETURN NUMBER
  IS
    vQtdeRet  NUMBER;
    
    vQtdeEmbTrunc  NUMBER;
    vQtdeEmb       NUMBER;
    vFracao        NUMBER;
    vFatorEmb      NUMBER;
  BEGIN
    vQtdeEmb      := (P_QDTE_ORIGINAL / P_EMB_FORNEC);
    vQtdeEmbTrunc := trunc(P_QDTE_ORIGINAL / P_EMB_FORNEC);    
    
    if (vQtdeEmb = vQtdeEmbTrunc) THEN
      
      vQtdeRet := vQtdeEmb;
      
    ELSE
      
      vFracao := trunc((vQtdeEmb - vQtdeEmbTrunc) * 100);
      
      if (vFracao > P_PERC_ARRED) THEN
        vFatorEmb := 1;
      ELSE
        vFatorEmb := 0;
      END IF;
      
      vQtdeRet := (vQtdeEmbTrunc + vFatorEmb) * P_EMB_FORNEC;        
    END IF;   
    
    return vQtdeRet;      
  END;
  
  PROCEDURE P_ATUAL_TIPO_FRETE_TRANS_OBSER
  IS
  BEGIN
  
    FOR vPedidos IN (
                     SELECT RCOM_PEDIDO, RCOM_LOJA FROM RCOM_PEDIDOS_GERADOS
                    )
    LOOP

      UPDATE T108_PEDIDOS_PEND
      SET T108_TRANSPORTADORA_E = CASE WHEN PI_TRANSPORTADORA = 0 THEN NULL ELSE PI_TRANSPORTADORA END, 
          T108_TIPO_FRETE       = CASE WHEN PI_TIPO_FRETE = 1 THEN 'F' ELSE 'C' END,
          T108_OBSERVACAO       = PI_OBSERVACAO
      WHERE T108_UNIDADE_IE       = vPedidos.RCOM_LOJA
        AND T108_NUMERO_PEDIDO_IU = vPedidos.RCOM_PEDIDO;     

    END LOOP;  

  END;
  
  PROCEDURE P_GERA_PRAZOS_PEDIDOS
  IS
  BEGIN
  
    FOR vPedidos IN (
                     SELECT RCOM_PEDIDO, RCOM_LOJA FROM RCOM_PEDIDOS_GERADOS
                    )
    LOOP
      I_MDLOG.P_GRAVA_PREVISOES_PED_RA(vPedidos.RCOM_LOJA,                                       
                                       vPedidos.RCOM_PEDIDO); 
									   
	  PO_PEDIDOS_GERADOS := PO_PEDIDOS_GERADOS || vPedidos.RCOM_LOJA ||','||vPedidos.RCOM_PEDIDO ||';'; 							   
									   
    END LOOP;  

  END;     

BEGIN

	PO_PEDIDOS_GERADOS := '';

  SELECT MAX(M03_016_DATA_SUG_IE)
    INTO vDataAtual  
  FROM I_MDLOG.M03_016_OC_ESPECIAL
  WHERE M03_016_REPOSICAO_UND > 0
    AND M03_016_FORNECEDOR_IE = PI_FORNECEDOR;
  
  --//DIRETO LOJA/CD  OU   CROSS DOCIKING FISCAL
  IF PI_TIPO_PEDIDO IN (1, 3) THEN
    
    FOR vReg IN (SELECT DISTINCT M03_016_UNIDADE_IE, M03_016_FORNECEDOR_IE, M03_016_DATA_SUG_IE, T007_USUARIO_ORIGEM_IU
                 FROM I_MDLOG.M03_016_OC_ESPECIAL,
                      I_MDLOG.T007_USUARIO_ORIGEM
                 WHERE M03_016_COMPRADOR_IE  = T007_USUARIO_IE
                   and M03_016_FORNECEDOR_IE = PI_FORNECEDOR
                   AND M03_016_REPOSICAO_UND > 0
                   AND M03_016_DATA_SUG_IE = vDataAtual
                 )
    LOOP
      if PI_TIPO_PEDIDO = 1 then--//DIRETO LOJA ASSUME A LOJA COMO A UNIDADE DO PEDIDO
        vUnidadeDoPedido := vReg.M03_016_UNIDADE_IE;
      else--//CROSS DOCIKING ASSUME A UNIDADE CENTRALIZDARA COMO A UNIDADE DO PEDIDO
        vUnidadeDoPedido := PI_UNIDADE_CENTRALIZADORA;
      end if;

      nSUGESTAO := FSIS_INCREMENTACAMPO_rv('T900_PARAM_NUMFISC',
                                           'T900_ULT_SUGESTAO_COMPRA',
                                           'WHERE T900_UNIDADE_IE = ' || TO_CHAR(vUnidadeDoPedido));

      --Inserir registro de sugest�o
      INSERT INTO T190_SUGESTAO_COMPRA (
         T190_UNIDADE_IE,          
         T190_SUGESTAO_COMPRA_IU,  
         T190_DATA_SUGESTAO,      
         T190_USUARIO_E,
         T190_TIPOPENDFOREC,       
         T190_TIPOSUGESTAO,
         T190_TIPO_GERACAO) 
       VALUES (
         vUnidadeDoPedido,             
         nSUGESTAO,     
         TRUNC(SYSDATE), 
         vReg.T007_USUARIO_ORIGEM_IU,
         'S',                      
         0,                      
         'C');
      
      FOR vRegItens IN (SELECT M03_016_PRODUTO_IE, 
                               M03_016_PRECO_SUGESTAO, 
                               M03_016_REPOSICAO_UND,
                               M03_016_DESCONTO_ITEM, 
                               M03_016_DESCONTO_REPASSE, 
                               M03_016_DESCONTO_COMERCIAL,
                               M03_016_EMBALAGEM_COMPRA,
                               CASE WHEN M03_016_PRODUTO_NOVO = 1 THEN 'S' ELSE 'N' END PRODUTO_NOVO,
                               M03_016_TAXA_VENDOR,
                               M03_016_PERC_ARRED_EMB_PROD
                        FROM I_MDLOG.M03_016_OC_ESPECIAL
                        WHERE M03_016_REPOSICAO_UND > 0
                          and M03_016_FORNECEDOR_IE = PI_FORNECEDOR
                          AND M03_016_UNIDADE_IE    = vReg.M03_016_UNIDADE_IE
                          AND M03_016_DATA_SUG_IE = vDataAtual)
      LOOP
      
        
        INSERT INTO T191_SUGESTAO_COMPRA_ITEM(
             T191_UNIDADE_SUGESTAO_IE, 
             T191_SUGESTAO_COMPRA_IE,   
             T191_UNIDADE_FATURA_IE,    
             T191_PRODUTO_IE,
             T191_SALDO_ESTOQUE,        
             T191_SUGESTAO_ORIGINAL,    
             T191_MEDIA_VENDASDIA,
             T191_PENDENCIA,           
             T191_SUGESTAO_CXFECHADA,   
             T191_SUGESTAO_UNITARIA,    
             T191_DIAS_ESTOQUE,        
             T191_FORNECEDOR_E,         
             T191_CURVA_PRODUTO,        
             T191_ESTQ_PAD_FACING_SUGESTAO,
             T191_PRECO_UNITARIO,      
             T191_DESCONTO_ITEM,        
             T191_SUGESTAO_PALETT,
             T191_PERC_EMBALAGEM,
             T191_PERC_DESPACESS,
             T191_ITEM_SELECIONADO,
             T191_EMB_PADRAO  ) 
          VALUES(
             vUnidadeDoPedido,             
             nSUGESTAO,                 
             vUnidadeDoPedido,        
             vRegItens.M03_016_PRODUTO_IE,
             NULL, --VN_SALDO_ESTOQUE,          
             vRegItens.M03_016_REPOSICAO_UND,      
             NULL,--VN_T077_MEDIA_VENDADIA,
             NULL,--VN_PENDENCIA_FORNECEDOR,  
             NULL,--VN_SUGESTAO_CAIXA,         
             vRegItens.M03_016_REPOSICAO_UND,        
             NULL,--VN_DIASESTOQUE,           
             vReg.M03_016_FORNECEDOR_IE,             
             NULL,--VV_T079_CURVAABC_UNIDADE,  
             NULL,--vnEstoqueFacingSugestao,
             vRegItens.M03_016_PRECO_SUGESTAO,         
             vRegItens.M03_016_DESCONTO_ITEM,                         
             NULL,
             0,
             vRegItens.M03_016_TAXA_VENDOR,
             'S',
             vRegItens.M03_016_EMBALAGEM_COMPRA);

      END LOOP;
      
      I_MDLOG.P_GRAVA_PEDIDO_RA_ERP(vUnidadeDoPedido,
                                    nSUGESTAO,
                                    null,
                                    poPedidosGerados); 
          
                              
      IF PI_TIPO_PEDIDO = 3 THEN
        /*
        IF vUnidadeDoPedido <> PI_UNIDADE_CENTRALIZADORA THEN
          
          FOR vPedidos IN (
                 SELECT RCOM_PEDIDO PEDIDO FROM RCOM_PEDIDOS_GERADOS
                )
          LOOP
            UPDATE T108_PEDIDOS_PEND
            SET T108_FORMA_ENTREGA = 'C'
            WHERE T108_UNIDADE_IE       = vUnidadeDoPedido
              AND T108_NUMERO_PEDIDO_IU = vPedidos.PEDIDO;

            INSERT INTO T139_PEDIDOSPEN_CROSSDOCK_LOJA
            (T139_UNIDADE_IE, T139_NUMERO_PEDIDO_IE, T139_PRODUTO_IE, T139_UNIDADE_DESTINO_IE, T139_QTDE)
            VALUES
            (vUnidadeDoPedido, vPedidos.PEDIDO, );

            INSERT INTO T668_PEDIDOS_PEND_UNID_CRDOCK
            T139_PEDIDOSPEN_CROSSDOCK_LOJA
            (T668_UNIDADE_IE, T668_NUMERO_PEDIDO_IE, T668_UNID_CROSSDOCKING)
            values
            (vUnidadeDoPedido, vPedidos.PEDIDO, vReg.M03_016_UNIDADE_IE);

          END LOOP; 

        END IF;
        */
        null;

      END IF;

      if poPedidosGerados = 'S' then
        P_ATUAL_TIPO_FRETE_TRANS_OBSER;
        P_GERA_PRAZOS_PEDIDOS;
      end if;                                                  

    END LOOP;

  --//PEDIDO UNICO 
  ELSIF PI_TIPO_PEDIDO = 2 THEN
  
    nSUGESTAO := FSIS_INCREMENTACAMPO_RV('T900_PARAM_NUMFISC',
                                         'T900_ULT_SUGESTAO_COMPRA',
                                         'WHERE T900_UNIDADE_IE = ' || TO_CHAR(PI_UNIDADE_CENTRALIZADORA));
                                        
     SELECT DISTINCT T007_USUARIO_ORIGEM_IU 
     into vUsuarioOrigem       
     FROM I_MDLOG.M03_016_OC_ESPECIAL,
          I_MDLOG.T007_USUARIO_ORIGEM     
     WHERE M03_016_COMPRADOR_IE  = T007_USUARIO_IE
       and M03_016_FORNECEDOR_IE = PI_FORNECEDOR
       and M03_016_UNIDADE_IE    = PI_UNIDADE_CENTRALIZADORA
       AND M03_016_DATA_SUG_IE   = vDataAtual
       and rownum <= 1;  
    
    --Inserir registro de sugest�o
    INSERT INTO T190_SUGESTAO_COMPRA (
       T190_UNIDADE_IE,          
       T190_SUGESTAO_COMPRA_IU,  
       T190_DATA_SUGESTAO,      
       T190_USUARIO_E,
       T190_TIPOPENDFOREC,       
       T190_TIPOSUGESTAO,
       T190_TIPO_GERACAO) 
     VALUES (
       PI_UNIDADE_CENTRALIZADORA,             
       nSUGESTAO,     
       TRUNC(SYSDATE), 
       vUsuarioOrigem,
       'S',                      
       0,   
       'C');
         
    FOR vRegItens IN (SELECT M03_016_PRODUTO_IE,
                             SUM(M03_016_REPOSICAO_UND) QTDE_REPOSICAO
                      FROM I_MDLOG.M03_016_OC_ESPECIAL
                      WHERE M03_016_REPOSICAO_UND > 0
                        AND M03_016_FORNECEDOR_IE = PI_FORNECEDOR
                        AND M03_016_DATA_SUG_IE = vDataAtual
                      GROUP BY M03_016_PRODUTO_IE)
    LOOP
    
      begin
        --//PARA CADA PRODUTO, BUSCAR AS INFORMA��ES DO PRODUTO NA UNIDADE CENTRALIZADORA
        SELECT distinct M03_016_PRECO_SUGESTAO, 
               M03_016_REPOSICAO_UND,
               M03_016_DESCONTO_ITEM, 
               M03_016_DESCONTO_REPASSE, 
               M03_016_DESCONTO_COMERCIAL,
               M03_016_EMBALAGEM_COMPRA,
               CASE WHEN M03_016_PRODUTO_NOVO = 1 THEN 'S' ELSE 'N' END PRODUTO_NOVO,
               M03_016_TAXA_VENDOR,
               M03_016_PERC_ARRED_EMB_PROD
          INTO vM03_016_PRECO_SUGESTAO, 
               vM03_016_REPOSICAO_UND,
               vM03_016_DESCONTO_ITEM, 
               vM03_016_DESCONTO_REPASSE, 
               vM03_016_DESCONTO_COMERCIAL,
               vM03_016_EMBALAGEM_COMPRA,
               vPRODUTO_NOVO,
               vM03_016_TAXA_VENDOR,
               vM03_016_PERC_ARRED_EMB_PROD         
        FROM I_MDLOG.M03_016_OC_ESPECIAL
        WHERE M03_016_REPOSICAO_UND > 0
          AND M03_016_UNIDADE_IE    = PI_UNIDADE_CENTRALIZADORA 
          AND M03_016_FORNECEDOR_IE = PI_FORNECEDOR
          AND M03_016_PRODUTO_IE    = vRegItens.M03_016_PRODUTO_IE
          and rownum <= 1;
      exception 
        when others then
        begin
          --//PARA CADA PRODUTO, BUSCAR AS INFORMA��ES DO PRODUTO NA UNIDADE CENTRALIZADORA
          SELECT distinct M03_016_PRECO_SUGESTAO, 
                 M03_016_REPOSICAO_UND,
                 M03_016_DESCONTO_ITEM, 
                 M03_016_DESCONTO_REPASSE, 
                 M03_016_DESCONTO_COMERCIAL,
                 M03_016_EMBALAGEM_COMPRA,
                 CASE WHEN M03_016_PRODUTO_NOVO = 1 THEN 'S' ELSE 'N' END PRODUTO_NOVO,
                 M03_016_TAXA_VENDOR,
                 M03_016_PERC_ARRED_EMB_PROD
            INTO vM03_016_PRECO_SUGESTAO, 
                 vM03_016_REPOSICAO_UND,
                 vM03_016_DESCONTO_ITEM, 
                 vM03_016_DESCONTO_REPASSE, 
                 vM03_016_DESCONTO_COMERCIAL,
                 vM03_016_EMBALAGEM_COMPRA,
                 vPRODUTO_NOVO,
                 vM03_016_TAXA_VENDOR,
                 vM03_016_PERC_ARRED_EMB_PROD         
          FROM I_MDLOG.M03_016_OC_ESPECIAL
          WHERE M03_016_REPOSICAO_UND > 0
            AND M03_016_FORNECEDOR_IE = PI_FORNECEDOR
            AND M03_016_PRODUTO_IE    = vRegItens.M03_016_PRODUTO_IE
            AND M03_016_DATA_SUG_IE = vDataAtual
            and rownum <= 1;
        end;
      end;
                
      vNovaQtde := F_CALC_QTDE_EMB(vRegItens.QTDE_REPOSICAO,vM03_016_EMBALAGEM_COMPRA,vM03_016_PERC_ARRED_EMB_PROD);      
      

      INSERT INTO T191_SUGESTAO_COMPRA_ITEM(
             T191_UNIDADE_SUGESTAO_IE, 
             T191_SUGESTAO_COMPRA_IE,   
             T191_UNIDADE_FATURA_IE,    
             T191_PRODUTO_IE,  
             T191_SALDO_ESTOQUE,        
             T191_SUGESTAO_ORIGINAL,    
             T191_MEDIA_VENDASDIA,
             T191_PENDENCIA,           
             T191_SUGESTAO_CXFECHADA,   
             T191_SUGESTAO_UNITARIA,    
             T191_DIAS_ESTOQUE,
             T191_FORNECEDOR_E,
             T191_CURVA_PRODUTO,
             T191_PRECO_UNITARIO,      
             T191_DESCONTO_ITEM,        
             T191_SUGESTAO_PALETT,
             T191_PERC_EMBALAGEM,
             T191_PERC_DESPACESS,
             T191_ITEM_SELECIONADO,
             T191_EMB_PADRAO  ) 
          VALUES(
             PI_UNIDADE_CENTRALIZADORA ,             
             nSUGESTAO,                 
             PI_UNIDADE_CENTRALIZADORA ,        
             vRegItens.M03_016_PRODUTO_IE,
             NULL, --VN_SALDO_ESTOQUE,          
             vNovaQtde,      
             NULL,--VN_T077_MEDIA_VENDADIA,
             NULL,--VN_PENDENCIA_FORNECEDOR,  
             NULL,--VN_SUGESTAO_CAIXA,         
             vNovaQtde,        
             NULL,--VN_DIASESTOQUE,      
             PI_FORNECEDOR,
             NULL,--VV_T079_CURVAABC_UNIDADE,
             vM03_016_PRECO_SUGESTAO,
             vM03_016_DESCONTO_ITEM,
             NULL,
             0,
             vM03_016_TAXA_VENDOR,
             'S',                          
             vM03_016_EMBALAGEM_COMPRA);

    END LOOP;
    
    I_MDLOG.P_GRAVA_PEDIDO_RA_ERP(PI_UNIDADE_CENTRALIZADORA,
                                  nSUGESTAO,
                                  NULL,
                                  poPedidosGerados);
                                  
    P_ATUAL_TIPO_FRETE_TRANS_OBSER;
     
    P_GERA_PRAZOS_PEDIDOS;                                 
    
  END IF;
  
  
  COMMIT;
  
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('C�digo Oracle: ' || SQLCODE);
    DBMS_OUTPUT.PUT_LINE('Mensagem Oracle: ' || SQLERRM);
    RAISE_APPLICATION_ERROR(-20000, TO_CHAR(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE),TRUE);   
END;
/