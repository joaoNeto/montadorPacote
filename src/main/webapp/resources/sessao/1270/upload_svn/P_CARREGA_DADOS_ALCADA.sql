CREATE OR REPLACE PROCEDURE I_MDLOG.P_CARREGA_DADOS_ALCADA(PI_USUARIO NUMBER)
IS
--123456789012345678901234567890

  vUsuarioOrigem                 NUMBER;
  vM04_001_NSE_FORNECEDOR        M04_001_ALCADA_PEDIDOS.M04_001_NSE_FORNECEDOR%type;
  vM04_001_SEMAFORO_NSE_FORNEC   M04_001_ALCADA_PEDIDOS.M04_001_SEMAFORO_NSE_FORNEC%type;
  vM04_001_NSE_COMPRADOR         M04_001_ALCADA_PEDIDOS.M04_001_NSE_COMPRADOR%type;
  vM04_001_SEMAFORO_NSE_COMP     M04_001_ALCADA_PEDIDOS.M04_001_SEMAFORO_NSE_COMP%type;
  
  vM04_001_QTDE_CRV_A_RA         M04_001_ALCADA_PEDIDOS.M04_001_QTDE_CRV_A_RA%type;      
  vM04_001_QTDE_CRV_A_ALTERADO   M04_001_ALCADA_PEDIDOS.M04_001_QTDE_CRV_A_ALTERADO%type;
  vM04_001_QTDE_CRV_A_PERC       M04_001_ALCADA_PEDIDOS.M04_001_QTDE_CRV_A_PERC%type;    
  vM04_001_QTDE_CRV_B_RA         M04_001_ALCADA_PEDIDOS.M04_001_QTDE_CRV_B_RA%type;      
  vM04_001_QTDE_CRV_B_ALTERADO   M04_001_ALCADA_PEDIDOS.M04_001_QTDE_CRV_B_ALTERADO%type;
  vM04_001_QTDE_CRV_B_PERC       M04_001_ALCADA_PEDIDOS.M04_001_QTDE_CRV_B_PERC%type;    
  vM04_001_QTDE_CRV_C_RA         M04_001_ALCADA_PEDIDOS.M04_001_QTDE_CRV_C_RA%type;      
  vM04_001_QTDE_CRV_C_ALTERADO   M04_001_ALCADA_PEDIDOS.M04_001_QTDE_CRV_C_ALTERADO%type;
  vM04_001_QTDE_CRV_C_PERC       M04_001_ALCADA_PEDIDOS.M04_001_QTDE_CRV_C_PERC%type;
  
  vM04_001_PRC_CRV_A_RA          M04_001_ALCADA_PEDIDOS.M04_001_PRC_CRV_A_RA%type;      
  vM04_001_PRC_CRV_A_ALTERADO    M04_001_ALCADA_PEDIDOS.M04_001_PRC_CRV_A_ALTERADO%type;
  vM04_001_PRC_CRV_A_PERC        M04_001_ALCADA_PEDIDOS.M04_001_PRC_CRV_A_PERC%type;    
  vM04_001_PRC_CRV_B_RA          M04_001_ALCADA_PEDIDOS.M04_001_PRC_CRV_B_RA%type;      
  vM04_001_PRC_CRV_B_ALTERADO    M04_001_ALCADA_PEDIDOS.M04_001_PRC_CRV_B_ALTERADO%type;
  vM04_001_PRC_CRV_B_PERC        M04_001_ALCADA_PEDIDOS.M04_001_PRC_CRV_B_PERC%type;    
  vM04_001_PRC_CRV_C_RA          M04_001_ALCADA_PEDIDOS.M04_001_PRC_CRV_C_RA%type;      
  vM04_001_PRC_CRV_C_ALTERADO    M04_001_ALCADA_PEDIDOS.M04_001_PRC_CRV_C_ALTERADO%type;
  vM04_001_PRC_CRV_C_PERC        M04_001_ALCADA_PEDIDOS.M04_001_PRC_CRV_C_PERC%type;  
       
  
  vM04_001_QTDE_PRODNOVO_RA      M04_001_ALCADA_PEDIDOS.M04_001_QTDE_PRODNOVO_RA%type;      
  vM04_001_QTDE_PRODNOVO_ALTERAD M04_001_ALCADA_PEDIDOS.M04_001_QTDE_PRODNOVO_ALTERADO%type;
  vM04_001_QTDE_PRODNOVO_PERC    M04_001_ALCADA_PEDIDOS.M04_001_QTDE_PRODNOVO_PERC%type;
  
  vM04_001_PRC_PRODNOVO_RA      M04_001_ALCADA_PEDIDOS.M04_001_PRC_PRODNOVO_RA%type;      
  vM04_001_PRC_PRODNOVO_ALTERAD M04_001_ALCADA_PEDIDOS.M04_001_PRC_PRODNOVO_ALTERADO%type;
  vM04_001_PRC_PRODNOVO_PERC    M04_001_ALCADA_PEDIDOS.M04_001_PRC_PRODNOVO_PERC%type;
  
  
  vM04_001_RESUMO_TOTAL_RA       M04_001_ALCADA_PEDIDOS.M04_001_RESUMO_TOTAL_ALTERADO%type; 
  vM04_001_RESUMO_TOTAL_ALTERADO M04_001_ALCADA_PEDIDOS.M04_001_RESUMO_TOTAL_ALTERADO%type;     
  vM04_001_RESUMO_TOTAL_PERC     M04_001_ALCADA_PEDIDOS.M04_001_RESUMO_TOTAL_PERC%type;
  vM04_001_RESUMO_PRAZO_ALTERADO M04_001_ALCADA_PEDIDOS.M04_001_RESUMO_PRAZO_ALTERADO%type;
  
  
  vM04_001_RESUMO_DESC_RA         M04_001_ALCADA_PEDIDOS.M04_001_RESUMO_DESC_RA%type;
  vM04_001_RESUMO_DESC_ALTERADO   M04_001_ALCADA_PEDIDOS.M04_001_RESUMO_DESC_ALTERADO%type;
  vM04_001_RESUMO_DESC_PERC       M04_001_ALCADA_PEDIDOS.M04_001_RESUMO_DESC_PERC%type;
  
  vM04_001_SEMAFORO_PEDIDO        M04_001_ALCADA_PEDIDOS.M04_001_SEMAFORO_PEDIDO%type;
  
  vM04_001_QTDE_ITENS             M04_001_ALCADA_PEDIDOS.M04_001_QTDE_ITENS%type;
  
  vM04_001_GIRO                   M04_001_ALCADA_PEDIDOS.M04_001_GIRO%type;
  vM04_001_MARGEM                 M04_001_ALCADA_PEDIDOS.M04_001_MARGEM%type;
  vM04_001_INCIDENTES             M04_001_ALCADA_PEDIDOS.M04_001_INCIDENTES%type;
  
  
  CURSOR C_QTDE_POR_CURVA(PUNIDADE NUMBER, PPEDIDO NUMBER, PCURVA CHAR) IS  
    SELECT nvl(SUM(nvl(TCOM_QTDE_MAXIMA,0)),0) QTDE_RA,
           nvl(SUM(nvl(TCOM_QTDE_PEDIDA,0)),0) QTDE_ALTERADA,           
           nvl(SUM(nvl(TCOM_QTDE_PEDIDA,0) * nvl(TCOM_PRECO_LISTA,nvl(TCOM_PRECO_MAXIMO,0))),0)  PRECO_RA,
           nvl(SUM(nvl(TCOM_QTDE_PEDIDA,0) * nvl(TCOM_PRECO_COMPRA,0)),0) PRECO_ALTERADO
    FROM TCOM_INSPECAO_PED_COMPRA_ITENS                
    WHERE TCOM_UNIDADE       = PUNIDADE                       
      AND TCOM_NUMERO_PEDIDO = PPEDIDO
      --AND TCOM_PRODUTO_BLOQUEADO = 'S'                        
      AND SUBSTR(TCOM_CURVA_ABC_VALOR,1,1) = PCURVA        
    GROUP BY TCOM_UNIDADE,                             
             TCOM_NUMERO_PEDIDO;
             
  CURSOR C_QTDE_PROD_NOVO(PUNIDADE NUMBER, PPEDIDO NUMBER) IS  
    SELECT nvl(SUM(nvl(TCOM_QTDE_MAXIMA,0)),0) QTDE_RA,
           nvl(SUM(nvl(TCOM_QTDE_PEDIDA,0)),0) QTDE_ALTERADA,
           nvl(SUM(nvl(TCOM_QTDE_PEDIDA,0) * nvl(TCOM_PRECO_LISTA,nvl(TCOM_PRECO_MAXIMO,0))),0)  PRECO_RA,
           nvl(SUM(nvl(TCOM_QTDE_PEDIDA,0) * nvl(TCOM_PRECO_COMPRA,0)),0) PRECO_ALTERADO           
    FROM TCOM_INSPECAO_PED_COMPRA_ITENS
    WHERE TCOM_UNIDADE       = PUNIDADE                       
      AND TCOM_NUMERO_PEDIDO = PPEDIDO    
      --AND TCOM_PRODUTO_BLOQUEADO = 'S'                    
      AND NVL(TCOM_MEDIA_VENDADIA,0) = 0        
    GROUP BY TCOM_UNIDADE,                             
             TCOM_NUMERO_PEDIDO;             
  
  
  FUNCTION RETORNA_SEMAFORO( PI_INDICADOR  NUMBER,
                             PI_NSE        NUMBER) RETURN NUMBER
  IS                             
    vRet NUMBER;
  BEGIN                           
    
    SELECT CASE WHEN PI_NSE < M02_001_CRITICO THEN 3
                WHEN PI_NSE BETWEEN M02_001_ACEITAVEL AND M02_001_NO_PRAZO THEN 2
                ELSE 1 
           END
      INTO vRet                           
    FROM I_MDLOG.M02_001_CONF_IND
    WHERE M02_001_INDICADOR = PI_INDICADOR;
    
    RETURN nvl(vRet,0);    
  END;
  
  
  FUNCTION RETORNA_PRAZO(PI_PRAZO  VARCHAR2) RETURN NUMBER
  IS  
    vAuxNum number;
    vAuxVar VARCHAR2(5);
    RETORNO NUMBER;    
    CARACT  CHAR(1);
    CONT    NUMBER;    
  BEGIN
    
    BEGIN
       vAuxNum := TO_NUMBER(PI_PRAZO);
       RETORNO := PI_PRAZO;
    EXCEPTION
      WHEN VALUE_ERROR THEN
       
      CARACT := '';
      CONT := 0;
      
      LOOP      
         
        CARACT := SUBSTR(PI_PRAZO,LENGTH(PI_PRAZO) - CONT,1);
        
        EXIT WHEN CARACT = '/';
         
        vAuxVar := CARACT || NVL(vAuxVar,'');
        
        CONT := CONT + 1;
        
      END LOOP;
      
      RETORNO := vAuxVar;
       
    END;
    
    RETURN RETORNO;
  
  END;
                                                        
  
BEGIN
  
  DELETE FROM I_MDLOG.M04_001_ALCADA_PEDIDOS
  WHERE M04_001_USUARIO = PI_USUARIO;
  COMMIT;

  --//OBTEM O USUÁRIO DE ORIGEM CORRESPONDENTE NO MDLOG    
  SELECT
    T007_USUARIO_ORIGEM_IU
  INTO vUsuarioOrigem
  FROM I_MDLOG.T007_USUARIO_ORIGEM
  WHERE T007_USUARIO_IE = PI_USUARIO;
  
  --//CARREGA OS DADOS NA TABELA TEMPORÁRIA
  INFOMLOG.PCOM_GESTAO_PEDIDOS_BLOQUEADOS(vUsuarioOrigem);

  --//CARREGAR A TABELA DO I_MDLOG...  
  FOR vReg in ( SELECT TCOM_UNIDADE,           
                      TCOM_NUMERO_PEDIDO,      
                      TCOM_FORNECEDOR,         
                      TCOM_RAZAO_SOCIAL,       
                      TCOM_COMPRADOR,          
                      TCOM_NOME_COMPRADOR,     
                      TCOM_MENSAGEM_BLOQUEIO,  
                      TCOM_TOTAL_PEDIDO,
                      TCOM_PRAZOS_PAGAMENTO,
                      TCOM_TIPO_BLOQUEIO
                FROM TCOM_INSPECAO_PEDIDO_COMPRA
                )
  LOOP
    
    vM04_001_QTDE_CRV_A_RA       := 0;       
    vM04_001_QTDE_CRV_A_ALTERADO := 0;  
    vM04_001_QTDE_CRV_A_PERC     := 0;  
    vM04_001_QTDE_CRV_B_RA       := 0; 
    vM04_001_QTDE_CRV_B_ALTERADO := 0; 
    vM04_001_QTDE_CRV_B_PERC     := 0; 
    vM04_001_QTDE_CRV_C_RA       := 0; 
    vM04_001_QTDE_CRV_C_ALTERADO := 0; 
    vM04_001_QTDE_CRV_C_PERC     := 0; 
    
    vM04_001_PRC_CRV_A_RA        := 0; 
    vM04_001_PRC_CRV_A_ALTERADO  := 0; 
    vM04_001_PRC_CRV_A_PERC      := 0; 
    vM04_001_PRC_CRV_B_RA        := 0; 
    vM04_001_PRC_CRV_B_ALTERADO  := 0; 
    vM04_001_PRC_CRV_B_PERC      := 0; 
    vM04_001_PRC_CRV_C_RA        := 0; 
    vM04_001_PRC_CRV_C_ALTERADO  := 0; 
    vM04_001_PRC_CRV_C_PERC      := 0;
    
    vM04_001_QTDE_PRODNOVO_RA     := 0;
    vM04_001_QTDE_PRODNOVO_ALTERAD:= 0;
    vM04_001_QTDE_PRODNOVO_PERC   := 0;
    
    vM04_001_PRC_PRODNOVO_RA      := 0;
    vM04_001_PRC_PRODNOVO_ALTERAD := 0;
    vM04_001_PRC_PRODNOVO_PERC    := 0;
  
    --//NSE FORNECEDOR
    BEGIN
      SELECT
        ROUND 
        (
          (
          SUM(NVL(T016_TOTAL_DE_PRODUTO_CURVA_A,0) + NVL(T016_TOTAL_DE_PRODUTO_CURVA_B,0) + NVL(T016_TOTAL_DE_PRODUTO_CURVA_C,0) + NVL(T016_TOTAL_DE_PRODUTO_CURVA_D,0))
          -
          SUM(NVL(T016_TOTAL_PROD_S_EST_CURVA_A,0) + NVL(T016_TOTAL_PROD_S_EST_CURVA_B,0) + NVL(T016_TOTAL_PROD_S_EST_CURVA_C,0) + NVL(T016_TOTAL_PROD_S_EST_CURVA_D,0))
          )
          / SUM(NVL(T016_TOTAL_DE_PRODUTO_CURVA_A,0) + NVL(T016_TOTAL_DE_PRODUTO_CURVA_B,0) + NVL(T016_TOTAL_DE_PRODUTO_CURVA_C,0) + NVL(T016_TOTAL_DE_PRODUTO_CURVA_D,0))
        ,2) * 100
        INTO vM04_001_NSE_FORNECEDOR
      FROM I_MDLOG.T016_FORN_ESTATISTICA
      WHERE T016_FORNECEDOR_IE = vreg.TCOM_FORNECEDOR;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN 
        vM04_001_NSE_FORNECEDOR := 0;
    END;              
    
    --SEMAFORO NSE FORNECEDOR
    vM04_001_SEMAFORO_NSE_FORNEC := RETORNA_SEMAFORO(13,vM04_001_NSE_FORNECEDOR);
    
    BEGIN
      SELECT T509_NSE
        INTO vM04_001_NSE_COMPRADOR
      FROM I_MDLOG.T509_NSE_COMPRADOR
      WHERE T509_COMPRADOR = vreg.TCOM_COMPRADOR;      
    EXCEPTION
      WHEN NO_DATA_FOUND THEN 
        vM04_001_NSE_COMPRADOR := 0;
    END;
    
    --SEMAFORO NSE FORNECEDOR
    vM04_001_SEMAFORO_NSE_COMP := RETORNA_SEMAFORO(13,vM04_001_NSE_COMPRADOR);
    
    BEGIN
      OPEN C_QTDE_POR_CURVA(vReg.TCOM_UNIDADE, vReg.TCOM_NUMERO_PEDIDO, 'A'); 
      FETCH C_QTDE_POR_CURVA INTO vM04_001_QTDE_CRV_A_RA, vM04_001_QTDE_CRV_A_ALTERADO,
                                  vM04_001_PRC_CRV_A_RA, vM04_001_PRC_CRV_A_ALTERADO;  
      CLOSE C_QTDE_POR_CURVA;
      
      IF vM04_001_QTDE_CRV_A_RA > 0 THEN
        vM04_001_QTDE_CRV_A_PERC := ((vM04_001_QTDE_CRV_A_ALTERADO - vM04_001_QTDE_CRV_A_RA) / vM04_001_QTDE_CRV_A_RA) * 100;
      ELSE
        
        IF vM04_001_QTDE_CRV_A_ALTERADO > 0 THEN
          vM04_001_QTDE_CRV_A_PERC := 100;
        ELSE
          vM04_001_QTDE_CRV_A_PERC := 0;
        END IF;
                  
      END IF;
      
      IF vM04_001_PRC_CRV_A_ALTERADO > 0 THEN
        vM04_001_PRC_CRV_A_PERC := ((vM04_001_PRC_CRV_A_ALTERADO - vM04_001_PRC_CRV_A_RA) / vM04_001_PRC_CRV_A_ALTERADO) * 100;
      ELSE
      
        IF vM04_001_PRC_CRV_A_RA > 0 THEN
          vM04_001_PRC_CRV_A_PERC  := 100;
        ELSE
          vM04_001_PRC_CRV_A_PERC  := 0;
        END IF;
      END IF;
      
      
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        vM04_001_QTDE_CRV_A_RA       := 0;
        vM04_001_QTDE_CRV_A_ALTERADO := 0;
        vM04_001_QTDE_CRV_A_PERC     := 0; 
    END;
    
    BEGIN
      OPEN C_QTDE_POR_CURVA(vReg.TCOM_UNIDADE, vReg.TCOM_NUMERO_PEDIDO, 'B'); 
      FETCH C_QTDE_POR_CURVA INTO vM04_001_QTDE_CRV_B_RA, vM04_001_QTDE_CRV_B_ALTERADO,
                                  vM04_001_PRC_CRV_B_RA, vM04_001_PRC_CRV_B_ALTERADO;  
      CLOSE C_QTDE_POR_CURVA;
      
      IF vM04_001_QTDE_CRV_B_RA > 0 THEN
        vM04_001_QTDE_CRV_B_PERC := ((vM04_001_QTDE_CRV_B_ALTERADO - vM04_001_QTDE_CRV_B_RA) / vM04_001_QTDE_CRV_B_RA) * 100;
      ELSE
        IF vM04_001_QTDE_CRV_B_ALTERADO > 0 THEN
          vM04_001_QTDE_CRV_B_PERC := 100;
        ELSE
          vM04_001_QTDE_CRV_B_PERC := 0;
        END IF;
      END IF;
      
      IF vM04_001_PRC_CRV_B_ALTERADO > 0 THEN
        vM04_001_PRC_CRV_B_PERC := ((vM04_001_PRC_CRV_B_ALTERADO - vM04_001_PRC_CRV_B_RA) / vM04_001_PRC_CRV_B_RA) * 100;
      ELSE
        IF vM04_001_PRC_CRV_B_RA > 0 THEN
          vM04_001_PRC_CRV_B_PERC  := 100;
        ELSE
          vM04_001_PRC_CRV_B_PERC  := 0;
        END IF;
      END IF;
      
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        vM04_001_QTDE_CRV_B_RA       := 0;
        vM04_001_QTDE_CRV_B_ALTERADO := 0;
        vM04_001_QTDE_CRV_B_PERC     := 0; 
    END;
    
    BEGIN
      OPEN C_QTDE_POR_CURVA(vReg.TCOM_UNIDADE, vReg.TCOM_NUMERO_PEDIDO, 'C'); 
      FETCH C_QTDE_POR_CURVA INTO vM04_001_QTDE_CRV_C_RA, vM04_001_QTDE_CRV_C_ALTERADO,
                                  vM04_001_PRC_CRV_C_RA, vM04_001_PRC_CRV_C_ALTERADO;
      CLOSE C_QTDE_POR_CURVA;
      
      IF vM04_001_QTDE_CRV_C_RA > 0 THEN
        vM04_001_QTDE_CRV_C_PERC := ((vM04_001_QTDE_CRV_C_ALTERADO - vM04_001_QTDE_CRV_C_RA) / vM04_001_QTDE_CRV_C_RA) * 100;        
      ELSE
        IF vM04_001_QTDE_CRV_C_ALTERADO > 0 THEN
          vM04_001_QTDE_CRV_C_PERC := 100;
        ELSE
          vM04_001_QTDE_CRV_C_PERC := 0;
        END IF;
      END IF;
      
      IF vM04_001_PRC_CRV_C_ALTERADO > 0 THEN
        vM04_001_PRC_CRV_C_PERC := ((vM04_001_PRC_CRV_C_ALTERADO - vM04_001_PRC_CRV_C_RA) / vM04_001_PRC_CRV_C_RA) * 100;
      ELSE
        IF vM04_001_PRC_CRV_C_RA > 0 THEN
          vM04_001_PRC_CRV_C_PERC  := 100;
        ELSE
          vM04_001_PRC_CRV_C_PERC  := 0;
        END IF;
      END IF;      
      
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        vM04_001_QTDE_CRV_A_RA       := 0;
        vM04_001_QTDE_CRV_A_ALTERADO := 0;
        vM04_001_QTDE_CRV_A_PERC     := 0; 
    END;        

    BEGIN

      OPEN C_QTDE_PROD_NOVO(vReg.TCOM_UNIDADE, vReg.TCOM_NUMERO_PEDIDO); 
      FETCH C_QTDE_PROD_NOVO INTO vM04_001_QTDE_PRODNOVO_RA, vM04_001_QTDE_PRODNOVO_ALTERAD,
                                  vM04_001_PRC_PRODNOVO_RA, vM04_001_PRC_PRODNOVO_ALTERAD;
      CLOSE C_QTDE_PROD_NOVO;
      
      IF vM04_001_QTDE_PRODNOVO_RA > 0 THEN
        vM04_001_QTDE_PRODNOVO_PERC := ((vM04_001_QTDE_PRODNOVO_ALTERAD - vM04_001_QTDE_PRODNOVO_RA) / vM04_001_QTDE_PRODNOVO_RA) * 100;
      ELSE 
        IF vM04_001_QTDE_PRODNOVO_ALTERAD > 0 THEN
          vM04_001_QTDE_PRODNOVO_PERC := 100;
        ELSE
          vM04_001_QTDE_PRODNOVO_PERC := 0;
        END IF;
      END IF;
         
      IF vM04_001_PRC_PRODNOVO_ALTERAD > 0 THEN
        vM04_001_PRC_PRODNOVO_PERC := ((vM04_001_PRC_PRODNOVO_ALTERAD - vM04_001_PRC_PRODNOVO_RA) / vM04_001_PRC_PRODNOVO_RA) * 100;
      ELSE        
        IF vM04_001_PRC_PRODNOVO_RA > 0 THEN
          vM04_001_PRC_PRODNOVO_PERC  := 100;
        ELSE
          vM04_001_PRC_PRODNOVO_PERC  := 0;
        END IF;
        
      END IF;
      

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        vM04_001_QTDE_PRODNOVO_RA      := 0;
        vM04_001_QTDE_PRODNOVO_ALTERAD := 0;
        vM04_001_QTDE_PRODNOVO_PERC    := 0; 
    END;
    
    SELECT SUM(nvl(TCOM_QTDE_PEDIDA,0) * nvl(TCOM_PRECO_LISTA,nvl(TCOM_PRECO_MAXIMO,0)))
      into vM04_001_RESUMO_TOTAL_RA
    FROM TCOM_INSPECAO_PED_COMPRA_ITENS                
    WHERE TCOM_UNIDADE       = vReg.TCOM_UNIDADE                       
      AND TCOM_NUMERO_PEDIDO = vReg.TCOM_NUMERO_PEDIDO;
    
    if vM04_001_RESUMO_TOTAL_RA > 0 then
      vM04_001_RESUMO_TOTAL_PERC := ((vReg.TCOM_TOTAL_PEDIDO - vM04_001_RESUMO_TOTAL_RA) / vM04_001_RESUMO_TOTAL_RA) * 100; 
      
      --//vM04_001_RESUMO_TOTAL_PERC := round(1 - (vReg.TCOM_TOTAL_PEDIDO/vM04_001_RESUMO_TOTAL_RA),2) * 100;
    else
      vM04_001_RESUMO_TOTAL_PERC := vM04_001_RESUMO_TOTAL_RA;
    end if;
    
    SELECT count(*)
    INTO vM04_001_QTDE_ITENS
    FROM TCOM_INSPECAO_PED_COMPRA_ITENS                
    WHERE TCOM_UNIDADE       = vReg.TCOM_UNIDADE                            
      AND TCOM_NUMERO_PEDIDO = vReg.TCOM_NUMERO_PEDIDO;
    
    
    vM04_001_RESUMO_PRAZO_ALTERADO := RETORNA_PRAZO(vreg.TCOM_PRAZOS_PAGAMENTO);
    
    vM04_001_RESUMO_PRAZO_ALTERADO := nvl(vM04_001_RESUMO_PRAZO_ALTERADO,30);
    
    vM04_001_RESUMO_DESC_RA       := trunc(dbms_random.value(3,10));      
    vM04_001_RESUMO_DESC_ALTERADO := trunc(dbms_random.value(3,10));
    --//vM04_001_RESUMO_DESC_PERC     := trunc(dbms_random.value(3,10));
    
    if vM04_001_RESUMO_DESC_RA > 0 then
      vM04_001_RESUMO_DESC_PERC := ((vM04_001_RESUMO_DESC_ALTERADO - vM04_001_RESUMO_DESC_RA) / vM04_001_RESUMO_DESC_RA) * 100; 
    else
      vM04_001_RESUMO_DESC_PERC := vM04_001_RESUMO_DESC_RA;
    end if;
     
    vM04_001_SEMAFORO_PEDIDO := trunc(dbms_random.value(1,3.5));
    
    begin
      select T016_GIRO, T016_MARGEM, T016_INCIDENTE
        into vM04_001_GIRO, vM04_001_MARGEM, vM04_001_INCIDENTES 
      from T016_FORN_ESTATISTICA
      where T016_UNIDADE_IE    = vReg.TCOM_UNIDADE
        and T016_FORNECEDOR_IE = vReg.TCOM_FORNECEDOR;
    exception
      when others then
        vM04_001_GIRO   := 0;
        vM04_001_MARGEM := 0;
    end;    

    SELECT CASE   SUM (NVL (t016_total_de_produto_curva_a, 0))
              + SUM (NVL (t016_total_de_produto_curva_b, 0))
              + SUM (NVL (t016_total_de_produto_curva_c, 0))
              + SUM (NVL (t016_total_de_produto_curva_d, 0))
            WHEN 0
               THEN 0
            ELSE TRUNC ((  (  SUM (NVL (t016_total_de_produto_curva_a, 0))
                            + SUM (NVL (t016_total_de_produto_curva_b, 0))
                            + SUM (NVL (t016_total_de_produto_curva_c, 0))
                            + SUM (NVL (t016_total_de_produto_curva_d, 0))
                            - (  SUM (NVL (t016_total_prod_s_est_curva_a, 0))
                               + SUM (NVL (t016_total_prod_s_est_curva_b, 0))
                               + SUM (NVL (t016_total_prod_s_est_curva_c, 0))
                               + SUM (NVL (t016_total_prod_s_est_curva_d, 0))
                              )
                           )
                         / (  SUM (NVL (t016_total_de_produto_curva_a, 0))
                            + SUM (NVL (t016_total_de_produto_curva_b, 0))
                            + SUM (NVL (t016_total_de_produto_curva_c, 0))
                            + SUM (NVL (t016_total_de_produto_curva_d, 0))
                           )
                         * 100
                        )
                       )
         END t016_nse,
         SUM (NVL (t016_giro, 0)) AS t016_giro,
         SUM (NVL (t016_margem, 0)) AS t016_margem,
         SUM (NVL (t016_incidente, 0)) AS t016_incidente
     into vM04_001_NSE_FORNECEDOR,
          vM04_001_GIRO, 
          vM04_001_MARGEM, 
          vM04_001_INCIDENTES
    FROM i_mdlog.t016_forn_estatistica
    WHERE t016_fornecedor_ie = vReg.TCOM_FORNECEDOR;    
    
    
    /*
    SELECT COUNT(*)
      INTO vM04_001_INCIDENTES
    FROM T1043_NOTIFIC_EMAIL_RECEBIM
    WHERE T1043_FORNECEDOR_E = vReg.TCOM_FORNECEDOR;
    */    
    
      
    BEGIN
      INSERT INTO I_MDLOG.M04_001_ALCADA_PEDIDOS
      (
        M04_001_UNIDADE_PEDIDO, 
        M04_001_PEDIDO, 
        M04_001_USUARIO,
        M04_001_FORNECEDOR, 
        M04_001_NOME_FORNECEDOR, 
        M04_001_COMPRADOR, 
        M04_001_NOME_COMPRADOR,
        M04_001_MSG_BLOQUEIO,
        M04_001_NSE_FORNECEDOR, 
        M04_001_SEMAFORO_NSE_FORNEC, 
        M04_001_NSE_COMPRADOR, 
        M04_001_SEMAFORO_NSE_COMP,
        M04_001_RESUMO_TOTAL_ALTERADO,
        M04_001_RESUMO_TOTAL_RA,
        M04_001_RESUMO_TOTAL_PERC,
        M04_001_QTDE_CRV_A_RA,          
        M04_001_QTDE_CRV_A_ALTERADO,    
        M04_001_QTDE_CRV_A_PERC,            
        M04_001_QTDE_CRV_B_RA,          
        M04_001_QTDE_CRV_B_ALTERADO,    
        M04_001_QTDE_CRV_B_PERC,        
        M04_001_QTDE_CRV_C_RA,          
        M04_001_QTDE_CRV_C_ALTERADO,    
        M04_001_QTDE_CRV_C_PERC,
        M04_001_QTDE_PRODNOVO_RA,     
        M04_001_QTDE_PRODNOVO_ALTERADO,
        M04_001_QTDE_PRODNOVO_PERC,
        M04_001_PRC_CRV_A_RA,          
        M04_001_PRC_CRV_A_ALTERADO,    
        M04_001_PRC_CRV_A_PERC,            
        M04_001_PRC_CRV_B_RA,          
        M04_001_PRC_CRV_B_ALTERADO,    
        M04_001_PRC_CRV_B_PERC,        
        M04_001_PRC_CRV_C_RA,          
        M04_001_PRC_CRV_C_ALTERADO,    
        M04_001_PRC_CRV_C_PERC,
        M04_001_PRC_PRODNOVO_RA,     
        M04_001_PRC_PRODNOVO_ALTERADO,
        M04_001_PRC_PRODNOVO_PERC,
        M04_001_RESUMO_PRAZO_ALTERADO,
        M04_001_RESUMO_PRAZO_RA,
        M04_001_RESUMO_PRAZO_PERC,
        M04_001_RESUMO_DESC_RA, 
        M04_001_RESUMO_DESC_ALTERADO, 
        M04_001_RESUMO_DESC_PERC,
        M04_001_TIPO_BLOQUEIO,
        M04_001_SEMAFORO_PEDIDO,
        M04_001_QTDE_ITENS,
        M04_001_INCIDENTES,
        M04_001_GIRO,      
        M04_001_MARGEM    
        
      )
      VALUES
      (
        vReg.TCOM_UNIDADE,           
        vReg.TCOM_NUMERO_PEDIDO,
        PI_USUARIO,      
        vReg.TCOM_FORNECEDOR,         
        vReg.TCOM_RAZAO_SOCIAL,       
        vReg.TCOM_COMPRADOR,          
        vReg.TCOM_NOME_COMPRADOR,     
        vReg.TCOM_MENSAGEM_BLOQUEIO,
        vM04_001_NSE_FORNECEDOR,
        vM04_001_SEMAFORO_NSE_FORNEC,
        vM04_001_NSE_COMPRADOR,     
        vM04_001_SEMAFORO_NSE_COMP,      
        vReg.TCOM_TOTAL_PEDIDO,
        vM04_001_RESUMO_TOTAL_RA,
        vM04_001_RESUMO_TOTAL_PERC,
        vM04_001_QTDE_CRV_A_RA,          
        vM04_001_QTDE_CRV_A_ALTERADO,    
        vM04_001_QTDE_CRV_A_PERC,            
        vM04_001_QTDE_CRV_B_RA,          
        vM04_001_QTDE_CRV_B_ALTERADO,    
        vM04_001_QTDE_CRV_B_PERC,        
        vM04_001_QTDE_CRV_C_RA,          
        vM04_001_QTDE_CRV_C_ALTERADO,    
        vM04_001_QTDE_CRV_C_PERC,
        vM04_001_QTDE_PRODNOVO_RA,     
        vM04_001_QTDE_PRODNOVO_ALTERAD,
        vM04_001_QTDE_PRODNOVO_PERC,
        vM04_001_PRC_CRV_A_RA,          
        vM04_001_PRC_CRV_A_ALTERADO,    
        vM04_001_PRC_CRV_A_PERC,            
        vM04_001_PRC_CRV_B_RA,          
        vM04_001_PRC_CRV_B_ALTERADO,    
        vM04_001_PRC_CRV_B_PERC,        
        vM04_001_PRC_CRV_C_RA,          
        vM04_001_PRC_CRV_C_ALTERADO,    
        vM04_001_PRC_CRV_C_PERC,
        vM04_001_PRC_PRODNOVO_RA,     
        vM04_001_PRC_PRODNOVO_ALTERAD,
        vM04_001_PRC_PRODNOVO_PERC,
        nvl(vM04_001_RESUMO_PRAZO_ALTERADO,0),
        nvl(vM04_001_RESUMO_PRAZO_ALTERADO,0),
        0,
        vM04_001_RESUMO_DESC_RA,      
        vM04_001_RESUMO_DESC_ALTERADO,
        vM04_001_RESUMO_DESC_PERC,    
        vReg.TCOM_TIPO_BLOQUEIO,
        vM04_001_SEMAFORO_PEDIDO,
        vM04_001_QTDE_ITENS,
        vM04_001_INCIDENTES,
        vM04_001_GIRO,      
        vM04_001_MARGEM 
         
      );
    EXCEPTION
      WHEN DUP_VAL_ON_INDEX THEN
        UPDATE I_MDLOG.M04_001_ALCADA_PEDIDOS
        SET M04_001_USUARIO               = PI_USUARIO,      
            M04_001_FORNECEDOR            = vReg.TCOM_FORNECEDOR,          
            M04_001_NOME_FORNECEDOR       = vReg.TCOM_RAZAO_SOCIAL,        
            M04_001_COMPRADOR             = vReg.TCOM_COMPRADOR,          
            M04_001_NOME_COMPRADOR        = vReg.TCOM_NOME_COMPRADOR,     
            M04_001_MSG_BLOQUEIO          = vReg.TCOM_MENSAGEM_BLOQUEIO,
            M04_001_NSE_FORNECEDOR        = vM04_001_NSE_FORNECEDOR,
            M04_001_SEMAFORO_NSE_FORNEC   = vM04_001_SEMAFORO_NSE_FORNEC,
            M04_001_NSE_COMPRADOR         = vM04_001_NSE_COMPRADOR,     
            M04_001_SEMAFORO_NSE_COMP     = vM04_001_SEMAFORO_NSE_COMP,      
            M04_001_RESUMO_TOTAL_ALTERADO = vReg.TCOM_TOTAL_PEDIDO,
            M04_001_RESUMO_TOTAL_RA       = vM04_001_RESUMO_TOTAL_RA,
            M04_001_RESUMO_TOTAL_PERC     = vM04_001_RESUMO_TOTAL_PERC,
            M04_001_QTDE_CRV_A_RA         = vM04_001_QTDE_CRV_A_RA,          
            M04_001_QTDE_CRV_A_ALTERADO   = vM04_001_QTDE_CRV_A_ALTERADO,    
            M04_001_QTDE_CRV_A_PERC       = vM04_001_QTDE_CRV_A_PERC,            
            M04_001_QTDE_CRV_B_RA         = vM04_001_QTDE_CRV_B_RA,          
            M04_001_QTDE_CRV_B_ALTERADO   = vM04_001_QTDE_CRV_B_ALTERADO,    
            M04_001_QTDE_CRV_B_PERC       = vM04_001_QTDE_CRV_B_PERC,        
            M04_001_QTDE_CRV_C_RA         = vM04_001_QTDE_CRV_C_RA,          
            M04_001_QTDE_CRV_C_ALTERADO   = vM04_001_QTDE_CRV_C_ALTERADO,    
            M04_001_QTDE_CRV_C_PERC       = vM04_001_QTDE_CRV_C_PERC,
            M04_001_QTDE_PRODNOVO_RA      = vM04_001_QTDE_PRODNOVO_RA,     
            M04_001_QTDE_PRODNOVO_ALTERADO= vM04_001_QTDE_PRODNOVO_ALTERAD,
            M04_001_QTDE_PRODNOVO_PERC    = vM04_001_QTDE_PRODNOVO_PERC,
            M04_001_PRC_CRV_A_RA          = vM04_001_PRC_CRV_A_RA,          
            M04_001_PRC_CRV_A_ALTERADO    = vM04_001_PRC_CRV_A_ALTERADO,    
            M04_001_PRC_CRV_A_PERC        = vM04_001_PRC_CRV_A_PERC,             
            M04_001_PRC_CRV_B_RA          = vM04_001_PRC_CRV_B_RA,          
            M04_001_PRC_CRV_B_ALTERADO    = vM04_001_PRC_CRV_B_ALTERADO,    
            M04_001_PRC_CRV_B_PERC        = vM04_001_PRC_CRV_B_PERC,        
            M04_001_PRC_CRV_C_RA          = vM04_001_PRC_CRV_C_RA,          
            M04_001_PRC_CRV_C_ALTERADO    = vM04_001_PRC_CRV_C_ALTERADO,    
            M04_001_PRC_CRV_C_PERC        = vM04_001_PRC_CRV_C_PERC,
            M04_001_PRC_PRODNOVO_RA       = vM04_001_PRC_PRODNOVO_RA,     
            M04_001_PRC_PRODNOVO_ALTERADO = vM04_001_PRC_PRODNOVO_ALTERAD,
            M04_001_PRC_PRODNOVO_PERC     = vM04_001_PRC_PRODNOVO_PERC,
            M04_001_RESUMO_PRAZO_ALTERADO = nvl(vM04_001_RESUMO_PRAZO_ALTERADO,0),
            M04_001_RESUMO_PRAZO_RA       = nvl(vM04_001_RESUMO_PRAZO_ALTERADO,0),
            M04_001_RESUMO_PRAZO_PERC     = 0,             
            M04_001_RESUMO_DESC_RA        = vM04_001_RESUMO_DESC_RA,      
            M04_001_RESUMO_DESC_ALTERADO  = vM04_001_RESUMO_DESC_ALTERADO,              
            M04_001_RESUMO_DESC_PERC      = vM04_001_RESUMO_DESC_PERC,    
            M04_001_TIPO_BLOQUEIO         = vReg.TCOM_TIPO_BLOQUEIO,
            M04_001_SEMAFORO_PEDIDO       = vM04_001_SEMAFORO_PEDIDO,
            M04_001_QTDE_ITENS            = vM04_001_QTDE_ITENS,
            M04_001_INCIDENTES            = vM04_001_INCIDENTES,
            M04_001_GIRO                  = vM04_001_GIRO  ,
            M04_001_MARGEM                = vM04_001_MARGEM
        WHERE M04_001_UNIDADE_PEDIDO = vReg.TCOM_UNIDADE     
          AND M04_001_PEDIDO         = vReg.TCOM_NUMERO_PEDIDO;
    END;
  
  END LOOP;
  
  COMMIT;  

END;
/
