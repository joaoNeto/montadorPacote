---Schema    : I_MDLOG
---Analista  : PEDRO FELIPE
---Data      : 26/12/2017
---CRC       : 73309
---Descricao : reversão da criação da coluna M03_002_ESTOCADO

ALTER TABLE I_MDLOG.M03_002_PARAMETROS_UNIDADES DROP(M03_002_ESTOCADO);
