CREATE OR REPLACE PROCEDURE I_MDLOG.PCADASTRO_TIPO_BONIFICACAO(PI_TIPO_BONIFICACAO  NUMBER,
															   PI_DESCRICAO VARCHAR2,
															   PI_OPERACAO   VARCHAR2,
															   STATUS OUT VARCHAR2
															   ) IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  
  
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE :	PCADASTRO_TIPO_BONIFICACAO
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DO TIPO DE BONIFICACAO	.
  AUTOR     : Luis Eduardo Pinheiro Pedrosa
  DATA      : 214/11/2016
  CRC       : 
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN	

STATUS     := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
  
    BEGIN
      INSERT INTO DBAMDATA.T049_TIPO_BONIFICACAO(T049_TIPO_BONIFICACAO_IU,T049_DESCRICAO)
             VALUES (PI_TIPO_BONIFICACAO,PI_DESCRICAO);
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  
  
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
  
    BEGIN
      UPDATE DBAMDATA.T049_TIPO_BONIFICACAO 
         SET T049_DESCRICAO             = PI_DESCRICAO
       WHERE T049_TIPO_BONIFICACAO_IU   = PI_TIPO_BONIFICACAO;
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
  
    BEGIN
      DELETE FROM DBAMDATA.T049_TIPO_BONIFICACAO 
            WHERE T049_TIPO_BONIFICACAO_IU   = PI_TIPO_BONIFICACAO;
            
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
    
    
    --CONFIRMAR OPERCAO
    
    COMMIT;
    
    
EXCEPTION
	WHEN OTHERS THEN
		ROLLBACK;
		ecode := SQLCODE;
		emesg := SUBSTR(SQLERRM,1, 2048);
		STATUS     := 'ER';
END;