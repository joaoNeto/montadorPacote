CREATE OR REPLACE PROCEDURE I_MDLOG.PCADASTRO_GRUPO_FOR(PI_CODGRUPO_FORNECEDOR   VARCHAR2,
													   PI_DESCRICAO  VARCHAR2,
													   PI_OPERACAO   VARCHAR2,
													   STATUS OUT VARCHAR2
													   ) IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  
  
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE :	PCADASTRO_GRUPO_FOR
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DO GRUPO DE FORNECEDOR	.
  AUTOR     : Luis Eduardo Pinheiro Pedrosa
  DATA      : 21/11/2016
  CRC       : 
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN	

STATUS     := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
  
    BEGIN
      INSERT INTO DBAMDATA.T182_GRUPO_FORNECEDOR(T182_GRUPO_FORNECEDOR_IU,T182_DESCRICAO)
             VALUES (PI_CODGRUPO_FORNECEDOR,PI_DESCRICAO);
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  
  
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
  
    BEGIN
      UPDATE DBAMDATA.T182_GRUPO_FORNECEDOR 
         SET T182_DESCRICAO = PI_DESCRICAO
       WHERE T182_GRUPO_FORNECEDOR_IU   = PI_CODGRUPO_FORNECEDOR;
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
  
    BEGIN
      DELETE FROM DBAMDATA.T182_GRUPO_FORNECEDOR 
            WHERE T182_GRUPO_FORNECEDOR_IU   = PI_CODGRUPO_FORNECEDOR;
            
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
    
    
    --CONFIRMAR OPERCAO
    
    COMMIT;
    
    
EXCEPTION
	WHEN OTHERS THEN
		ROLLBACK;
		ecode := SQLCODE;
		emesg := SUBSTR(SQLERRM,1, 2048);
		STATUS     := 'ER';
END;
/