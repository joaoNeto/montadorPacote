/******************************************************************************
Schema    : I_MDLOG
Analista  : ALBERTO MEDEIROS
Data      : 31/05/2016
CRC       : 68310
Descricao : IR�? RETORNAR OS DETALHES DE OS's
******************************************************************************/

CREATE OR REPLACE FORCE VIEW "I_MDLOG"."VIEW_DETALHE_OS_FORNECEDOR" ("COMPRADOR", "DATAPREVISTA_FATURA", "SEMAFORO", "NUMERO_PEDIDO", "QTDITENS", "QTDUNIDADES", "VALOR", "DATA_PEDIDO", "UNIDADE", "PRAZO", "DATA_ENTREGA", "FORMA_ENTREGA", "TIPO_PEDIDO", "STATUS_PEDIDO", "MAIORCURVA", "USUARIO", "FORNECEDOR") AS   
SELECT 
    PED.COMPRADOR,
    PED.DATAPREVISTA_FATURA,
    OS.M02_009_SEMAFORO SEMAFORO,
    PED.NUMERO_PEDIDO,
    PED.QTDITEM QTDITENS,
    QTDUNIDADES QTDUNIDADES,
    round(VALOR_LIQUIDO,2) VALOR,    
    PED.DATA_PEDIDO,
    PED.UNIDADE,
    PED.PRAZO,
    PED.DATA_ENTREGA,
    PED.FORMA_ENTREGA,  
    CASE PED.TIPO_PEDIDO  
              WHEN 1 THEN '1 - Normal'
              WHEN 2 THEN '2 - Especial'
              WHEN 3 THEN '3 - Bonificado'
              WHEN 4 THEN '4 - Reposi��o Manual'
              WHEN 5 THEN '5 - Reposi��o Autom�tica'
              WHEN 6 THEN '6 - EDI'
              WHEN 7 THEN '7 - Reposi��o PBM'
              WHEN 8 THEN '8 - Transfer�ncia'
              WHEN 9 THEN '9 - Devolu��o'
    END TIPO_PEDIDO,    
    CASE PED.STATUS_PEDIDO WHEN 1 THEN '1 - Liberado'
             WHEN 2 THEN '2 - Em An�lise'
             WHEN 3 THEN '3 - Em Recebimento'
             WHEN 4 THEN '4 - Cancelado'
             WHEN 5 THEN '5 - Recebido'
             WHEN 6 THEN '6 - Recebido Parcial'
    END STATUS_PEDIDO,  
    0 MAIORCURVA,
    OS.M02_009_USUARIO_IE,
    PED.FORNECEDOR    
FROM 
   I_MDLOG.VIEW_M02_PEDIDO_NAO_FATURADO PED, 
   I_MDLOG.M02_009_ORDEM_SERVICO OS 
WHERE
   OS.M02_009_ID_ORIGEM = PED.NUMERO_PEDIDO||','||PED.UNIDADE 
   AND OS.M02_009_STATUS = 1 
   AND OS.M02_009_ENTIDADE_IE = 12    
ORDER BY DATAPREVISTA_FATURA DESC;