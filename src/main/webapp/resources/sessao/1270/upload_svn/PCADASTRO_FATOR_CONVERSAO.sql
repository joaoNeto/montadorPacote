create or replace PROCEDURE         PCADASTRO_FATOR_CONVERSAO (
  PI_OPERACAO VARCHAR2,
  PI_UNIDADE_MEDIDA_ORIG VARCHAR2,
  PI_UNIDADE_MEDIDA_DEST VARCHAR2,
  PI_FATOR_PADRAO CHAR,
  PI_FATOR_CONVERSAO NUMBER,
  PO_STATUS OUT VARCHAR2,
  PO_MSG OUT VARCHAR2
)
IS
  ecode NUMBER(10);
  emesg VARCHAR2(2000);

BEGIN

  PO_STATUS := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT INTO T148_UNIDADE_MEDIDA_FATOR
        (T148_UNIDADE_MEDIDA_ORIG_IE, T148_UNIDADE_MEDIDA_DEST_IE, T148_FATOR_CONVERSAO, T148_FATOR_PADRAO)
      VALUES
        (PI_UNIDADE_MEDIDA_ORIG, PI_UNIDADE_MEDIDA_DEST, PI_FATOR_CONVERSAO, PI_FATOR_PADRAO);
      EXCEPTION
      WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      PO_STATUS := 'ER';
	  PO_MSG := ecode ||'-'|| emesg;
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/

  /******************************BEGIN - EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
      UPDATE T148_UNIDADE_MEDIDA_FATOR
      SET
        T148_FATOR_CONVERSAO = PI_FATOR_CONVERSAO,
        T148_FATOR_PADRAO = PI_FATOR_PADRAO
      WHERE
        T148_UNIDADE_MEDIDA_ORIG_IE = PI_UNIDADE_MEDIDA_ORIG
        AND
        T148_UNIDADE_MEDIDA_DEST_IE = PI_UNIDADE_MEDIDA_DEST;
      EXCEPTION
      WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      PO_STATUS := 'ER';
	  PO_MSG := ecode ||'-'|| emesg;
    END;
  END IF;
  /********************************END - EDITAR********************************/

  /******************************BEGIN - EXCLUIR*******************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE FROM T148_UNIDADE_MEDIDA_FATOR
      WHERE
        T148_UNIDADE_MEDIDA_ORIG_IE = PI_UNIDADE_MEDIDA_ORIG
        AND
        T148_UNIDADE_MEDIDA_DEST_IE = PI_UNIDADE_MEDIDA_DEST;
      EXCEPTION
      WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      PO_STATUS := 'ER';
	  PO_MSG := ecode ||'-'|| emesg;
    END;
  END IF;
  /********************************END - EXCLUIR*******************************/
END;