create or replace PROCEDURE I_MDLOG.PCADASTRO_SEG_CLIENTE(
											PI_GRUPOS_CLIENTES          NUMBER, 
											PI_DESCRICAO                VARCHAR2,
											PI_SEGMENTO_PRODUTOS        VARCHAR2,
											PI_COMPRA_TODAS_CATEGORIAS  CHAR, 
											PI_OPERACAO                 CHAR,
											STATUS OUT VARCHAR2
                                            ) IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_SEG_CLIENTE
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DO SEGMENTO DE CLIENTES.
  AUTOR     : Luis Eduardo Pinheiro Pedrosa
  DATA      : 16/11/2016
  CRC       : 70542
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN
	STATUS     := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT INTO DBAMDATA.T010_GRUPOS_CLIENTES(
		T010_GRUPOS_CLIENTES_IU
		T010_DESCRICAO, 
		T010_SEGMENTO_PRODUTOS, 
		T010_COMPRA_TODAS_CATEGORIAS
		)
	 VALUES (
		PI_GRUPOS_CLIENTES
		PI_DESCRICAO,
		PI_SEGMENTO_PRODUTOS,
		PI_COMPRA_TODAS_CATEGORIAS
	);
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  
  
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
     UPDATE DBAMDATA.T010_GRUPOS_CLIENTES
     SET
		T010_DESCRICAO                 = PI_DESCRICAO, 
		T010_SEGMENTO_PRODUTOS         = PI_SEGMENTO_PRODUTOS, 
		T010_COMPRA_TODAS_CATEGORIAS   = PI_COMPRA_TODAS_CATEGORIAS
       WHERE T010_GRUPOS_CLIENTES_IU   = PI_GRUPOS_CLIENTES;
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE FROM DBAMDATA.T010_GRUPOS_CLIENTES
            WHERE T010_GRUPOS_CLIENTES_IU   = PI_GRUPOS_CLIENTES;
            
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
    
    
    --CONFIRMAR OPERCAO
    
    COMMIT;
    
    
EXCEPTION
	WHEN OTHERS THEN
		ROLLBACK;
		ecode := SQLCODE;
		emesg := SUBSTR(SQLERRM,1, 2048);
		STATUS     := 'ER';
END;