create or replace PROCEDURE I_MDLOG.PREC_NUMERO_PRENOTA_PEDIDO(PI_UNIDADE NUMBER,
                                                               PI_PEDIDO NUMBER,
                                                               PO_PRENOTA OUT VARCHAR,
                                                               PO_STATUS  OUT BOOLEAN) IS

  --MSG DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  
  --VARIAVEIS DO INTO
  V_PRENOTA                DBAMDATA.T119_PRENOTA.T119_NUMERO_PRENOTA_IU%TYPE;
  
  
  --VARIAVEIS DE EXECPTION 
  ERRO_SELECT  EXCEPTION;   
  
  /*******************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PREC_NUMERO_PRENOTA_PEDIDO
  OBJETIVO  : ROTINA PARA RECUPERAR O NUMERO DA PRE NOTA A PARTIR DO PEDIDO
  AUTOR     : ALBERTO MEDEIROS
  DATA      : 27/11/2015
  CRC       : 66980
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *******************************************************************************/
  
BEGIN

  --RECUPERA DADOS
  BEGIN
  
    SELECT T119_NUMERO_PRENOTA_IU
      INTO  V_PRENOTA 
    FROM T119_PRENOTA 
    WHERE T119_UNIDADE_ARMAZEM = PI_PEDIDO
      AND T119_PEDIDO_ARMAZEM = PI_UNIDADE;
  
    PO_PRENOTA           := V_PRENOTA;
    PO_STATUS            := TRUE;

  EXCEPTION  
  WHEN OTHERS THEN
    RAISE ERRO_SELECT;
  END;
  
EXCEPTION 

  WHEN ERRO_SELECT THEN
    PO_PRENOTA           := V_PRENOTA;
    PO_STATUS            := FALSE;

  WHEN OTHERS THEN
    ecode      := SQLCODE;
    emesg      := SUBSTR(SQLERRM,1, 2048);
    PO_PRENOTA           := V_PRENOTA;
    PO_STATUS            := FALSE;

END;
/