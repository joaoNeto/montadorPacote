/*******************************************************************************
Schema    : I_MDLOG
Analista  : TIAGO MONTEZUMA
Data      : 09/08/2016
CRC       : 69543 
Descricao : LISTAR ORDEM DE COMPRA POR UNIDADE
*******************************************************************************/
CREATE OR REPLACE VIEW I_MDLOG.VIEW_M03_UNIDADE_OCS
(
   FORNECEDOR,
   UNIDADE,
   UNIDADE_NOME,
   DATA_SUG,
   SALDO_ESTOQUE_DISP,
   NSE,
   VALOR_TRANSF,
   VALOR_OC_PDT,
   VALOR_SUGESTAO,
   VALOR_REPOSICAO,
   VENDA_LIQUIDA_30,
   CMV_30,
   GIRO,
   QTD_ITENS,
   VALOR_CURVA_A,
   VALOR_CURVA_B,
   VALOR_CURVA_C,
   VALOR_CURVA_D,
   VALOR_ITENSNOVOS,
   VALOR_EXCESSO,
   VALOR_RUPTURA,
   VALOR_RESTRINCOES,
   ORDEM_SERVICO,
   TIPO_UNIDADE
)
AS
     SELECT M03_012_FORNECEDOR_IE FORNECEDOR,
            M03_012_UNIDADE_IE UNIDADE,
            M03_012_NOME_UNIDADE UNIDADE_NOME,
            MAX (M03_012_DATA_SUG_IE) DATA_SUG,
            SUM (M03_012_VALOR_ESTOQUE/*M03_012_SALDO_ESTOQUE_DISP*/) SALDO_ESTOQUE_DISP,
            MAX (
               (SELECT MAX (M03_010_NSE)
                  FROM I_MDLOG.M03_010_UNIDADE_SUGESTAO
                 WHERE     M03_010_DATA_SUG_IE = M03_012_DATA_SUG_IE
                       AND M03_010_UNIDADE_IE = M03_012_UNIDADE_IE))
               AS NSE,
            SUM (
                 NVL (M03_012_SALDO_EM_TRANSITO, 0)
               * NVL (M03_012_CUSTO_COMPRA, 0))
               VALOR_TRANSF,
              sum(M003_003_VALOR_PENDENCIA) AS VALOR_OC_PDT,
            
            
            sum(
              (M03_012_PRECO_SUGESTAO * M03_012_SUGESTAO_FINAL) +
              ((M03_012_PRECO_SUGESTAO * M03_012_SUGESTAO_FINAL) * (M03_012_ALIQ_IPI/100)) -
              ((M03_012_PRECO_SUGESTAO * M03_012_SUGESTAO_FINAL) * (M03_012_DESCONTO_ITEM/100))
            ) VALOR_SUGESTAO,
            --SUM (nvl(M03_012_REPOSICAO_FINAL,0)) 
            
            sum(
              (M03_012_PRECO_REPOSICAO * M03_012_REPOSICAO_UND) +
              ((M03_012_PRECO_REPOSICAO * M03_012_REPOSICAO_UND) * (M03_012_ALIQ_IPI/100)) -
              ((M03_012_PRECO_REPOSICAO * M03_012_REPOSICAO_UND) * (M03_012_DESCONTO_ITEM/100))
            ) VALOR_REPOSICAO,
            SUM (M03_012_VENDA_LIQUIDA_30) VENDA_LIQUIDA_30,
            SUM (M03_012_CMV_30) CMV_30,
              NVL (SUM (M03_012_CMV_30), 1)
            / NVL (NULLIF (SUM (M03_012_VALOR_ESTOQUE), 0), 1)
               GIRO,
            COUNT (DISTINCT M03_012_PRODUTO_IE) QTD_ITENS,
            SUM (
               CASE
                  WHEN SUBSTR (M03_012_CURVA, 1, 1) = 'A'
                  THEN
                     NVL (M03_012_VALOR_SUGESTAO_FINAL, 0)
                  ELSE
                     0
               END)
               VALOR_CURVA_A,
            SUM (
               CASE
                  WHEN SUBSTR (M03_012_CURVA, 1, 1) = 'B'
                  THEN
                     NVL (M03_012_VALOR_SUGESTAO_FINAL, 0)
                  ELSE
                     0
               END)
               VALOR_CURVA_B,
            SUM (
               CASE
                  WHEN SUBSTR (M03_012_CURVA, 1, 1) = 'C'
                  THEN
                     NVL (M03_012_VALOR_SUGESTAO_FINAL, 0)
                  ELSE
                     0
               END)
               VALOR_CURVA_C,
            SUM (
               CASE
                  WHEN SUBSTR (M03_012_CURVA, 1, 1) = 'D'
                  THEN
                     NVL (M03_012_VALOR_SUGESTAO_FINAL, 0)
                  ELSE
                     0
               END)
               VALOR_CURVA_D,
            SUM (
               CASE
                  WHEN NVL (M03_012_PRODUTO_NOVO, 0) = 1
                  THEN
                     NVL (M03_012_VALOR_SUGESTAO_FINAL, 0)
                  ELSE
                     0
               END)
               VALOR_ITENSNOVOS,
            SUM (
               CASE
                  WHEN NVL (M03_012_SALDO_ESTOQUE_DISP, 0) >
                          NVL (M03_012_QTD_FACING, 0)
                  THEN
                     NVL (M03_012_VALOR_SUGESTAO_FINAL, 0)
                  ELSE
                     0
               END)
               VALOR_EXCESSO,
            SUM (NVL (M03_012_QTDE_FALTA, 0) * NVL (M03_012_CUSTO_COMPRA, 0))
               VALOR_RUPTURA,
            SUM (
               NVL (M03_012_QTDE_BLOQUEIO, 0) * NVL (M03_012_CUSTO_COMPRA, 0))
               VALOR_RESTRINCOES,
               M03_012_ORDEM_SERVICO_IE ORDEM_SERVICO,
               T003_TIPO_UNIDADE TIPO_UNIDADE
       FROM I_MDLOG.M03_012_SUG_COMPRA_DIA A2
         JOIN I_MDLOG.M003_003_PRODUTO_SUGESTAO FORN ON
           FORN.M003_003_FORNECEDOR_IE  = A2.M03_012_FORNECEDOR_IE
           AND FORN.M003_003_UNIDADE_IE = A2.M03_012_UNIDADE_IE
           AND FORN.M003_003_PRODUTO_IE = A2.M03_012_PRODUTO_IE
           AND FORN.M003_003_DATA_RA_IE = M03_012_DATA_SUG_IE
         LEFT JOIN T202_LINHA_RESPONSAVEL ON
           T202_USUARIO_IE = FORN.M003_003_GRUPO_COMPRADOR
           AND T202_LINHA_IE  = FORN.M003_003_LINHA
         JOIN T003_UNIDADE ON
           T003_UNIDADE_IU = M03_012_UNIDADE_IE 		   
      WHERE M03_012_DATA_SUG_IE =
               (SELECT MAX (M03_012_DATA_SUG_IE)
                  FROM I_MDLOG.M03_012_SUG_COMPRA_DIA A1
                 WHERE A1.M03_012_FORNECEDOR_IE = A2.M03_012_FORNECEDOR_IE
                   AND A1.M03_012_UNIDADE_IE = A2.M03_012_UNIDADE_IE
                   AND A1.M03_012_ORDEM_SERVICO_IE = A2.M03_012_ORDEM_SERVICO_IE)
   GROUP BY M03_012_FORNECEDOR_IE, M03_012_UNIDADE_IE, M03_012_NOME_UNIDADE, M03_012_ORDEM_SERVICO_IE, T003_TIPO_UNIDADE;


GRANT SELECT ON I_MDLOG.VIEW_M03_UNIDADE_OCS TO INTEGRA;
