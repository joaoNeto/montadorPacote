CREATE OR REPLACE PROCEDURE I_MDLOG.PCADASTRO_IBGE(PI_CODIBGE    VARCHAR2,
                                                   PI_MUNICIPIO  VARCHAR2,
                                                   PI_UF         VARCHAR2,
                                                   PI_OPERACAO   VARCHAR2,
                                                   STATUS OUT VARCHAR2
                                                   ) IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  
  
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_IBGE
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DO IBGE.
  AUTOR     : JOSE GABRIEL
  DATA      : 01/11/2016
  CRC       : 
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN	

STATUS     := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
  
    BEGIN
      INSERT INTO DBAMDATA.T402_MUNICIPIO_IBGE(T402_IBGE_IU,T402_MUNICIPIO,T402_UF_E)
             VALUES (PI_CODIBGE,PI_MUNICIPIO,PI_UF);
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  
  
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
  
    BEGIN
      UPDATE DBAMDATA.T402_MUNICIPIO_IBGE 
         SET T402_MUNICIPIO = PI_MUNICIPIO,
             T402_UF_E        = PI_UF
       WHERE T402_IBGE_IU   = PI_CODIBGE;
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
  
    BEGIN
      DELETE FROM DBAMDATA.T402_MUNICIPIO_IBGE 
            WHERE T402_IBGE_IU   = PI_CODIBGE;
            
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
    
    
    --CONFIRMAR OPERCAO
    
    COMMIT;
    
    
EXCEPTION
	WHEN OTHERS THEN
		ROLLBACK;
		ecode := SQLCODE;
		emesg := SUBSTR(SQLERRM,1, 2048);
		STATUS     := 'ER';
END;
/