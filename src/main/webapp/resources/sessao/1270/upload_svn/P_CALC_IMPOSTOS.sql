CREATE OR REPLACE PROCEDURE I_MDLOG.P_CALC_IMPOSTOS(pi_nUnidade               IN  NUMBER,
                                                    pi_nFornecedor            IN  NUMBER,
                                                    pi_nProduto               IN  NUMBER,
                                                    pi_vGRUPO_TRIBUT_FORNEC   IN  VARCHAR2,
                                                    pi_vUfOrigem              IN  VARCHAR2,
                                                    pi_vUfDestino             IN  VARCHAR2,
                                                    pi_nQuantidade            IN  NUMBER,
                                                    pi_nPreco                 IN  NUMBER,
                                                    pi_nDescontoItem          IN  NUMBER,                                                   
                                                    --pi_nDespesasAcessorias    IN  NUMBER,
                                                    po_nValorIpi              OUT NUMBER,
                                                    po_nAliqIpi               OUT NUMBER,
                                                    po_nValor_IcmsSt          OUT NUMBER,
                                                    po_MVA_ST                 OUT NUMBER,
                                                    po_AlqIcmsST              OUT NUMBER
                                                    )
IS
  nNaturezaOperacao      NUMBER;
  pinDespesaEmbalagem      NUMBER;
  pinDespesasAcessorias    NUMBER;
    
  PO_AUX_NUM               NUMBER;
  PO_AUX_VARCHAR           VARCHAR2(1000);
BEGIN

  if pi_vUfDestino = 'EX' then
    nNaturezaOperacao := '3102'|| pi_vGRUPO_TRIBUT_FORNEC;
  elsif pi_vUfOrigem = pi_vUfDestino then  
    nNaturezaOperacao := '1102'|| pi_vGRUPO_TRIBUT_FORNEC;  
  elsif pi_vUfOrigem <> pi_vUfDestino then
    nNaturezaOperacao := '2102'|| pi_vGRUPO_TRIBUT_FORNEC;
  end if;
  
  
  BEGIN  
    FARMA.PMOV_CALC_IMPOSTOS_ENTRADA_RV(
                                        pi_nUnidade,                            --PI_NUNIDADE                 IN 
                                        pi_nFornecedor,                         --PI_NFORNECEDOR              IN 
                                        'F',                                    --PI_CTIPOEMITENTE            IN 
                                        pi_nProduto,                            --PI_NPRODUTO                 IN 
                                        nNaturezaOperacao,                      --PI_NNATUREZAOPERACAO        IN 
                                        pi_vUfOrigem,                           --PI_VUFORIGEM                IN 
                                        pi_vUfDestino,                          --PI_VUFDESTINO               IN 
                                        pi_nQuantidade,                         --PI_NQUANTIDADE              IN 
                                        pi_nPreco,                              --PI_NPRECO                   IN 
                                        pi_nDescontoItem,                       --PI_NDESCONTOITEM            IN 
                                        NULL,                                   --PI_NDESCONTOREPASSE         IN 
                                        NULL,                                   --PI_NDESCONTOCOMERCIAL       IN 
                                        NULL,                                   --PI_NDESPESAEMBALAGEM        IN 
                                        NULL,                                   --PI_NDESPESASACESSORIAS      IN 
                                        'P',                                    --PI_ORIGEMCHAMADA            IN 
                                        TRUNC(SYSDATE),                         --PI_DATAOPERACAO             IN 
                                        'N',                                    --PI_VALTEROUREGRAFISCAL      IN 
                                        NULL,                                   --PI_NNUMEROLOTE              IN 
                                        PO_AUX_NUM,                             --PO_NALIQUOTAICMSNORMAL      OUT
                                        po_nAliqIpi,                            --PO_NALIQUOTAIPI             OUT
                                        po_MVA_ST,                              --PO_NAGREGADOCOMPRA          OUT
                                        PO_AUX_NUM,                             --PO_CLASSIFICACAO_FISCAL     OUT
                                        PO_AUX_NUM,                             --PO_NBASEICMSNORMAL          OUT
                                        PO_AUX_NUM,                             --PO_NVALORICMSNORMAL         OUT
                                        PO_AUX_NUM,                             --PO_NBASEICMSSUBSTITUTO      OUT
                                        po_nValor_IcmsSt,                       --PO_NVALORICMSSUBSTITUTO     OUT
                                        PO_AUX_NUM,                             --PO_NBASEIPI                 OUT
                                        po_nValorIpi,                           --PO_NVALORIPI                OUT
                                        PO_AUX_NUM,                             --PO_NOUTRASICMSNORMAL        OUT
                                        PO_AUX_NUM,                             --PO_NISENTASICMSNORMAL       OUT
                                        PO_AUX_NUM,                             --PO_NOUTRASREDUCAO           OUT
                                        PO_AUX_NUM,                             --PO_NISENTASREDUCAO          OUT
                                        PO_AUX_NUM,                             --PO_NOUTRASREDUCAOICMSST     OUT
                                        PO_AUX_NUM,                             --PO_NISENTASREDUCAOICMSST    OUT
                                        PO_AUX_NUM,                             --PO_NOUTRASIPI               OUT
                                        PO_AUX_NUM,                             --PO_NISENTASIPI              OUT
                                        PO_AUX_NUM,                             --PO_NVALORDIFA               OUT
                                        PO_AUX_NUM,                             --PO_NDESCONTOITEM            OUT
                                        PO_AUX_NUM,                             --PO_NDESCONTOREPASSE         OUT
                                        PO_AUX_NUM,                             --PO_NDESCONTOCOMERCIAL       OUT
                                        PO_AUX_NUM,                             --PO_NTOTALDESCONTOS          OUT
                                        PO_AUX_VARCHAR,                         --PO_CTRIBPIS                 OUT
                                        PO_AUX_NUM,                             --PO_NBASEPISCOFINSISENTO   	OUT
                                        PO_AUX_NUM,                             --PO_NBASEPISCOFINSRETIDO   	OUT
                                        PO_AUX_NUM,                             --PO_NALIQUOTAPISRETIDO     	OUT
                                        PO_AUX_NUM,                             --PO_NALIQUOTACOFINSRETIDO  	OUT
                                        PO_AUX_NUM,                             --PO_NVALORPISRETIDO        	OUT
                                        PO_AUX_NUM,                             --PO_NVALORCOFINSRETIDO     	OUT
                                        PO_AUX_NUM,                             --PO_NBASEPISCOFINSNORMAL   	OUT
                                        PO_AUX_NUM,                             --PO_NALIQUOTAPISNORMAL     	OUT
                                        PO_AUX_NUM,                             --PO_NALIQUOTACOFINSNORMAL  	OUT
                                        PO_AUX_NUM,                             --PO_NVALORPISNORMAL        	OUT
                                        PO_AUX_NUM,                             --PO_NVALORCOFINSNORMAL     	OUT
                                        PO_AUX_VARCHAR,                         --PO_CMENSAGEMERRO          	OUT
                                        PO_AUX_VARCHAR,                         --PO_TEMINCENTIVOALC        	OUT
                                        PO_AUX_NUM,                             --PO_NALIQCREDITOPRESUMIDO  	OUT
                                        PO_AUX_NUM,                             --PO_NVALORDESCONTOPIS      	OUT
                                        PO_AUX_NUM,                             --PO_NVALORDESCONTOCOFINS   	OUT
                                        PO_AUX_NUM,                             --PO_NVALORDESCONTOICMS     	OUT
                                        PO_AUX_NUM,                             --PO_NVALORCREDITOPRESUMIDO 	OUT
                                        PO_AUX_VARCHAR,                         --po_MsgIcms                	OUT
                                        PO_AUX_VARCHAR,                         --po_MsgIcmsst              	OUT
                                        PO_AUX_VARCHAR,                         --po_MsgIpi                 	OUT
                                        PO_AUX_VARCHAR,                         --po_MsgPis                 	OUT
                                        PO_AUX_VARCHAR,                         --po_MsgCofins              	OUT
                                        PO_AUX_NUM,                             --po_FiguraFiscal           	OUT
                                        PO_AUX_NUM,                             --PO_PERCREDUCAOBASEICMS    	OUT
                                        po_AlqIcmsST,                           --PO_PERCALIQINTERNA        	OUT
                                        PO_AUX_NUM,                             --PO_ALIQINTERESTADUAL      	OUT
                                        PO_AUX_NUM,                             --PO_ALIQDIFA               	OUT
                                        PO_AUX_NUM,                             --PO_BASECALCULODIFA        	OUT
                                        PO_AUX_NUM,                             --PO_BASEREDUCAOICMS        	OUT
                                        PO_AUX_VARCHAR,                         --PO_CST_IPI                	OUT
                                        PO_AUX_VARCHAR,                         --PO_CST_ICMS               	OUT
                                        PO_AUX_VARCHAR,                         --PO_CST_PIS                	OUT
                                        PO_AUX_VARCHAR,                         --PO_CST_COFINS             	OUT
                                        PO_AUX_NUM,                             --PO_BASEREDUCAOICMS_ST     	OUT
                                        PO_AUX_NUM,                             --PO_PERCREDUCAOBASEICMS_ST 	OUT
                                        PO_AUX_NUM,                             --PI_NPRECO_MAX_CONSUMIDOR     	IN 
                                        PO_AUX_VARCHAR,                         --PI_VCST_ICMS_INFORMADO       	IN 
                                        PO_AUX_NUM,                             --PI_NBASEICMSSUBSNDESTAC_INF  	IN 
                                        PO_AUX_NUM,                             --PI_NVALORICMSSUBSNDESTAC_INF 	IN 
                                        PO_AUX_VARCHAR,                         --PO_VCST_SOMA_ST_TOTAL_NF     	OUT
                                        PO_AUX_VARCHAR,                         --PO_VCST_AGREGA_ST_CUSTO      	OUT
                                        PO_AUX_NUM,                             --PO_NBASEICMSSUBSANTECIP      	OUT
                                        PO_AUX_NUM,                             --PO_NVALORICMSSUBSANTECIP     	OUT
                                        PO_AUX_NUM,                             --PO_NBASEREDICMSUBSANTECIP    	OUT
                                        PO_AUX_NUM,                             --PO_NPERCREDICMSUBSANTECIP    	OUT
                                        PO_AUX_NUM,                             --PO_NOUTREDICMSUBSANTECIP     	OUT
                                        PO_AUX_NUM,                             --PO_NISEREDICMSUBSANTECIP     	OUT
                                        PO_AUX_VARCHAR,                         --PO_CICMSNORMAL_TIPO_BASEDIF  	OUT
                                        PO_AUX_NUM,                             --PO_NICMSNORMAL_VALOR_BASEDIF 	OUT
                                        PO_AUX_VARCHAR,                         --PO_CICMSSUBST_TIPO_BASEDIF   	OUT
                                        PO_AUX_NUM,                             --PO_NICMSSUBST_VALOR_BASEDIF  	OUT
                                        PO_AUX_NUM,                             --PO_NCOD_ANTECIPACAO_ST       	OUT
                                        PO_AUX_NUM,                             --PO_NBASEICMSSUBSNAODEST      	OUT
                                        PO_AUX_NUM,                             --PO_NVALORICMSSUBSNAODEST     	OUT
                                        PO_AUX_NUM,                             --PO_VOPERACAO_FISCAL          	OUT
                                        'N',                                    --PI_CCHAMADACARTA             	IN 
                                        PO_AUX_NUM,                              --PO_CENQ_IPI                   OUT                                         
                                        PO_AUX_NUM
                                       );
  EXCEPTION
    WHEN OTHERS THEN
      po_nValorIpi     := 0;    
      po_nAliqIpi      := 0;
      po_nValor_IcmsSt := 0;
      po_MVA_ST        := 0;
      po_AlqIcmsST     := 0;
                                                        
  END;                                          
END;                                                               
/
