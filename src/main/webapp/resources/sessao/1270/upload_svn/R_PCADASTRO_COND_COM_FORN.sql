CREATE OR REPLACE PROCEDURE I_MDLOG.PCADASTRO_COND_COM_FORN(  PI_OPERACAO VARCHAR2,
                                                              STATUS OUT VARCHAR2 )
IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_COND_COM_FORN
  OBJETIVO  : ROTINA PARA OPERACAO DA CONDICAO COMERCIAL DO FORNECEDOR
  AUTOR     : JOSE GABRIEL
  DATA      : 16/12/2016
  CRC       : CRC70542
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
  
  
  
  CURSOR C_CONDICAO IS
  SELECT FORNECEDOR
        ,RAZAO_SOCIAL
        ,LOJA
        ,NOME
        ,GRUPO_COMPRA
        ,GRUPO_COMPRA_DESC
        ,DESCONTO_NOTA
        ,DESCONTO_REPASSE
        ,COBRA_IPI
        ,GRUPO_TRIBUTARIO
        ,SEQUENCIA_DESC
        ,GERA_SUGESTAO
        ,GUARDA_SALDO
        ,DIAS_CNC
        ,FATURA_MINIMA
        ,EXC_AUTOM_ABAIXO
        ,PRZ_FORN_LOJA
        ,GERA_RECEB_SEM_ESP
        ,PRZ_PAG_IMP_NFE
  FROM  I_MDLOG.TEMP_CAD_COND_COMERC; 
  
BEGIN
	
   STATUS := 'NP';
  /******************************BEGIN - CADASTRAR*****************************/
  
  FOR VC_CONDICAO IN C_CONDICAO LOOP
    
    
    IF PI_OPERACAO = 'C' THEN
      BEGIN
       STATUS := 'ER';
      EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        ecode  := SQLCODE;
        emesg  := SUBSTR(SQLERRM,1, 2048);
        STATUS := emesg;
      END;
    END IF;
    /********************************END - CADASTRAR*****************************/
    /********************************BEGIN EDITAR********************************/
    IF PI_OPERACAO = 'U' THEN
      BEGIN
        UPDATE DBAMDATA.T054_FORNECEDOR_UNIDADE
        SET 
          T054_COMPRADOR_E            = VC_CONDICAO.GRUPO_COMPRA,
          T054_DESCONTO_NOTA          = VC_CONDICAO.DESCONTO_NOTA,
          T054_REPASSE_ICM            = VC_CONDICAO.DESCONTO_REPASSE,
          T054_COBRA_IPI              = VC_CONDICAO.COBRA_IPI,
          T054_TIPO_NATUREZA_E        = VC_CONDICAO.GRUPO_TRIBUTARIO,
          T054_SEQUENCIA_DESCONTOS    = VC_CONDICAO.SEQUENCIA_DESC,
          T054_SUG_COMPRA             = VC_CONDICAO.GERA_SUGESTAO,
          T054_GUARDA_SALDO_PEDIDO    = VC_CONDICAO.GUARDA_SALDO,
          T054_DIAS_CANC_PEDIDO       = VC_CONDICAO.DIAS_CNC,
          T054_VALOR_MINIMO_COMPRA    = VC_CONDICAO.FATURA_MINIMA,
          T054_EXCLUI_PED_ABAIXO_MINIMO  = VC_CONDICAO.EXC_AUTOM_ABAIXO,
          T054_PRAZO_ENTREGA             = VC_CONDICAO.PRZ_FORN_LOJA,
          T054_PER_GERA_RECEB_SEM_ESP_NF = VC_CONDICAO.GERA_RECEB_SEM_ESP,
          T054_RECEPCAO_PRAZOPAG         = VC_CONDICAO.PRZ_PAG_IMP_NFE
        WHERE 
          T054_FORNECEDOR_IE             =  VC_CONDICAO.FORNECEDOR
        AND 
          T054_UNIDADE_IE                =  VC_CONDICAO.LOJA;
           STATUS := 'PR';
      EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        ecode  := SQLCODE;
        emesg  := SUBSTR(SQLERRM,1, 2048);
        STATUS := 'ER';
      END;
    END IF;
    /********************************END EDITAR********************************/
    /********************************BEGIN EXCLUIR********************************/
    IF PI_OPERACAO = 'D' THEN
      BEGIN
        
        STATUS := 'ER';
      EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        ecode  := SQLCODE;
        emesg  := SUBSTR(SQLERRM,1, 2048);
        STATUS := 'ER';
      END;
    END IF;
    /********************************END EXCLUIR********************************/
    --CONFIRMAR OPERCAO
    COMMIT;
    
    END LOOP;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  ecode  := SQLCODE;
  emesg  := SUBSTR(SQLERRM,1, 2048);
  STATUS := 'ER';
END;
/