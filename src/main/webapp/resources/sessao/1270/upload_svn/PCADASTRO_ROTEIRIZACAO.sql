create or replace PROCEDURE I_MDLOG.PCADASTRO_ROTEIRIZACAO(
											PI_CODIGO_ROTA         NUMBER, 
											PI_NOME                VARCHAR2,
											PI_KILOMETRAGEM        NUMBER, 
											PI_LITROS_CONSUMIDOS   NUMBER,
											PI_OPERACAO            CHAR,
											STATUS OUT VARCHAR2
                                            ) IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_LOJA
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DE RETA
  AUTOR     : Luis Eduardo Pinheiro Pedrosa
  DATA      : 05/12/2016
  CRC       : 70542
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN
	STATUS     := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT INTO DBAMDATA.T006_ROTA(
		T006_ROTA_IU ,
		T006_NOME,
		T006_KILOMETRAGEM ,
		T006_LITROS_CONSUMIDOS
		)
	 VALUES (
		PI_CODIGO_ROTA , 
		PI_NOME,
		PI_KILOMETRAGEM , 
		PI_LITROS_CONSUMIDOS
	);
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/

  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
     UPDATE DBAMDATA.T006_ROTA
     SET 
		T006_NOME                     = PI_NOME,
		T006_KILOMETRAGEM             = PI_KILOMETRAGEM,
		T006_LITROS_CONSUMIDOS        = PI_LITROS_CONSUMIDOS
		WHERE T006_ROTA_IU            = PI_CODIGO_ROTA;
		
	EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE FROM DBAMDATA.T006_ROTA
            WHERE T006_ROTA_IU   = PI_CODIGO_ROTA;
            
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
    
    
    --CONFIRMAR OPERCAO
    
    COMMIT;
    
    
EXCEPTION
	WHEN OTHERS THEN
		ROLLBACK;
		ecode := SQLCODE;
		emesg := SUBSTR(SQLERRM,1, 2048);
		STATUS     := 'ER';
END;	