create or replace PROCEDURE          PCADASTRO_NCM(
											PI_COD_NCM             VARCHAR2, 
											PI_DESCRICAO           VARCHAR2,
											PI_TRIBUTACAOPIS       VARCHAR2,
											PI_ALIQ_NACIONAL       NUMBER, 
											PI_ALIQ_IMPORTACAO     NUMBER, 
											PI_OPERACAO            CHAR,
											STATUS OUT VARCHAR2
                                            ) IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  
  ERRONCM EXCEPTION;
  
  ROW_NCM  boolean;
  
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_SEG_CLIENTE
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DOS CLUSTER
  AUTOR     : Luis Eduardo Pinheiro Pedrosa
  DATA      : 16/11/2016
  CRC       : 70542
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
  cursor c_ncmfiguracaofiscal is
  SELECT  COD_NCM,FIGURA_FISCAL FROM I_MDLOG.TEMP_CAD_NCM_FIG;
  
  cursor c_nve is
  SELECT  COD_NVE,COD_NCM ,DESCRICAO FROM I_MDLOG.TEMP_CAD_NVE;
  
  
BEGIN
	STATUS     := 'PR';
	ROW_NCM  := FALSE;
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT INTO DBAMDATA.T414_NCM(
		T414_NCM_IU, 
		T414_DESCRICAO, 
		T414_TRIBUTACAO_PIS, 
		T414_ALIQ_NACIONAL,
		T414_ALIQ_IMPORTACAO
		)
	 VALUES (
		PI_COD_NCM, 
		PI_DESCRICAO,
		PI_TRIBUTACAOPIS,
		PI_ALIQ_NACIONAL,
		PI_ALIQ_IMPORTACAO
	);
	
  for vc_ncmfiguracaofiscal in c_ncmfiguracaofiscal loop
    BEGIN
  INSERT INTO DBAMDATA.T428_NCM_FIGURA_FISCAL(T428_NCM_IE,T428_FIGURA_FISCAL_IE)
				VALUES(vc_ncmfiguracaofiscal.COD_NCM,vc_ncmfiguracaofiscal.FIGURA_FISCAL);
     END;
  end loop;
  
  for vc_nve in c_nve loop
     BEGIN
				INSERT INTO DBAMDATA.T415_NVE(T415_NVE_IU,T415_NCM_IE,T415_DESCRICAO_NVE)
				VALUES(vc_nve.COD_NVE,vc_nve.COD_NCM,vc_nve.DESCRICAO);
     END;
  end loop;   
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  
  
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
      UPDATE DBAMDATA.T414_NCM
     SET
		T414_DESCRICAO          = PI_DESCRICAO, 
		T414_TRIBUTACAO_PIS     = PI_TRIBUTACAOPIS, 
		T414_ALIQ_NACIONAL      = PI_ALIQ_NACIONAL,
		T414_ALIQ_IMPORTACAO    = PI_ALIQ_IMPORTACAO
       WHERE T414_NCM_IU        = PI_COD_NCM;

	   
    for vc_ncmfiguracaofiscal in c_ncmfiguracaofiscal loop
			BEGIN
				UPDATE DBAMDATA.T428_NCM_FIGURA_FISCAL set 
				T428_FIGURA_FISCAL_IE = vc_ncmfiguracaofiscal.FIGURA_FISCAL
				WHERE T428_NCM_IE = vc_ncmfiguracaofiscal.COD_NCM;
			END;
			end loop;
		
	  
		for vc_nve in c_nve loop
			BEGIN
				UPDATE DBAMDATA.T415_NVE SET 
				T415_DESCRICAO_NVE = vc_nve.DESCRICAO
				WHERE 
				T415_NVE_IU = vc_nve.COD_NVE and 
				T415_NCM_IE = vc_nve.COD_NCM;
			END;
		end loop;
	
	 EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE FROM DBAMDATA.T414_NCM
            WHERE T414_NCM_IU   = PI_COD_NCM;
			
	  DELETE FROM DBAMDATA.T428_NCM_FIGURA_FISCAL
		     WHERE T428_NCM_IE   = PI_COD_NCM;	
	  
	  DELETE FROM DBAMDATA.T415_NVE
		        WHERE T415_NCM_IE = PI_COD_NCM;	
            
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
    --CONFIRMAR OPERCAO
    COMMIT;
EXCEPTION
	WHEN ERRONCM THEN
		ROLLBACK;
		ecode := SQLCODE;
		emesg := SUBSTR(SQLERRM,1, 2048);
		STATUS     := 'ER';
	WHEN OTHERS THEN
		ROLLBACK;
		ecode := SQLCODE;
		emesg := SUBSTR(SQLERRM,1, 2048);
		STATUS     := 'ER';
END;