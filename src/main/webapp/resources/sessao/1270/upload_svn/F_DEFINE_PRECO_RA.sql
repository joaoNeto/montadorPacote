CREATE OR REPLACE FUNCTION I_MDLOG.F_DEFINE_PRECO_RA(PI_UNIDADE    NUMBER,
                                                     PI_PRODUTO    NUMBER,
                                                     PI_FORNECEDOR NUMBER) RETURN NUMBER
IS
  vPreco         NUMBER;
  vAuxCatOut     NUMBER;
  vAuxFornecOut  NUMBER;
BEGIN

  vPreco := FCOM_OBTEM_PRECOPROD_PEDCOMPRA(PI_UNIDADE, PI_PRODUTO, PI_FORNECEDOR,0);
  
  IF vPreco = 0 THEN
    PPRC_OBTEM_TIPOUNIVERSO(PI_UNIDADE, PI_PRODUTO,  vAuxCatOut, vPreco, vAuxFornecOut);
    vPreco := NVL(vPreco,0);
  END IF;
  
  RETURN vPreco;
END;
/
