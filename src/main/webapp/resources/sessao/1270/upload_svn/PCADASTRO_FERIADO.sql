
create or replace PROCEDURE I_MDLOG.PCADASTRO_FERIADO(
											PI_ABRANGENCIA          CHAR, 
											PI_UF                   VARCHAR2,
											PI_CEP                  VARCHAR2,
											PI_DATA_FERIADO         DATE,
											PI_COD_FERIADOS         NUMBER,
											PI_DESCRICAO_FERIADO    VARCHAR2,
											PI_OPERACAO           	CHAR,
											STATUS OUT VARCHAR2
                                            ) IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_SEG_NEGOCIO
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DO SEGMENTO DE CLIENTES.
  AUTOR     : Luis Eduardo Pinheiro Pedrosa
  DATA      : 16/11/2016
  CRC       : 70542
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN
	STATUS     := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT INTO DBAMDATA.T090_DATAS_FERIADOS(
			T090_ABRANGENCIA,
			T090_UF_E,
			T090_CEP_E,
			T090_DATA,
			T090_DATAS_FERIADOS_IU,
			T090_DESCRICAO_FERIADO
		)
	 VALUES (
		PI_ABRANGENCIA, 
		PI_UF,
		PI_CEP,
		PI_DATA_FERIADO,
		PI_COD_FERIADOS,
		PI_DESCRICAO_FERIADO
	);
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  
  
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
     UPDATE DBAMDATA.T090_DATAS_FERIADOS 
     SET
		T090_ABRANGENCIA              = PI_ABRANGENCIA,
		T090_UF_E                     = PI_UF,
		T090_CEP_E                    = PI_CEP,
		T090_DATA                     = PI_DATA_FERIADO ,
		T090_DESCRICAO_FERIADO        = PI_DESCRICAO_FERIADO
       WHERE T090_DATAS_FERIADOS_IU   = PI_COD_FERIADOS;
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE FROM DBAMDATA.T090_DATAS_FERIADOS
            WHERE T090_DATAS_FERIADOS_IU   = PI_COD_FERIADOS;
            
    EXCEPTION WHEN OTHERS THEN
      ROLLBACK;
      ecode := SQLCODE;
      emesg := SUBSTR(SQLERRM,1, 2048);
      STATUS     := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
    
    
    --CONFIRMAR OPERCAO
    
    COMMIT;
    
    
EXCEPTION
	WHEN OTHERS THEN
		ROLLBACK;
		ecode := SQLCODE;
		emesg := SUBSTR(SQLERRM,1, 2048);
		STATUS     := 'ER';
END;