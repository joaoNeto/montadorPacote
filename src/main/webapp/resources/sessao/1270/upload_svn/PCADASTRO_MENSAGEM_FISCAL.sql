CREATE OR REPLACE PROCEDURE I_MDLOG.PCADASTRO_MENSAGEM_FISCAL(
    PI_DESCRICAO VARCHAR2,
    PI_DOCUMENTOS_VALIDOS NUMBER,
    PI_MENSAGEM_FISCAL_IU NUMBER,
    PI_TIPO_IMPOSTO NUMBER,
    PI_TIPO_MOVIMENTACAO CHAR,
    PI_OPERACAO VARCHAR2,
    STATUS OUT VARCHAR2 )
IS
  --VARIAVEIS DE ERRO
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
  /*****************************************************************************
  SCHEMA    : I_MDLOG
  PROCEDURE : PCADASTRO_MENSAGEM_FISCAL
  OBJETIVO  : ROTINA PARA OPERA��ES DE CADASTRO DE MENSAGEM FISCAL.
  AUTOR     : FILIPE FALC�O
  DATA      : 13/12/2016
  CRC       : CRC70542
  ALTERACAO : NOME - 00/00/0000 - CRC99999
  DESCRICAO DA ALTERACAO.
  *****************************************************************************/
BEGIN
	
   STATUS := 'PR';
  /******************************BEGIN - CADASTRAR*****************************/
  IF PI_OPERACAO = 'C' THEN
    BEGIN
      INSERT
      INTO DBAMDATA.T424_MENSAGEM_FISCAL
        (
          T424_DESCRICAO,
          T424_DOCUMENTOS_VALIDOS,
          T424_MENSAGEM_FISCAL_IU,
          T424_TIPO_IMPOSTO,
          T424_TIPO_MOVIMENTACAO
        )
        VALUES
        (
          PI_DESCRICAO,
          PI_DOCUMENTOS_VALIDOS,
          PI_MENSAGEM_FISCAL_IU,
          PI_TIPO_IMPOSTO,
          PI_TIPO_MOVIMENTACAO
        );
    EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      STATUS := 'ER';
    END;
  END IF;
  /********************************END - CADASTRAR*****************************/
  /********************************BEGIN EDITAR********************************/
  IF PI_OPERACAO = 'U' THEN
    BEGIN
      UPDATE DBAMDATA.T424_MENSAGEM_FISCAL
      SET T424_DESCRICAO = PI_DESCRICAO,
          T424_MENSAGEM_FISCAL_IU = PI_MENSAGEM_FISCAL_IU,
          T424_DOCUMENTOS_VALIDOS = PI_DOCUMENTOS_VALIDOS,
          T424_TIPO_IMPOSTO = PI_TIPO_IMPOSTO,
          T424_TIPO_MOVIMENTACAO = PI_TIPO_MOVIMENTACAO
      
      WHERE T424_MENSAGEM_FISCAL_IU = PI_MENSAGEM_FISCAL_IU;
    EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      STATUS := 'ER';
    END;
  END IF;
  /********************************END EDITAR********************************/
  /********************************BEGIN EXCLUIR********************************/
  IF PI_OPERACAO = 'D' THEN
    BEGIN
      DELETE
      FROM DBAMDATA.T424_MENSAGEM_FISCAL
      WHERE T424_MENSAGEM_FISCAL_IU = PI_MENSAGEM_FISCAL_IU;
    EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      ecode  := SQLCODE;
      emesg  := SUBSTR(SQLERRM,1, 2048);
      STATUS := 'ER';
    END;
  END IF;
  /********************************END EXCLUIR********************************/
  --CONFIRMAR OPERCAO
  COMMIT;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  ecode  := SQLCODE;
  emesg  := SUBSTR(SQLERRM,1, 2048);
  STATUS := 'ER';
END;
/