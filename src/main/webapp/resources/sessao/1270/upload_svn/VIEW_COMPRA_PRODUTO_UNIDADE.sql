/***
*
* SCHEMA  : I_MDLOG
* MODULO  : I_MDLOG
* ANALISTA: TIAGO MONTEZUMA
* MOTIVO  : PEDIDOS PENDENTES POR UNIDADE POR PRODUTO
* CRC     : 68102
* DATA    : 03/03/2016
*
*/


CREATE OR REPLACE VIEW I_MDLOG.VIEW_NSE_PRODUTO_COMPRA_ESTAT AS
select 
  T077_PRODUTO_IE PRODUTO,
  T077_UNIDADE_IE UNIDADE,
  SUM(T110_QTDE_PEDIDO) QTD_EM_OC, 
  COUNT(DISTINCT T108_UNIDADE_IE||T108_NUMERO_PEDIDO_IU) QTD_DE_OC,
  SUM((T110_QTDE_PEDIDO * T110_PRECO_ITEM) * (1 - (NVL(T110_DESCONTO_ITEM,0)+NVL(T110_DESCONTO_COMERCIAL,0)/100) ) ) VALOR_EM_OC
from T108_PEDIDOS_PEND
  JOIN T110_PEDIDOS_PEND_ITENS ON 
    T110_UNIDADE_IE       = T108_UNIDADE_IE AND
    T110_NUMERO_PEDIDO_IE = T108_NUMERO_PEDIDO_IU
  JOIN T077_PRODUTO_UNIDADE ON 
    T077_UNIDADE_IE = T110_UNIDADE_IE AND
    T077_PRODUTO_IE = T110_PRODUTO_IE 
  JOIN T003_UNIDADE ON 
    T003_UNIDADE_IU = T077_UNIDADE_IE
WHERE ( T108_TIPO_PEDIDO  IN ('P','4','5','8') )
  AND NVL(T108_SITUACAO_LOTE_PEDIDO,'999') IN ('999','0','1')
  AND T110_QTDE_PEDIDO > 0
  AND T003_UNIDADE_ATIVA = 'S'
  AND NOT EXISTS (SELECT 1 
                  FROM T209_LOTESTOQUE, T211_LOTESTOQUE_ITENS
                  WHERE T209_UNIDADE_IE     = T211_UNIDADE_IE
                    AND T209_NUMERO_LOTE_IU = T211_NUMERO_LOTE_IE
                    AND T209_UNIDADE_IE = T108_UNIDADE_IE 
                    AND T209_NUMERO_PEDIDO = T108_NUMERO_PEDIDO_IU 
                    AND T211_PRODUTO_IE = T110_PRODUTO_IE
                    AND T209_DATA_LIBERACAO IS NULL)
GROUP BY 
  T077_PRODUTO_IE,
  T077_UNIDADE_IE;

GRANT SELECT ON I_MDLOG.VIEW_NSE_PRODUTO_COMPRA_ESTAT TO INTEGRA;