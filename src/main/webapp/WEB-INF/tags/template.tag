<%-- 
    Document   : template.tag
    Created on : 04/09/2017, 15:06:54
    Author     : danilo.muniz
--%>
<%@tag language="java" description="put the tag description here" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="titulo" required="true" %>
<%@attribute name="bodyClass" required="false" %>
<%@attribute name="extraScripts" fragment="true" %>
<%@variable name-given="pathTemaTemplate" scope="session"%>
<%@variable name-given="pathResources" scope="session"%>
<%@variable name-given="urlContext" scope="session"%>
<%@variable name-given="userName" scope="session"%>
<!DOCTYPE html>
<html lang="en">
    <head>    
        <meta charset="utf-8">    
        <meta name="_csrf" content="${_csrf.token}"/>
        <!-- default header name is X-CSRF-TOKEN -->
        <meta name="_csrf_header" content="${_csrf.headerName}"/>
        <%--variaveis globais da aplicacao --%>    
        <c:url value="/resources/tema" var="pathTemaTemplate"/>    
        <c:url value="/resources" var="pathResources"/>    
        <c:url value="/" var="urlContext"/>    
        <c:set var="pathTemaTemplate" value="${pathTemaTemplate}"/>    
        <c:set var="pathResources" value="${pathResources}"/>
        <c:set var="urlContext" value="${urlContext}"/>
        <%--<c:set var="username" value="${username}"/>--%>
        <%--variaveis globais da aplicacao --%>   
        <link rel="shortcut icon" href="${pathTemaTemplate}/images/favicon.png">

        <title>${titulo} - CustomerLine</title>

        <!--Core CSS -->
        <link href="${pathTemaTemplate}/bs3/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pathTemaTemplate}/js/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
        <link href="${pathTemaTemplate}/css/bootstrap-reset.css" rel="stylesheet">
        <link href="${pathTemaTemplate}/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pathTemaTemplate}/css/style.css" rel="stylesheet">
        <link href="${pathTemaTemplate}/css/style-responsive.css" rel="stylesheet"/>    
        <link href="${pathResources}/css/style-default.css" rel="stylesheet"/>
        <link href="${pathResources}/css/style-datatable.css" rel="stylesheet"/>        
        <script src="${pathResources}/js/jquery/jquery-3.1.1.min.js"></script>        
        <script type="text/javascript">
            var pathResources = '${pathResources}';
            var pathTemaTemplate = '${pathTemaTemplate}';
            var urlContext = '${urlContext}';       
            
            
            function jsonConverter(data){
                   data = JSON.parse(data);
                   if(data){
                       return data.data;
                   }else{
                       alert(data.data);
                   }
                   return null;
            }
            
            
        </script>
    </head>
    <body class="${bodyClass}">
        <section id="container">

            <%@include file="/WEB-INF/views/header.jsp" %>
            <%@include file="/WEB-INF/views/sidebar.jsp" %>
            <section id="main-content" class="merge-left">
                <jsp:doBody />
            </section>
            <%--<%@include file="/WEB-INF/views/footer.jsp" %>--%>

            <jsp:invoke fragment="extraScripts" />

        </section>       

        <!--Core js-->        
        <script src="${pathTemaTemplate}/js/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
        <script src="${pathTemaTemplate}/bs3/js/bootstrap.min.js"></script>        
        <script src="${pathTemaTemplate}/js/jquery.nicescroll.js"></script>
        <script src="${pathTemaTemplate}/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="${pathTemaTemplate}/js/scripts.js"></script>        
    </body>
</html>
