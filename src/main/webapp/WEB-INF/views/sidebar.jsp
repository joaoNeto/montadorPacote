<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>
<!--sidebar start-->
<aside>    
<div id="sidebar" class="nav-collapse hide-left-bar">
        <!-- sidebar menu start-->
        <div class="leftside-navigation" tabindex="5000" style="overflow: hidden; outline: none;">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a class="active" href="${s:mvcUrl('HC#index').build()}">
                        <i class="fa fa-archive"></i>
                        <span>Inicio</span>
                    </a>
                </li>
                <li>
                    <a class="active" href="${s:mvcUrl('WC#index').build()}">
                        <i class="fa fa-unlock"></i>
                        <span>Wrap</span>
                    </a>
                </li>
                <li>
                    <a class="active" href="${s:mvcUrl('VC#index').build()}">
                        <i class="fa fa-cloud-upload"></i>
                        <span>Versionamento</span>
                    </a>
                </li>    
            </ul>
        </div>
        <!-- sidebar menu end-->
    <div id="ascrail2000" class="nicescroll-rails" style="width: 3px; z-index: auto; cursor: default; position: absolute; top: 0px; left: 237px; height: 618px; display: none; opacity: 0;"><div style="position: relative; top: 0px; float: right; width: 3px; height: 0px; background-color: rgb(31, 181, 173); border: 0px solid rgb(255, 255, 255); background-clip: padding-box; border-radius: 0px;"></div></div><div id="ascrail2000-hr" class="nicescroll-rails" style="height: 3px; z-index: auto; top: 615px; left: 0px; position: absolute; cursor: default; display: none; opacity: 0; width: 237px;"><div style="position: relative; top: 0px; height: 3px; width: 0px; background-color: rgb(31, 181, 173); border: 0px solid rgb(255, 255, 255); background-clip: padding-box; border-radius: 0px; left: 0px;"></div></div></div>    
</aside>
<!--sidebar end-->
