<%-- 
    Document   : login
    Created on : 04/09/2017, 15:06:06
    Author     : danilo.muniz
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="spform" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="_csrf" content="${_csrf.token}"/>
        <!-- default header name is X-CSRF-TOKEN -->
        <meta name="_csrf_header" content="${_csrf.headerName}"/>
        <title>Login</title>
        <c:url value="/resources/tema" var="pathTema" />
        <c:url value="/resources/" var="pathResources" />
        <c:url value="/" var="urlContext"/>            
        <c:set var="urlContext" value="${urlContext}"/>
        <%--variaveis globais da aplicacao --%>    
        <script type="text/javascript">
            var urlContext = '${urlContext}';
        </script>
        <link rel="stylesheet" href="${pathTema}/bs3/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="${pathTema}/css/bootstrap-reset.css"/>
        <link rel="stylesheet" href="${pathTema}/font-awesome/css/font-awesome.css"/>        
        <link rel="stylesheet" href="${pathTema}/css/style.css"/>        
        <link rel="stylesheet" href="${pathTema}/css/style-responsive.css"/>    
        <link rel="stylesheet" href="${pathResources}/css/style-default.css" rel="stylesheet"/>        
        <script src="${pathResources}/js/jquery/jquery-3.1.1.min.js"></script>        
        
    </head>
    <body>
    <body class="login-body">
        <div class="container">
            <spform:form id="loginForm" name="loginForm" class="form-signin" servletRelativeAction="/login" method="POST">
                <h2 class="form-signin-heading"> <img class="img-login" src="${pathResources}/img/logo.png" alt=""></h2>
                <c:if test="${not empty error}">
                    <!--<div class="error alert-danger">${error}</div>-->
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Erro:</strong> ${error}
                    </div>
                </c:if>
                <c:if test="${not empty msg}">
                    <!--<div class="msg alert-succes">${msg}</div>-->
                    <div class="alert alert-block alert-success fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Informação:</strong> ${msg}
                    </div>
                </c:if>
                <div class="login-wrap">
                    <div class="user-login-info">
                        <input id="cliente"     name="cliente"    type="text" class="form-control" placeholder="cliente" style="display:none;" value="informata" >
                        <input id="usuario"     name="usuario"    type="text" class="form-control" placeholder="Nome usuário" autofocus>
                        <input id="senha"       name="senha"      type="password" class="form-control" placeholder="senha">
                        <input id="username"    name="username"   type="hidden" class="form-control" placeholder="código usuário" autofocus>
                        <input id="hidden-hash" name="password"   type="hidden" class="form-control" placeholder="senha">
                    </div>
                    <button id="btn-entrar" class="btn btn-lg btn-login btn-block" type="submit">Entrar</button>
                </div>
                <!-- Modal -->
                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Forgot Password ?</h4>
                            </div>
                            <div class="modal-body">
                                <p>Enter your e-mail address below to reset your password.</p>
                                <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                            </div>
                            <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                <button class="btn btn-success" type="button">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- modal -->
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </spform:form>
        </div>
    </body>
    <!--Core js-->
    <script src="${pathTema}/js/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
    <script src="${pathTema}/bs3/js/bootstrap.min.js"></script>        
    <script src="${pathResources}/js/login/login.js"></script>
</html>
