<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="tags"%>

<tags:template titulo="home">
    <jsp:attribute name="extraScripts">
        <link href="${pathTemaTemplate}/js/mini-upload-form/assets/css/bucketmin.css" rel="stylesheet">
        <style type="text/css">
            .jqstooltip {
                position: absolute;
                left: 30px;top: 0px;display: block;visibility: 
                hidden;background: rgb(0, 0, 0) transparent;
                background-color: rgba(0,0,0,0.6);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
                color: white;
                font: 10px arial, san serif;text-align: left;
                white-space: nowrap;
                border: 0px solid white;
                border-radius: 3px;
                -webkit-border-radius: 3px;
                z-index: 10000;
            }
            .jqsfield { 
                color: white;
                padding: 5px 5px 5px 5px;
                font: 10px arial, san serif;
                text-align: left;
            }
        </style>        
    </jsp:attribute>
    <jsp:body>         
        
    <section id="main-content" class="merge-left">
        <section class="wrapper">


            <h3 class="text-muted">Wrap e unwrap por texto</h3>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <div class="panel-body">
                            <textarea class="form-control col-sm-12 text-wrap-unwrap" rows="10" placeholder="Digite aqui o seu texto." style="resize: none;"></textarea> 
                            <div class="todo-action-bar">
                                <div class="row">
                                    <div class="col-xs-2 btn-add-task pull-right">
                                        <br>
                                        <button type="submit" class="btn btn-default btn-primary pull-right col-md-2 btn-text-wrap-unwrap"><i class="fa fa-unlock-alt"></i> Encriptografar / <i class="fa fa-unlock"></i>  Decriptografar</button>
                                    </div>
                                </div>
                            </div>                                                 
                        </div>
                    </section>
                </div>
            </div>            
            
            <h3 class="text-muted">Wrap e unwrap por arquivo</h3>
            <br>
            <label for="upload-file" class="btn btn-primary btn-lg col-md-2 col-md-offset-5"> <i class="fa  fa-cloud-upload"></i> Upload dos arquivos</label>
            <input type="file" id="upload-file" style="display:none" class="btn btn-primary btn-lg col-md-6 col-md-offset-3 upload-arquivo" multiple="" value="Upload dos arquivos">

            <div class="row">
                <div class="col-lg-6">
                    <div class="text-center text-muted">
                        <h2> <i class="fa fa-unlock"></i> Unwrap</h2>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="text-center text-muted">
                        <h2> <i class="fa fa-unlock-alt"></i> Wrap</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <section class="panel">
                        <header class="panel-heading">
                            Do arquivo
                        </header>
                        <div class="panel-body ">
                            <ul class="to-do-list ui-sortable lista-file-uncripto" diretorio="wrap/unwrap" id="sortable-todo" style="overflow: hidden; width: auto; height: 300px;">
                            </ul>
                            <div class="todo-action-bar">
                                <div class="row">
                                    <!--<div class="col-xs-4 btn-add-task pull-left delete-all">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        deletar todos
                                    </div>-->
                                    <div class="col-xs-4 btn-add-task pull-right">
                                        <button type="submit" class="btn btn-default btn-primary pull-right zippar-arquivo-unwrap">
                                            <i class="fa fa-cloud-download"></i> Baixar zip do arquivo
                                        </button>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-lg-6">
                    <section class="panel">
                        <header class="panel-heading">
                            Do arquivo
                        </header>
                        <div class="panel-body">
                            <ul class="to-do-list lista-file-cripto" diretorio="wrap/wrap"  id="sortable-todo">
                            </ul>
                            <div class="todo-action-bar">
                                <div class="row">
                                    <div class="col-xs-4 btn-add-task pull-right">
                                        <button type="submit" class="btn btn-default btn-primary pull-right zippar-arquivo-wrap"><i class="fa fa-cloud-download"></i> Baixar zip do arquivo</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

        </section>
    </section>
        <script src="${pathTemaTemplate}/js/mini-upload-form/assets/js/jquery.knob.js"></script>
        <script src="${pathTemaTemplate}/js/mini-upload-form/assets/js/jquery.ui.widget.js"></script>
        <script src="${pathTemaTemplate}/js/mini-upload-form/assets/js/jquery.iframe-transport.js"></script>
        <script src="${pathTemaTemplate}/js/mini-upload-form/assets/js/jquery.fileupload.js"></script>
        <script src="${pathTemaTemplate}/js/mini-upload-form/assets/js/script.js"></script>
        <script src="${pathTemaTemplate}/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="${pathTemaTemplate}/js/jquery.scrollTo.min.js"></script>
        <script src="${pathTemaTemplate}/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
        <script src="${pathTemaTemplate}/js/jquery.nicescroll.js"></script>
        <script src="${pathTemaTemplate}/js/skycons/skycons.js"></script>
        <script src="${pathTemaTemplate}/js/jquery.scrollTo/jquery.scrollTo.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="${pathTemaTemplate}/js/calendar/clndr.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script> 
        <script type="text/javascript" src="${pathResources}/js/wrap/index.js"></script>
    </jsp:body>
</tags:template>

