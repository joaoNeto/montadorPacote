<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="tags"%>

<tags:template titulo="home">
    <jsp:attribute name="extraScripts">
        <link href="${pathTemaTemplate}/js/mini-upload-form/assets/css/bucketmin.css" rel="stylesheet">
        <style type="text/css">
            .jqstooltip {
                position: absolute;
                left: 30px;top: 0px;display: block;visibility: 
                hidden;background: rgb(0, 0, 0) transparent;
                background-color: rgba(0,0,0,0.6);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
                color: white;
                font: 10px arial, san serif;text-align: left;
                white-space: nowrap;
                border: 0px solid white;
                border-radius: 3px;
                -webkit-border-radius: 3px;
                z-index: 10000;
            }
            .jqsfield { 
                color: white;
                padding: 5px 5px 5px 5px;
                font: 10px arial, san serif;
                text-align: left;
            }
        </style>        
    </jsp:attribute>
    <jsp:body>         
        
    <section id="main-content" class="merge-left">
        <section class="wrapper">

            <div class="row">
                <div class="col-lg-4 col-md-offset-4">
                    <section class="panel">
                        <header class="panel-heading">
                            Versionamento
                        </header>                        
                        <div class="panel-body">
                            <input class="form-control m-bot15" type="text" placeholder="Informe o produto">
                            <input class="form-control m-bot15" type="text" placeholder="Informe a versão">
                            <input class="form-control m-bot15" type="text" placeholder="Informe a reversão">
                            <button type="button" class="btn btn-primary">Gerar .zip</button>                            
                        </div>
                    </section>
                </div>
            </div>            
            
        </section>
    </section>
        <script src="${pathTemaTemplate}/js/mini-upload-form/assets/js/jquery.knob.js"></script>
        <script src="${pathTemaTemplate}/js/mini-upload-form/assets/js/jquery.ui.widget.js"></script>
        <script src="${pathTemaTemplate}/js/mini-upload-form/assets/js/jquery.iframe-transport.js"></script>
        <script src="${pathTemaTemplate}/js/mini-upload-form/assets/js/jquery.fileupload.js"></script>
        <script src="${pathTemaTemplate}/js/mini-upload-form/assets/js/script.js"></script>
        <script src="${pathTemaTemplate}/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="${pathTemaTemplate}/js/jquery.scrollTo.min.js"></script>
        <script src="${pathTemaTemplate}/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
        <script src="${pathTemaTemplate}/js/jquery.nicescroll.js"></script>
        <script src="${pathTemaTemplate}/js/skycons/skycons.js"></script>
        <script src="${pathTemaTemplate}/js/jquery.scrollTo/jquery.scrollTo.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="${pathTemaTemplate}/js/calendar/clndr.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script> 
        <script type="text/javascript" src="${pathResources}/js/zip/index.js"></script>
    </jsp:body>
</tags:template>

