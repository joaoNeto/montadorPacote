<%-- 
    Document   : error
    Created on : 04/09/2017, 15:05:06
    Author     : danilo.muniz
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <c:url value="/resources/tema" var="cssTemaPath" />
        <link rel="stylesheet" href="${cssTemaPath}/bs3/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="${cssTemaPath}/css/bootstrap-reset.css"/>
        <link rel="stylesheet" href="${cssTemaPath}/font-awesome/css/font-awesome.css"/>        
        <link rel="stylesheet" href="${cssTemaPath}/css/style.css"/>        
        <link rel="stylesheet" href="${cssTemaPath}/css/style-responsive.css"/>                
        <title>500</title>

    </head>
    <body class="body-500">
        <div class="error-head"> </div>
        <div class="container">
            <section class="error-wrapper text-center">
                <h1><img src="${cssTemaPath}/images/500.png" alt=""></h1>
                <div class="error-desk">
                    <h2>OOOPS!!!</h2>
                    <p class="nrml-txt-alt">Something went wrong.</p>
                    <p>Why not try refreshing you page? Or you can <a href="#" class="sp-link">contact our support</a> if the problem persists.</p>
                </div>
                <a href="${s:mvcUrl('HC#index').build()}" class="back-btn"><i class="fa fa-home"></i> Back To Home</a>
            </section>
        </div>
    </body>
</html>
