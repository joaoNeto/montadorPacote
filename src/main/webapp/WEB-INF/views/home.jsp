<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="tags"%>

<tags:template titulo="home">
    <jsp:attribute name="extraScripts">	              
        <link rel="stylesheet" type="text/css" href="${pathResources}/css/home/home.css">
        <link rel="stylesheet" type="text/css" href="${pathResources}/css/home/main.css">
    </jsp:attribute>
    <jsp:body>         
        <div class="modal-carregar-tortoise" style="display:none;"><font class="texto-modal-carregar-tortoise"></font></div>
        <div class="modal-gerar-pacote">
            <font class='fechar-modal'>X</font>
            <font class="titulo" >Gerador de pacotes informata!</font>
            <input type="text" name="nome_pacote" placeholder="Digite o nome do produto!" id="nome-pacote"  maxlength="25">
            <input type="text" name="versao_banco" 	placeholder="Release Aplicação" id="nome-versao-banco"  maxlength="25" title="Digite a versão do release há ser aplicada no banco!">
            <input type="text" name="versao_reversao" placeholder="Release Reversão" id="nome-versao-reversao"  maxlength="25" title="Digite a versão do release de reversão!">
            <div class="baixar-file">
                <input type="button" value="Gerar pacote" class="gerar-pacote-dinamico estilo-button-pacote">
            </div>
            <font class="observacao">Obs.: o backup do pacote gerado estará na maquina 192.168.0.232 em E:/app/Monta_Pacote/(ano)/(mes)/(dia)/(nome do projeto)_(release da aplicacao) </font>
        </div>
        
    <section id="main-content" class="merge-left">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12 text-center">
                    <form id="formulario" name="formulario" method="post" enctype="multipart/form-data">
                        <label for='selecao-arquivo-files' type="button" class="subir-arquivo-files btn btn-primary col-md-2 col-md-offset-3">upload por arquivo</label>
                        <input id='selecao-arquivo-files'  type='file' name="arquivo[]" multiple >
                        <label type="button" id="subir-arquivo-tortoise" class="btn btn-primary col-md-2  col-md-offset-2">upload pelo tortoise</label>
                    </form>                    
                    <div id="urlRepo" class="col-md-12" style="display:none">
                        <input type="text" name="nome_pacote" placeholder="Digite a url do repositorio tortoise!" class="col-md-5 url-repo form-repo style-input-repo big-input">
                        <input type="text" name="nome_pacote" placeholder="usuario" class="col-md-2 usuario-repo form-repo style-input-repo litle-input">
                        <input type="password" name="nome_pacote" placeholder="senha" class="col-md-2 senha-repo form-repo style-input-repo litle-input">
                        <i class="fa fa-check upload-file-tortoise style-btn-tortoise col-md-1" aria-hidden="true"></i>		    
                        <i class="fa fa-times esconderUrlRepo style-btn-tortoise col-md-1" aria-hidden="true"></i>
                    </div>   
                </div>                
            </div>
                        
            <div class="row">
                <div class="col-md-6 text-center text-muted"><h2>Banco</h2></div>
                <div class="col-md-6 text-center text-muted"><h2>Reversao</h2></div>                
            </div>
            
                <div class="conteudo col-md-12">

                    <div class="style-div-banco banco col-md-6">
                        <font id="titulo-banco" class="col-md-6">PROCEDURE</font>
                        <font id="titulo-banco" class="col-md-6">SCRIPT</font>
                        <div class="subpastas col-md-6">		
                            <font class="infomdlog"></font>
                        </div>
                        <div class="subpastas col-md-6">
                            <div id='listar-arquivo-dba' class="col-md-12">
                                <font id='subtitulo-dbamdata'>Raiz</font>
                                <font class="dbamdata"></font>
                            </div>
                            <div id='listar-arquivo-scripts-dba' class="col-md-12">
                                <font id='subtitulo-dbamdata'>Trigger</font>
                                <font class="dbamdatatrigger"></font>
                            </div>
                            <div id='listar-arquivo-scripts-dba' class="col-md-12">
                                <font id='subtitulo-dbamdata'>Grant</font>
                                <font class="dbamdatagrant"></font>
                            </div>
                            <div id='listar-arquivo-scripts-dba' class="col-md-12">
                                <font id='subtitulo-dbamdata'>Scripts</font>
                                <font class="dbamdatascripts"></font>
                            </div>
                        </div>
                    </div>
                    <div class="style-div-banco reversao col-md-6">
                        <font id="titulo-banco" class="col-md-6">PROCEDURE</font>
                        <font id="titulo-banco" class="col-md-6">SCRIPT</font>
                        <div class="subpastas col-md-6">		
                            <font class="infomdlog"></font>
                        </div>
                        <div class="subpastas col-md-6">
                            <div id='listar-arquivo-dba' class="col-md-12">
                                <font id='subtitulo-dbamdata'>Raiz</font>
                                <font class="dbamdata"></font>
                            </div>
                            <div id='listar-arquivo-scripts-dba' class="col-md-12">
                                <font id='subtitulo-dbamdata'>Trigger</font>
                                <font class="dbamdatatrigger"></font>
                            </div>
                            <div id='listar-arquivo-scripts-dba' class="col-md-12">
                                <font id='subtitulo-dbamdata'>Grant</font>
                                <font class="dbamdatagrant"></font>
                            </div>
                            <div id='listar-arquivo-scripts-dba' class="col-md-12">
                                <font id='subtitulo-dbamdata'>Scripts</font>
                                <font class="dbamdatascripts"></font>
                            </div>
                        </div>
                    </div>
                </div>
                    
                    <div class="trash-style col-md-12">
                        <i class="fa fa-trash-o fa-2x excluir-todos-arquivos" aria-hidden="true"></i>
                    </div>      
                
                    <div class="rodape col-md-12">
                        <form>
                            <input type="text" name="nome_pacote"  placeholder="Digite o nome do produto!" class="nome-pacote col-md-3">
                            <input type="text" name="versao_banco"  placeholder="Release Aplicação"     class="nome-versao-banco col-md-3" title="Digite a versão do release há ser aplicada no banco!">
                            <input type="text" name="versao_reversao"  placeholder="Release Reversão"   class="nome-versao-reversao col-md-3" title="Digite a versão do release de reversão!" >
                            <div class="baixar-file-pack">
                                <input type="button" value="Gerar pacote" class="gerar-pacote-files gerar-pacote estilo-button-pacote col-md-3" disabled="" style="background: #999;opacity: 0.5;">
                            </div>
                        </form>
                    </div>
                
            </section>
        </section>
        
        
        
        
        
<!----------------------- MODAL DA LEITURA DO ARQUIVO ------------------------->        
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalLerArquivo" class="modal fade" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content modal-ler-arquivo">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title titulo-arquivo"></h4>
            </div>
            <div class="modal-body conteudo-arquivo">
                
            </div>
        </div>
    </div>
</div>             
        
        
<!------------------------ MODAL DA MOVER DO ARQUIVO -------------------------->        
<div aria-hidden="true" aria-labelledby="modalMoverArquivo" role="dialog" tabindex="-1" id="modalMoverArquivo" class="modal fade" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content modal-mover-arquivo">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close close-modal-mover" type="button">×</button>
                <h4 class="modal-title titulo-arquivo"></h4>
            </div>
            <div class="modal-body conteudo-arquivo">
                
            </div>
        </div>
    </div>
</div>
        
        <script type="text/javascript" src="${pathResources}/js/home/home.js"></script>
        <script type="text/javascript" src="${pathResources}/js/home/index.js"></script>
    </jsp:body>
</tags:template>

