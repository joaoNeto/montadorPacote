<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- 
    Document   : header
    Created on : 04/09/2017, 15:05:33
    Author     : danilo.muniz
--%>
<!--header start-->
<header class="header fixed-top clearfix">
<!--logo start-->
<div class="brand">
    <a href="${s:mvcUrl('HC#index').build()}" class="logo">
        <img class="img-logo" src="${pathResources}/img/logo.png" alt="">
    </a>
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars"></div>
    </div>
</div>
<div class="nav notify-row" id="top_menu">
    <!--  notification start -->
    <ul class="nav top-menu">
        <!-- settings start -->
        <li class="dropdown">
            <a class="dropdown-toggle" href="${s:mvcUrl('WC#index').build()}">
                <i class="fa fa-unlock"></i>
            </a>
        </li>
        <!-- settings end -->
        <!-- inbox dropdown start-->
        <li id="header_inbox_bar" class="dropdown">
            <a class="dropdown-toggle" href="${s:mvcUrl('VC#index').build()}">
                <i class="fa fa-archive"></i>
            </a>
        </li>
        <!-- inbox dropdown end -->
        <!-- notification dropdown start-->
        <li id="header_notification_bar" class="dropdown">
            <a class="dropdown-toggle" href="${s:mvcUrl('HC#index').build()}">
                <i class="fa fa-cloud-upload"></i>
            </a>
        </li>
        <!-- notification dropdown end -->
    </ul>
    <!--  notification end -->
</div>
<div class="top-nav clearfix">    
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
        <!-- user login dropdown start-->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img alt="" src="${pathTemaTemplate}/images/avatar1_small.jpg">
                <c:if test="${sessionScope.loginNomeStakeholder == null}">
                    <span id="username" class="username">Cliente : Informata   |  Usu�rio : Usu�rio</span>
                </c:if>                
                <c:if test="${sessionScope.loginNomeStakeholder != null}">
                   <span id="username" class="username">Cliente: <font class="nomeCliente"><c:out value="${sessionScope.loginNomeCliente}"></c:out></font> | Usu�rio: <c:out value="${sessionScope.loginNomeStakeholder}"></c:out></span>
                </c:if>                
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
                <li><a href="${s:mvcUrl('LC#logout').build()}"><i class="fa fa-key"></i>Desconectar</a></li>
            </ul>
        </li>
         <!--user login dropdown end--> 
    </ul>
    <!--search & user info end-->
</div>
</header>
<!--header end-->
