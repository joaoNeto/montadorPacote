package br.com.informata.customerline.infra;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author danilo.muniz
 */
public class MapViewMonitorProperties {

    protected final Map map;

    public Map getMap() {
        return map;
    }

    public MapViewMonitorProperties() {
        map = new LinkedHashMap();

        /*--------------- CONEXÃO COM O ORACLE PARA PEGAR ARQUIVOS ---------------*/
        map.put(NomesParametrosViewMonitorProperties.ORACLE_BD_USER, "system");
        map.put(NomesParametrosViewMonitorProperties.ORACLE_BD_PASS, "pacote");
        map.put(NomesParametrosViewMonitorProperties.ORACLE_BD_DRIVER, "oracle.jdbc.driver.OracleDriver");
        map.put(NomesParametrosViewMonitorProperties.ORACLE_STR_BANCO,"jdbc:oracle:thin:@192.168.0.232:1521:pacote");

        /*----------------- CONEXÃO COM O ONE STUDIO PARA LOGIN ------------------*/
        map.put(NomesParametrosViewMonitorProperties.SQL_SERVER_BD_USER, "sa");
        map.put(NomesParametrosViewMonitorProperties.SQL_SERVER_BD_PASS, "");        
        map.put(NomesParametrosViewMonitorProperties.SQL_SERVER_BD_DRIVER, "net.sourceforge.jtds.jdbc.Driver");
        map.put(NomesParametrosViewMonitorProperties.SQL_SERVER_STR_BANCO, "jdbc:jtds:sqlserver://192.168.0.20:1433;databaseName=innovativeSuiteHomologa");
    }
}
