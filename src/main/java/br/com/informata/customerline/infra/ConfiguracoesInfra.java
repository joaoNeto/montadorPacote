/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.customerline.infra;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author helio.sales
 * @author danilo.muniz
 *
 * Executa a configuração da estrutura para configuração da aplicação.
 */
public class ConfiguracoesInfra {

    private final Properties viewMonitorProperties;
    private final MapViewMonitorProperties listaMapInfoJobProperties;

    public ConfiguracoesInfra(MapViewMonitorProperties listaMapInfoJobProperties) {
        this.viewMonitorProperties = new Properties();
        this.listaMapInfoJobProperties = listaMapInfoJobProperties;
    }

    private OutputStream stream;

    /**
     * Configura o ambiente da pasta do usuario onde fica o arquivos properties,
     * o arquivos log local e os schemas dos json.
     */
    public void configurar() {
        criarEstruturaDePastaDoInfoJob();
        criarArquivoDeConfiguracaoSeNaoExistir();
    }

    /**
     * Método capaz de criar as pastas: informata, infoJob, schemas e o log na
     * pasta do usuario.
     */
    private void criarEstruturaDePastaDoInfoJob() {
        if (pastaInformataNaoExiste()) {
            Arquivo.criarPasta(ConstanteEstrutura.PASTA_INFORMATA);
        }
        if (pastaInfoJobNaoExiste()) {
            Arquivo.criarPasta(ConstanteEstrutura.PASTA_VIEW_MONITOR);
        }
    }

    /**
     * Método que verifica e a pasta informata não exite na pasta do usuário.
     *
     * @return Retorna True se não existir, caso contrario False se existir.
     */
    private boolean pastaInformataNaoExiste() {
        return !Arquivo.arquivoExiste(ConstanteEstrutura.PASTA_INFORMATA);
    }

    /**
     * Método que verifica e a pasta InfoJob não exite na pasta do usuário.
     *
     * @return Retorna True se não existir, caso contrario False se existir.
     */
    private boolean pastaInfoJobNaoExiste() {
        return !Arquivo.arquivoExiste(ConstanteEstrutura.PASTA_VIEW_MONITOR);
    }

    /**
     * Método que criar o arquivo de configuração do banco "viewmonitor.properties"
     * carrega as inforamações padrões e salva no arquivo properties. Essas
     * informações do banco poderam ser substituidas pelo banco do cliente.
     */
    private void criarArquivoDeConfiguracaoSeNaoExistir() {
        if (arquivoDeConfiguracaoNaoExiste()) {
            try {
                stream = Arquivo.criarStreamDeArquivo(ConstanteEstrutura.PASTA_VIEW_MONITOR + File.separator + ConstanteEstrutura.ARQUIVO_CONFIGURACAO);
                carregarPropertieComConfiguracaoPadrao();
                salvarArquivoConfiguracao();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ConfiguracoesInfra.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Método capaz de salvar as informações no arquivo
     * "viewmonitor.properties".
     */
    private void salvarArquivoConfiguracao() {
        try {
            this.viewMonitorProperties.store(stream, null);
        } catch (IOException ex) {
            Logger.getLogger(ConfiguracoesInfra.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método que verifica se o arquivo "viewmonitor.properties" não existe.
     *
     * @return Retorna True se não existir, caso contrario False se existir.
     */
    private boolean arquivoDeConfiguracaoNaoExiste() {
        return !Arquivo.arquivoExiste(ConstanteEstrutura.PASTA_VIEW_MONITOR + File.separator + ConstanteEstrutura.ARQUIVO_CONFIGURACAO);
    }

    /**
     * Método capaz de carregar as informações no arquivo "viewmonitor.properties"
     * na pasta da informata contida na pasta do usuário.
     */
    private void carregarPropertieComConfiguracaoPadrao() {
        if (listaMapInfoJobProperties != null) {
            Map<String, String> mapProperties = (Map<String, String>) listaMapInfoJobProperties.getMap();
            for (Map.Entry<String, String> entry : mapProperties.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                viewMonitorProperties.setProperty(key, value);
            }
        }
    }

}
