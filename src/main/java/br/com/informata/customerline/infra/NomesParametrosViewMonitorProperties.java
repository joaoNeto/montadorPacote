package br.com.informata.customerline.infra;

/**
 * Clase com as constantes dos nomes dos parametros que serão usados no
 * infows.properties
 *
 * @author danilo.muniz
 * @version 0.4
 * @since 19/08/2016
 */
public class NomesParametrosViewMonitorProperties {

    /*--------------- CONEXÃO COM O ORACLE PARA PEGAR ARQUIVOS ---------------*/
    public static final String ORACLE_BD_USER 	= "ORACLE_BD_USER";
    public static final String ORACLE_BD_PASS 	= "ORACLE_BD_PASS";
    public static final String ORACLE_BD_DRIVER = "ORACLE_BD_DRIVER";
    public static final String ORACLE_STR_BANCO = "ORACLE_STR_BANCO";

    /*----------------- CONEXÃO COM O ONE STUDIO PARA LOGIN ------------------*/
    public static final String SQL_SERVER_BD_USER   = "SQL_SERVER_BD_USER";
    public static final String SQL_SERVER_BD_PASS   = "SQL_SERVER_BD_PASS";
    public static final String SQL_SERVER_BD_DRIVER = "SQL_SERVER_BD_DRIVER";
    public static final String SQL_SERVER_STR_BANCO = "SQL_SERVER_STR_BANCO";

}
