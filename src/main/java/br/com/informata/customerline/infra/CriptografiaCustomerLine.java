/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.customerline.infra;

import java.io.UnsupportedEncodingException;

/**
 *
 * @author danilomuniz
 */
public class CriptografiaCustomerLine {

    private boolean In(char c, String Conj) {
        boolean result = false;
        for (int i = 0; i < Conj.length(); i++) {
            if (Conj.charAt(i) == c) {
                result = true;
            }
        }
        return result;
    }

    public String Crypt(String Word) {
        String text = "";
        String conj = "abcdefghijklmnopqrstuvwxy";
        while (!Word.equals("")) {
            char c = Word.charAt(0);
            Word = Word.substring(1);
            if (this.In(Character.toLowerCase(c), conj)) {
                int num = (int) (c + '\u0001');
                c = (char) num;
                text += c;
            } else if (c == 'z') {
                text += "a";
            } else if (c == 'Z') {
                text += "A";
            } else {
//                text += String.valueOf(9 - ((int) c));
                text += String.valueOf(9 - Integer.parseInt(String.valueOf(c)));
            }
        }
        return text;
    }

    public String DeCrypt(String Word)  {
        String text = "";
        String conj = "bcdefghijklmnopqrstuvwxyz";
        while (!Word.equals("")) {
            char c = Word.charAt(0);
            Word = Word.substring(1);
            if (this.In(Character.toLowerCase(c), conj)) {
                int num = (int) (c - '\u0001');
                c = (char) num;
                text += c;
            } else if (c == 'a') {
                text += "z";
            } else if (c == 'A') {
                text += "Z";
            } else {
                text += String.valueOf(57 - ((int) c));
            }
        }
        return text;
    }

}
