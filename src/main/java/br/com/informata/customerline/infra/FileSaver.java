package br.com.informata.customerline.infra;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileSaver {

    @Autowired
    private HttpServletRequest request;

    public String write(String baseFolder, MultipartFile file) {
        try {
            // request.getServletContext().getRealPath("/") // caminho do arquivo
            file.transferTo(new File(baseFolder+file.getOriginalFilename()));
            return baseFolder+file.getOriginalFilename();
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return null;
    }

    public String write(String baseFolder, MultipartFile file, String novoNome) {
        try {
            String realPath2 = request.getServletContext().getRealPath("/");
            String realPath = request.getServletContext().getRealPath("/" + baseFolder);
            String path = realPath + "/" + novoNome;
            //file.transferTo(new File(path));
            file.transferTo(new File("C:" + File.separator + "wamp" + File.separator + "www" + File.separator + "infows_rv" + File.separator + "CustomerLine" + File.separator + "repositorio" + File.separator + novoNome));
            return baseFolder + "/" + file.getOriginalFilename();
        } catch (IllegalStateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

}
