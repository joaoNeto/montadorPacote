package br.com.informata.customerline.infra;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 *
 * @author danilo.muniz
 */
public class Arquivo {

    /**
     *
     * @param caminhoArquivo
     * @param conteudo
     */
    public static void criarArquivo(String caminhoArquivo, String conteudo) {

        byte[] jsonArray = conteudo.getBytes();
        try {
            File arquivoJson = new File(caminhoArquivo);

            BufferedOutputStream bos;

            bos = new BufferedOutputStream(new FileOutputStream(arquivoJson));
            bos.write(jsonArray);
            bos.flush();
            bos.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            jsonArray = null;
            System.gc();
        }

    }

    /**
     *
     * @param caminho
     */
    public static void criarPasta(String caminho) {
        File pasta = new File(caminho);
        if (!pasta.exists()) {
            try {
                pasta.mkdirs();
            } catch (SecurityException se) {
                // Falta LOG
            }
        }
    }

    /**
     *
     * @param caminhoPasta
     * @param extensao
     */
    public static void apagarArquivos(String caminhoPasta, String extensao) {
        File pasta = new File(caminhoPasta);
        if (pasta.exists()) {
            File listaArquivos[] = pasta.listFiles();
            for (File listaArquivo : listaArquivos) {
                String arquivo = listaArquivo.getAbsolutePath();
                if (arquivo.endsWith("." + extensao)) {
                    new File(arquivo).delete();
                }
            }
        }
    }

    /**
     *
     * @param caminho
     */
    public static void apagarArquivo(String caminho) {
        File arquivo = new File(caminho);
        if (arquivo.exists()) {
            arquivo.delete();
        }
    }

    /**
     *
     * @param caminho
     * @return
     */
    public static boolean arquivoExiste(String caminho) {
        boolean retorno = false;
        File arquivo = new File(caminho);
        if (arquivo.exists()) {
            retorno = true;
        }
        return retorno;
    }

    /**
     *
     * @param caminho
     * @return
     * @throws FileNotFoundException
     */
    public static OutputStream criarStreamDeArquivo(String caminho) throws FileNotFoundException {
        OutputStream output = null;
        try {
            output = new FileOutputStream(new File(caminho));
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException(e.getMessage());
        }
        return output;
    }

    public static OutputStream carregaOutputStreamDeArquivo(String caminho) throws FileNotFoundException {
        OutputStream output = null;
        try {
            output = new FileOutputStream(caminho);
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException(e.getMessage());
        }
        return output;
    }

    public static InputStream carregaInputStreamDeArquivo(String caminho) throws FileNotFoundException {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(caminho);
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException(e.getMessage());
        }
        return inputStream;
    }

    public static void gravaTexto(String caminho, String texto) {
        try {
            FileWriter arq = new FileWriter(caminho);
            PrintWriter gravarArq = new PrintWriter(arq);
            gravarArq.printf(texto + "%n");
            arq.close();
        } catch (Exception e) {
            System.out.println("Erro ao grava o arquivo: " + e.getLocalizedMessage());
        }
    }
}
