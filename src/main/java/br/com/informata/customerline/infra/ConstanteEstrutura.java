/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.customerline.infra;

import java.io.File;

/**
 *
 * @author danilomuniz
 */
public class ConstanteEstrutura {
          
    public static final String PASTA_USUARIO = System.getProperty("user.home");
    
    public static final String PASTA_PROJETO = System.getProperty("user.dir");
    
    public static final String PASTA_TEMP = System.getProperty("java.io.tmpdir");
    
    public static final String PASTA_INFORMATA = ConstanteEstrutura.PASTA_USUARIO + File.separator + "Informata";
    
    public static final String PASTA_VIEW_MONITOR = ConstanteEstrutura.PASTA_INFORMATA + File.separator + "montadorPacote";
    
    public static final String ARQUIVO_CONFIGURACAO =  "montadorPacote.properties";            
    
}
