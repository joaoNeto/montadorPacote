/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.customerline.infra;

import br.com.informata.customerline.dao.onestudio.UsuarioDAO;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * @author danilomuniz
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UsuarioDAO usuarioDAO;

    public CustomAuthenticationProvider() {
    }

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {
        HttpServletRequest curRequest = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String mensagemAutenticacao = "";
        String name = authentication.getName();
        String senha = String.valueOf(authentication.getCredentials());
        String[] arrayValores = name.split(";");
        CriptografiaCustomerLine criptografiaCustomerLine = new CriptografiaCustomerLine();
        Map dadosUsuario = usuarioDAO.validarLogin(arrayValores[0], arrayValores[1], criptografiaCustomerLine.Crypt(senha));
        if (dadosUsuario != null && dadosUsuario.size() > 0) {
            if (usuarioDAO.verificarPermissaoUsuarioParaSistema(1027, 3010, (int) dadosUsuario.get("codigoUsuario"))) {
                Map dadosCliente = usuarioDAO.obterDadosClientePorCodigo((int) dadosUsuario.get("codigoCliente"));
                if (!((String) dadosCliente.get("statusCliente")).equalsIgnoreCase("A")) {
                    mensagemAutenticacao = "Falha na autênticação, Cliente status não aprovado.";
                    curRequest.getSession().setAttribute("mensagemAutenticacao", mensagemAutenticacao);
                    throw new BadCredentialsException(mensagemAutenticacao);
                } else {
                    //atribuir os dados na session do usuário logado...                     
                    curRequest.getSession().setAttribute("loginCodigoUsuario", (int) dadosUsuario.get("codigoUsuario"));
                    curRequest.getSession().setAttribute("loginNomeUsuario", (String) arrayValores[1]);
                    curRequest.getSession().setAttribute("loginCodigoCliente", (int) dadosCliente.get("codigoCliente"));
                    curRequest.getSession().setAttribute("loginNomeCliente", (String) dadosCliente.get("nomeCliente"));
                    curRequest.getSession().setAttribute("loginCodigoStakeholder", (int) dadosUsuario.get("codigoStakeholder"));
                    curRequest.getSession().setAttribute("loginNomeStakeholder", (String) usuarioDAO.obterNomeStakeholderPorCodigo((int) dadosUsuario.get("codigoStakeholder")));
                    curRequest.getSession().setAttribute("loginCodigoProfileRole", (int) dadosUsuario.get("codigoProfileRole"));
                }

            } else {
                mensagemAutenticacao = "Falha na autênticação, permissão negada.";
                curRequest.getSession().setAttribute("mensagemAutenticacao", mensagemAutenticacao);
                throw new BadCredentialsException(mensagemAutenticacao);
            }

        } else {
            mensagemAutenticacao = "Falha na autênticação, dados inválidos.";
            curRequest.getSession().setAttribute("mensagemAutenticacao", mensagemAutenticacao);
            throw new BadCredentialsException(mensagemAutenticacao);
        }
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        Authentication auth = new UsernamePasswordAuthenticationToken(name, senha, grantedAuthorities);
        return auth;
    }

    @Override
    public boolean supports(Class<?> arg0) {
        return true;
    }

}
