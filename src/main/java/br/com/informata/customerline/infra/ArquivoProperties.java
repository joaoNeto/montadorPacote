package br.com.informata.customerline.infra;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe que carrega as configurações do properties.
 *
 * @author flavio.araujo
 * @autor danilo.muniz
 */
public final class ArquivoProperties {

    private static ArquivoProperties instance;
    private Properties viewMonitorProperties;
    private static InputStream stream = null;

    private ArquivoProperties() {
        carregarProperties();
    }

    private Properties carregarProperties() {
        try {
            viewMonitorProperties = new Properties();
            stream = new FileInputStream(ConstanteEstrutura.PASTA_VIEW_MONITOR + File.separator + ConstanteEstrutura.ARQUIVO_CONFIGURACAO);
            viewMonitorProperties.load(stream);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ArquivoProperties.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ArquivoProperties.class.getName()).log(Level.SEVERE, null, ex);
        }
        return viewMonitorProperties;
    }

    /**
     *
     * @return
     */
    public static ArquivoProperties getInstance() {
        if (instance == null) {
            instance = new ArquivoProperties();
        }
        return instance;
    }

    /**
     *
     * @return
     */
    public Properties getViewMonitorProperties() {
        return this.viewMonitorProperties;
    }

    /**
     *
     * @param parametro
     * @return
     */
    public String obterValueDeParametro(String parametro) {
        return viewMonitorProperties.getProperty(parametro);
    }

}
