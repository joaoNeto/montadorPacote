package br.com.informata.customerline.util;

import br.com.informata.customerline.conf.Constante;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author joao.neto
 */
public class Compactador {

    static final int TAMANHO_BUFFER = 40096;

    /**
     * pega todas as pastas que estiver em upload/ e zipa colocando em sessaoUsuario/zip/nomeArquivo
     *
     * @param usuarioLogado
     * @param nomeArquivo
     * @param pasta
     * @param temSubdiretorio
     * @param deley
     * @return {"status" : true, "data" : dirZip}
     */
    public static JSONObject compactarParaZip(String usuarioLogado, String nomeArquivo, String pasta, Boolean temSubdiretorio, Boolean deley) {

        String dirZip = Constante.PATH_SESSAO;
        dirZip += usuarioLogado + File.separator;
        dirZip += Constante.PATH_ZIP + File.separator;
        dirZip += nomeArquivo + ".zip";

        String dirUpload = Constante.PATH_SESSAO;
        dirUpload += usuarioLogado + File.separator;
        dirUpload += pasta + File.separator;

        final byte[] dados = new byte[TAMANHO_BUFFER];
        int cont;
        String[] arrFiles = null;
        try {
            final FileOutputStream destino = new FileOutputStream(new File(dirZip));
            final ZipOutputStream saida = new ZipOutputStream(new BufferedOutputStream(destino));

            if (temSubdiretorio) {
                for (String pastasnvl1 : Constante.subpasta_nvl1) {
                    for (int i = 0; i < Constante.subpasta_nvl2.length; i++) {
                        String diretoriosUpload = pastasnvl1 + File.separator + Constante.subpasta_nvl2[i] + File.separator;
                        arrFiles = new File(dirUpload + diretoriosUpload).list();
                        for (String arquivo : arrFiles) {
                            File file = new File(dirUpload + diretoriosUpload + arquivo);
                            if (file.isFile()) {
                                final FileInputStream streamDeEntrada = new FileInputStream(file);
                                final BufferedInputStream origem = new BufferedInputStream(streamDeEntrada, TAMANHO_BUFFER);
                                final ZipEntry entry = new ZipEntry(diretoriosUpload + file.getName());
                                saida.putNextEntry(entry);
                                while ((cont = origem.read(dados, 0, TAMANHO_BUFFER)) != -1) {
                                    saida.write(dados, 0, cont);
                                }
                                origem.close();
                                streamDeEntrada.close();
                            }
                        }
                    }
                }
            } else {
                arrFiles = new File(dirUpload).list();
                for (String arquivo : arrFiles) {
                    File file = new File(dirUpload + arquivo);
                    if (file.isFile()) {
                        final FileInputStream streamDeEntrada = new FileInputStream(file);
                        final BufferedInputStream origem = new BufferedInputStream(streamDeEntrada, TAMANHO_BUFFER);
                        final ZipEntry entry = new ZipEntry(file.getName());
                        saida.putNextEntry(entry);
                        while ((cont = origem.read(dados, 0, TAMANHO_BUFFER)) != -1) {
                            saida.write(dados, 0, cont);
                        }
                        origem.close();
                        streamDeEntrada.close();
                    }
                }
            }
            saida.close();
            destino.close();
            if(deley){
                Compactador.executaCalculoTempoPorQuantidadeArquivosEspera(((arrFiles != null) ? arrFiles.length : 0));
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }

        return servletsUtil.retorno(true, usuarioLogado + File.separator + Constante.PATH_ZIP + File.separator + nomeArquivo + ".zip");
    }

    private static void executaCalculoTempoPorQuantidadeArquivosEspera(int quantidadeArquivos) throws InterruptedException {
        //calculo com base na quantidades de arquivos do 
        int tempoEspera = 2000;
        tempoEspera += ((quantidadeArquivos * 100) / 2);
        System.out.println("tempoEspera: " + tempoEspera);
        Thread.sleep(tempoEspera);
    }

}
