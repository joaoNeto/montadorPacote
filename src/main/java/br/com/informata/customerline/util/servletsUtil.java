/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.customerline.util;

import java.util.HashMap;
import java.util.Map;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author joaoNeto
 */
public class servletsUtil {

    public static JSONObject retorno(Boolean status, String mensagem){
        JSONObject jsonObj = new JSONObject();
        try{
            jsonObj.put("status", status);
            jsonObj.put("data",mensagem);
        }catch(Exception ex){
            System.out.println("error servlet util");
        }
        return jsonObj;
    }
    
}
