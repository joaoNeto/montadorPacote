/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.customerline.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

/**
 *
 * @author joao.neto
 */
public class ModelUtil {

    @PersistenceContext
    private EntityManager manager;    
    
    public List<Object[]> executarQuery(String sql){
        Session session = manager.unwrap(Session.class);
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        return sqlQuery.list();          
    }    
    
    public ArrayList execQueryReturnArrayList(String sql)
    {
        ArrayList array = new ArrayList();   
        List<Object[]> results = executarQuery(sql);           
        for(Object[] t : results){
            array.add(t);
        }
        return array;
    }    

    
    // os parametros devem estar na ordem das interrogações da query
    public ArrayList execQueryReturnArrayList(String sql, ArrayList param)
    {
        ArrayList array = new ArrayList();   
        
        Session session = manager.unwrap(Session.class);
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        
        int tamParam = param.size();
        for(int i = 0 ; i < tamParam ; i++)
            sqlQuery.setParameter(i, param.get(i));
        
        // .addScalar("name",StandardBasicTypes.STRING) 
        
        List<Object[]> results =  sqlQuery.list();
        for(Object[] t : results){
            array.add(t);
        }
        return array;
    }    

    
    public List<Integer> executarQueryReturnListInt(String sql){
        Session session = manager.unwrap(Session.class);
        SQLQuery sqlQuery = session.createSQLQuery(sql);
        return sqlQuery.list();          
    }    
    
    
}
