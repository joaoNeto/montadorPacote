/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.customerline.conf;

import java.io.File;

/**
 *
 * @author joaoNeto
 */
public class Constante {
    
    /*--------------------- DIRETORIO PARA FAZER O WRAP ----------------------*/
    //public static final String PATH_WRAP     = "E:\\app\\oracle\\product\\11.2.0\\dbhome_2";
    //public static final String PATH_PACK_DINAMIC = "E:\\app\\Monta_Pacote\\";          
    
    /*---------------------- PASTAS PARA SESSÃO USUARIO ----------------------*/
    // Diretorio onde ira ficar a sessão do usuário
    public static final String PATH_SESSAO   = "C:"+File.separator+"wamp"+File.separator+"www"+File.separator+"ProjetosInternos"+File.separator+"MontadorRelease"+File.separator+"Versao_01.00.00.00"+File.separator+"src"+File.separator+"main"+File.separator+"webapp"+File.separator+"resources"+File.separator+"sessao"+File.separator;
    // Diretorio onde ficam todos os arquivos que são exibidos na tela do usuario logado
    public static final String PATH_FILES    = "upload";
    // Diretorio onde ficam todos os arquivos criptografados que vinheram de PATH_FILES
    public static final String PATH_FLE_CRI  = "upload_cripto";
    // Diretorio que ficam temporariamente os arquivos que são baixados pelo tortoise
    public static final String PATH_REPO_SVN = "upload_svn";
    // Diretorio que ficam temporariamente os arquivos que são baixados pelo usuário atravez do file upload
    public static final String PATH_UPL_FILE = "upload_file";    
    // Diretorio que ficam todos os pacotes gerados pelo usuário
    public static final String PATH_ZIP      = "zip";
    // Diretorio para ficar os arquivos que foram subidos pelo wrap
    public static final String PATH_WRAP     = "wrap";
    public static final String PATH_WRAP_WRAP   = PATH_WRAP+File.separator+"wrap";
    public static final String PATH_WRAP_UNWRAP = PATH_WRAP+File.separator+"unwrap";
    // array com as pastas que serão criadas ao se criar sessão do usuário
    public static String[] pasta = {PATH_FILES, PATH_FLE_CRI, PATH_REPO_SVN, PATH_ZIP, PATH_UPL_FILE, PATH_WRAP,PATH_WRAP_WRAP,PATH_WRAP_UNWRAP};
    // array com as pastas que irão ter subdiretorios
    public static String[] pastasComSubdiretorios  = {PATH_FILES, PATH_FLE_CRI};
    // primeira camada dos subdiretorios
    public static String[] subpasta_nvl1 = {"banco", "reversao"};
    // cada pasta da camada subpasta_nvl1 terá as pastas que tiverem em subpasta_nvl2
    public static String[] subpasta_nvl2 = {"dbamdata", "dbamdata"+File.separator+"grant", "dbamdata"+File.separator+"scripts", "dbamdata"+File.separator+"trigger", "infomdlog"};
    // mesma estrutura do subpasta_nvl2 só que preparado para ser manipulado em formato json
    public static String[] subpasta_nvl2_json = {"dbamdata", "dbamdatagrant", "dbamdatascripts", "dbamdatatrigger", "infomdlog"};
    
}
