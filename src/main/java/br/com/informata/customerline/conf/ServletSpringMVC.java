package br.com.informata.customerline.conf;

import br.com.informata.customerline.infra.ConfiguracoesInfra;
import br.com.informata.customerline.infra.MapViewMonitorProperties;
import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class ServletSpringMVC extends AbstractAnnotationConfigDispatcherServletInitializer {

    private final ConfiguracoesInfra configuracoesInfra;

    public ServletSpringMVC() {
        this.configuracoesInfra = new ConfiguracoesInfra(new MapViewMonitorProperties());
    }    
    
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{
            AppWebConfiguration.class,
            JPAConnectionOneStudio.class,
            JPAConnectionPacote.class,
            SecurityConfiguration.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    protected Filter[] getServletFilters() {
        CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
        encodingFilter.setEncoding("UTF-8");

        return new Filter[]{encodingFilter, new OpenEntityManagerInViewFilter()};
    }

    @Override
    protected void customizeRegistration(Dynamic registration) {
        registration.setMultipartConfig(new MultipartConfigElement(""));
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        super.onStartup(servletContext);        
        this.configuracoesInfra.configurar();
        servletContext.addListener(RequestContextListener.class);
        servletContext.setInitParameter("spring.profiles.active", "dev");
    }

}
