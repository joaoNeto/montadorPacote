package br.com.informata.customerline.conf;

import br.com.informata.customerline.infra.ArquivoProperties;
import br.com.informata.customerline.infra.NomesParametrosViewMonitorProperties;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.beans.PropertyVetoException;
import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaVendorAdapter;
/**
 *
 * @author joao.neto
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "oneStudioEntityManagerFactory",
        transactionManagerRef = "oneStudioTransactionManager",
        basePackages = {"br.com.informata.customerline.dao.onestudio"})
public class JPAConnectionOneStudio {
    
    @Primary
    @Bean(name = "oneStudioEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean oneStudioEntityManagerFactory() throws PropertyVetoException {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setPackagesToScan("br.com.informata.customerline.model.onestudio");
        factoryBean.setDataSource(oneStudioDataSource());        
        factoryBean.setPersistenceUnitName("oneStudioEntityManagerFactory");
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        factoryBean.setJpaVendorAdapter(vendorAdapter);
        factoryBean.setJpaProperties(oneStudioAdditionalProperties());

        return factoryBean;
    }
    
    @Primary
    @Bean
    public Properties oneStudioAdditionalProperties() {
        Properties props = new Properties();
        props.setProperty("hibernate.dialect", "org.hibernate.dialect.SQLServerDialect");
        props.setProperty("hibernate.show_sql", "true");
        props.setProperty("hibernate.cache.use_second_level_cache", "true");
        props.setProperty("hibernate.cache.use_query_cache", "true");
        props.setProperty("hibernate.cache.region.factory_class", "org.hibernate.cache.ehcache.SingletonEhCacheRegionFactory");
        return props;
    }

    @Primary
    @Bean
    public DataSource oneStudioDataSource() throws PropertyVetoException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setDriverClass(ArquivoProperties.getInstance().obterValueDeParametro(NomesParametrosViewMonitorProperties.SQL_SERVER_BD_DRIVER));
        dataSource.setUser(ArquivoProperties.getInstance().obterValueDeParametro(NomesParametrosViewMonitorProperties.SQL_SERVER_BD_USER));
        dataSource.setPassword(ArquivoProperties.getInstance().obterValueDeParametro(NomesParametrosViewMonitorProperties.SQL_SERVER_BD_PASS));
        dataSource.setJdbcUrl(ArquivoProperties.getInstance().obterValueDeParametro(NomesParametrosViewMonitorProperties.SQL_SERVER_STR_BANCO));
        dataSource.setMinPoolSize(3);
        dataSource.setMaxPoolSize(5);
        dataSource.setMaxStatements(80);
        return dataSource;
    }

    @Primary
    @Bean(name = "oneStudioTransactionManager")
    public JpaTransactionManager oneStudioTransactionManager(EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }

}
