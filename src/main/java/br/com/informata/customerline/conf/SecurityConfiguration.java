package br.com.informata.customerline.conf;

import br.com.informata.customerline.infra.CustomAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

//@EnableWebMvcSecurity
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

//    @Autowired
//    private UsuarioDAO usuarioDao;
    @Autowired
    private CustomAuthenticationProvider authProvider;

//    @Autowired
//    DataSource dataSource;
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        System.out.println("security.");
//		http.authorizeRequests()                            
//			.antMatchers("/produtos/form").hasRole("ADMIN")
//			.antMatchers("/carrinho/**").permitAll()
//			.antMatchers("/pagamento/**").permitAll()
//			.antMatchers(HttpMethod.POST, "/produtos").hasRole("ADMIN")
//			.antMatchers(HttpMethod.GET, "/produtos").hasRole("ADMIN")
//			.antMatchers("/produtos/**").permitAll()
//			.antMatchers("/resources/**").permitAll()
//			.antMatchers("/").permitAll()
//                        .antMatchers("/infocron/").permitAll()                        
//			.antMatchers("/url-magica-maluca-oajksfbvad6584i57j54f9684nvi658efnoewfmnvowefnoeijn").permitAll()
//			.anyRequest().authenticated()
//			.and().formLogin().loginPage("/login").permitAll();
//			.and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"));

        http.authorizeRequests()
                .antMatchers("/").access("hasRole('ADMIN') or hasRole('USER')")                                               
                .antMatchers("/versionamento/**").access("hasRole('ADMIN') or hasRole('USER')") 
                .antMatchers("/wrap/**").access("hasRole('ADMIN') or hasRole('USER')") 
                .and()
                .formLogin().loginPage("/login")
                .failureForwardUrl("/error")                
                .usernameParameter("username").passwordParameter("password")                
                .defaultSuccessUrl("/",true)                
                .and().csrf()
                .and().exceptionHandling().accessDeniedPage("/error");
    }

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
//        auth.jdbcAuthentication().dataSource(dataSource)
//        .usersByUsernameQuery("select username,password, enabled from users where username=?")
//	.authoritiesByUsernameQuery("select username, role from user_roles where username=?");
        
//        auth.inMemoryAuthentication().withUser("bill").password("abc123").roles("USER");
//        auth.inMemoryAuthentication().withUser("admin").password("root123").roles("ADMIN");
//        auth.inMemoryAuthentication().withUser("dba").password("root123").roles("ADMIN", "DBA");

        auth.authenticationProvider(authProvider);
    }

}
