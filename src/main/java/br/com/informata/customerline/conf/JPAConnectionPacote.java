/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.customerline.conf;

import br.com.informata.customerline.infra.ArquivoProperties;
import br.com.informata.customerline.infra.NomesParametrosViewMonitorProperties;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.beans.PropertyVetoException;
import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author joao.neto
 */

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "pacoteEntityManagerFactory",
        transactionManagerRef = "pacoteTransactionManager",        
        basePackages = {"br.com.informata.customerline.dao.pacote"})
public class JPAConnectionPacote {

    @Bean(name = "pacoteEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean pacoteEntityManagerFactory() throws PropertyVetoException {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setPackagesToScan("br.com.informata.customerline.model.pacote");
        factoryBean.setDataSource(pacoteDataSource());
        factoryBean.setPersistenceUnitName("pacoteEntityManagerFactory");
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        factoryBean.setJpaVendorAdapter(vendorAdapter);
        factoryBean.setJpaProperties(pacoteAdditionalProperties());

        return factoryBean;
    }

    @Bean
    public Properties pacoteAdditionalProperties() {
        Properties props = new Properties();
        props.setProperty("hibernate.dialect", "org.hibernate.dialect.SQLServerDialect");
        props.setProperty("hibernate.show_sql", "true");
        props.setProperty("hibernate.cache.use_second_level_cache", "true");
        props.setProperty("hibernate.cache.use_query_cache", "true");
        props.setProperty("hibernate.cache.region.factory_class", "org.hibernate.cache.ehcache.SingletonEhCacheRegionFactory");
        return props;
    }
    
    @Bean
    public DataSource pacoteDataSource() throws PropertyVetoException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setDriverClass(ArquivoProperties.getInstance().obterValueDeParametro(NomesParametrosViewMonitorProperties.ORACLE_BD_DRIVER));
        dataSource.setUser(ArquivoProperties.getInstance().obterValueDeParametro(NomesParametrosViewMonitorProperties.ORACLE_BD_USER));
        dataSource.setPassword(ArquivoProperties.getInstance().obterValueDeParametro(NomesParametrosViewMonitorProperties.ORACLE_BD_PASS));
        dataSource.setJdbcUrl(ArquivoProperties.getInstance().obterValueDeParametro(NomesParametrosViewMonitorProperties.ORACLE_STR_BANCO));
        dataSource.setMinPoolSize(3);
        dataSource.setMaxPoolSize(5);
        dataSource.setMaxStatements(80);
        return dataSource;
    }
    
    @Bean(name = "pacoteTransactionManager")
    public JpaTransactionManager pacoteTransactionManager(EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }

}
