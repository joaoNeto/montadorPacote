/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.customerline.service;

import br.com.informata.customerline.dao.HomeDAO;
import java.util.ArrayList;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author danilomuniz
 */
@Service
public class HomeService {

    @Autowired
    private HomeDAO homeDAO;

    public ArrayList obterDadosTotalizadorPorCodigoCostumer(String codigoClienteLogado){
        return homeDAO.obterDadosTotalizadorPorCodigoCostumer(codigoClienteLogado);
    }
    
    public JSONObject obterDadosGridPorCodigoProfileRole(Integer codigoProfileRole, Integer codigoClienteLogado, Integer codigoStackeholderLogado) {
        return homeDAO.obterDadosGridPorCodigoProfileRole(codigoProfileRole, codigoClienteLogado, codigoStackeholderLogado);
    }

}
