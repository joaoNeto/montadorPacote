package br.com.informata.customerline.service;

import br.com.informata.customerline.conf.Constante;
import br.com.informata.customerline.util.Compactador;
import br.com.informata.customerline.util.servletsUtil;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

/**
 *
 * @author joaoNeto
 */
public class GerenciarArquivosService {
    
    /**
     *Responsável por criar as pastas e subpastas necessarias para criar uma sessão para 
     *apenas aquele usuario manipular seus arquivos e gerar seus pacotes
     *@param  usuarioLogado
     */
    public static void criarSesaoUsuario(String usuarioLogado){
        String strDirUsuario = Constante.PATH_SESSAO+usuarioLogado+File.separator;
        
        new File(Constante.PATH_SESSAO).mkdir();
        new File(strDirUsuario).mkdir();
        for(String diretorio : Constante.pasta)
            new File(strDirUsuario+diretorio+File.separator).mkdir();
        
        for (String pasta : Constante.pastasComSubdiretorios) 
            for(String subpastanvl1 : Constante.subpasta_nvl1){
                String dirBanco = strDirUsuario+pasta+File.separator+subpastanvl1+File.separator;
                new File(dirBanco).mkdir();
                for(String subpastanvl2 : Constante.subpasta_nvl2)
                    new File(dirBanco+subpastanvl2+File.separator).mkdir();
            }
    }
    
    /**
     *Metodo responsavel por criar uma sessão para o usuário e fazer a listagem de 
     *todos os arquivos que possam ter em suas pastas principais e secundarias nos
     *diretorios upload ou upload_cripto.
     *@param pasta    
     *@param usuarioLogado
     *@see listaTodosArquivosPasta( pasta, usuarioLogado, temSubdiretorio)
     *@Deprecated
     *@return {"banco" : {"infomdlog" : [],
     *                    "dbamdata" : {
     *                                "Raiz" : [],
     *                                "Trigger" : [],
     *                                "Grant" : [],
     *                                "Scripts" : []                                    
     *                               }
     *                  }, 
     *         "reversao" : {
     *                    ...
     *                   }
     *        }    
     */
    @Deprecated
    public static JSONObject listaTodosArquivosPasta(String pasta, String usuarioLogado){
        
        // CRIANDO UMA SESSÃO PARA O USUARIO
        criarSesaoUsuario(usuarioLogado);
        
        // FAZENDO A LISTAGEM DOS ARQUIVOS
        JSONObject retorno = new JSONObject();
        String path_sessao_usuario = Constante.PATH_SESSAO+usuarioLogado;
        for (String pastasnvl1 : Constante.subpasta_nvl1) {
            JSONObject mapPastaNvl2 = new JSONObject();
            for (int i = 0; i < Constante.subpasta_nvl2.length; i++) {
                String diretorio = path_sessao_usuario+File.separator+pasta+File.separator+pastasnvl1+File.separator+Constante.subpasta_nvl2[i]+File.separator;
                ArrayList arrArquivos = new ArrayList();
                String[] arrFiles = new File(diretorio).list();
                for(String arquivo : arrFiles)
                    if(new File(diretorio+arquivo).isFile())
                        arrArquivos.add(arquivo);                   
                    
                try {
                    mapPastaNvl2.put(Constante.subpasta_nvl2_json[i],arrArquivos);
                } catch (Exception ex) {
                    Logger.getLogger(GerenciarArquivosService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                retorno.put(pastasnvl1, mapPastaNvl2);
            } catch (JSONException ex) {
                Logger.getLogger(GerenciarArquivosService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return servletsUtil.retorno(true,retorno.toString());
    }

    
    /**
     *Metodo responsavel por criar uma sessão para o usuário e fazer a listagem de 
     *todos os arquivos que possam ter em suas pastas principais e secundarias nos
     *diretorios upload ou upload_cripto.
     *@param pasta    
     *@param usuarioLogado
     *@param temSubdiretorio
     *@see listaTodosArquivosPasta( pasta, usuarioLogado, temSubdiretorio)
     *@Deprecated
     *@return {"banco" : {"infomdlog" : [],
     *                    "dbamdata" : {
     *                                "Raiz" : [],
     *                                "Trigger" : [],
     *                                "Grant" : [],
     *                                "Scripts" : []                                    
     *                               }
     *                  }, 
     *         "reversao" : {
     *                    ...
     *                   }
     *        }    
     */    
    public static JSONObject listaTodosArquivosPasta(String pasta, String usuarioLogado, Boolean temSubdiretorio){
        JSONObject retorno = new JSONObject();
        String path_sessao_usuario = Constante.PATH_SESSAO+usuarioLogado;

        // CRIANDO UMA SESSÃO PARA O USUARIO
        criarSesaoUsuario(usuarioLogado);

        // FAZENDO A LISTAGEM DOS ARQUIVOS
        if(temSubdiretorio){
            for (String pastasnvl1 : Constante.subpasta_nvl1) {
                JSONObject mapPastaNvl2 = new JSONObject();
                for (int i = 0; i < Constante.subpasta_nvl2.length; i++) {
                    String diretorio = path_sessao_usuario+File.separator+pasta+File.separator+pastasnvl1+File.separator+Constante.subpasta_nvl2[i]+File.separator;
                    ArrayList arrArquivos = new ArrayList();
                    String[] arrFiles = new File(diretorio).list();
                    for(String arquivo : arrFiles)
                        if(new File(diretorio+arquivo).isFile())
                            arrArquivos.add(arquivo);                   

                    try {
                        mapPastaNvl2.put(Constante.subpasta_nvl2_json[i],arrArquivos);
                    } catch (Exception ex) {
                        Logger.getLogger(GerenciarArquivosService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                try {
                    retorno.put(pastasnvl1, mapPastaNvl2);
                } catch (JSONException ex) {
                    Logger.getLogger(GerenciarArquivosService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }else{
            String diretorio = path_sessao_usuario+File.separator+pasta+File.separator;
            ArrayList arrArquivos = new ArrayList();
            String[] arrFiles = new File(diretorio).list();
            for(String arquivo : arrFiles)
                if(new File(diretorio+arquivo).isFile())
                    arrArquivos.add(arquivo); 
            
            try {
                retorno.put("arrFiles", arrArquivos);
            } catch (JSONException ex) {
                Logger.getLogger(GerenciarArquivosService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return servletsUtil.retorno(true,retorno.toString());
    }
    
    /**
     * deletar todos os arquivos que possam ter em suas pastas principais e 
     * secundarias nos diretorios upload ou upload_cripto.
     * @param pasta
     * @param usuarioLogado
     * @return {"status" : Boolean, "data" : String}
     */
    public static JSONObject deletarTodosArquivosPasta(String pasta, String usuarioLogado){
        Boolean retorno = true;
        String mensagem = "";
        String path_sessao_usuario = Constante.PATH_SESSAO+usuarioLogado;
        for (String pastasnvl1 : Constante.subpasta_nvl1)
            for (String pastanvl2 : Constante.subpasta_nvl2) {
                String diretorio = path_sessao_usuario+File.separator+pasta+File.separator+pastasnvl1+File.separator+pastanvl2+File.separator;
                String[] arrFiles = new File(diretorio).list();
                for(String arquivo : arrFiles){
                    File objFile = new File(diretorio+arquivo);
                    if(objFile.isFile()){
                        if(!objFile.delete()){
                            retorno  = false;
                            mensagem += "Erro ao deletar arquivo ["+diretorio+arquivo+"]; \n ";
                        }
                    }
                }
            }
        return servletsUtil.retorno(retorno,mensagem);        
    }

    /**
     * deletar um arquivo que possa ter em suas pastas principais e 
     * secundarias nos diretorios upload ou upload_cripto do usuário logado.
     * @param usuarioLogado
     * @param dirArquivo, ex: "upload/banco/dbamdata/grant/sql.sql"
     * @return {"status" : Boolean, "data" : String}
     */
    public static JSONObject deletarArquivo(String usuarioLogado, String dirArquivo){
        Boolean retorno;
        String mensagem = "";
        String localArquivo = Constante.PATH_SESSAO+usuarioLogado+File.separator+dirArquivo;
        try{
            File objFile = new File(localArquivo);
            retorno = objFile.delete();
            if(!retorno){
                mensagem += "Não pode deletar arquivo!";
            }
        }catch(Exception ex){
            retorno = false;
            mensagem += "Erro ao deletar arquivo : "+ex;
        }        
        return servletsUtil.retorno(retorno,mensagem);        
    }

    /**
     * mover um arquivo que possa ter em suas pastas principais e 
     * secundarias nos diretorios upload ou upload_cripto do usuário logado.
     * @param usuarioLogado
     * @param dirOrigem, ex: "upload/banco/dbamdata/grant/sql.sql"
     * @param dirDestino, ex: "upload/banco/dbamdata/sql.sql"
     * @return {"status" : Boolean, "data" : String}
     */
    public static JSONObject moverArquivo(String usuarioLogado, String dirOrigem, String dirDestino){
        Boolean retorno;
        String mensagem = "";
        int c;

        String localOrigem  = Constante.PATH_SESSAO+usuarioLogado+File.separator+dirOrigem;
        String localDestino = Constante.PATH_SESSAO+usuarioLogado+File.separator+dirDestino;
        try{
            File arq_entrada = new File(localOrigem);
            File arq_saida   = new File(localDestino);
            arq_saida.createNewFile();
            
            FileReader entrada = new FileReader(arq_entrada);
            FileWriter saida = new FileWriter(arq_saida);
            
            while ((c = entrada.read()) != -1)
                saida.write(c);
    
            entrada.close();
            saida.close();
            arq_entrada.delete();
            retorno = true;
            mensagem += "mudado com sucesso!";
        }catch(Exception ex){
            retorno = false;
            mensagem += "Erro ao mover arquivo : "+ex;
        }
        
        return servletsUtil.retorno(retorno,mensagem);        
    }

    /**
     * metodo responsável por fazer a leitura do conteudo do arquivo, o conteudo do
     * arquivo em questão fica armazenado dentro de data no parametro de retorno.
     * @param usuarioLogado
     * @param arquivo, ex: "upload/banco/dbamdata/myFile.sql"
     * @return {"status" : Boolean, "data" : String}
     */
    public static JSONObject lerArquivo(String usuarioLogado, String arquivo){
        Boolean retorno = true;
        String mensagem = "";
        int c;
        try{
            FileReader entrada = new FileReader(new File(Constante.PATH_SESSAO+usuarioLogado+File.separator+arquivo));
            while ((c = entrada.read()) != -1){
                mensagem += (char) c;
            }
            entrada.close();
        }catch(Exception ex){
            retorno = false;
            mensagem += "Erro ao fazer a leitura do do arquivo : "+ex;
        }
        return servletsUtil.retorno(retorno,mensagem);        
    }
    
    /**
     * retorna lista os diretorios e subdiretorios e retornando em formato de 
     * Array.
     * @return {"status" : true, "data" : ArrayList.toString()}
     */
    public static JSONObject listaDiretorios(){
        JSONArray jsonArr = new JSONArray();
        for(String subpastanvl1 : Constante.subpasta_nvl1)
            for (String subpastanvl2 : Constante.subpasta_nvl2)
                jsonArr.put(subpastanvl1+File.separator+subpastanvl2);
        return servletsUtil.retorno(true,jsonArr.toString());        
    }
    
    /**
     * pega o arquivo, lê, verifica os padrões e retorna o caminho espesifico que
     * onde aquele arquivo deve estar, o caminho que deve estar arquivo estara 
     * armazenado no atributo data do parametro de retorno.
     * @param usuarioLogado
     * @param tituloArquivo
     * @param dirOrigem
     * @return {"status" : Boolean, "data" : String}
     */
    public static JSONObject separarArquivo(String usuarioLogado, String tituloArquivo, String dirOrigem){
        String conteudoArquivo = "", mensagem;
        Boolean retorno = true;
        Boolean bool_reversao_titulo, bool_grant_titulo, bool_numero_titulo;
        Boolean bool_have_insert,bool_have_update,bool_have_delete,bool_have_select,bool_have_grant,bool_have_trigger,bool_have_create,bool_have_after,bool_have_create_pub;
        String[] numeros = {"0","1","2","3","4","5","6","7","8","9"};
        int c;
        
        // PROCESSO DE LEITURA DO ARQUIVO
        try{
            FileReader entrada = new FileReader(new File(dirOrigem+File.separator+tituloArquivo));
            while ((c = entrada.read()) != -1)
                conteudoArquivo += (char) c;
            conteudoArquivo = conteudoArquivo.toUpperCase();
            entrada.close();
        }catch(Exception ex){
            return servletsUtil.retorno(false,"Erro ao fazer a leitura do arquivo! "+ex);        
        }
        
        // REGRAS DE NEGOCIO PARA SEPARAR OS ARQUIVOS
        bool_numero_titulo 	= false;
        
        for (String numero : numeros)
            if(tituloArquivo.substring(2, 3).equals(numero))
                bool_numero_titulo = true;
        
        // (tituloArquivo.toUpperCase()).contains("R_")
        bool_reversao_titulo 	= tituloArquivo.toUpperCase().substring(0, 2).equals("R_");
        bool_grant_titulo 	= (tituloArquivo.toUpperCase()).contains("GRANT");
        bool_have_insert 	= conteudoArquivo.contains("INSERT");
        bool_have_update 	= conteudoArquivo.contains("UPDATE");
        bool_have_delete 	= conteudoArquivo.contains("DELETE");
        bool_have_select 	= conteudoArquivo.contains("SELECT");
        bool_have_grant 	= conteudoArquivo.contains("GRANT");
        bool_have_trigger 	= conteudoArquivo.contains("TRIGGER");
        bool_have_create 	= conteudoArquivo.contains("CREATE");
        bool_have_after 	= conteudoArquivo.contains("AFTER");
        bool_have_create_pub	= conteudoArquivo.contains("CREATE PUBLIC");
        
        // DEBUG
        /*System.out.println("---------------------------------------------------");
        System.out.println(tituloArquivo+" - "+tituloArquivo.toUpperCase().substring(0, 2));
        System.out.println("tem r_ no titulo do arquivo: "+bool_reversao_titulo);
        System.out.println("tem numeros no titulo do arquivo: "+bool_numero_titulo);
        System.out.println("tem o nome GRANT no titulo do arquivo: "+bool_grant_titulo);
        System.out.println("tem o nome INSERT no conteudo: "+bool_have_insert);	
        System.out.println("tem o nome UPDATE no conteudo: "+bool_have_update);	
        System.out.println("tem o nome DELETE no conteudo: "+bool_have_delete);	
        System.out.println("tem o nome SELECT no conteudo: "+bool_have_select);	
        System.out.println("tem o nome GRANT no conteudo: "+bool_have_grant);
        System.out.println("tem o nome TRIGGER no conteudo: "+bool_have_trigger); 	
        System.out.println("tem o nome CREATE no conteudo: "+bool_have_create);	
        System.out.println("tem o nome AFTER no conteudo: "+bool_have_after);
        System.out.println("tem o nome CREATE PUBLIC no conteudo: "+bool_have_create_pub);	
        System.out.println("---------------------------------------------------");*/
        
        
        // se o titulo for reversao
        if(bool_reversao_titulo){
            // não houver numero no titulo
            if((!bool_numero_titulo)){
                mensagem = Constante.subpasta_nvl1[1]+File.separator+Constante.subpasta_nvl2[4]; //infomdlog
                if(bool_grant_titulo)
                    mensagem = Constante.subpasta_nvl1[1]+File.separator+Constante.subpasta_nvl2[1];

            }else{
                mensagem = Constante.subpasta_nvl1[1]+File.separator+Constante.subpasta_nvl2[0];
                if(bool_have_insert || bool_have_update || bool_have_delete || bool_have_select)
                    mensagem = Constante.subpasta_nvl1[1]+File.separator+Constante.subpasta_nvl2[2];
                if(bool_have_grant)
                    mensagem = Constante.subpasta_nvl1[1]+File.separator+Constante.subpasta_nvl2[1];
                if(bool_have_trigger)
                    mensagem = Constante.subpasta_nvl1[1]+File.separator+Constante.subpasta_nvl2[3];
                if(bool_have_create || bool_have_after && !bool_have_create_pub)
                    mensagem = Constante.subpasta_nvl1[1]+File.separator+Constante.subpasta_nvl2[0];
                if(bool_grant_titulo)
                    mensagem = Constante.subpasta_nvl1[1]+File.separator+Constante.subpasta_nvl2[1];
            }	
        }else{
            // não houver numero no titulo
            if((!bool_numero_titulo)){
                mensagem = Constante.subpasta_nvl1[0]+File.separator+Constante.subpasta_nvl2[4]; //infomdlog
                if(bool_grant_titulo)
                    mensagem = Constante.subpasta_nvl1[0]+File.separator+Constante.subpasta_nvl2[1];
            }else{
                mensagem = Constante.subpasta_nvl1[0]+File.separator+Constante.subpasta_nvl2[0];
                if(bool_have_insert || bool_have_update || bool_have_delete || bool_have_select)
                    mensagem = Constante.subpasta_nvl1[0]+File.separator+Constante.subpasta_nvl2[2];
                if(bool_have_grant)
                    mensagem = Constante.subpasta_nvl1[0]+File.separator+Constante.subpasta_nvl2[1];
                if(bool_have_trigger)
                    mensagem = Constante.subpasta_nvl1[0]+File.separator+Constante.subpasta_nvl2[3];
                if(bool_have_create || bool_have_after && !bool_have_create_pub)
                    mensagem = Constante.subpasta_nvl1[0]+File.separator+Constante.subpasta_nvl2[0];
                if(bool_grant_titulo)
                    mensagem = Constante.subpasta_nvl1[0]+File.separator+Constante.subpasta_nvl2[1];
            }	
        }
        
        return servletsUtil.retorno(retorno,Constante.PATH_FILES+File.separator+mensagem+File.separator);        
    }
    
    public static JSONObject criarArquivo(String usuarioLogado, String localizacao, String conteudo) throws IOException{    
        String pathSessaoUser = Constante.PATH_SESSAO+usuarioLogado+File.separator;
        String pathFile       = pathSessaoUser+localizacao;
        
        // criando arquivo
        File file = new File(pathFile);
        file.createNewFile();
        
        // inserindo conteudo no arquivo
        FileWriter conteudoFile = new FileWriter(file);
        conteudoFile.write(conteudo);
        conteudoFile.close();        
        
        return servletsUtil.retorno(true,"");        
    }
    
    public static String ziparArquivosWrap(HttpServletRequest request){
        String usuarioLogado = request.getSession().getAttribute("loginCodigoUsuario").toString();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_hhmmss");
        String opZip = (String) request.getParameter("op");
        if(opZip.equals("unwrap")){
            return Compactador.compactarParaZip(usuarioLogado, "arquivos_unwrap_"+sdf.format(c.getTime()) , Constante.PATH_WRAP_UNWRAP, false, true).toString();
        }
        if(opZip.equals("wrap")){
            return Compactador.compactarParaZip(usuarioLogado, "arquivos_wrap_"+sdf.format(c.getTime()) , Constante.PATH_WRAP_WRAP, false, true).toString();
        }
        return "";
    }    
    
    
    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
    ---- METODOS RESPONSÁVEIS PELO FUNCIONAMENTO DO DOWNLOAD PELO TORTOISE -----
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

    public static Map baixarFileLinkTortoise(Map<String,String> args){
        Map<String,String> retorno = new HashMap<String,String>();
        
        String url = args.get("url");
        String name = args.get("usuario");
        String password = args.get("senha");
        String caminhoArquivo = args.get("caminho");
        
        String[] numeros  = {"0","1","2","3","4","5","6","7","8","9"};
        String pathFile, branchAnterior = "", urlBranchAnterior;
        int tamFile;
        SVNRepository repository = null; // checkoutFiles
        Collection logEntries = null;
        SVNRepository repositoryBA = null; 
        Collection logEntriesBA = null;        
        setupLibrary();

        try{
            repository = SVNRepositoryFactory.create(SVNURL.parseURIEncoded(url));
            ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(name, password);
            repository.setAuthenticationManager(authManager);
            long endRevision = repository.getLatestRevision();
            logEntries = repository.log(new String[] {""}, null, 0, endRevision, true, true);
        }catch(Exception ex){
            retorno.put("msg","Erro ocorreu na autenticação: "+ex);
            retorno.put("retorno","false");
            return retorno;
        }
                
        for (Iterator entries = logEntries.iterator(); entries.hasNext();) {
            SVNLogEntry logEntry = (SVNLogEntry) entries.next();
            if (logEntry.getChangedPaths().size() > 0) {
                Set changedPathsSet = logEntry.getChangedPaths().keySet();
                for (Iterator changedPaths = changedPathsSet.iterator(); changedPaths.hasNext();) {

                    SVNLogEntryPath entryPath = (SVNLogEntryPath) logEntry.getChangedPaths().get(changedPaths.next());
                     
                    // PEGANDO URL DA BRANCH ANTERIOR E LOG DO BRANCH ANTERIOR
                    if(branchAnterior.equals("")){
                        branchAnterior = entryPath.getCopyPath();
                        String pastaBranchAnterior   = branchAnterior.substring(1, branchAnterior.substring(1,branchAnterior.length()).indexOf("/")+1);
                        String releaseBranchAnterior = branchAnterior.substring(branchAnterior.substring(1,branchAnterior.length()).indexOf("/")+2, branchAnterior.length());
                        String preUrl = url.substring(0,url.indexOf(pastaBranchAnterior)+pastaBranchAnterior.length()+1);
                        
                        // BUSCANDO A POSIÇÃO DOS ULTIMOS DIGITOS DA RELEASE PARA SEPARAR O QUE TEM DEPOIS DA RELEASE
                        int ultimosDigitosRelease = url.lastIndexOf(".00");
                        for (int i = 0; i < 10; i++) {
                            ultimosDigitosRelease = url.lastIndexOf(".0"+i);
                            if(ultimosDigitosRelease != -1){
                                i = 100;
                            }
                        }
                        String posUrl = url.substring( ultimosDigitosRelease+3,url.length());
                        urlBranchAnterior = preUrl+releaseBranchAnterior+posUrl;   
                        
                        try{
                            repositoryBA = SVNRepositoryFactory.create(SVNURL.parseURIEncoded(url));
                            ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(name, password);
                            repositoryBA.setAuthenticationManager(authManager);
                            long endRevision = repositoryBA.getLatestRevision();
                            logEntriesBA = repositoryBA.log(new String[] {""}, null, 0, endRevision, true, true);
                        }catch(Exception ex){
                            retorno.put("msg","Erro ocorreu na autenticação do branch :  "+branchAnterior);
                            retorno.put("retorno","false");
                            return retorno;
                        }
                    }
                    
                    pathFile = entryPath.getPath(); 
                    tamFile  = pathFile.length();
                    boolean renomearArquivo = false;
                    // VERIFICANDO DOS ARQUIVOS QUAIS TEM A EXTENSÃO .SQL
                    if((pathFile.substring(tamFile-4,tamFile)).equals(".sql")){
                        
                        // PEGANDO A URL BASE SVN DO PROJETO
                        Integer posicaoPrimeiraBarra = 1;
                        Integer posicaoSegundaBarra = (pathFile.substring(posicaoPrimeiraBarra,pathFile.length())).indexOf("/")+1;
                        String primeiraPastaUrlRepo = pathFile.substring(posicaoPrimeiraBarra,posicaoSegundaBarra);
                        String urlRepo = url.substring(0,url.indexOf(primeiraPastaUrlRepo)-1);
                        
                        // CAMINHO SVN DO ARQUIVO .SQL
                        String urlFileRepo = urlRepo+pathFile;

                        // PEGANDO O NOME DO ARQUIVO                        
                        String nomeArquivo = pathFile.substring(pathFile.lastIndexOf("/")+1,pathFile.length());
                        
                        // VERIFICA SE O ARQUIVO EH OU NAO UMA REVERSAO DO BRANCH ANTERIOR
                        if( !nomeArquivo.substring(0,2).equals("R_") && !Arrays.asList(numeros).contains(nomeArquivo.substring(0,1))){
                            if(existeArquivoBranchAnterior(logEntriesBA , nomeArquivo)){// colocando esse if aki por causa da performace
                                renomearArquivo = true;
                            }
                        }
                        
                        // EXECUTANDO COMANDO SQL PARA COLOCAR NO DIRETORIO
                        cmd("cmd.exe /c cd "+caminhoArquivo+" && svn export "+urlFileRepo+" --username "+name+" --password "+password);
                        
                        if(renomearArquivo){
                            File objFile = new File(caminhoArquivo+nomeArquivo);
                            File newNameFile = new File(caminhoArquivo+"R_"+nomeArquivo);
                            objFile.renameTo(newNameFile);
                        }
                    }
                }
            }
        }

        retorno.put("msg","OK");
        retorno.put("retorno","true");
        return retorno;
    }
    
    public static void separarArquivo(String caminho){
        
        File raiz = new File(caminho);
        for(File f: raiz.listFiles()) {
            if(f.isFile()) {
                System.out.println(f.getName());
            }
        }        
        
    }

    private static void setupLibrary() {
        DAVRepositoryFactory.setup();
        SVNRepositoryFactoryImpl.setup();
        FSRepositoryFactory.setup();
    }
    
    private static boolean existeArquivoBranchAnterior(Collection logEntries, String fileName){
        String pathFile;
        int tamFile;
        for (Iterator entries = logEntries.iterator(); entries.hasNext();) {
            SVNLogEntry logEntry = (SVNLogEntry) entries.next();
            if (logEntry.getChangedPaths().size() > 0) {
                Set changedPathsSet = logEntry.getChangedPaths().keySet();
                for (Iterator changedPaths = changedPathsSet.iterator(); changedPaths.hasNext();) {
                    SVNLogEntryPath entryPath = (SVNLogEntryPath) logEntry.getChangedPaths().get(changedPaths.next());
                    pathFile = entryPath.getPath(); 
                    tamFile  = pathFile.length();
                    if((pathFile.substring(tamFile-4,tamFile)).equals(".sql") && pathFile.indexOf(fileName) != -1){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    private static void cmd(String cmd){
        try {
           Process obj = Runtime.getRuntime().exec(cmd);
        } catch (IOException ex) {
           System.out.println("deu merda ao executar comando cmd: "+ex);
        }    
    }

}
