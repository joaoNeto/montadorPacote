package br.com.informata.customerline.service;

import br.com.informata.customerline.conf.Constante;
import br.com.informata.customerline.dao.pacote.WrapDao;
import br.com.informata.customerline.infra.FileSaver;
import br.com.informata.customerline.util.Compactador;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author joao.neto
 */
@Service
public class WrapService {

    @Autowired
    private FileSaver fileSaver;    
    
    @Autowired
    private WrapDao wrapDao;   
    
    public Boolean isWrap(String text){
        
        return false;
    }
    
    public String criptografarTexto(String textUnwrap){
        try{
            return wrapDao.wrapText(textUnwrap);
        }catch(Exception ex){
            System.out.println("erro: "+ex);
        }
        return textUnwrap;
    }
 
    public String subirArquivo(String usuarioLogado, MultipartFile[] arquivo) throws JSONException, IOException{

        String dirUploadFile  = Constante.PATH_SESSAO;
               dirUploadFile += usuarioLogado+File.separator;
               dirUploadFile += Constante.PATH_WRAP + File.separator;
               
        for (MultipartFile file : arquivo) {
            String nomeArquivo = file.getOriginalFilename();
            String dirFileOrigem = Constante.PATH_WRAP+File.separator+nomeArquivo;
            String dirFileWrap   = Constante.PATH_WRAP_WRAP+File.separator+nomeArquivo;
            String dirFileUnwrap = Constante.PATH_WRAP_UNWRAP+File.separator+nomeArquivo;

            fileSaver.write(dirUploadFile, file);
            JSONObject obj = GerenciarArquivosService.lerArquivo(usuarioLogado, dirFileOrigem);
            String conteudoArquivo = obj.getString("data");
            
            // SEPARANDO O ARQUIVO PARA SEU LOCAL DE DESTINO
            if(!conteudoArquivo.contains("wrapped")){
                // mover para descriptografado
                GerenciarArquivosService.moverArquivo(usuarioLogado, dirFileOrigem, dirFileUnwrap);
                String textoWrap = wrapDao.wrapText(conteudoArquivo);
                // ENCRIPTOGRAFAR ESSE ARQUIVO E MOVER PARA A PASTA DOS ENCRIPTOGRAFADOS
                GerenciarArquivosService.criarArquivo(usuarioLogado, dirFileWrap, textoWrap);
            }else{
                // mover para criptografado
                GerenciarArquivosService.moverArquivo(usuarioLogado, dirFileOrigem, dirFileWrap);                
                String textoUnwrap = wrapDao.unwrapText(conteudoArquivo);
                // DESCRIPTOGRAFAR ESSE ARQUIVO E MOVER PARA A PASTA DOS DESCRIPTOGRAFADOS
                GerenciarArquivosService.criarArquivo(usuarioLogado, dirFileUnwrap, textoUnwrap);
            }
        }          

        return "";
    }
    
    public void wrapPath(String usuarioLogado, String pathOrigem, String pathDestino) throws Exception {
            String path_sessao_usuario         = Constante.PATH_SESSAO+usuarioLogado+File.separator;
            String path_sessao_usuario_origem  = path_sessao_usuario+pathOrigem+File.separator;
            
            // LISTANDO TODOS OS ARQUIVOS DE pathOrigem
            for (String pastasnvl1 : Constante.subpasta_nvl1) 
                for (int i = 0; i < Constante.subpasta_nvl2.length; i++) {
                    String subDir     = pastasnvl1+File.separator+Constante.subpasta_nvl2[i]+File.separator;
                    String dirOrigem  = path_sessao_usuario_origem+subDir;
                    
                    String[] arrFiles = new File(dirOrigem).list();
                    for(String arquivo : arrFiles){
                        File fileOrigem = new File(dirOrigem+arquivo);
                        if(fileOrigem.isFile()){
                            String dirFileOrigem  = pathOrigem+File.separator+subDir+arquivo; // upload\banco\infomdlog\teste_cripto.sql
                            String dirFileDestino = pathDestino+File.separator+subDir+arquivo; // upload_cripto\banco\infomdlog\teste_cripto.sql
                            
                            // ler arquivo
                            JSONObject obj = GerenciarArquivosService.lerArquivo(usuarioLogado, dirFileOrigem);
                            String conteudoArquivo = obj.getString("data");                            
                            
                            // criptografar texto se o texto ja não estiver criptografado
                            if(!conteudoArquivo.contains("wrapped"))
                                conteudoArquivo = criptografarTexto(conteudoArquivo);
                            
                            // criar arquivo e inserir o texto criptografado
                            GerenciarArquivosService.criarArquivo(usuarioLogado, dirFileDestino, conteudoArquivo);
                        }
                    }
                }
            
    }
   
}
