package br.com.informata.customerline.controller;

import br.com.informata.customerline.conf.Constante;
import br.com.informata.customerline.dao.pacote.PacoteDao;
import br.com.informata.customerline.service.GerenciarArquivosService;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import br.com.informata.customerline.infra.FileSaver;
import br.com.informata.customerline.service.WrapService;
import br.com.informata.customerline.util.Compactador;
import java.io.IOException;
import org.codehaus.jettison.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;

@Controller
public class HomeController {

    @Autowired
    private FileSaver fileSaver;

    @Autowired
    private PacoteDao pacoteDao;

    @Autowired
    private WrapService wrapService;
    
    /**
     * Tela inicial
     *
     * @url [URL_DO_PROJETO]/
     * @param request
     * @return {"status" : Boolean, "data" : String}
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("home");
        return modelAndView;
    }

    /**
     * Devolve uma string json contendo a estrutura do arquivo
     *
     * @url [URL_DO_PROJETO]/listaArquivos
     * @param request
     * @return {"status" : Boolean, "data" : String}
     */
    @RequestMapping(value = "/listaArquivos", method = RequestMethod.GET)
    @ResponseBody
    public String listaArquivos(HttpServletRequest request) {
        return GerenciarArquivosService.listaTodosArquivosPasta(Constante.PATH_FILES, request.getSession().getAttribute("loginCodigoUsuario").toString()).toString();
    }

    /**
     * Retorna um array com os subdiretorios da pasta
     *
     * @url [URL_DO_PROJETO]/listaDiretoriosSubdiretorios
     * @param request
     * @return {"status" : Boolean, "data" : String}
     */
    @RequestMapping(value = "/listaDiretoriosSubdiretorios", method = RequestMethod.GET)
    @ResponseBody
    public String listaDiretoriosSubdiretorios(HttpServletRequest request) {
        return GerenciarArquivosService.listaDiretorios().toString();
    }

    /**
     * Exclui todos os arquivos passando por parametro via get "diretorio"
     * contento a pasta que ira ser deletada
     *
     * @url [URL_DO_PROJETO]/excluirTodosArquivos?diretorio=
     * @getparam diretorio = parametro de Constante.pasta, ex: upload
     * @param request
     * @return {"status" : Boolean, "data" : String}
     */
    @RequestMapping(value = "/excluirTodosArquivos", method = RequestMethod.GET)
    @ResponseBody
    public String excluirTodosArquivos(HttpServletRequest request) {
        return GerenciarArquivosService.deletarTodosArquivosPasta((String) request.getParameter("diretorio"), request.getSession().getAttribute("loginCodigoUsuario").toString()).toString();
    }

    /**
     * Retorna uma string contendo o conteudo do arquivo.
     *
     * @getparam dirPrincipal = parametro de Constante.subpasta_nvl1, ex: banco
     * @getparam dirSecundario = parametro de Constante.subpasta_nvl2, ex:
     * dbamdata
     * @getparam nomeArquivo = nome do arquivo, ex: meuArquivo.txt
     * @url
     * [URL_DO_PROJETO]/lerArquivo?dirPrincipal=&dirSecundario=&nomeArquivo=
     * @param request
     * @return {"status" : Boolean, "data" : String}
     */
    @RequestMapping(value = "/lerArquivo", method = RequestMethod.GET)
    @ResponseBody
    public String lerArquivo(HttpServletRequest request) {
        String diretorio = Constante.PATH_FILES + File.separator + request.getParameter("dirPrincipal") + File.separator;
        for (int posDirSecundario = 0; posDirSecundario < Constante.subpasta_nvl2_json.length; posDirSecundario++) {
            if (Constante.subpasta_nvl2_json[posDirSecundario].equals(request.getParameter("dirSecundario"))) {
                diretorio += Constante.subpasta_nvl2[posDirSecundario] + File.separator;
            }
        }

        diretorio += request.getParameter("nomeArquivo");
        String usuario = request.getSession().getAttribute("loginCodigoUsuario").toString();
        return GerenciarArquivosService.lerArquivo(usuario, diretorio).toString();
    }

    /**
     * Link para poder deletar um arquivo
     *
     * @url
     * [URL_DO_PROJETO]/deletarArquivo?dirPrincipal=&dirSecundario=&nomeArquivo=
     * @getparam dirPrincipal = parametro de Constante.subpasta_nvl1, ex: banco
     * @getparam dirSecundario = parametro de Constante.subpasta_nvl2, ex:
     * dbamdata
     * @getparam nomeArquivo = ex: meuArquivo.txt
     * @param request
     * @return {"status" : Boolean, "data" : String}
     */
    @RequestMapping(value = "/deletarArquivo", method = RequestMethod.GET)
    @ResponseBody
    public String deletarArquivo(HttpServletRequest request) {
        String diretorio = Constante.PATH_FILES + File.separator + request.getParameter("dirPrincipal") + File.separator;
        for (int posDirSecundario = 0; posDirSecundario < Constante.subpasta_nvl2_json.length; posDirSecundario++) {
            if (Constante.subpasta_nvl2_json[posDirSecundario].equals(request.getParameter("dirSecundario"))) {
                diretorio += Constante.subpasta_nvl2[posDirSecundario] + File.separator;
            }
        }

        diretorio += request.getParameter("nomeArquivo");
        return GerenciarArquivosService.deletarArquivo(request.getSession().getAttribute("loginCodigoUsuario").toString(), diretorio).toString();
    }

    /**
     * Move um arquivo que esta em algum subdiretorio em Constante.PATH_FILES e
     * move para outro subdiretorio de Constante.PATH_FILES.
     *
     * @url
     * [URL_DO_PROJETO]/moverArquivo?dirPrincipalOrigem=&dirSecundarioOrigem=&arquivo=&dirDestino
     * @getparam dirPrincipalOrigem = pasta que esta em Constante.subpasta_nvl1
     * contendo o arquivo a ser movido, ex: banco
     * @getparam dirSecundarioOrigem = pasta que esta em
     * Constante.subpasta_nvl2, ex: infomdlog
     * @getparam arquivo = o arquivo que será movido,ex:
     * PWPRE_REEMISSAO_ETIQUETA.sql
     * @getparam dirDestino = diretório completo onde será movido o arquivo, ex:
     * banco\dbamdata\trigger
     * @param request
     * @return {"status" : Boolean, "data" : String}
     */
    @RequestMapping(value = "/moverArquivo", method = RequestMethod.GET)
    @ResponseBody
    public String moverArquivo(HttpServletRequest request) {
        String diretorioOrigem = Constante.PATH_FILES + File.separator, diretorioDestino = Constante.PATH_FILES + File.separator;
        diretorioOrigem += request.getParameter("dirPrincipalOrigem") + File.separator;

        for (int posDirSecundario = 0; posDirSecundario < Constante.subpasta_nvl2_json.length; posDirSecundario++) {
            if (Constante.subpasta_nvl2_json[posDirSecundario].equals(request.getParameter("dirSecundarioOrigem"))) {
                diretorioOrigem += Constante.subpasta_nvl2[posDirSecundario] + File.separator;
            }
        }

        diretorioOrigem += request.getParameter("arquivo");
        diretorioDestino += request.getParameter("dirDestino") + File.separator + request.getParameter("arquivo");
        return GerenciarArquivosService.moverArquivo(request.getSession().getAttribute("loginCodigoUsuario").toString(), diretorioOrigem, diretorioDestino).toString();
    }

    /**
     * baixa os arquivos que estão no link do tortoise e colocam em
     * Constante.PATH_ZIP
     *
     * @url [URL_DO_PROJETO]/uploadArquivosViaTortoise
     * @param request
     * @param url url do tortoise que serão baixados os arquivos, ex:
     * https://192.168.0.235:8443/svn/IMdlog/branches/71.11.01.00
     * @param usuario usuario que tem permissão para acessar o link do tortoise,
     * ex: joao.neto
     * @param senha senha do usuario que tem permissão para acessar o link do
     * tortoise, ex: JNInfo@2018
     * @return {"status" : Boolean, "data" : String}
     */
    @RequestMapping(value = "/uploadArquivosViaTortoise", method = RequestMethod.POST)
    @ResponseBody
    public String uploadArquivosViaTortoise(HttpServletRequest request,
            @RequestParam("url") String url,
            @RequestParam("usuario") String usuario,
            @RequestParam("senha") String senha) {

        Map param = new HashMap();
        String caminho;

        caminho  = Constante.PATH_SESSAO;
        caminho += request.getSession().getAttribute("loginCodigoUsuario").toString() + File.separator;
        caminho += Constante.PATH_REPO_SVN + File.separator;

        param.put("url", url);
        param.put("usuario", usuario);
        param.put("senha", senha);
        param.put("caminho", caminho);

        return GerenciarArquivosService.baixarFileLinkTortoise(param).toString();
    }

    /**
     * Pega os arquivos que estão em Constante.PATH_ZIP e coloca em
     * Constante.PATH_FILES
     *
     * @url [URL_DO_PROJETO]/separarArquivosViaTortoise
     * @param request
     * @return {"status" : Boolean, "data" : String}
     */
    @RequestMapping(value = "/separarArquivosViaTortoise", method = RequestMethod.GET)
    @ResponseBody
    public String separarArquivosViaTortoise(HttpServletRequest request) {
        String usuario = request.getSession().getAttribute("loginCodigoUsuario").toString();
        String dirRepo = Constante.PATH_SESSAO + usuario + File.separator;
        String dirRepoSvn = dirRepo + Constante.PATH_REPO_SVN;
        String[] arrFiles = new File(dirRepoSvn).list();
        for (String nomeArquivo : arrFiles) {
            try {
                JSONObject objFile = GerenciarArquivosService.separarArquivo(usuario, nomeArquivo, dirRepoSvn);
                String dirOrigem = Constante.PATH_REPO_SVN + File.separator + nomeArquivo;
                String dirDestino = objFile.get("data") + nomeArquivo;
                GerenciarArquivosService.moverArquivo(usuario, dirOrigem, dirDestino);
            } catch (Exception ex) {
                Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return "";
    }

    /**
     * Pega os arquivos que o usuário selecionou no momento em que clicou no
     * botão "upload por arquivo" e coloca na pasta Constante.PATH_UPL_FILE
     *
     * @url [URL_DO_PROJETO]/uploadArquivosViaFile
     * @param request
     * @param arquivo MultipartFile é um tipo de dados do spring onde é um array
     * contendo os arquivos
     * @return {"status" : Boolean, "data" : String}
     */
    @RequestMapping(value = "/uploadArquivosViaFile", method = RequestMethod.POST)
    @ResponseBody
    public String uploadArquivosViaFile(HttpServletRequest request, @RequestParam("file") MultipartFile[] arquivo) {
        String dirUploadFile = Constante.PATH_SESSAO + (request.getSession().getAttribute("loginCodigoUsuario").toString()) + File.separator + Constante.PATH_UPL_FILE + File.separator;
        for (MultipartFile file : arquivo) {
            fileSaver.write(dirUploadFile, file);
        }

        return "";
    }

    /**
     * Pega os arquivos que estão em Constante.PATH_UPL_FILE e coloca em
     * Constante.PATH_FILES
     *
     * @url [URL_DO_PROJETO]/separarArquivosViaFile
     * @param request
     * @return {"status" : Boolean, "data" : String}
     */
    @RequestMapping(value = "/separarArquivosViaFile", method = RequestMethod.GET)
    @ResponseBody
    public String separarArquivosViaFile(HttpServletRequest request) {
        String usuario = request.getSession().getAttribute("loginCodigoUsuario").toString();
        String dirRepo = Constante.PATH_SESSAO + usuario + File.separator;
        String dirRepoFile = dirRepo + Constante.PATH_UPL_FILE;
        String[] arrFiles = new File(dirRepoFile).list();
        for (String nomeArquivo : arrFiles) {
            try {
                JSONObject objFile = GerenciarArquivosService.separarArquivo(usuario, nomeArquivo, dirRepoFile);
                String dirOrigem = Constante.PATH_UPL_FILE + File.separator + nomeArquivo;
                String dirDestino = objFile.get("data") + nomeArquivo;
                GerenciarArquivosService.moverArquivo(usuario, dirOrigem, dirDestino);
            } catch (Exception ex) {
                Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "";
    }

    /**
     * monta um pacote zipando o arquivo e colocando o arquivo zipado em 
     * Constante.PATH_ZIP.
     * @url [URL_DO_PROJETO]/montarPacoteFile?nome=&versaobanco=&versaoreversao=
     * @getparam nome = nome do pacote, ex: meu pacote
     * @getparam versaobanco = versao do pacote, ex: 35.01.01.01
     * @getparam versaoreversao = versao de reversao, ex: 35.01.01.00
     * @param request
     * @return {"status" : Boolean, "data" : String}
     */
    @RequestMapping(value = "/montarPacoteFile", method = RequestMethod.GET)
    @ResponseBody
    public String montarPacoteFile(HttpServletRequest request) throws JSONException, IOException {
        String usuario = request.getSession().getAttribute("loginCodigoUsuario").toString();
        String nomeArquivo = request.getParameter("nome") + "_" + request.getParameter("versaoreversao");
        
        wrapService.wrapPath(usuario, Constante.PATH_FILES,Constante.PATH_FLE_CRI);
        GerenciarArquivosService.deletarTodosArquivosPasta(Constante.PATH_FILES, usuario);
        return Compactador.compactarParaZip(usuario, nomeArquivo, Constante.PATH_FLE_CRI, true, true).toString();
    }
}
