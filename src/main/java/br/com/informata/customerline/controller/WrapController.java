/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.customerline.controller;

import br.com.informata.customerline.conf.Constante;
import br.com.informata.customerline.infra.FileSaver;
import br.com.informata.customerline.service.GerenciarArquivosService;
import br.com.informata.customerline.service.WrapService;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author joao.neto
 */
@Controller
@RequestMapping(value = "/wrap")
public class WrapController {

    @Autowired
    private WrapService wrapService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("wrap");
        return modelAndView;
    }

    @RequestMapping(value = "/subirArquivo", method = RequestMethod.POST)
    @ResponseBody
    public String subirArquivo(HttpServletRequest request, @RequestParam("file") MultipartFile[] arquivo) throws JSONException, IOException {
        String usuarioLogado  = request.getSession().getAttribute("loginCodigoUsuario").toString();       
        return wrapService.subirArquivo(usuarioLogado, arquivo);
    }
    
    @RequestMapping(value = "/listarArquivo", method = RequestMethod.GET)
    @ResponseBody
    public String listarArquivo(HttpServletRequest request) {
        String usuarioLogado = request.getSession().getAttribute("loginCodigoUsuario").toString();   
        JSONObject retorno = new JSONObject();
        try {
            retorno.put("arquivosWrap",GerenciarArquivosService.listaTodosArquivosPasta(Constante.PATH_WRAP_WRAP , usuarioLogado, false));
            retorno.put("arquivosUnwrap",GerenciarArquivosService.listaTodosArquivosPasta(Constante.PATH_WRAP_UNWRAP , usuarioLogado, false));
        } catch (JSONException ex) {
            Logger.getLogger(WrapController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno.toString();
    }
    
    @RequestMapping(value = "/zipparArquivoUnwrap", method = RequestMethod.GET)
    @ResponseBody
    public String zipparArquivoUnwrap(HttpServletRequest request) {
        return GerenciarArquivosService.ziparArquivosWrap(request);
    }
    
    @RequestMapping(value = "/deletarArquivo", method = RequestMethod.GET)
    @ResponseBody
    public String deletarArquivo(HttpServletRequest request) {
        String usuarioLogado = request.getSession().getAttribute("loginCodigoUsuario").toString();
        return GerenciarArquivosService.deletarArquivo(usuarioLogado, (String) request.getParameter("arquivo")).toString();
    }
        
    @RequestMapping(value = "/criptografarTexto", method = RequestMethod.POST)
    @ResponseBody
    public String criptografarTexto(HttpServletRequest request, @RequestParam(value = "texto", required = true) String texto) {
        //String name = (String) request.getParameter("texto");
        return wrapService.criptografarTexto(texto);
    }
    
}
