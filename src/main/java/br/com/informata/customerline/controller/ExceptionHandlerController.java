package br.com.informata.customerline.controller;

import br.com.informata.customerline.infra.ArquivoProperties;
import br.com.informata.customerline.infra.NomesParametrosViewMonitorProperties;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionHandlerController {

	@ExceptionHandler(Exception.class)
	public ModelAndView trataExceptionGenerica(Exception exception) {
		System.out.println("Erro genérico acontecendo");
		exception.printStackTrace();
		
		ModelAndView modelAndView = new ModelAndView("error");
		modelAndView.addObject("exception", exception);
//                String s = ArquivoProperties.getInstance().obterValueDeParametro(NomesParametrosViewMonitorProperties.HIBERNATE_DIALECT);
		return modelAndView;
	}
	
}
