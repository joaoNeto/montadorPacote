/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.customerline.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author danilomuniz
 */
@Controller
public class ErrorController {
    
    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public ModelAndView errorAcessoNegado() {
        ModelAndView modelAndView = new ModelAndView("error");
        modelAndView.addObject("oi", "ola");
        return modelAndView;
    }    
    
}
