package br.com.informata.customerline.dao.onestudio;

import br.com.informata.customerline.model.Usuario;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional("oneStudioTransactionManager")
public class UsuarioDAO {

    @PersistenceContext(unitName = "oneStudioEntityManagerFactory")
    private EntityManager manager;

    public Usuario loadUserByUsername(String email) {
        Usuario usuario = new Usuario();

        if (usuario.equals(email)) {
            throw new UsernameNotFoundException("Usuário " + email + " não foi encontrado");
        }

        return usuario;
    }

    private int obterCodigoClientePorChave(String clienteChave) {
        int codigo = 0;
        try {
            Session session = manager.unwrap(Session.class);
            SQLQuery sqlQuery = session.createSQLQuery("Select Code \n"
                    + "from dbo.CL_Customer \n"
                    + "where CustomerKey = :CustomerKey");
            sqlQuery.setParameter("CustomerKey", clienteChave);
            codigo = (int) sqlQuery.uniqueResult();
        } catch (Exception e) {
            System.out.println("e:" + e.getLocalizedMessage());
        }
        return codigo;
    }

    public String obterNomeStakeholderPorCodigo(int codigoStakeholder) {
        String nomeStakeholder = "";
        try {
            Session session = manager.unwrap(Session.class);
            SQLQuery sqlQuery = session.createSQLQuery("Select\n"
                    + "    SHNome\n"
                    + "from dbo.stakeholder\n"
                    + "where SHCode = :SHCode");
            sqlQuery.setParameter("SHCode", codigoStakeholder);
            nomeStakeholder = (String) sqlQuery.uniqueResult();
        } catch (Exception e) {
            System.out.println("e:" + e.getLocalizedMessage());
        }
        return nomeStakeholder;
    }

    public boolean verificarPermissaoUsuarioParaSistema(int sistemaCodigo, int transacaoCodigo, int usuarioCodigo) {
        boolean exiteAcesso = false;
        try {
            Session session = manager.unwrap(Session.class);
            SQLQuery sqlQuery = session.createSQLQuery("Select\n"
                    + "    count(SistemaCodigo) as Qtd\n"
                    + "From  dbo.SEGPERMISSAOUSUARIO \n"
                    + "Where SistemaCodigo = :SistemaCodigo \n"
                    + "  And TransacaoCodigo = :TransacaoCodigo \n"
                    + "  And UsuarioCodigo = :UsuarioCodigo");
            sqlQuery.setParameter("SistemaCodigo", sistemaCodigo);
            sqlQuery.setParameter("TransacaoCodigo", transacaoCodigo);
            sqlQuery.setParameter("UsuarioCodigo", usuarioCodigo);
            int codigo = 0;
            codigo = (int) sqlQuery.uniqueResult();
            exiteAcesso = (codigo > 0);
        } catch (Exception e) {
            System.out.println("e:" + e.getLocalizedMessage());
        }
        return exiteAcesso;
    }

    public Map<String, Object> obterDadosClientePorCodigo(int codigoCliente) {
        Map<String, Object> map = new LinkedHashMap();
        try {
            Session session = manager.unwrap(Session.class);
            SQLQuery sqlQuery = session.createSQLQuery("select \n"
                    + "  Code,\n"
                    + "  CustomerName,\n"
                    + "  CustomerKey,\n"
                    + "  CustomerStatus\n"
                    + "from dbo.CL_Customer\n"
                    + "    where Code = :Code");
            sqlQuery.setParameter("Code", codigoCliente);
            List<Object[]> lista = sqlQuery.list();
            for (Object[] object : lista) {
                map.put("codigoCliente", object[0]);
                map.put("nomeCliente", object[1]);
                map.put("chaveCliente", object[2]);
                map.put("statusCliente", object[3]);
            }
//            lista.forEach((object) -> {
//                map.put("codigoCliente", object[0]);
//                map.put("nomeCliente", object[1]);
//                map.put("chaveCliente", object[2]);
//                map.put("statusCliente", object[3]);
//            });
        } catch (Exception e) {
            System.out.println("e:" + e.getLocalizedMessage());
        }
        return map;
    }

    public Map<String, Object> validarLogin(String clienteChave, String nomeUsuario, String senha) {
        Map<String, Object> map = new LinkedHashMap();
        try {
            Session session = manager.unwrap(Session.class);
            int codigoCliente = obterCodigoClientePorChave(clienteChave);
            SQLQuery sqlQuery = session.createSQLQuery("Select csh.CustomerCode,\n"
                    + "       up.UsuarioCode,\n"
                    + "       csh.StakeholderCode,\n"
                    + "       csh.RoleCode \n"
                    + "from dbo.CL_CustomerStakeholder csh, \n"
                    + "     dbo.SegUsuario su,\n"
                    + "     dbo.UsuarioParticipante up \n"
                    + "where csh.CustomerCode = :CustomerCode \n"
                    + "and csh.StakeholderCode = up.participanteCode \n"
                    + "and up.usuarioCode = su.UsuarioCodigo \n"
                    + "and su.Login = :UserName \n"
                    + "and su.Senha = :Password \n"
                    + "and su.Habilitado = :Habilitado");
            sqlQuery.setParameter("CustomerCode", codigoCliente);
            sqlQuery.setParameter("UserName", nomeUsuario);
            sqlQuery.setParameter("Password", senha);
            sqlQuery.setParameter("Habilitado", 1);
            List<Object[]> lista = sqlQuery.list();
            for (Object[] object : lista) {
                map.put("codigoCliente", object[0]);
                map.put("codigoUsuario", object[1]);
                map.put("codigoStakeholder", object[2]);
                map.put("codigoProfileRole", object[3]);
            }
//            lista.forEach((object) -> {
//                map.put("codigoCliente", object[0]);
//                map.put("codigoUsuario", object[1]);
//                map.put("codigoStakeholder", object[2]);
//                map.put("codigoProfileRole", object[3]);
//            });
        } catch (Exception e) {
            System.out.println("e:" + e.getLocalizedMessage());
        }
        
        return map;

        
    }

}


