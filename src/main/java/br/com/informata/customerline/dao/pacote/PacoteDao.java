/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.customerline.dao.pacote;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author joao.neto
 */
@Repository
@Transactional("pacoteTransactionManager")
public class PacoteDao {

    @PersistenceContext(unitName = "pacoteEntityManagerFactory")
    private EntityManager manager;
    
    public int  uploadArquivoDB(String diretorio, String versaoBanco, String versaoReversao) {
        int i = 0;
        try {
            
            Session session = manager.unwrap(Session.class);
            SQLQuery sqlQuery = session.createSQLQuery("dbagabos.montagem_pacotes.monta_pacote(:dirPacote, :versaoBanco, :versaoReversao, 'PRJ', 'NORMAL', 'S')");
            sqlQuery.setParameter("dirPacote",      diretorio);
            sqlQuery.setParameter("versaoBanco",    versaoBanco);
            sqlQuery.setParameter("versaoReversao", versaoReversao);
            i = sqlQuery.executeUpdate();
            session.getTransaction().commit();  
            
        } catch (Exception e) {
            System.out.println("e:" + e.getLocalizedMessage());
        }
        return i;
    }        
    
}
