package br.com.informata.customerline.dao.pacote;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author joao.neto
 */
@Repository
@Transactional("pacoteTransactionManager")
public class WrapDao {
    
    @PersistenceContext(unitName = "pacoteEntityManagerFactory")
    private EntityManager manager;    
    
    public String wrapText(String textUnwrap){
        
        StoredProcedureQuery query = manager.createStoredProcedureQuery("dbagabos.pproc_criptografa_objeto")
            .registerStoredProcedureParameter("pi_l_source", String.class, ParameterMode.IN)
            .registerStoredProcedureParameter("po_l_wrap", String.class, ParameterMode.OUT)
            .setParameter("pi_l_source", textUnwrap);
        query.execute(); 
        
        return (String) query.getOutputParameterValue("po_l_wrap");        
    }
    
    public String unwrapText(String textWrap){
    
        String sql = "";
        
        return "";
    }
    
}
