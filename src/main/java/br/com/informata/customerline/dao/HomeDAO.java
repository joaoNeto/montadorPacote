/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.customerline.dao;

import br.com.informata.customerline.util.ModelUtil;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Repository;

/**
 * Responsavel por servir a pagina inicia da aplicacao.
 *
 * @author danilomuniz
 */
@Repository
public class HomeDAO extends ModelUtil {

    @PersistenceContext
    private EntityManager manager;

    @Autowired
    private ApplicationContext applicationContext;

    public ArrayList obterDadosTotalizadorPorCodigoCostumer(String codigoClienteLogado){
        ArrayList arrRetorno = new ArrayList();
        
        String sql = "";
        sql = montarSqlDadosTotalizadorPorCodigoCostumer(codigoClienteLogado);   
        
        List<Object[]> results = executarQuery(sql);
        for (Object[] rs : results) {
            JSONObject jsonObj = new JSONObject();
            try {
                jsonObj.put("TIPO",rs[0]);
                jsonObj.put("ATENDIMENTO",rs[1]);
                jsonObj.put("COMERCIAL",rs[2]);
                jsonObj.put("FABRICA",rs[3]);
                jsonObj.put("HOMOLOGACAO",rs[4]);
                jsonObj.put("EXPEDICAO",rs[5]);
                jsonObj.put("VALIDACAO",rs[6]);
                jsonObj.put("TOTAL",rs[7]);
            } catch (JSONException ex) {
                Logger.getLogger(HomeDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            arrRetorno.add(jsonObj);
        }
        
        return arrRetorno;
    }
    
    public JSONObject obterDadosGridPorCodigoProfileRole(Integer codigoProfileRole, Integer codigoClienteLogado, Integer codigoStackeholderLogado) {
        JSONArray array = new JSONArray();
        JSONObject objRetorno = new JSONObject();
        try {
            String sql = "";
            sql = montarSqlDadosGridPorCodigoProfileRole(codigoProfileRole, codigoClienteLogado.toString(), codigoStackeholderLogado.toString());
            Session session = manager.unwrap(Session.class);
            SQLQuery sqlQuery = session.createSQLQuery(sql);
            
            sqlQuery.addScalar("smccode", StandardBasicTypes.STRING);
            sqlQuery.addScalar("projectcode", StandardBasicTypes.STRING);
            sqlQuery.addScalar("descricao", StandardBasicTypes.STRING);
            sqlQuery.addScalar("dtsubmissao", StandardBasicTypes.TIMESTAMP);
            sqlQuery.addScalar("customercode", StandardBasicTypes.STRING);
            sqlQuery.addScalar("SituacaoDesc", StandardBasicTypes.STRING);
            sqlQuery.addScalar("ProjectName", StandardBasicTypes.STRING);
            sqlQuery.addScalar("customername", StandardBasicTypes.STRING);
            sqlQuery.addScalar("attendancelevelcode", StandardBasicTypes.STRING);
            sqlQuery.addScalar("attendancelevelname", StandardBasicTypes.STRING);
            sqlQuery.addScalar("factory", StandardBasicTypes.STRING);
            sqlQuery.addScalar("factoryname", StandardBasicTypes.STRING);
            sqlQuery.addScalar("situacao", StandardBasicTypes.STRING);
            sqlQuery.addScalar("estado", StandardBasicTypes.STRING);
            sqlQuery.addScalar("attendanceresp", StandardBasicTypes.STRING);
            sqlQuery.addScalar("attendancerespname", StandardBasicTypes.STRING);
            sqlQuery.addScalar("docs", StandardBasicTypes.STRING);
            sqlQuery.addScalar("forms", StandardBasicTypes.STRING);
            sqlQuery.addScalar("LocalizationPath", StandardBasicTypes.STRING);
            sqlQuery.addScalar("RequesterName", StandardBasicTypes.STRING);
            sqlQuery.addScalar("ReleaseName", StandardBasicTypes.STRING);
            sqlQuery.addScalar("Formalyzed", StandardBasicTypes.STRING);
            sqlQuery.addScalar("TypeName", StandardBasicTypes.STRING);
            sqlQuery.addScalar("AttendanceSequence", StandardBasicTypes.STRING);
            sqlQuery.addScalar("EstadoDesc", StandardBasicTypes.STRING);
            sqlQuery.addScalar("ReturnLevelCode", StandardBasicTypes.STRING);
            sqlQuery.addScalar("Rework", StandardBasicTypes.STRING);
            sqlQuery.addScalar("Complexity", StandardBasicTypes.STRING);
            sqlQuery.addScalar("ReturnResponsibleCode", StandardBasicTypes.STRING);
            sqlQuery.addScalar("ReturnStatusCode", StandardBasicTypes.STRING);
            sqlQuery.addScalar("ReturnStateCode", StandardBasicTypes.STRING);
            sqlQuery.addScalar("codCategoria", StandardBasicTypes.STRING);
            sqlQuery.addScalar("nomeCategoria", StandardBasicTypes.STRING);
            sqlQuery.addScalar("nomePrioridade", StandardBasicTypes.STRING);

            List<Object[]> results = sqlQuery.list();
            for (Object[] rs : results) {
                JSONObject json = new JSONObject();
                // "<img src=\"" + applicationContext.getApplicationName() + "/resources/img/details_open.png\" class=\"btn-drilldown\" data-url=\"" + applicationContext.getApplicationName() + "/griddetalhamento?crc=" + rs[0] + "&codProj=" + rs[1] + "\" />"
                json.put("A", "");
                json.put("B", (String) rs[0]);
                json.put("C", rs[7]);
                json.put("D", formatarData((Timestamp) rs[3]));
                json.put("E", rs[6]);
                json.put("F", rs[32]);
                json.put("G", rs[5]+" / "+rs[24]);
                json.put("H", rs[33]);
                json.put("I", rs[2]);
                json.put("J", rs[15]);
                json.put("L",  "<i class='fa fa-edit editar-crc' title='Editar'  numcrc='"+rs[0]+"' codproj='"+rs[1]+"' style='cursor:pointer;' ></i>");
                array.put(json);
            }

            try {
                objRetorno.put("status", true);
                objRetorno.put("data", array);
            } catch (JSONException ex) {
                System.out.println("erro json." + ex.getLocalizedMessage());
            }
//            lista.forEach((rs) -> {
//                JSONObject objeto = new JSONObject();
//                try {
//                    objeto.put("smccode", rs[0]);//string
//                    objeto.put("projectcode", rs[1]);//string
//                    objeto.put("descricao", "");//rs[2]);//clob
//                    objeto.put("dtsubmissao", rs[3]);//timestamp
//                    objeto.put("customercode", rs[4]);//integer
//                    objeto.put("SituacaoDesc", rs[5]);//string
//                    objeto.put("ProjectName", rs[6]);//string
//                    objeto.put("customername", rs[7]);//string
//                    objeto.put("attendancelevelcode", rs[8]);//integer
//                    objeto.put("attendancelevelname", rs[9]);//string
//                    objeto.put("factory", rs[10]);//integer
//                    objeto.put("factoryname", rs[11]);//string
//                    objeto.put("situacao", rs[12]);//integer
//                    objeto.put("estado", rs[13]);//integer
//                    objeto.put("attendanceresp", rs[14]);//integer
//                    objeto.put("attendancerespname", rs[15]);//string
//                    objeto.put("docs", rs[16]);//integer
//                    objeto.put("forms", rs[17]);//integer
//                    objeto.put("LocalizationPath", rs[18]);//string
//                    objeto.put("RequesterName", rs[19]);//string
//                    objeto.put("ReleaseName", rs[20]);//string
//                    objeto.put("Formalyzed", rs[21]);
//                    objeto.put("TypeName", rs[22]);//string
//                    objeto.put("AttendanceSequence", rs[23]);
//                    objeto.put("EstadoDesc", rs[24]);
//                    objeto.put("ReturnLevelCode", rs[25]);
//                    objeto.put("Rework", rs[26]);//boolean
//                    objeto.put("Complexity", rs[27]);//integer
//                    objeto.put("ReturnResponsibleCode", rs[28]);
//                    objeto.put("ReturnStateCode", rs[29]);
//                } catch (JSONException e) {
//                    System.out.println("e:" + e.getLocalizedMessage());
//                }
//                array.put(objeto);
//            });
        } catch (Exception e) {
            System.out.println("e:" + e.getLocalizedMessage());
        }
        return objRetorno;
    }

    public JSONObject obterDadosGridPorCodigoProfileRoleDetalhamento(String smccode) {
        JSONArray array = new JSONArray();
        JSONObject objRetorno = new JSONObject();
        try {
            String sql = "SELECT\n"
                    + "  smcan.AttendanceSequence,\n"
                    + "  EstadoDesc = (select estado.estadodesc from dbo.estadossmc estado where smcan.estado = estado.estadoCod),\n"
                    + "  RequesterName = sh.shnome,  \n"
                    + "  ReleaseNameCrcOrigem  = scmrel.name,    \n"
                    + "  LocalizationPath = smcan.LocalizationPath,\n"
                    + "  TypeName = tipo.MudancaDesc,  \n"
                    + "  sm.smcode,      \n"
                    + "  sit.sitdesc,  \n"
                    + "  est.estadodesc,  \n"
                    + "  ReleaseNameEntrega = IsNull(rel.Name, ''), \n"
                    + "  DeliverDate = IsNull(CONVERT(CHAR(10), rel.DeliverDate,103), ''),\n"
                    + "  smcan.descricao\n"
                    + "from  dbo.solicitacaomudanca sm,\n"
                    + "      dbo.situacaosm sit,\n"
                    + "      dbo.estadossituacao est,\n"
                    + "      dbo.scm_release rel,\n"
                    + "      dbo.smcandidata smcan,\n"
                    + "      dbo.scm_release scmrel,\n"
                    + "      dbo.stakeholder sh,\n"
                    + "      dbo.tipomudanca tipo\n"
                    + "where smcode IN\n"
                    + "   (\n"
                    + "   SELECT\n"
                    + "     MAX(relSMFormCode)\n"
                    + "   FROM\n"
                    + "     dbo.smcrelatedsmc\n"
                    + "   WHERE     \n"
                    + "     smcode = smcan.smccode\n"
                    + "   )\n"
                    + "   and smcan.smccode = :smccode\n"
                    + "   And sm.situacao = sit.sitcode\n"
                    + "   and smcan.solicitante = sh.shcode\n"
                    + "   AND smcan.VersaoProduto = scmrel.code\n"
                    + "   and smcan.TipoMudanca = tipo.MudancaCode \n"
                    + "   And sm.estado = estadocod \n"
                    + "   And sm.nextRelease *= rel.code";
            Session session = manager.unwrap(Session.class);
            SQLQuery sqlQuery = session.createSQLQuery(sql);
            sqlQuery.setParameter("smccode", smccode);
            List<Object[]> results = sqlQuery.list();
            for (Object[] rs : results) {
                JSONObject json = new JSONObject();
                json.put("A", "");
                json.put("B", (Integer) rs[0]);//smcan.AttendanceSequence
                json.put("C", (String) rs[1]);//EstadoDesc
                json.put("D", (String) rs[2]);//RequesterName
                json.put("E", (String) rs[3]);//ReleaseNameCrcOrigem
                json.put("F", (String) rs[4]);//LocalizationPath
                json.put("G", (String) rs[5]);//TypeName
                json.put("H", (String) rs[6]);//sm.smcode
                json.put("I", (String) rs[7]);//sit.sitdesc
                json.put("J", (String) rs[8]);//est.estadodesc
                json.put("K", (String) rs[9]);//ReleaseNameEntrega
                json.put("L", (String) rs[10]);//DeliverDate
                json.put("M", (String) rs[11]);//descricao
                array.put(json);
            }

            try {
                objRetorno.put("status", true);
                objRetorno.put("data", array);
            } catch (JSONException ex) {
                System.out.println("erro json." + ex.getLocalizedMessage());
            }
        } catch (Exception e) {
            System.out.println("e:" + e.getLocalizedMessage());
        }
        return objRetorno;
    }

    public Map obterChamadosComCRsFormalizadas(String crc, String codProj) {
        Map<String, String> map = new LinkedHashMap();
        try {
            Session session = manager.unwrap(Session.class);
            SQLQuery sqlQuery = session.createSQLQuery("select\n"
                    + "      CR  = sm.smcode,   \n"
                    + "      situacao_desc = sit.sitdesc,  \n"
                    + "      estado_desc = est.estadodesc,  \n"
                    + "      Release_Name_Entrega = IsNull(rel.Name, ''), \n"
                    + "      Deliver_Date = IsNull(CONVERT(CHAR(10), rel.DeliverDate,103), '')\n"
                    + "from  dbo.solicitacaomudanca sm\n"
                    + "left join dbo.situacaosm sit on\n"
                    + "    sm.situacao = sit.sitcode\n"
                    + "left join dbo.estadossituacao est on \n"
                    + "    sm.estado = est.estadocod \n"
                    + "left join  dbo.scm_release rel on    \n"
                    + "    sm.nextRelease = rel.code\n"
                    + "where sm.smcode IN\n"
                    + "   (\n"
                    + "   SELECT\n"
                    + "     MAX(relSMFormCode)\n"
                    + "   FROM\n"
                    + "     dbo.smcrelatedsmc\n"
                    + "   WHERE     \n"
                    + "     smcode = :CRC and projectCode = :codProj ) ");
            sqlQuery.setParameter("CRC", crc);
            sqlQuery.setParameter("codProj", codProj);            
            List<Object[]> lista = sqlQuery.list();
            for (Object[] object : lista) {
                   map.put("CR", String.valueOf(object[0]));
                map.put("situacaoDesc", String.valueOf(object[1]));
                map.put("estadoDesc", String.valueOf(object[2]));
                map.put("ReleaseNameEntrega", String.valueOf(object[3]));
                map.put("DeliverDate", String.valueOf(object[4]));
            }
//            lista.forEach((object) -> {
//                map.put("CR", String.valueOf(object[0]));
//                map.put("situacaoDesc", String.valueOf(object[1]));
//                map.put("estadoDesc", String.valueOf(object[2]));
//                map.put("ReleaseNameEntrega", String.valueOf(object[3]));
//                map.put("DeliverDate", String.valueOf(object[4]));
//            });
        } catch (Exception e) {
            System.out.println("e:" + e.getLocalizedMessage());
        }
        return map;
    }

    public Map obterChamadosComCRCsNaoFormalizadas(String crc, String codProj) {
        Map<String, Object> map = new LinkedHashMap();
        try {
            Session session = manager.unwrap(Session.class);
            SQLQuery sqlQuery = session.createSQLQuery("select\n"
                    + "      Attendance_Sequence =  smcan.AttendanceSequence,\n"
                    + "      Estado_Desc = IsNull((select estado.estadodesc from dbo.estadossmc estado where smcan.estado = estado.estadoCod),''),\n"
                    + "      Requester_Name = sh.shnome,  \n"
                    + "      Release_Name_Crc_Origem  = scmrel.name,    \n"
                    + "      Localization_Path = smcan.LocalizationPath,\n"
                    + "      Type_Name = tipo.MudancaDesc,  \n"
                    + "      CRC = smcan.smccode,\n"                    
                    + "      descricao_crc = IsNull(smcan.descricao,'')\n"
                    + "from  dbo.smcandidata smcan\n"
                    + "join dbo.stakeholder sh on\n"
                    + "     smcan.solicitante = sh.shcode\n"
                    + "join     dbo.scm_release scmrel on\n"
                    + "  smcan.VersaoProduto = scmrel.code\n"
                    + "join dbo.tipomudanca tipo  on\n"
                    + "  smcan.TipoMudanca = tipo.MudancaCode \n"
                    + "where   smcan.smccode = :CRC and smcan.projectCode = :codProj");
            sqlQuery.setParameter("CRC", crc);
            sqlQuery.setParameter("codProj", codProj);            
            sqlQuery.addScalar("Attendance_Sequence", StandardBasicTypes.INTEGER);
            sqlQuery.addScalar("Estado_Desc", StandardBasicTypes.STRING);
            sqlQuery.addScalar("Requester_Name", StandardBasicTypes.STRING);
            sqlQuery.addScalar("Release_Name_Crc_Origem", StandardBasicTypes.STRING);
            sqlQuery.addScalar("Localization_Path", StandardBasicTypes.STRING);
            sqlQuery.addScalar("Type_Name", StandardBasicTypes.STRING);
            sqlQuery.addScalar("CRC", StandardBasicTypes.STRING);
            sqlQuery.addScalar("descricao_crc", StandardBasicTypes.MATERIALIZED_CLOB);
            List<Object[]> lista = sqlQuery.list();
            for (Object[] rs : lista) {
                 map.put("Attendance_Sequence", rs[0]);
                map.put("Estado_Desc", String.valueOf(rs[1]));
                map.put("Requester_Name", String.valueOf(rs[2]));
                map.put("Release_Name_Crc_Origem", String.valueOf(rs[3]));
                map.put("Localization_Path", String.valueOf(rs[4]));
                map.put("Type_Name", String.valueOf(rs[5]));
                map.put("CRC", String.valueOf(rs[6]));//não precisa da crc                
                map.put("descricao_crc", (String)rs[7]);
            }
//            lista.forEach((rs) -> {
//                map.put("Attendance_Sequence", rs[0]);
//                map.put("Estado_Desc", String.valueOf(rs[1]));
//                map.put("Requester_Name", String.valueOf(rs[2]));
//                map.put("Release_Name_Crc_Origem", String.valueOf(rs[3]));
//                map.put("Localization_Path", String.valueOf(rs[4]));
//                map.put("Type_Name", String.valueOf(rs[5]));
//                map.put("CRC", String.valueOf(rs[6]));//não precisa da crc                
//                map.put("descricao_crc", (String)rs[7]);
//            });
        } catch (Exception e) {
            System.out.println("e:" + e.getLocalizedMessage());
        }
        return map;
    }

    private boolean obterConfigExibirTodasCRCsLevel() {
        boolean showAllCRCLevel = false;
        try {
            Session session = manager.unwrap(Session.class);
            SQLQuery sqlQuery = session.createSQLQuery("Select ShowAllCRCLevel from dbo.CL_Config");
            showAllCRCLevel = (boolean) sqlQuery.uniqueResult();
        } catch (Exception e) {
            System.out.println("e:" + e.getLocalizedMessage());
        }
        return showAllCRCLevel;
    }

    private List<Map<String, String>> obterStatusStatesPorCodigoProfileRole(int codigoProfileRole) {
        List<Map<String, String>> listaStatusState = new ArrayList<>();
        try {
            Session session = manager.unwrap(Session.class);
            SQLQuery sqlQuery = session.createSQLQuery("Select \n"
                    + "    c.ProfileCode,\n"
                    + "    c.StatusCode,\n"
                    + "    c.StateCode,\n"
                    + "    StatusName = s.sitDesc,\n"
                    + "    StateName = e.estadoDesc,\n"
                    + "    ProfileName = p.Name\n"
                    + "from dbo.CL_ConfigCustomerState c\n"
                    + "    left join dbo.situacaosmc s on (s.sitCode = c.StatusCode) \n"
                    + "    left join dbo.estadossmc e on (e.estadoCod = c.StateCode) \n"
                    + "    left join dbo.CL_Profile p on (p.Code = c.ProfileCode)\n"
                    + "where ProfileCode = :ProfileCode\n"
                    + " order by ProfileName, StatusName, StateName");
            sqlQuery.setParameter("ProfileCode", codigoProfileRole);
            List<Object[]> lista = sqlQuery.list();
            for (Object[] object : lista) {
                Map<String, String> map = new LinkedHashMap();
                map.put("codigoProfileRole", String.valueOf(object[0]));
                map.put("codigoStatus", String.valueOf(object[1]));
                map.put("codigoState", String.valueOf(object[2]));
                map.put("nomeStatus", String.valueOf(object[3]));
                map.put("nomeState", String.valueOf(object[4]));
                map.put("nomeProfileRole", String.valueOf(object[5]));
                listaStatusState.add(map);
            }
 
//            lista.forEach((object) -> {
//                Map<String, String> map = new LinkedHashMap();
//                map.put("codigoProfileRole", String.valueOf(object[0]));
//                map.put("codigoStatus", String.valueOf(object[1]));
//                map.put("codigoState", String.valueOf(object[2]));
//                map.put("nomeStatus", String.valueOf(object[3]));
//                map.put("nomeState", String.valueOf(object[4]));
//                map.put("nomeProfileRole", String.valueOf(object[5]));
//                listaStatusState.add(map);
//            });
        } catch (Exception e) {
            System.out.println("e:" + e.getLocalizedMessage());
        }
        return listaStatusState;
    }

    private String montarSqlDadosTotalizadorPorCodigoCostumer(String codigoClienteLogado){
        String sql = "";
        
        String filtroCodigoCliente = "";
        if (codigoClienteLogado != null && !codigoClienteLogado.isEmpty()){
          filtroCodigoCliente = "CustomerCode = " + codigoClienteLogado + " AND ";
        }
        
        sql += "  SELECT TOP 1 TIPO = 'SAC',\n";
        sql += "  (SELECT COUNT(*) FROM DBO.SMCANDIDATA WHERE " + filtroCodigoCliente + " TIPOMUDANCA IN (4,7,130,138,142) AND SITUACAO IN (1,2)) AS ATENDIMENTO, \n";
        sql += "  COMERCIAL='0',\n";                                            
        sql += "  (SELECT COUNT(*) FROM DBO.SMCANDIDATA WHERE " + filtroCodigoCliente + " TIPOMUDANCA IN (4,7,130,138,142) AND SITUACAO = 4) AS FABRICA, \n";
        sql += "  (SELECT COUNT(*) FROM DBO.SMCANDIDATA WHERE " + filtroCodigoCliente + " TIPOMUDANCA IN (4,7,130,138,142) AND SITUACAO = 7) AS HOMOLOGACAO, \n";
        sql += "  (SELECT COUNT(*) FROM DBO.SMCANDIDATA WHERE " + filtroCodigoCliente + " TIPOMUDANCA IN (4,7,130,138,142) AND SITUACAO = 9) AS EXPEDICAO,\n";
        sql += "  (SELECT COUNT(*) FROM DBO.SMCANDIDATA WHERE " + filtroCodigoCliente + " TIPOMUDANCA IN (4,7,130,138,142) AND SITUACAO = 5) AS VALIDACAO,\n";
        sql += "  (SELECT COUNT(*) FROM DBO.SMCANDIDATA WHERE " + filtroCodigoCliente + " TIPOMUDANCA IN (4,7,130,138,142) AND SITUACAO IN (1,2,4,5,7,9)) AS TOTAL \n";
        sql += "  FROM\n";                                                      
        sql += "  DBO.SMCANDIDATA\n";                          
        sql += "  UNION\n";                                                     
        sql += "  SELECT TOP 1 TIPO = 'PRJ',\n";                                
        sql += "  (SELECT COUNT(*) FROM DBO.SMCANDIDATA WHERE " + filtroCodigoCliente + " TIPOMUDANCA IN (127,144,146) AND SITUACAO IN (1,2)) AS ATENDIMENTO, \n";
        sql += "  (SELECT COUNT(*) FROM DBO.SMCANDIDATA WHERE " + filtroCodigoCliente + " TIPOMUDANCA IN (127,144,146) AND SITUACAO IN (6)) AS COMERCIAL, \n";
        sql += "  (SELECT COUNT(*) FROM DBO.SMCANDIDATA WHERE " + filtroCodigoCliente + " TIPOMUDANCA IN (127,144,146) AND SITUACAO = 4) AS FABRICA, \n";
        sql += "  (SELECT COUNT(*) FROM DBO.SMCANDIDATA WHERE " + filtroCodigoCliente + " TIPOMUDANCA IN (127,144,146) AND SITUACAO = 7) AS HOMOLOGACAO, \n";
        sql += "  (SELECT COUNT(*) FROM DBO.SMCANDIDATA WHERE " + filtroCodigoCliente + " TIPOMUDANCA IN (127,144,146) AND SITUACAO = 9) AS EXPEDICAO,\n";
        sql += "  (SELECT COUNT(*) FROM DBO.SMCANDIDATA WHERE " + filtroCodigoCliente + " TIPOMUDANCA IN (127,144,146) AND SITUACAO IN (5)) AS VALIDACAO, \n";
        sql += "  (SELECT COUNT(*) FROM DBO.SMCANDIDATA WHERE " + filtroCodigoCliente + " TIPOMUDANCA IN (127,144,146) AND SITUACAO IN (1,2,4,5,6,7,9)) AS TOTAL \n";
        sql += "  FROM\n";
        sql += "  DBO.SMCANDIDATA\n";
        
        return sql;
    }
    
    private String montarSqlDadosGridPorCodigoProfileRole(int codigoProfileRole, String codigoClienteLogado, String codigoStackeholderLogado) {
        String sql = "";
        String customers = new String();
        boolean inAttendanceLevel = false;
        boolean inFactory = false;
        String loggedStakeholder = new String();
        System.out.println("codigoProfileRole:"+codigoProfileRole);
        System.out.println("codigoClienteLogado:"+codigoClienteLogado);
        String attendanceResponsibles = new String();
        List<Map<String, String>> listaStatusState = new ArrayList<>();
        switch (codigoProfileRole) {
            case 1:
                customers = codigoClienteLogado;
                listaStatusState = obterStatusStatesPorCodigoProfileRole(1);
                break;
            case 2:
                inAttendanceLevel = true;
                inFactory = true;
                attendanceResponsibles = codigoStackeholderLogado;
                loggedStakeholder = codigoStackeholderLogado;
                listaStatusState = obterStatusStatesPorCodigoProfileRole(2);
                break;
            case 3:
                inAttendanceLevel = true;
                inFactory = true;
                loggedStakeholder = codigoStackeholderLogado;
                listaStatusState = obterStatusStatesPorCodigoProfileRole(3);
                if (!obterConfigExibirTodasCRCsLevel()) {
                    attendanceResponsibles = codigoStackeholderLogado;
                }
                break;
            case 4:
                inAttendanceLevel = true;
                inFactory = true;
                attendanceResponsibles = codigoStackeholderLogado;
                loggedStakeholder = codigoStackeholderLogado;
                listaStatusState = obterStatusStatesPorCodigoProfileRole(4);
                break;
            case 5:
                inAttendanceLevel = true;
                inFactory = true;
                attendanceResponsibles = codigoStackeholderLogado;
                loggedStakeholder = codigoStackeholderLogado;
                listaStatusState = obterStatusStatesPorCodigoProfileRole(5);
                break;
            default:
                break;
        }
        String parametrosStatus = "";
        String parametrosStates = "";
        for (Map<String, String> map : listaStatusState) {
            //remove status os duplicados 
            if (!parametrosStatus.contains("'" + map.get("codigoStatus") + "'")) {
                parametrosStatus += ",'" + map.get("codigoStatus") + "'";
            }
            //remove estados os duplicados 
            if (!parametrosStates.contains("'" + map.get("codigoState") + "'")) {
                parametrosStates += ",'" + map.get("codigoState") + "'";
            }
        }
        parametrosStatus = parametrosStatus.replaceFirst(",", "");
        parametrosStates = parametrosStates.replaceFirst(",", "");
        sql += "SELECT\n";
        sql += "  smccode = SUBSTRING(t1.smccode,4,LEN(t1.smccode)),\n";
        sql += "  t1.projectcode,\n";
        sql += "  t1.descricao,\n";
        sql += "  t1.dtsubmissao,\n";
        sql += "  t1.customercode,\n";
        sql += "  SituacaoDesc = t4.sitdesc,\n";
        sql += "  ProjectName  = t3.projdescription,\n";
        sql += "  t2.customername,\n";
        sql += "  attendancelevelcode,\n";
        sql += "  attendancelevelname = t5.title,\n";
        sql += "  factory,\n";
        sql += "  factoryname = t6.name,\n";
        sql += "  t1.situacao,\n";
        sql += "  t1.estado,\n";
        sql += "  t1.attendanceresp,\n";
        sql += "  attendancerespname = t7.shnome,\n";
        sql += "  docs               =\n";
        sql += "  (\n";
        sql += "    SELECT\n";
        sql += "      COUNT(candidatecode)\n";
        sql += "    FROM\n";
        sql += "      dbo.cl_requestdocument\n";
        sql += "    WHERE\n";
        sql += "      candidatecode = t1.smccode\n";
        sql += "  )\n";
        sql += "  ,\n";
        sql += "  forms =\n";
        sql += "  (\n";
        sql += "    SELECT\n";
        sql += "      COUNT(smcode)\n";
        sql += "    FROM\n";
        sql += "      dbo.smcrelatedsmc\n";
        sql += "    WHERE\n";
        sql += "      smcode = t1.smccode\n";
        sql += "  )\n";
        sql += "  ,\n";
        sql += "  LocalizationPath,\n";
        sql += "  RequesterName          = t8.shnome,\n";
        sql += "  ReleaseName            = t9.name,\n";
        sql += "  Formalyzed =\n";
        sql += "  (\n";
        sql += "    SELECT\n";
        sql += "      sm.smcode     + ';' + sit.sitdesc + ';' + est.estadodesc + ';' + IsNull(\n";
        sql += "      rel.Name, '') + ';' + IsNull(CONVERT(CHAR(10),\n";
        sql += "      rel.DeliverDate,103), '') \n";
        sql += "      from  dbo.solicitacaomudanca sm,\n";
        sql += "            dbo.situacaosm sit,\n";
        sql += "            dbo.estadossituacao est,\n";
        sql += "            dbo.scm_release rel\n";
        sql += "      where smcode IN\n";
        sql += "      (\n";
        sql += "        SELECT\n";
        sql += "          MAX(relSMFormCode)\n";
        sql += "        FROM\n";
        sql += "          dbo.smcrelatedsmc\n";
        sql += "        WHERE\n";
        sql += "          smcode = t1.smccode\n";
        sql += "      )\n";
        sql += "      And sm.situacao              = sit.sitcode And sm.estado =\n";
        sql += "      estadocod And sm.nextRelease *= rel.code \n";
        sql += "  )\n";
        sql += "  ,\n";
        sql += "  TypeName = t10.MudancaDesc,\n";
        sql += "  AttendanceSequence,\n";
        sql += "  EstadoDesc,\n";
        sql += "  ReturnLevelCode,\n";
        sql += "  Rework,\n";
        sql += "  Complexity,\n";
        sql += "  ReturnResponsibleCode,\n";
        sql += "  ReturnStatusCode,\n";
        sql += "  ReturnStateCode, \n";
        sql += "  codCategoria = t1.Category, \n";
        sql += "  nomeCategoria = t12.Title, \n";
        sql += "  nomePrioridade = t13.ConfigLevelDesc \n";
        sql += "from dbo.smcandidata t1 ,\n";
        sql += "  dbo.cl_customer t2,\n";
        sql += "  dbo.project t3,\n";
        sql += "  dbo.situacaosmc t4,\n";
        sql += "  dbo.cl_attendancelevel t5,\n";
        sql += "  dbo.pmt_factoryunit t6 ,\n";
        sql += "  dbo.stakeholder t7,\n";
        sql += "  dbo.stakeholder t8,\n";
        sql += "  dbo.scm_release t9,\n";
        sql += "  dbo.tipomudanca t10,\n";
        sql += "  dbo.estadossmc t11, \n";
        sql += "  dbo.RequestCategory t12, \n";
        sql += "  dbo.ConfigLevel t13 \n";        
        sql += "  where\n";
        sql += "  t12.Code = t1.Category and \n" +
               "  t13.ConfigLevelCode = t1.prioridade and \n";
        if (!parametrosStatus.isEmpty() && !parametrosStates.isEmpty()) {
            sql += "  t1.smccode in (   \n";
            sql += "  select smcandidata.smccode from dbo.smcandidata smcandidata where \n";
            sql += "    smcandidata.situacao in(\n";
            sql += parametrosStatus + "\n";
            sql += "			)\n";
            sql += "  and \n";
            sql += "    smcandidata.estado in(\n";
            sql += parametrosStates + "\n";
            sql += " )\n";
            sql += " )\n";
            sql += "    And      \n";
        }
        sql += "  t1.customercode = t2.code and\n";
        sql += "  t1.projectcode = t3.Projcode and\n";
        sql += "  t1.situacao = t4.sitcode and\n";
        sql += "  t1.AttendanceLevelCode = t5.code and\n";
        sql += "  t1.Factory = t6.Code and\n";
        sql += "  t1.AttendanceResp *= t7.shcode and\n";
        sql += "  t1.solicitante = t8.shcode and\n";
        sql += "  t1.VersaoProduto = t9.code and\n";
        sql += "  t1.TipoMudanca = t10.MudancaCode and\n";
        sql += "  t1.estado = t11.estadoCod  \n";
        //load condicions abaixo
        if (customers != null && !customers.isEmpty()) {
            sql += "and t1.customercode in(" + customers + ")\n";
        }
        if (attendanceResponsibles != null && !attendanceResponsibles.isEmpty()) {
            sql += "and t1.AttendanceResp in(" + attendanceResponsibles + ")\n";
        }
        if (inAttendanceLevel) {
            sql += "and t1.AttendanceLevelCode in ( select AttendanceLevelCode from  dbo.CL_ALStakeholder where StakeholderCode in ( " + loggedStakeholder + " ) )\n";
        }
        if (inFactory) {
            sql += "and t1.Factory in ( select FactoryCode  from  dbo.CL_CustomerStakeholderFactory where StakeholderCode in (" + loggedStakeholder + "))\n";
            sql += "and t1.Situacao <> 3 ";
        }

        return sql;
    }

    private String formatarData(Timestamp data) {
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        return f.format(data);
    }
}
